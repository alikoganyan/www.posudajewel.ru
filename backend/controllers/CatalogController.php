<?php

namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use backend\widgets\activeForm\ActiveForm;
use backend\components\Controller;
use common\models\Service;
use common\models\Catalog;
use common\models\CatalogLink;
use common\models\QuickSearch;

class CatalogController extends Controller {

    public function actionIndex() {
        $searchModel = new Catalog();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($id = false) {
        $model = new Catalog();
        $model->setScenario('insert');
        $model->sort = 1;

        if ($id) {
            $parent = Catalog::findOne(['id' => (int) $id, 'parent_id' => null]);
            if ($parent instanceof Catalog) {
                $model->parent_id = $parent->id;
            }
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            
            if (!empty($model->service_id)) {
                $listService = Service::find()->
                        select(['name', 'id'])->
                        where(['id' => $model->service_id])->
                        indexBy('id')->
                        column();
            } else {
                $listService = [];
            }
            
            return $this->render('form', [
                        'model' => $model,
                        'listService' => $listService,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->setScenario('update');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (is_null($model->parent_id)) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['update', 'id' => $model->parent_id]);
            }
        } else {
            // Ссылки быстрого доступа
            $quickSearch = QuickSearch::find()->
                    select(['name', 'id'])->
                    indexBy('id')->
                    column();


            if (is_null($model->parent_id)) {
                $searchModel = new Catalog();
            } else {
                $searchModel = new CatalogLink();
            }

            $searchModel->setScenario('search');
            $searchModel->parent_id = $model->id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            if (!empty($model->service_id)) {
                $listService = Service::find()->
                        select(['name', 'id'])->
                        where(['id' => $model->service_id])->
                        indexBy('id')->
                        column();
            } else {
                $listService = [];
            }

            return $this->render('form', [
                        'model' => $model,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'quickSearch' => $quickSearch,
                        'listService' => $listService,
            ]);
        }
    }

    public function actionSaveLink($id) {
        $model = new CatalogLink([
            'parent_id' => (int) $id,
        ]);
        $model->setScenario('insert');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
        }

        return $this->redirect(['update', 'id' => $model->parent_id]);
    }

    public function actionDeleteLink($id, $parent_id) {
        CatalogLink::deleteAll(['parent_id' => (int) $parent_id, 'search_id' => (int) $id]);

        return $this->redirect(['update', 'id' => $parent_id]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Catalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

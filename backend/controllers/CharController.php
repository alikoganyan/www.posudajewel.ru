<?php
namespace backend\controllers;

use backend\models\ValueSearch;
use backend\widgets\activeForm\ActiveForm;
use common\models\ServiceType;
use Yii;
use common\models\Char;
use backend\models\CharSearch;
use yii\helpers\Json;
use backend\components\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class CharController extends Controller {
    public function actionIndex() {
        $searchModel  = new CharSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Char();
        $model->sort = 1;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }
        else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!isset(Yii::$app->request->post('Char')['is_multiple'])) {
                $model->is_multiple = false;
            }

            $model->save();
            return $this->redirect(['index']);
        }
        else {
            $searchModel  = new ValueSearch(['charTypeId' => $id]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('form', [
                'model'        => $model,
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    // TODO >>> refactor
    public function actionValues($charId, $serviceTypeId) {
        if (!\Yii::$app->request->isAjax) {
            throw new HttpException('405', 'method must be ajax');
        }

        $char        = Char::findOne(['id' => $charId]);
        $serviceType = ServiceType::findOne(['id' => $serviceTypeId]);

        return $this->renderPartial('values', [
            'char'        => $char,
            'serviceType' => $serviceType,
        ]);
    }

    // TODO >>> refactor
    public function actionGetValues($id) {
        $model = $this->findModel($id);

        return Json::encode([
            'values' => $model->values,
            'is_multiple' => $model->is_multiple,
        ]);
    }

    protected function findModel($id) {
        if (($model = Char::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
namespace backend\controllers;

use common\models\Company;
use common\models\Role;
use common\models\Service;
use Yii;
use backend\widgets\activeForm\ActiveForm;
use backend\components\Controller;
use common\models\User;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProfileController extends Controller {
    public function actionIndex() {
        $model = $this->findModel();
        $redirectUrl = ['main-info'];

        if ($model == null) {
            $role = Role::findOne(['name' => 'Партнер']);

            $model = new User(['role_id' => $role->id]);
            $model->setScenario(User::SCENARIO_CREATE);
            $redirectUrl = ['finish-registration'];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->isNewRecord) {
                $model->sendConfirmCode();
            }

            $model->save();
            $model->login();

            return $this->redirect($redirectUrl);
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionFinishRegistration() {
        $model = $this->findModel();

        if (!$model) {
            throw new NotFoundHttpException('user does not exist');
        }

        $model->setScenario(User::SCENARIO_CONFIRM_CODE);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->confirm_code = null;
            $model->save();

            return $this->redirect(['main-info']);
        }

        return $this->render('confirm', [
            'model' => $model,
        ]);
    }

    public function actionMainInfo() {
        $model = $this->findModel();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['company-create']);
        }

        return $this->render('main-info', [
            'model' => $model,
        ]);
    }

    public function actionCompanyCreate() {
        $user = $this->findModel();
        $model = new Company(['user_id' => $user->id]);

        if ($user->companies) {
            $model = $user->companies[0];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['company-locality']);
        }

        return $this->render('company-create', [
            'model' => $model,
        ]);
    }

    public function actionCompanyLocality() {
        $user = $this->findModel();
        $model = Company::findOne(['user_id' => $user->id]);
        $model->setScenario(Company::SCENARIO_GEO);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['service-create']);
        }

        return $this->render('company-locality', [
            'model' => $model,
        ]);
    }

    public function actionServiceCreate($id = null) {
        if ($id != null) {
            $model = Service::findOne(['id' => $id]);
        }
        else {
            $user = $this->findModel();
            $company = Company::findOne(['user_id' => $user->id]);
            $model = new Service(['company_id' => $company->id]);
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['service-price', 'id' => $model->id]);
        }

        return $this->render('service-create', [
            'model' => $model,
            'serviceList' => Service::findAll(['company_id' => $model->company_id]),
        ]);
    }

    public function actionServicePrice($id) {
        $model = Service::findOne(['id' => $id]);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['image-upload', 'id' => $model->id]);
        }

        return $this->render('service-price', [
            'model' => $model,
            'serviceList' => Service::findAll(['company_id' => $model->company_id]),
        ]);
    }

    public function actionImageUpload($id) {
        $model = Service::findOne(['id' => $id]);

        if (Yii::$app->request->isAjax) {
            echo '<pre>';
            print_r($_FILES);
            echo '</pre>';
            die();
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->redirect(['finish']);
        }

        return $this->render('image-upload', [
            'model' => $model,
        ]);
    }

    public function actionFinish() {
        return $this->render('finish');
    }

    protected function findModel() {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        if (!$user && Yii::$app->controller->action->id != 'index') {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        if (Yii::$app->controller->action->id != 'index') {
            if (($model = User::findOne(['id' => $user->id])) !== null) {
                return $model;
            }
        }

        return null;
    }
}

<?php
namespace backend\controllers;

use Yii;
use common\models\QuickSearchCharValue;
use backend\models\QuickSearchCharValueSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class QuickSearchCharValueController extends Controller {
    public function actionCreate() {
        if (Yii::$app->request->isPost) {
            $requestData = Yii::$app->request->post('QuickSearchCharValue');
            foreach ($requestData['value_id'] as $item) {
                $model = new QuickSearchCharValue([
                    'quick_search_id' => $requestData['quick_search_id'],
                    'char_id'         => $requestData['char_id'],
                    'value_id'        => $item,
                ]);

                $model->save();
            }

            return $this->redirect(['quick-search/update', 'id' => $requestData['quick_search_id']]);
        }

        return $this->render('form', [
            'model' => new QuickSearchCharValue(),
        ]);
    }

    public function actionDelete($quick_search_id, $char_id) {
        QuickSearchCharValue::deleteAll(['quick_search_id' => $quick_search_id, 'char_id' => $char_id]);

        return $this->redirect(['quick-search/update', 'id' => $quick_search_id]);
    }

    protected function findModel($quick_search_id, $char_id, $value_id) {
        if (($model = QuickSearchCharValue::findOne(['quick_search_id' => $quick_search_id, 'char_id' => $char_id, 'value_id' => $value_id])) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use backend\components\Controller;
use backend\models\DiscountSearch;
use common\models\Discount;
use Yii;
use yii\web\NotFoundHttpException;

class DiscountController extends Controller {
    public function actionIndex() {
        $searchModel  = new DiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Discount();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->setPercent('discount', $model->discount);
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->discount = $model->getPercent('discount');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->setPercent('discount', $model->discount);
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        $searchModel  = new DiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('form', [
            'model'        => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        } else {
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php
namespace backend\controllers;

use backend\widgets\activeForm\ActiveForm;
use common\models\Region;
use Yii;
use common\models\Locality;
use yii\helpers\Json;
use yii\helpers\Url;
use backend\components\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class LocalityController extends Controller {
    public function actionIndex($id) {
        $model = $this->findModel($id);

        return $this->redirect(['region/update', 'id' => $model->region_id]);
    }

    public function actionCreate($region_id) {
        $model = new Locality([
            'region_id' => $region_id,
        ]);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['region/update', 'id' => $model->region_id]);
        }
        else {
            return $this->render('form', [
                'model'  => $model,
                'region' => Region::findOne(['id' => $region_id]),
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['region/update', 'id' => $model->region_id]);
        }
        else {
            return $this->render('form', [
                'model'  => $model,
                'region' => Region::findOne(['id' => $model->region_id]),
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['region/update', 'id' => $model->region_id]);
    }

    protected function findModel($id) {
        if (($model = Locality::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetCoords($id) {
        if (!\Yii::$app->request->isAjax) {
            throw new BadRequestHttpException();
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->setCoords();

        return $model->getAttributes(['latitude', 'longitude']);
    }

    public function actionLocalityByRegion($id){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        $model = Locality::find()->select(['id','name'])->where(['region_id'=>$id])->all();
        return $model;
    }
}

<?php
namespace backend\controllers;

use backend\models\CharSearch;
use backend\widgets\activeForm\ActiveForm;
use common\models\Char;
use common\models\CharValue;
use common\models\ServiceTypeChar;
use Yii;
use common\models\ServiceType;
use backend\models\ServiceTypeSearch;
use yii\helpers\ArrayHelper;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ServiceTypeController extends Controller {
    public function actionIndex() {
        $searchModel = new ServiceTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new ServiceType();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (!isset(Yii::$app->request->post('ServiceType')['is_mass'])) {
                $model->is_mass = 0;
            }

            $model->save();
            return $this->redirect(['index']);
        } else {
            $searchModel = new CharSearch(['service_type_id' => $id]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // todo >>> move to common method
            $allChars = Char::find()->innerJoin(CharValue::tableName(), 'char_id = id')
                ->where(['not in', 'id', ArrayHelper::getColumn($model->chars, 'id')])->all();

            return $this->render('form', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'allChars' => ArrayHelper::map($allChars, 'id', 'name'),
            ]);
        }
    }

    public function actionSaveChar($id) {
        $model = new ServiceTypeChar([
            'service_type_id' => $id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $id]);
        }

        return $model;
    }

    public function actionUpdateChar($id, $serviceTypeId) {
        return $this->redirect(['update', 'id' => $serviceTypeId]);
    }

    public function actionDeleteChar($id, $serviceTypeId) {
        ServiceTypeChar::deleteAll(['service_type_id' => $serviceTypeId, 'char_id' => $id]);

        return $this->redirect(['update', 'id' => $serviceTypeId]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        } else {
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = ServiceType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

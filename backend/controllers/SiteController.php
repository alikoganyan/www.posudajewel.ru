<?php

namespace backend\controllers;

use backend\components\Controller;
use backend\models\OrderSearch;
use Yii;
use common\models\LoginForm;
use common\models\Status;
use yii\helpers\Json;

class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $user = Yii::$app->user->getIdentity();

        $ordersSearch   = new OrderSearch();
        $ordersProvider = $ordersSearch->searchUserOrders(Yii::$app->request->queryParams);

        $statuses_list  = Status::find()->select('name')->indexBy('id')->column();

        return $this->render('profile', [
            'user'           => $user,
            'ordersSearch'   => $ordersSearch,
            'ordersProvider' => $ordersProvider,
            'statuses_list'  => $statuses_list,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    /**
     * Ajax server.
     */
    public function actionSavePanelSettings() {
        Yii::$app->user->identity->panel_settings = Json::encode(Yii::$app->request->post('settings'));
        Yii::$app->user->identity->save();
    }

    public function actionResetPanelSettings() {
        Yii::$app->user->identity->panel_settings = null;
        Yii::$app->user->identity->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

}

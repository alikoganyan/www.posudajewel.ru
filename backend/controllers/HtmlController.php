<?php
namespace backend\controllers;

use backend\components\Controller;
use common\models\Company;
use yii\web\NotFoundHttpException;

class HtmlController extends Controller {
    public function actionCloneService($company_id, $service_type_id) {
        $company = Company::findOne(['id' => $company_id]);
        if (!$company) {
            throw new NotFoundHttpException('company does not exist');
        }

        $services = $company->user->getServices($service_type_id);

        if ($services == null) {
            return '';
        }

        return $this->renderPartial('clone-service', [
            'services' => $services,
        ]);
    }
}

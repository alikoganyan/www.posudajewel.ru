<?php

namespace backend\controllers;

use backend\components\Controller;
use backend\models\CompanySearch;
use backend\models\UserSearch;
use Yii;
use common\models\User;
use common\models\DiscountToUser;
use common\models\Discount;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;

class UserController extends Controller {

    protected $model = 'common\models\User';

    public function actionIndex() {
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        if (!$user->isAdmin()) {
            return $this->redirect(['update', 'id' => $user->id]);
        }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate() {
        $user = new User();
        $user->setScenario(User::SCENARIO_CREATE);

        if (Yii::$app->request->isAjax && $user->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($user);
        }

        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $user,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $discountUserModel = $model->discountToUser ? $model->discountToUser : new DiscountToUser;
        $discountUserModel->user_id = $model->id;
        $model->setScenario(User::SCENARIO_DEFAULT);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $discountUserModel->load(Yii::$app->request->post())) {
            if ($model->save() && $discountUserModel->save()) {
                return $this->redirect(['index']);
            }
        }
        $companySearchModel = new CompanySearch(['user_id' => $model->id]);
        $companyDataProvider = $companySearchModel->search(Yii::$app->request->queryParams);

        $discountTypesList = Discount::find()->select(['name', 'id'])->indexBy('id')->column();
        return $this->render('form', [
            'model'               => $model,
            'discountUserModel'   => $discountUserModel,
            'discountModel'       => new Discount,
            'discountTypesList'   => $discountTypesList,
            'companySearchModel'  => $companySearchModel,
            'companyDataProvider' => $companyDataProvider,
        ]);

    }

    public function actionRemoveImage() {
        $userId = \Yii::$app->request->post('userId');
        $model = $this->findModel($userId);

        return $model->removeImage();
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }


        return $this->redirect(['index']);
    }

    public function actionChangePassword() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $postData = Yii::$app->request->post('User');

        $user = User::findOne(['email' => $postData['email']]);

        if (!$user) {
            throw new NotFoundHttpException('user does not exist');
        }

        $user->load(Yii::$app->request->post());
        $user->setScenario(User::SCENARIO_CHANGE_PASSWORD);

        $errors = ActiveForm::validate($user);

        if (!$errors) {
            $user->save();
        }

        return $errors;
    }

    public function actionPasswordForm($email) {
        $model = User::findOne(['email' => $email]);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->setScenario(User::SCENARIO_CHANGE_PASSWORD);
            return ActiveForm::validate($model);
        }

        if (!$model) {
            throw new NotFoundHttpException('user does not exist');
        }

        return $this->renderAjax('change-password-form', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

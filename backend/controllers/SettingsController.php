<?php
namespace backend\controllers;

use backend\components\Controller;
use common\models\Settings;
use common\models\SettingsGroup;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\UploadedFile;

class SettingsController extends Controller {
    public function actionIndex() {
        $settingsGroup = SettingsGroup::find()->orderBy('order')->all();

        return $this->render('index', [
            'settingsGroup' => $settingsGroup,
        ]);
    }


    public function actionSave() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $postData = \Yii::$app->request->post('Settings');
        $key = key($postData);

        $model = new Settings();
        $file = UploadedFile::getInstance($model, $key);
        if ($file) {
            $path = \Yii::getAlias('@frontend') . '/web/uploads';

            if (!file_exists($path)) {
                FileHelper::createDirectory($path);
            }

            $name = \Yii::$app->security->generateRandomString();
            $value = "$name.{$file->extension}";

            $file->saveAs("$path/{$value}");
        }
        else {
            $value = $postData[$key];

            if (count($value) > 1) {
                $value = Json::encode($value);
            }
        }

        return Settings::updateAll(['value' => $value], ['key' => $key]);
    }
}

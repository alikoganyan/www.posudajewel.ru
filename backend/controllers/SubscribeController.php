<?php

namespace backend\controllers;

use Yii;
use backend\components\Controller;
use common\models\Subscribe;
use common\models\User;

/**
 * Подписчики
 */
class SubscribeController extends Controller {

    /**
     * Список подписчиков
     * @return type
     */
    public function actionIndex() {
        $searchModel = new Subscribe();
        $searchModel->setScenario('search');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        // Список email
        $email = [];
        foreach ($dataProvider->getModels() as $subscribe) {
            $email[] = $subscribe->email;
        }

        if (!empty($email)) {
            $listUser = User::find()->
                    select(['id', 'email'])->
                    where(['email' => $email])->
                    indexBy('email')->
                    column();
        } else {
            $listUser = [];
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'listUser' => $listUser,
        ]);
    }

}

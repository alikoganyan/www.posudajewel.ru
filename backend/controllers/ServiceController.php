<?php
namespace backend\controllers;

use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use backend\components\Controller;
use backend\models\ServiceSearch;
use backend\widgets\activeForm\ActiveForm;
use backend\models\ImportForm;
use common\models\Service;
use common\models\ServiceImage;
use common\models\ServiceType;
use common\models\ServiceTypeChar;

class ServiceController extends Controller {
    public function behaviors () {
        return [
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    return (new Query())->from('service')->max('time_update');
                },
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $open = (bool) Yii::$app->getRequest()->post('open');

        $model = new Service();
        $model->buy = $model->getServiceBuy();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($open) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            $image = new ServiceImage();

            if (!empty($model->buy)) {
                $serviceBuy = Service::find()->
                        select(['CONCAT(code, "/", name)', 'id'])->
                        where(['id' => array_map('intval', $model->buy)])->
                        indexBy('id')->
                        column();
            } else {
                $serviceBuy = [];
            }

            return $this->render('form', [
                        'model' => $model,
                        'image' => $image,
                        'serviceBuy' => $serviceBuy,
            ]);
        }
    }

    public function actionChars($id, $name) {
        if (!in_array($name, ['Service', 'QuickSearch'])) {
            $name = 'Service';
        }

        $serviceType = ServiceType::findOne([['id' => (int) $id]]);

        if ($serviceType instanceof ServiceType) {
            return $this->renderAjax('chars', [
                        'serviceType' => ServiceType::findOne([['id' => $id]]),
                        'name' => $name,
            ]);
        } else {
            return '';
        }
    }

    public function actionUpdate($id) {
        $open = (bool) Yii::$app->getRequest()->post('open');

        $model = $this->findModel($id);
        $model->buy = $model->getServiceBuy();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($open) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            $image = new ServiceImage();

            if (!empty($model->buy)) {
                $serviceBuy = Service::find()->
                        select(['CONCAT(code, "/", name)', 'id'])->
                        where(['id' => array_map('intval', $model->buy)])->
                        indexBy('id')->
                        column();
            } else {
                $serviceBuy = [];
            }

            return $this->render('form', [
                        'model' => $model,
                        'image' => $image,
                        'serviceBuy' => $serviceBuy,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCharValue($id) {
        $data = Yii::$app->request->post('ServiceTypeChar');

        foreach ($data['value_id'] as $value) {
            $model = new ServiceTypeChar([
                'service_type_id' => $data['service_type_id'],
                'char_id' => $data['char_id'],
                'value_id' => $value,
            ]);

            $model->save();
        }

        return $this->redirect(['update', 'id' => $data['service_type_id']]);
    }

    public function actionImageUpload() {
        $image = new ServiceImage();

        if ($image->load(Yii::$app->request->post()) && $image->save()) {
            return Json::encode(['status' => 'success', 'data' => [
                'content' => $this->renderPartial('_image', ['img' => $image]),
            ]]);
        } else {
            return Json::encode(['status' => 'error', 'message' => 'Не удалось загрузить изображение.']);
        }
    }

    public function actionImageRemove($id) {
        $image = ServiceImage::findOne(['id' => (int) $id]);
        if ($image instanceof ServiceImage) {
            $image->delete();

            return Json::encode(['status' => 'success', 'data' => []]);
        } else {
            return Json::encode(['status' => 'error', 'message' => 'Изобрпажение не найдено']);
        }
    }

    public function actionCharsClone($id) {
        $service = $this->findModel($id);
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'html' => $this->renderAjax('chars', [
                'model' => $service,
            ]),
            'serviceTypeId' => $service->service_type_id,
        ];
    }

    public function actionOptionsClone($id) {
        $service = $this->findModel($id);

        return $this->renderAjax('options', [
            'model' => $service,
        ]);
    }

    public function actionExport () {
        return \Yii::$app->response->sendFile(Service::export());
    }

    public function actionImport() {
        $model = new ImportForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->import()) {
                $this->redirect(['index']);
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    public function actionRemove() {
        $id = Yii::$app->getRequest()->get('id');

        if (is_array($id) && !empty($id)) {
            $id = array_map('intval', $id);

            $services = Service::findAll(['id' => $id]);
            foreach ($services as $service) {
                $service->delete();
            }
        }

        return Json::encode(['status' => 'success', 'message' => 'Ok']);
    }

    public function actionCopy($id) {
        $service = Service::findOne(['id' => $id]);

        if ($service instanceof Service) {
            $service->copy();
        }

        return $this->redirect(['index']);
    }

    public function actionSearch() {
        $query = (string) Yii::$app->getRequest()->get('q');
        $params = (string) Yii::$app->getRequest()->get('p');


        $cond = ['AND'];

        // Поиск по артикулу
        if ($params == 'code') {
            if (!empty($query)) {
                foreach (explode(' ', $query) as $str) {
                    $cond[] = ['LIKE', 'code', $str];
                }
            }
        }


        $results = Service::find()->
                select(['id', 'CONCAT(code, "/", name) AS text'])->
                where($cond)->
                orderBy('name ASC')->
                limit(15)->
                asArray()->
                all();


        return Json::encode(['results' => $results]);
    }

    protected function findModel($id) {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

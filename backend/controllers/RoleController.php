<?php
namespace backend\controllers;

use backend\components\Controller;
use Yii;
use common\models\Entity;
use common\models\User;
use common\models\Role;
use backend\models\RoleSearch;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class RoleController extends Controller {
    public function beforeAction($action) {
        if (!\Yii::$app->user->isGuest) {
            /** @var User $user */
            $user = \Yii::$app->user->identity;
            $permissions = $user->role->getParsedPermissions();

            if ((!isset($permissions['role']) || $permissions['role']['access'] == 0) && $user->role->id != 5) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Role();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }
        else {
            return $this->render('form', [
                'model' => $model,
                'userPermissions' => [],
                'entities' => Entity::$collection,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->post() != null) {
            $model->setPermissions(Yii::$app->request->post());
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('form', [
            'model' => $model,
            'userPermissions' => Json::decode($model->permissions),
            'entities' => Entity::$collection,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

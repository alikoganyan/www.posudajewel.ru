<?php
namespace backend\controllers;

use backend\widgets\activeForm\ActiveForm;
use Yii;
use common\models\Char;
use common\models\Value;
use yii\helpers\Url;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ValueController extends Controller {
    public function actionIndex($id) {
        $model = $this->findModel($id);

        return $this->redirect(['char/update', 'id' => $model->char->id]);
    }

    public function actionCreate($char_id) {
        $char = Char::findOne(['id' => (int) $char_id]);
        if (!$char instanceof Char) {
            return $this->redirect(['char/index']);
        }

        $model = new Value([
            'charId' => $char->id,
        ]);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::to(['char/update', 'id' => $char->id]));
        }
        else {
            return $this->render('form', [
                'model' => $model,
                'char' => $char,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::to(['char/update', 'id' => $model->char->id]));
        }
        else {
            return $this->render('form', [
                'model' => $model,
                'char' => $model->char,
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }


        return $this->redirect(['char/index']);
    }

    protected function findModel($id) {
        if (($model = Value::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
namespace backend\controllers;

use backend\models\LocalitySearch;
use backend\widgets\activeForm\ActiveForm;
use common\models\Locality;
use Yii;
use common\models\Region;
use backend\models\RegionSearch;
use yii\web\BadRequestHttpException;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\Json;
use dosamigos\google\maps\services\GeocodingClient;

class RegionController extends Controller {
    public function actionIndex() {
        $searchModel = new RegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Region();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }
        else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        else {
            $searchModel = new LocalitySearch([
                'region_id' => $model->id,
            ]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('form', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    // TODO move to RestHTML
    public function actionLocalities ($region_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ( ! Yii::$app->request->isAjax)
        {
            throw new BadRequestHttpException('method must be only ajax');
        }

        return [
            'localities' => Locality::find()->where(['region_id' => $region_id])->orderBy('name')->all(),
            'region' => Region::findOne(['id' => $region_id]),
        ];
    }

    /**
     * Оппеделение координат региона или населенного пункта по названию
     * @param string $address
     * @return type
     */
    public function actionSearch ($address)
    {
        if ( ! empty($address))
        {
            $geo = new GeocodingClient();
            $result = $geo->lookup(['address' => trim($address)]);

            if (isset($result->results[0]))
            {
                return Json::encode(['status' => 'success', 'data' => [
                                'latitude' => $result->results[0]->geometry->location->lat,
                                'longitude' => $result->results[0]->geometry->location->lng,
                ]]);
            }
        }

        return Json::encode(['status' => 'error', 'message' => 'Не удалось определить координаты.']);
    }

    protected function findModel ($id)
    {
        if (($model = Region::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

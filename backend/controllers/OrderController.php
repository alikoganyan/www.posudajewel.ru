<?php

namespace backend\controllers;

use common\models\Region;
use Yii;
use common\models\Order;
use common\models\Cart;
use common\models\User;
use common\models\DeliveryService;
use common\models\Status;
use backend\models\OrderSearch;
use backend\components\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUserUpdate($id) {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post('Order');
        if ($post) {
            $comment = $post['comment'];
            $model->comment = $comment;
            if ($model->save()) {
                Yii::$app->session->setFlash('notify', 'Вы обновили примечание.');
                return $this->redirect(['/site/index']);
            }
        }

        $cartProvider = new ArrayDataProvider([
            'allModels' => unserialize($model->cart->value),
        ]);
        $cart_total = $model->total;

        return $this->render('user-update', [
            'model' => $model,
            'cartProvider' => $cartProvider,
            'cart_total' => $cart_total,
        ]);
    }
    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionUserCreate() {
        $cart = Cart::getOrder();
        if (!$cart) {
            Yii::$app->session->setFlash('notify', 'Ваша корзина пуста.');
            return $this->redirect(['/site/index']);
        }
        $cartProvider = new ArrayDataProvider([
            'allModels' => unserialize($cart->value),
        ]);

        $modelF = new Order(['model_type'=>1]);
        $modelU = new Order(['model_type'=>2]);

        $delivery_services = DeliveryService::find()->select('name')->indexBy('id')->column();
        $regions = Region::find()->select('name')->indexBy('id')->column();
        if ($modelF->load(Yii::$app->request->post()) && Yii::$app->request->post('Order')['type'] == 1) {
            $modelF->cached_cart_id = $cart->id;
            $modelF->status_id = Order::STATUS_PLACED;
            $modelF->user_id = Yii::$app->user->getId();
            $modelF->total = Yii::$app->cart->getCost();
            if ($modelF->save()) {
                Yii::$app->cart->checkOut(false);
                return $this->redirect(['view', 'id' => $modelF->id]);
            }
        }
        if ($modelU->load(Yii::$app->request->post()) && Yii::$app->request->post('Order')['type'] == 2) {
            $modelU->cached_cart_id = $cart->id;
            $modelU->status_id = Order::STATUS_PLACED;
            $modelU->user_id = Yii::$app->user->getId();
            $modelU->total = Yii::$app->cart->getCost();
            if ($modelU->save()) {
                Yii::$app->cart->checkOut(false);
                return $this->redirect(['view', 'id' => $modelU->id]);
            }
        }
        $cart_total = Yii::$app->cart->getCost();

        return $this->render('create', [
            'modelF' => $modelF,
            'modelU' => $modelU,
            'regions'=>$regions,
            'delivery_services' => $delivery_services,
            'cartProvider' => $cartProvider,
            'cart_total'   => $cart_total,
        ]);

    }

/*
    public function actionCreate() {
        $model = new Order();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }*/
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
//        VarDumper::dump($model,10,true);die;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        $searchModel  = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $cartProvider = new ArrayDataProvider([
            'allModels' => unserialize($model->cart->value),
            'modelClass' => Cart::classname(),
        ]);

        $delivery_services = DeliveryService::find()->select('name')->indexBy('id')->column();
        $statuses_list     = Status::find()->select('name')->indexBy('id')->column();
        $users_list        = ArrayHelper::getColumn(User::find()->indexBy('id')->all(), function ($user) {
            return $user->fullName;
        });

        return $this->render('form', [
            'model'        => $model,
            'dataProvider' => $dataProvider,
            'cartProvider' => $cartProvider,
            'cart_total'=> $model->total,
            'delivery_services' => $delivery_services,
            'statuses_list'     => $statuses_list,
            'users_list'        => $users_list,
        ]);
    }
    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        } else {
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;
use backend\components\Controller;
use backend\widgets\activeForm\ActiveForm;
use common\models\News;

/**
 * Новости
 */
class NewsController extends Controller {

    /**
     * Список новостей
     * @return type
     */
    public function actionIndex() {
        $searchModel = new News();
        $searchModel->setScenario('search');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'listType' => News::listType(),
        ]);
    }

    /**
     * Создание новости
     * @return type
     */
    public function actionCreate() {
        $model = new News();
        $model->setScenario('insert');
        $model->date = date('Y-m-d');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->saveNews()) {

                return $this->redirect(['index']);
            }
        }

        return $this->render('form', [
                    'model' => $model,
                    'listType' => News::listType(),
        ]);
    }

    /**
     * Редактирование новости
     * @param type $id
     * @return type
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->setScenario('update');

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->saveNews()) {

                return $this->redirect(['index']);
            }
        }

        return $this->render('form', [
                    'model' => $model,
                    'listType' => News::listType(),
        ]);
    }

    /**
     * Удаление новости
     * @param type $id
     * @return type
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Удаление изображения
     * @param type $id
     * @return type
     */
    public function actionRemoveImage($id) {
        $model = $this->findModel($id);

        $model->image = '';
        $model->save(false, ['image']);

        return Json::encode([]);
    }

    /**
     * Поиск новости
     * @param type $id
     * @return type
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

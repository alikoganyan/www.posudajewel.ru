<?php
namespace backend\controllers;

use backend\models\QuickSearchCharValueSearch;
use backend\widgets\activeForm\ActiveForm;
use common\models\QuickSearchCharValue;
use Yii;
use common\models\QuickSearch;
use backend\models\QuickSearchSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

class QuickSearchController extends Controller {
    public function actionIndex() {
        $searchModel  = new QuickSearchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new QuickSearch();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();

            return $this->redirect(['index']);
        }
        else {
            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();

            return $this->redirect(['index']);
        }
        else {
            $searchModel  = new QuickSearchCharValueSearch(['quick_search_id' => $model->id]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('form', [
                'model'        => $model,
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->allowDelete()) {
            $model->delete();
        }else{
            Yii::$app->session->setFlash('notify', 'Данная запись используется в других объектах');
        }

        return $this->redirect(['index']);
    }

    public function actionRemoveImage($id) {
        $model = QuickSearch::findOne(['id' => $id]);
        $model->image = null;

        return $model->save();
    }

    protected function findModel($id) {
        if (($model = QuickSearch::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

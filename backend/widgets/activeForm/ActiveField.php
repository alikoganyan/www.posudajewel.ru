<?php
namespace backend\widgets\activeForm;

use yii\base\Model;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;

class ActiveField extends \yii\bootstrap\ActiveField {
    public $checkboxTemplate = "{input}";
    public $permission;

    public function textInput($options = []) {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);

        if ($this->permission == 1) {
            $options['readonly'] = true;
        }

        $input = Html::activeTextInput($this->model, $this->attribute, $options);
        $this->parts['{input}'] = Html::label($input, null, ['class' => 'input']);

        return $this;
    }

    public function dropDownList($items, $options = []) {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);

        if ($this->permission == 1) {
            $options['disabled'] = true;
        }

        $input = Html::activeDropDownList($this->model, $this->attribute, $items, $options) . '<i></i>';
        $this->parts['{input}'] = Html::label($input, null, ['class' => 'select']);;

        return $this;
    }

    public function checkbox($options = [], $enclosedByLabel = true) {
        if ($this->permission == 1) {
            $options['disabled'] = true;
        }

        if ($enclosedByLabel) {
            $label = isset($options['label']) ? $options['label'] : '';
            $this->parts['{input}'] = '<label class="checkbox pull-left">' . $this->activeCheckbox($this->model, $this->attribute, $options) . '<i></i>' . $label . '</label>';
            $this->parts['{label}'] = '';
        }

        $this->adjustLabelFor($options);

        return $this;
    }

    /**
     * @param $model Model
     * @param $attribute
     * @param array $options
     * @return mixed
     */
    protected function activeCheckbox($model, $attribute, $options = []) {
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($model, $attribute);
        $value = Html::getAttributeValue($model, $attribute);

        if (!array_key_exists('value', $options)) {
            $options['value'] = '1';
        }
        if (!array_key_exists('uncheck', $options)) {
            $options['uncheck'] = '0';
        }

        if ($this->permission == 1) {
            $options['readonly'] = true;
        }

        unset($options['label']);

        $checked = "$value" === "{$options['value']}";

        if (!array_key_exists('id', $options)) {
            $options['id'] = Html::getInputId($model, $attribute);
        }

        $type = 'checkbox';
        return Html::$type($name, $checked, $options);
    }

    public function label($label = null, $options = []) {
        if (is_bool($label)) {
            $this->enableLabel = $label;
            if ($label === false && $this->form->layout === 'horizontal') {
                Html::addCssClass($this->wrapperOptions, $this->horizontalCssClasses['offset']);
            }
        }
        else {
            if (isset($options['class'])) {
                $options['class'] .= ' label';
            }
            else {
                $options['class'] = 'label';
            }

            $this->enableLabel = true;
            $this->renderLabelParts($label, $options);

            parent::label($label, $options);
        }

        return $this;
    }

    public function textarea($options = []) {
        $options = array_merge($this->inputOptions, $options);

        if ($this->permission == 1) {
            $options['readonly'] = true;
        }

        $this->adjustLabelFor($options);
        $this->parts['{input}'] = '<label class="textarea">' . Html::activeTextarea($this->model, $this->attribute, $options) . '</label>';

        return $this;
    }

    public function passwordInput($options = []) {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);
        $input = Html::icon('icon-append', ['class' => 'icon-append fa fa-eye']).Html::activePasswordInput($this->model, $this->attribute, $options);
        $this->parts['{input}'] = Html::label($input, null, ['class' => 'input']);

        return $this;
    }

    public function datePicker($options = []) {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);

        $options['class'] .= ' datepicker';
        $options['data-dateformat'] = 'yy-mm-dd';

        $input = Html::icon('icon-append', ['class' => 'icon-append fa fa-calendar']).Html::activeTextInput($this->model, $this->attribute, $options);
        $this->parts['{input}'] = Html::label($input, null, ['class' => 'input']);

        return $this;
    }
}
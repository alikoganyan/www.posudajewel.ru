<?php
namespace backend\widgets\activeForm;

use yii\helpers\ArrayHelper;

class ActiveForm extends \yii\bootstrap\ActiveForm {
    public $fieldClass = 'backend\widgets\activeForm\ActiveField';
    public $enableAjaxValidation = true;
    public $options = ['class' => ''];
    public $allowFields = [];

    public function run() {
        if (isset($this->options['class'])) {
            $this->options['class'] .= ' smart-form';
        }
        else {
            $this->options['class'] = 'smart-form';
        }

        parent::run();
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param array $options
     * @return ActiveField
     */
    public function field($model, $attribute, $options = []) {
        $config = $this->fieldConfig;

        if ($config instanceof \Closure) {
            $config = call_user_func($config, $model, $attribute);
        }

        if (!isset($config['class'])) {
            $config['class'] = $this->fieldClass;
        }

        $this->options['class'] .= ' input';

        return \Yii::createObject(ArrayHelper::merge($config, $options, [
            'model'     => $model,
            'attribute' => $attribute,
            'form'      => $this,
        ]));
    }
}

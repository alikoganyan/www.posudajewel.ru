<?php
namespace backend\widgets;

use yii\bootstrap\Html;

class MaskedInput extends \yii\widgets\MaskedInput {
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            $input = Html::activeInput($this->type, $this->model, $this->attribute, $this->options);
            echo Html::label($input, null, ['class' => 'input']);
        } else {
            echo Html::input($this->type, $this->name, $this->value, $this->options);
        }
    }
}

<?php

namespace backend\widgets\gridView\html;

use yii\bootstrap\Html;

class HeaderLinkButton {

    protected $caption;
    protected $url;
    protected $class;

    public function __construct($caption, $url, $class = '') {
        $this->caption = $caption;
        $this->url = $url;
        $this->class = $class;
    }

    public function render($class = 'btn btn-default btn-md') {
        return Html::a($this->caption, $this->url, [
                    'class' => $class . ' ' . $this->class
        ]);
    }

}

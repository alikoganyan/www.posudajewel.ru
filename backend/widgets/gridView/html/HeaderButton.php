<?php
namespace backend\widgets\gridView\html;

use yii\bootstrap\Html;
use yii\web\JsExpression;

class HeaderButton {
    protected $caption;
    protected $onClick;

    public function __construct($caption, $onClick) {
        $this->caption = $caption;
        $this->onClick = $onClick;
    }

    public function render() {
        return Html::button($this->caption, [
            'class'   => 'btn btn-default btn-md',
            'onclick' => new JsExpression($this->onClick),
        ]);
    }
}

<?php
/**
 * @var $this         \yii\web\View
 * @var $buttons      \backend\widgets\gridView\html\HeaderButton[]
 * @var $caption      string
 * @var $isAddVisible boolean
 * @var $addUrl       string
 * @var $isAllowAdd   boolean
 */
?>

<header>git status

    <div class="buttons pull-right">
        <?php foreach ($buttons as $button): ?>
            <?= $button->render() ?>
        <?php endforeach ?>

        <?php if ($isAllowAdd): ?>
            <a class="btn btn-default btn-md<?php if (!$isAddVisible): ?> hidden<?php endif ?>" href="<?= $addUrl ?>">Добавить</a>
        <?php endif ?>

        <a class="btn btn-default btn-md grid-search" href="javascript:void(0);"><?= $showFilter ? 'Сбросить' : 'Поиск' ?></a>
        <span style="" class="page-size-button">
            <a class="btn btn-default btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
               href="javascript:void(0);">Записей</a>
            <ul class="dropdown-menu">
                <?php foreach ([10, 25, 50, 100] as $item): ?>
                    <li>
                        <a href="javascript:void(0);" id="<?= $item ?>"><?= $item ?></a>
                    </li>
                <?php endforeach ?>
            </ul>
        </span>
        <a class="btn btn-default btn-md btn" href="javascript:void(0);"
           onclick="$('.ColVis_Button').click(); return false;">Колонки</a>
    </div>

    <h3>
        <span class="widget-icon ac-buttons">
            <a href="javascript:void(0);" class="dropdown-toggle txt-color-black" data-toggle="dropdown"
               aria-expanded="false">
                <i class="fa fa-reorder fa-md " style="color: #333;"></i>
            </a>

            <ul class="dropdown-menu multi-level">
                <?php foreach ($buttons as $button): ?>
                    <li>
                        <?= $button->render('') ?>
                    </li>
                <?php endforeach ?>
                <li style="display: block" class="dropdown-submenu page-size-button">
                    <a style="width: 100%;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                       href="javascript:void(0); return false;">Записей на странице</a>

                    <ul class="dropdown-menu">
                        <?php foreach ([10, 25, 50, 100] as $item): ?>
                            <li>
                                <a href="javascript:void(0);" id="<?= $item ?>"><?= $item ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li>
                    <a class="" href="javascript:void(0);" onclick="$('.ColVis_Button').click(); return false;">Видимость полей</a>
                </li>
                <li>
                    <a onclick="$('.filters').toggle();" class="" href="javascript:void(0);">Поиск записей</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="" href="<?= $addUrl ?>">Добавить запись</a>
                </li>
            </ul>
        </span>

        <span><?= $caption ?></span>
    </h3>
</header>

<?php
/**
 * @var $this \yii\web\View
 * @var $urls []
 */

?>

<?php if (count($urls) > 0): ?>
    <div class="table-action-menu">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-gear fa-lg txt-color-green"></i>
        </a>
        <ul class="dropdown-menu">
            <?php if (isset($urls['update'])): ?>
                <li><a href="<?= $urls['update'] ?>" class="table-edit">Редактировать</a></li>
            <?php endif ?>

            <?php if (isset($urls['copy'])): ?>
                <li><a href="<?= $urls['copy'] ?>" class="grid-service-copy">Копировать</a></li>
            <?php endif ?>

            <?php if (isset($urls['delete'])): ?>
                <li><a href="#" onclick="gridView.confirm('<?= $urls['delete'] ?>')">Удалить</a></li>
            <?php endif ?>
        </ul>
    </div>
<?php endif ?>

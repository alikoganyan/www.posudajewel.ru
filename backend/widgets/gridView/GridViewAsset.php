<?php
namespace backend\widgets\gridView;

use yii\web\AssetBundle;

class GridViewAsset extends AssetBundle {
    public $sourcePath = '@backend/widgets/gridView';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/datatables/jquery.dataTables.min.js',
        'js/datatables/dataTables.colVis.js',
        'js/datatables/datatables.responsive.js',
        'js/datatables/dataTables.tableTools.min.js',
        'js/datatables/dataTables.bootstrap.min.js',

        'js/custom.js',
        'js/grid-view.js',
    ];
}

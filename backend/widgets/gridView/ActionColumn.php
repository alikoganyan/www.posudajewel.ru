<?php
namespace backend\widgets\gridView;

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class ActionColumn extends \yii\grid\ActionColumn {
    public function renderHeaderCellContent() {
        return trim($this->header) !== '' ? $this->header : $this->getHeaderCellLabel();
    }

    public function renderDataCell($model, $key, $index) {
        if ($this->contentOptions instanceof \Closure) {
            $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
        } else {
            $options = $this->contentOptions;
        }
        return Html::tag('td', $this->renderDataCellContent($model, $key, $index), ArrayHelper::merge($options, [
            'class' => 'service-button',
        ]));
    }
}
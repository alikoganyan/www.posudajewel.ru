<?php
namespace backend\widgets\gridView;

use yii\bootstrap\Html;

class DataColumn extends \yii\grid\DataColumn {
    public $enableSorting = false;

    public function init() {
        parent::init();

        if ($this->grid->filterModel) {
            $this->filterInputOptions['placeholder'] = $this->grid->filterModel->getAttributeLabel($this->attribute);
        }
    }

    public function renderFilterCell() {
        return Html::tag('th', $this->renderFilterCellContent(), $this->filterOptions);
    }

    public function renderHeaderCellContent() {
        if ($this->header !== null || $this->label === null && $this->attribute === null) {
            return parent::renderHeaderCellContent();
        }

        $label = $this->getHeaderCellLabel();
        if ($this->encodeLabel) {
            $label = Html::encode($label);
        }

        if ($this->attribute !== null && $this->enableSorting &&
            ($sort = $this->grid->dataProvider->getSort()) !== false && $sort->hasAttribute($this->attribute)) {
            return $sort->link($this->attribute, array_merge($this->sortLinkOptions, ['label' => $label]));
        } else {
            return $label;
        }
    }
}
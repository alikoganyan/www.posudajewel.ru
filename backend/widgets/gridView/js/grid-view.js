class GridView {
    confirm(deleteUrl) {
        $.SmartMessageBox({
            title : "Удаление записи",
            content : "Вы уверенны, что хотите удалить данную запись?",
            buttons : '[Нет][Да]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "Да") {
                location.href = deleteUrl;
            }
        });
    }
}

let gridView = new GridView();

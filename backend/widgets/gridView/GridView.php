<?php

namespace backend\widgets\gridView;

use Yii;
use backend\components\ActiveRecord;
use common\components\Controller;
use yii\base\InvalidConfigException;
use yii\bootstrap\Html;
use yii\grid\Column;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class GridView extends \yii\grid\GridView {

    public $emptyText = '';
    public $summary = false;
    public $tableOptions = ['class' => 'table table-striped table-bordered dataTable no-footer', 'id' => 'dt_basic'];
    // custom properties
    public $headerButtons = [];
    public $addUrlParams = [];

    /** @var Controller */
    public $controller = null;
    public $actions = [
        'update' => '/update',
        'copy' => '/copy',
        'delete' => '/delete',
    ];
    public $buttons = ['update', 'delete'];
    public $layout = "{summary}\n{items}";
    public $actionParams = [];
    public $isAddVisible = true;
    public $hideColumns = [];
    public $pk = 'id';
    public $showFilter = false;

    protected function initColumns() {
        $this->columns[] = [
            'template' => '{view}',
            'class' => 'backend\widgets\gridView\ActionColumn',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    /** @var ActiveRecord $model */
                    $this->actionParams[$this->pk] = $model->{$this->pk};
                    $this->actionParams = array_reverse($this->actionParams);

                    $urls = [];
                    foreach ($this->buttons as $key) {
                        if (isset($this->actions[$key])) {
                            if (is_array($this->actions[$key])) {
                                $urls[$key] = Url::to($this->actions[$key] + $this->actionParams);
                            }else{
                                $urls[$key] = Url::to([$this->controller->id . $this->actions[$key]] + $this->actionParams);
                            }
                        }
                    }

                    $className = explode('\\', get_class($model));

                    if ($model->getFieldPermission('delete') < 1 && array_pop($className) != 'ServiceOption') {
                        unset($urls['delete']);
                    }

                    return $this->render('edit-button', [
                                'urls' => $urls,
                    ]);
                },
                        'contentOptions' => [
                            'class' => 'service-button',
                        ],
                    ],
                ];

                parent::initColumns();
            }

            public function init() {
                parent::init();

                if ($this->controller === null) {
                    $this->controller = \Yii::$app->controller;
                }

                // Определим показывать фильтр или нет
                $get = Yii::$app->getRequest()->get();
                foreach ($get as $params) {
                    if (is_array($params)) {

                        foreach ($params as $key => $val) {
                            if ($val != '') {
                                $this->showFilter = true;
                            }
                        }
                    }
                }
            }

            public function run() {
                GridViewAsset::register($this->view);

                parent::run();
            }

            public function renderItems() {
                $content = parent::renderItems();

                return Html::tag('div', $content, [
                            'id' => 'dt_basic_wrapper',
                            'class' => 'dataTables_wrapper form-inline dt-bootstrap no-footer',
                ]);
            }

            public function renderTableHeader() {
                $cells = [];
                foreach ($this->columns as $key => $column) {
                    /* @var $column Column */
                    $options = [
                        'rowspan' => 1,
                        'colspan' => 1,
                    ];

                    if ($key == 0) {
                        $options['data-class'] = 'expand';
                    }

                    $className = explode('\\', get_class($column));
                    if (array_pop($className) != 'ActionColumn') {
                        if (in_array($column->attribute, $this->hideColumns))
                            $options['data-hide'] = 'phone, tablet';
                    }

                    $cells[] = Html::tag('th', $column->renderHeaderCellContent(), ArrayHelper::merge($column->headerOptions, $options));
                }

                $content = $this->renderFilters();
                $content .= Html::tag('tr', implode('', $cells), $this->headerRowOptions);

                return "<thead>\n" . $content . "\n</thead>";
            }

            public function renderFilters() {
                if ($this->filterModel !== null) {
                    $cells = [];
                    foreach ($this->columns as $column) {
                        /* @var $column Column */
                        $cells[] = $column->renderFilterCell();
                    }

                    if ($this->showFilter) {
                        $this->filterRowOptions['style'] = 'display: table-row;';
                    }

                    return Html::tag('tr', implode('', $cells), $this->filterRowOptions);
                } else {
                    return '';
                }
            }

            public function renderCaption() {
                if (!empty($this->caption)) {
                    $entityName = strtolower(str_replace('-', '', $this->controller->id));
                    $permissions = \Yii::$app->user->identity->role->getParsedPermissions();
                    $isAllowAdd = false;

                    if (isset($permissions[$entityName]['add'])) {
                        $isAllowAdd = $permissions[$entityName]['add'] == 1;
                    }

                    return $this->render('header', [
                                'caption' => $this->caption,
                                'buttons' => $this->headerButtons,
//                                'addUrl' => Url::to([$this->controller->id . '/create'] + $this->addUrlParams),
                                'addUrl' => Yii::$app->urlManagerToFrontend->createUrl(['/']),
//                                'addUrl' =>  Url::to(['/'] + $this->addUrlParams),
                                'isAddVisible' => $this->isAddVisible,
                                'isAllowAdd' => $isAllowAdd,
                                'showFilter' => $this->showFilter,
                    ]);
                } else {
                    return false;
                }
            }

            protected function createDataColumn($text) {
                if (!preg_match('/^([^:]+)(:(\w*))?(:(.*))?$/', $text, $matches)) {
                    throw new InvalidConfigException('The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
                }

                return \Yii::createObject([
                            'class' => $this->dataColumnClass ? : DataColumn::className(),
                            'grid' => $this,
                            'attribute' => $matches[1],
                            'format' => isset($matches[3]) ? $matches[3] : 'text',
                            'label' => isset($matches[5]) ? $matches[5] : null,
                ]);
            }

        }


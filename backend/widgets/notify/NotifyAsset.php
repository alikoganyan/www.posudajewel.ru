<?php
namespace backend\widgets\notify;

use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle {
    public $sourcePath = '@backend/widgets/notify';
    public $js = [
        'notify.min.js',
    ];
    public $css = [
        'style.css',
    ];

    public $depends  = [
        'yii\web\YiiAsset',
    ];
}

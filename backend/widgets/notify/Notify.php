<?php
namespace backend\widgets\notify;

use yii\base\Widget;

class Notify extends Widget {
    public $message;
    public $type = 'success';

    public function run() {
        NotifyAsset::register($this->view);

        $this->view->registerJs("$.notify('{$this->message}', '{$this->type}')");
    }
}

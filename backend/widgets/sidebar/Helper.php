<?php
namespace backend\widgets\sidebar;

class Helper {
    public static function parseUrl($url) {
        $result = explode('/', $url);

        return $result[2];
    }
}

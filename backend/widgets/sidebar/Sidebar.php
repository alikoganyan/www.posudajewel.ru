<?php
namespace backend\widgets\sidebar;

use common\models\User;
use yii\base\Widget;
use yii\helpers\Url;

class Sidebar extends Widget {
    public $items;

    public function init() {
        parent::init();

        $this->items = [
            [
                'title' => 'Заказы',
                'url'   => Url::to(['order/index']),
                'icon' => ' fa-shopping-cart',
            ],
            [
                'title' => 'Пользователи',
                'url'   => Url::to(['user/index']),
                'icon' => 'fa-user',
            ],
            [
                'title' => 'Типы скидок',
                'url'   => Url::to(['discount/index']),
                'icon' => 'fa-percent',
            ],
            [
                'title' => 'Компании',
                'url'   => Url::to(['company/index']),
                'icon' => 'fa-building',
            ],
            [
                'title' => 'Предложения',
                'url'   => Url::to(['service/index']),
                'icon' => 'fa-briefcase',
            ],
            [
                'title' => 'Категории',
                'url'   => Url::to(['service-type/index']),
                'icon' => 'fa-tags',
            ],
            [
                'title' => 'Свойства',
                'url'   => Url::to(['char/index']),
                'icon' => 'fa-external-link',
            ],
            [
                'title' => 'ОКЕИ',
                'url'   => Url::to(['unit/index']),
                'icon' => 'fa-cube',
            ],
            [
                'title' => 'Регионы',
                'url'   => Url::to(['region/index']),
                'icon' => 'fa-globe',
            ],
            [
                'title' => 'Новости',
                'url'   => Url::to(['news/index']),
                'icon' => 'fa-bullhorn',
            ],
            [
                'title' => 'Информация',
                'url'   => Url::to(['page/index']),
                'icon' => 'fa-newspaper-o',
            ],
            [
                'title' => 'Права доступа',
                'url'   => Url::to(['role/index']),
                'icon' => 'fa-group',
            ],
            [
                'title' => 'Каталог предложений',
                'url'   => Url::to(['catalog/index']),
                'icon' => 'fa-list-ul',
            ],
            [
                'title' => 'Быстрый поиск',
                'url'   => Url::to(['quick-search/index']),
                'icon' => 'fa-search',
            ],
            [
                'title' => 'Подписчики',
                'url'   => Url::to(['subscribe/index']),
                'icon' => 'fa-envelope-o',
            ],
            [
                'title' => 'Настройки сайта',
                'url'   => Url::to(['settings/index']),
                'icon' => 'fa-gear',
            ],
        ];
    }

    public function run() {
        SidebarAsset::register($this->view);
        /** @var User $user */
        $user = \Yii::$app->user->identity;

        if (!$user || $user->confirm_code) {
            $this->items = [];
        }

        return $this->render('index', [
            'user'       => $user,
            'items'      => $this->items,
            'currentUrl' => \Yii::$app->controller->id,
        ]);
    }
}

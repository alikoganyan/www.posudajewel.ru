<?php

/**
 * @var $this       \yii\web\View
 * @var $user       \common\models\User
 * @var $items      []
 * @var $currentUrl string
 */
use \backend\widgets\sidebar\Helper;
use \yii\bootstrap\Html;
use \yii\helpers\Url;
?>

<aside id="left-panel">
    <div class="login-info">
        <span>
            <a href="#">
                <?php if (!$user): ?>
                    <?= Html::icon(null, ['class' => 'glyphicon glyphicon-user']) ?>
                    <a href="<?= Url::to(['profile/index']) ?>"><span class="title">Заполнить анкету</span></a>
                <?php else: ?>
                    <?php if (!$user->image_crop): ?>
                        <?= Html::icon(null, ['class' => 'glyphicon glyphicon-user']) ?>
                    <?php else: ?>
                        <img src="<?= $user->getUrlPrev() ?>" />
                    <?php endif ?>

                    <a href="<?= Url::toRoute(['/site/index']) ?>"><span class="title"><?= $user->fullName ?></span></a>
                <?php endif ?>
            </a>
        </span>
    </div>

    <nav>
        <ul>
            <li>
                <ul style="display: block;">
                    <?php foreach ($items as $item): ?>
                        <li<?php if (Helper::parseUrl($item['url']) == $currentUrl): ?> class="active"<?php endif ?>>
                            <a href="<?= $item['url'] ?>" title="<?= $item['title'] ?>">
                                <i class="fa <?= $item['icon'] ?>"></i>
                                <span class="menu-item-parent"><?= $item['title'] ?></span>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </li>
        </ul>
    </nav>

    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>

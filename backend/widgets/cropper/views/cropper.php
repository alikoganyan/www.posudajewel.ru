<?php
/**
 * Created by PhpStorm.
 * User: phuon
 * Date: 9/23/2016
 * Time: 11:48 PM
 * @var string $name
 * @var string $value
 * @var string $attribute
 * @var array  $pluginOptions
 * @var array  $options
 */
use navatech\cropper\assets\CropperAsset;
use yii\bootstrap\Html;
use yii\helpers\Json;

CropperAsset::register($this);
?>
<style style="text/css">
	.image-cropper {
		width: <?=$options['width']?>px;
		height: <?=$options['height']?>px;
		position: relative;
	}

	.image-cropper label.hover {
		position: absolute;
		width: 100%;
		height: 100%;
		text-align: center;
	}

	.image-cropper label.hover i {
		font-size: 25px;
		margin-top: 40%;
		display: none;
	}

	.image-cropper label.hover:hover {
		background: #ccc;
		opacity: 0.4;
		cursor: pointer;
	}

	.image-cropper label.hover:hover i {
		display: block;
	}

	.image-cropper canvas {
		width: 100%;
		height: 100%;
	}

	.image-cropper .preview {
		margin: 0;
	}

	#image-crop {
		max-height: 400px;
	}
</style>
<div class="image-cropper">
	<label class="hover">
		<i class="glyphicon glyphicon-picture"></i>
		<input type="file" name="image-cropper" style="display: none;">
		<?= Html::hiddenInput($name, $value) ?>
	</label>
	<div class="preview">
		<canvas class="canvas-preview" width="<?= $options['width'] ?>px" height="<?= $options['height'] ?>px">
	</div>
</div>
<div class="modal fade" id="modal-crop" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalLabel">Crop the image</h4>
			</div>
			<div class="modal-body">
				<div>
					<img id="image-crop" src="<?= $value ?>">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-crop" data-dismiss="modal" onclick="service.onCrop(this)">Crop</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>
	var preview = $(".image-cropper .preview");
	imageCrop = $('#image-crop');
	function drawImage(canvas, url) {
		var context = canvas[0].getContext('2d');
		var base_image = new Image();
		base_image.src = url;
		base_image.onload = function () {
			context.drawImage(base_image, 0, 0, <?=$options['width']?>, <?=$options['height']?>);
		}
	}
	drawImage(preview.find('canvas'), '<?=$value?>');
	$(document).on("change", "input[name='image-cropper']", function () {
		var file_data = $(this).prop('files')[0];
		var fileReader = new FileReader();
		fileReader.onload = function (e) {
			drawImage(preview.find('canvas'), e.target.result);
			$("input[name='<?=$name?>']").val(e.target.result);
			$("#image-crop").attr('src', e.target.result);
			$('#modal-crop').modal('show');
		};
		console.log(file_data);
		fileReader.readAsDataURL(file_data);
	});
	$('#modal-crop').on('shown.bs.modal', function () {
		imageCrop.cropper(<?=Json::encode($pluginOptions)?>);
		$("#modal-crop .btn-crop").on('click', function () {
			var obj = imageCrop.cropper('getData');
			if (!obj) {
				return false;
			}
			preview.html(imageCrop.cropper('getCroppedCanvas'));
			$("input[name='<?=$name?>']").val(imageCrop.cropper('getCroppedCanvas').toDataURL());
		});
	}).on('hidden.bs.modal', function () {
		imageCrop.cropper('destroy');
	});
</script>
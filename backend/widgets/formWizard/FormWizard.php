<?php
namespace backend\widgets\formWizard;

use yii\base\Widget;
use yii\helpers\Url;

class FormWizard extends Widget {
    public function run() {
        FormWizardAsset::register($this->view);

        return $this->render('index', [
            'steps' => [
                Url::to(['profile/index']),
                Url::to(['profile/finish-registration']),
                Url::to(['profile/main-info']),
                Url::to(['profile/company-create']),
                Url::to(['profile/company-locality']),
                Url::to(['profile/service-create']),
                Url::to(['profile/service-price']),
            ],
        ]);
    }
}

<?php
/**
 * @var $this \yii\web\View
 * @var $steps []
 */
?>

<section>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach ($steps as $step): ?>
                    <li role="presentation" class="active">
                        <a href="<?= $step ?>" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                <span class="round-tab">
                                    1
                                </span>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</section>

<?php
namespace backend\widgets\formWizard;

use yii\web\AssetBundle;

class FormWizardAsset extends AssetBundle {
    public $sourcePath = '@backend/widgets/formWizard';

    public $css = [
        'css/form-wizard.css',
    ];
}
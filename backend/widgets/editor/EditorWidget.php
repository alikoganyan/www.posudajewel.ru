<?php

namespace backend\widgets\editor;

use yii\base\Widget;

class EditorWidget extends Widget {
    
    public $id;

    public function run() {
        $view = $this->getView();
        
        Asset::register($view);
        
        $script = "
        	CKEDITOR.replace('{$this->id}', {
        	 	height: '250px',
        	 	startupFocus: false,
        	 	'filebrowserBrowseUrl':'/kcfinder-3.12/browse.php?type=files',
                'filebrowserImageBrowseUrl':'/kcfinder-3.12/browse.php?type=images',
                'filebrowserFlashBrowseUrl':'/kcfinder-3.12/browse.php?type=flash',
                'filebrowserUploadUrl':'/kcfinder-3.12/upload.php?type=files',
                'filebrowserImageUploadUrl':'/kcfinder-3.12/upload.php?type=images',
                'filebrowserFlashUploadUrl':'/kcfinder-3.12/upload.php?type=flash'
	        });
	   	";

        $view->registerJs($script, \yii\web\View::POS_READY);
    }

}

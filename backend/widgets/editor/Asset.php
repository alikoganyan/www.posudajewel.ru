<?php

namespace backend\widgets\editor;

use yii\web\AssetBundle;

class Asset extends AssetBundle {

    public $sourcePath = '@backend/widgets/editor/assets';
    public $css = [
    ];
    public $js = [
        'js/ckeditor/ckeditor.js',
    ];
    public $depends = [
    ];

}

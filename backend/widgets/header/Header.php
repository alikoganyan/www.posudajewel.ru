<?php
namespace backend\widgets\header;

use yii\base\Widget;

class Header extends Widget {
    public function run() {
        HeaderAsset::register($this->view);

        $user = \Yii::$app->user->identity;

        return $this->render('index', [
            'user' => $user,
        ]);
    }
}

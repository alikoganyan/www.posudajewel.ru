<?php
namespace backend\widgets\settingsField;

use common\models\Settings;
use kartik\editable\Editable;
use yii\base\Widget;
use yii\helpers\Html;

class SettingsField extends Widget {
    /** @var $setting Settings */
    public $setting;

    /** @var $editable Editable  */
    public $editable;

    public function run() {
        $id = "id_{$this->setting->key}" . \Yii::$app->security->generateRandomString(5);
        $html = Html::textarea("Settings[{$this->setting->key}]", $this->setting->value, ['id' => $id, 'class' => 'form-control']);

        if ($this->setting->type == Settings::TYPE_JSON) {
            $value = json_decode($this->setting->value, true);

            if (json_last_error() != JSON_ERROR_NONE) {
                $value = ['title' => '', 'description' => ''];
            }

            $html = Html::input('text', "Settings[{$this->setting->key}][title]", $value['title'], [
                    'class' => 'form-control',
                    'placeholder' => 'Заголовок',
                ]) .
                Html::textarea("Settings[{$this->setting->key}][description]", $value['description'], [
                    'id' => $id,
                    'class' => 'form-control',
                    'placeholder' => 'Ссылка',
                ]);

            if (in_array($this->setting->key, ['vk', 'ok', 'fb', 'google'])) {
                $html = Html::input('text', "Settings[{$this->setting->key}][title]", $value['title'], [
                        'class' => 'form-control',
                        'placeholder' => 'Заголовок',
                    ]) .
                    Html::input('text', "Settings[{$this->setting->key}][description]", $value['description'], [
                        'id' => $id,
                        'class' => 'form-control',
                        'placeholder' => 'Ссылка',
                    ]);
            }
        } else if ($this->setting->type == Settings::TYPE_IMAGE) {
            $form = $this->editable->getForm();
            $html = $form->field($this->setting, $this->setting->key)->label(false)->fileInput();
        }

        return $html;
    }
}

<?php
/**
 * @var $this \yii\web\View
 * @var $createButtonLabel string
 */

use \yii\bootstrap\Html;
?>

<div class="buttons-block-ch">
    <div>
        <?= Html::button('Закрыть', ['class' => 'btn btn-default btn-footer', 'onclick' => 'formFooter.close()']) ?>
        <?= Html::submitButton($createButtonLabel, ['class' => 'btn btn-default btn-footer']) ?>
    </div>
</div>

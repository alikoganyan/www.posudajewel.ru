<?php
namespace backend\widgets\formFooter;

use yii\web\AssetBundle;

class FormFooterAsset extends AssetBundle {
    public $sourcePath = '@backend/widgets/formFooter';

    public $js = [
        'js/custom.js',
    ];
}
<?php
namespace backend\widgets\formFooter;

use yii\base\Widget;
use yii\bootstrap\Html;
use yii\helpers\Json;
use yii\web\View;

class FormFooter extends Widget {
    public $resourceId = null;
    public $createButtonLabel = 'Сохранить';

    public function run() {
        FormFooterAsset::register($this->view);

        $entity = \Yii::$app->controller->id;
        $js = "formFooter = new FormFooter('$entity'";
        if ($this->resourceId != null) {
            $js .= ", {$this->resourceId}";
        }
        $js .= ');';
        $this->view->registerJs($js);

        return $this->render('index', [
            'createButtonLabel' => $this->createButtonLabel,
        ]);
    }
}

class FormFooter {
    constructor(entity, resourceId) {
        this.entity = entity;
        this.resourceId = resourceId;
    }

    close(e) {
        let self = this;

        if (isChanged == 1) {
            $.SmartMessageBox({
                title: "Удаление записи",
                content: "Данные не сохранены. Вы уверенны что хотите покинуть страницу?",
                buttons: '[Нет][Да]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Да") {
                    location.href = '/admin/' + self.entity + '?id=' + this.resourceId;
                }
            });
        }
        else {
            location.href = '/admin/' + self.entity + '?id=' + this.resourceId;
        }
    }
}

class Char {
    constructor() {
        this.url = '/admin/char/';
    }

    onModalOpen() {
        if ($('#chars').length > 0) {
            $('#chars').change();
        }
        else {
            $('#modal-chars .save-button').prop('disabled', true);
        }
    }

    onChange(element) {
        let charId = $(element).val();
        this.destroySelect2();

        $.ajax({
            url: this.url + 'get-values',
            method: 'GET',
            data: {
                id: charId
            }
        }).done(response => {
            response = $.parseJSON(response);
            let html = '';

            response.values.forEach(value => {
                html += '<option value="' + value.id + '">' + value.name + '</option>';
            });

            $('#char-values').html(html);

            if (response.is_multiple) {
                $('#char-values').addClass('select2')
                    .prop('multiple', true)
                    .select2();
            }
            else {
                this.destroySelect2();
            }
        });
    }

    destroySelect2() {
        if ($('#char-values').hasClass('select2')) {
            $('#char-values').select2('destroy');
            $('#char-values').removeAttr('multiple');
        }
    }
}

let char = new Char();

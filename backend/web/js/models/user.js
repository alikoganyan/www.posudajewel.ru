class User {
    showRegForm(e) {
        let userEmail = $('[name="User[email]"]').val();

        $.ajax({
            url: '/admin/user/password-form',
            method: 'GET',
            data: {
                email: userEmail
            },
        }).done(response => {
            $('#change-password-modal .modal-body').html(response);
        });
    }

    savePassword() {
        let password = $('#user-password').val(),
            repeatPassword = $('#user-repeat_password').val(),
            errorField = $('.field-user-repeat_password .help-block-error'),
            email = $('[name="User[email]"]').val();

        if (password !== repeatPassword) {
            errorField.html('Пароли не совпадают');
            return false;
        }
        else {
            errorField.html('');
        }

        $.ajax({
            url: '/admin/user/change-password',
            method: 'POST',
            data: {
                User: {
                    email: email,
                    password: password,
                    repeat_password: repeatPassword
                },
            },
        }).done(response => {
            $('#change-password-modal').modal('hide');
        });
    }
}

let user = new User();

class Options {
    constructor() {
        this.optionsCount = $('#option-groups .group').length;
    }

    addOption(e) {
        let parent = $(e).parents('.group');

        $.ajax({
            url: '/admin/service/option-html',
            method: 'GET',
            data: {
                index: this.optionsCount + 1,
                group_id: parent.attr('group-id'),
            },
        }).done(html => {
            parent.find('.options').append(html);

            this.optionsCount++;
        });
    }

    addGroup() {
        $.ajax({
            url: '/admin/service/option-group-html',
            method: 'GET',
            data: {
                index: this.optionsCount + 1,
            },
        }).done(html => {
            $('#option-groups').append(html);
        });
    }

    removeOption(e) {
        $(e).parents('.option-input').remove();
    }

    removeGroup(e) {
        $(e).parents('.group').remove();
        $(e).parents('.group').remove();
    }
}

let options = new Options();

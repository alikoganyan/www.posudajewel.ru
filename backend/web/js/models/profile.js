class Profile {
    cloneChars(e) {
        $.ajax({
            url: '/admin/service/chars-clone',
            method: 'GET',
            data: {
                id: $(e).val()
            }
        }).done(response => {
            $('#chars').html(response.html);
            $('#service-service_type_id').val(response.serviceTypeId);
        });
    }

    cloneOptions(e) {
        $.ajax({
            url: '/admin/service/options-clone',
            method: 'GET',
            data: {
                id: $(e).val()
            }
        }).done(response => {
            $('#options-grid').html(response);
        });
    }
}

let profile = new Profile();

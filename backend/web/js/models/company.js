class Company {
    getLocalities(elem) {
        $.ajax({
            url: '/admin/region/localities',
            method: 'GET',
            data: {region_id: $(elem).val()},
        }).done((response) => {
            let html = '<option value="">Выберите значение</option>';

            response.localities.forEach(item => {
                html += '<option value="' + item.id + '">' + item.name + '</option>';
            });

            $('#locality select').html(html);
            
            if(response.region !== null){
                this.setCoords(response.region.latitude, response.region.longitude);
            }
            gMap.setZoom(8);
        });
    }

    getLocalityCoords(elem) {
        if (!$(elem).find(':selected').val()) {
            $('[name="region"]').change();
            return false;
        }

        return $.ajax({
            url: '/admin/locality/get-coords',
            method: 'GET',
            data: {
                id: $(elem).find(':selected').val(),
            }
        }).done(response => {
            if (response) {
                this.setCoords(response.latitude, response.longitude);
                gMap.setZoom(10);
            }
        });
    }

    setCoords(latitude, longitude) {
        if (!latitude || !longitude) {
            return false;
        }

        let latLng = new google.maps.LatLng(latitude, longitude);
        
        if(typeof gMarker === 'undefined'){
            gMarker = new google.maps.Marker({"map": gMap, "position": latLng});
        }else{
            gMarker.setPosition(latLng);
        }
        
        gMap.setCenter(latLng);

        $('[name="Company[latitude]"]').val(latitude);
        $('[name="Company[longitude]"]').val(longitude);
    }
    
    toggleMap() {
        $('#map').toggle();
        if($('#map').is(':visible')){
            let center = gMap.getCenter();
            google.maps.event.trigger(gMap, 'resize');
            gMap.setCenter(center);
        }
        $('#btn-map').html($('#map').is(':visible') ? 'Скрыть карту' : 'Показать карту');
    }
}

var gMarker;
let company = new Company();

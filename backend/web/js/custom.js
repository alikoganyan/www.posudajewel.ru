class Image {
    onChange(input) {
        let data = new FormData();

        data.append('file', $('[type="file"]')[0].files[0]);
        data.append('userId', $('#user-id').val());

        $.ajax({
            url: '/admin/user/upload-photo',
            data: data,
            method: 'POST',
            processData: false,
            contentType: false,
        }).done(response => {
            $('.imged').html('<img id="avatar" src="' + response.result + '" />');
        });
    }

    trigger(selector) {
        $(selector).click();
    }

    remove(selector, url, userId) {
        if ($('#avatar').length == 0) {
            return false;
        }

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                userId: userId,
            },
            success: (response) => {
                $('#avatar').remove();
            }
        });
    }
}

let image = new Image();

function setCords(map, event) {
    let cords = gm.setCords(map, event);

    company.setCoords(cords.lat(), cords.lng());
}

function changed() {
    isChanged = 1;
}

$(function () {
    $('.grid-search').click(function(){
        if(!$('.filters').is(':visible')){
            $('.filters').toggle();
            $(this).text('Сбросить');
        }else{
            $('.filters input, .filters select').val('').change();
        }

        return false;
    });

    $('.grid-service-remove').click(function(){
        var id = [];

        $('input[name=\'grid_id[]\']').each(function () {
            if ($(this).is(':checked')) {
                id.push($(this).val());
            }
        });

        if (id.length > 0) {
            $.ajax({
                data: {
                    id: id
                },
                type: 'GET',
                url: $(this).attr('href'),
                dataType: 'json',
                error: function () {
                    $.notify('При отправке данных произошла ошибка!', 'error');
                },
                success: function (data, textStatus) {
                    if (data.status == 'success') {
                        location.reload();
                    } else
                    {
                        $.notify(data.message, 'error');
                    }
                }
            });
        } else {
            $.notify('Выберите предложения', 'error');
        }

        return false;
    });

    $('#dt_basic tbody td').dblclick(function(){
        var edit = $(this).closest('tr').find('.table-edit');
        if(edit.length){
            window.location = edit.attr('href');
        }
    });

    $('.create-url').click(function(){
        var name = $('input.url-name').val(),
                url = '',
                fieldUrl = $('input.url-translit');

        if (fieldUrl) {
            var translit = {
                'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e',
                'ё': 'ye', 'ж': 'zh', 'з': 'z', 'и': 'i', 'к': 'k', 'л': 'l',
                'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's',
                'т': 't', 'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch',
                'ш': 'sh', 'щ': 'shch', 'ы': 'y', 'э': 'e', 'ю': 'yu', 'я': 'ya',
                'й': 'y', 'ь': '', 'ъ': '', ' ': '-', '_': '-'
            };

            temp = name.toLowerCase().replace(/[^a-zа-яё\d\-\_ ]/g, '').split('');

            $.each(temp, function(index, value) {
                if (typeof translit[value] === "undefined") {
                    url += value;
                } else
                {
                    url += translit[value];
                }
            });


            fieldUrl.val(url.replace(/[-]{2,}/g, '-'));
        }

        return false;
    });

    $('.dropdown-submenu>a').click(function(){
        return false;
    });

    $('.service-save').click(function () {
        var form = $(this).closest('form');

        form.find('input[name=open]').val(1);
        form.submit();

        return false;
    });

    $('.service-option .option-group-add').click(function () {
        $('.service-option .option-group-form').show();
        $(this).hide();

        return false;
    });
    $('.service-option').on('click', '.option-group-save', function () {
        var cont = $(this).closest('.option-add'),
                title = cont.find('input[name=title]').val(),
                one = cont.find('input[name=one]').is(':checked') ? 1 : 0,
                content;

        if (title.length > 0) {
            var group = $(this).closest('.option-group');

            var group_id = 0;
            $('.service-option .option-group').each(function () {
                var id = $(this).data('id');

                if (group_id < id) {
                    group_id = id;
                }
            });
            group_id++;

            if (group.hasClass('option-group-form')) {
                content = '<div class="option-group" data-id="' + group_id + '">' +
                        '<div class="option-group-title">' +
                        '<span>' + $('<div/>').html(title).text() + '</span>' +
                        '<input value="' + $('<div/>').html(title).text() + '" type="hidden" name="Service[groups][' + group_id + '][name]"/>' +
                        '<input value="' + one + '" type="hidden" name="Service[groups][' + group_id + '][one]"/>' +
                        '<div class="pull-right"><a href="#" class="option-group-update">Изменить</a> | <a href="#" class="option-group-remove">Удалить</a></div>' +
                        '</div>' +
                        '<div class="option-items"></div>' +
                        '<div class="option-add"><a href="#" class="option-item-add">Добавить новое условие...</a></div>' +
                        '</div>';

                group.before(content);
            } else {
                group.find('.option-group-title span').text(title);
                group.find('.option-group-title input:eq(0)').val(title);
                group.find('.option-group-title input:eq(1)').val(one);
            }
        }

        cont.find('input[name=title]').val('');

        $(this).closest('.option-group').find('.option-group-close').trigger('click');
    });
    $('.service-option').on('click', '.option-group-close', function () {
        var group = $(this).closest('.option-group');

        if (group.hasClass('option-group-form')) {
            $('.service-option .option-group-form').hide();
            $('.service-option .option-group-add').show();
        } else {
            $(this).closest('.option-add').remove();
            group.find('.option-group-title').show();
        }
    });
    $('.service-option').on('click', '.option-group-remove', function () {
        $(this).closest('.option-group').remove();

        return false;
    });
    $('.service-option').on('click', '.option-group-update', function () {
        var cont = $(this).closest('.option-group'),
                title = cont.find('.option-group-title span').text(),
                one = cont.find('.option-group-title input:eq(1)').val() == 1 ? true : false;

        cont.find('.option-group-title').hide();

        cont.prepend('<div class="option-add">' + $(".service-option .option-group-form").html() + '</div>');
        cont.find('input[name=title]').val(title);
        cont.find('input[name=one]').prop('checked', one);


        return false;
    });
    $('.service-option').on('click', '.option-item-add', function () {
        var content = '<div class="row">' +
                '<section class="col col-9">' +
                '<label class="input"><input type="text" name="title" class="form-control" value="" placeholder="Введите название условия изменения стоимости"></label>' +
                '</section>' +
                '<section class="col col-3">' +
                '<label class="input"><input type="text" name="price" class="form-control" value="" placeholder="Сумма изменения (руб.)"></label>' +
                '</section>' +
                '</div>' +
                '<div class="option-add-btn">' +
                '<button type="button" class="btn btn-default option-item-save">Сохранить</button> ' +
                '<button type="button" class="btn btn-default option-item-close">Закрыть</button>' +
                '</div>';

        $(this).closest('.option-add').html(content);

        return false;
    });
    $('.service-option').on('click', '.option-item-save', function () {
        var cont = $(this).closest('.option-add'),
                title = cont.find('input[name=title]').val(),
                price = parseInt(cont.find('input[name=price]').val());

        if (!$.isNumeric(price)) {
            price = 0;
        }

        if (title.length > 0) {
            var group = $(this).closest('.option-group'),
                    group_id = group.data('id'),
                    sign = '+',
                    price2 = price;

            if (price < 0) {
                sign = '-';
                price2 *= -1;
            }

            var option_id = 0;
            group.find('.option-item').each(function () {
                var id = $(this).data('id');

                if (option_id < id) {
                    option_id = id;
                }
            });
            option_id++;

            var content = '<div class="option-item" data-id="' + option_id + '">' +
                    '<span class="fa fa-remove option-item-remove"></span> ' +
                    $('<div/>').html(title).text() +
                    ' (' + sign + price2.toFixed(2) + ' руб.) - (' + sign + ') ' + price2.toFixed(2) + ' руб' +
                    '<input value="' + $('<div/>').html(title).text() + '" type="hidden" name="Service[groups][' + group_id + '][options][' + option_id + '][name]">' +
                    '<input value="' + price + '" type="hidden" name="Service[groups][' + group_id + '][options][' + option_id + '][price]">' +
                    '</div>';

            group.find('.option-items').append(content);

            cont.find('.option-item-close').trigger('click');
        }

        $(this).closest('.option-add').html('<a href="#" class="option-item-add">Добавить новое условие...</a>');
    });
    $('.service-option').on('click', '.option-item-close', function () {
        $(this).closest('.option-add').html('<a href="#" class="option-item-add">Добавить новое условие...</a>');
    });
    $('.service-option').on('click', '.option-item-remove', function () {
        $(this).closest('.option-item').remove();
    });

    $('.service-image-items').on('click', '.service-image a', function () {
        var cont = $(this).closest('div.service-image');

        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            dataType: 'json',
            error: function () {
                $.notify('При отправке данных произошла ошибка!', 'error');
            },
            success: function (data, textStatus) {
                if (data.status == 'success') {
                    cont.remove();
                } else
                {
                    $.notify(data.message, 'error');
                }
            }
        });

        return false;
    });
    $('.service-image-save').click(function () {
        var files = document.getElementById("serviceimage-name").files;
        if (files.length) {
            var data = new FormData();
            data.append('ServiceImage[name]', files[0]);
            data.append('ServiceImage[name][file]', $('#serviceimage-name_image').val());
            data.append('ServiceImage[name][0]', $('#serviceimage-name_crop0').val());

            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                error: function () {
                    $.notify('При отправке данных произошла ошибка!', 'error');
                },
                success: function (data, textStatus) {
                    if (data.status == 'success') {
                        data = data.data;
                        $('.service-image-items').append(data.content);
                        $('.service-image-cancel').trigger('click');
                    } else
                    {
                        $.notify(data.message, 'error');
                    }
                }
            });
        }

        return false;
    });
    $('.service-image-cancel').click(function(){
        $('#serviceimage-name').val('');
        $('.crop-image-upload-container').hide();
        $('.service-image-save, .service-image-cancel').hide();

        return false;
    });
    $('#serviceimage-name').change(function(){
        $('.service-image-save, .service-image-cancel').show();
    });

    isChanged = 0;
    $('.smart-form select').on('change', changed);
    $('.smart-form input').on('keydown', changed);
    $('.smart-form input').on('keydown', changed);

    $('#service-form').on('ajaxComplete', (event, response) => {
        if (response.responseJSON.success == true) { // success
            $('#service-id').val(response.responseJSON.id);
            $('#modal-options .modal-body').html(response.responseJSON.options);
        }
    });

    $('#change-password-modal').on('show.bs.modal', user.showRegForm);

    $('#show-on-map').click(function () {
        var link = $(this),
                form = $(this).closest('form'),
                id = $(this).data('id'),
                address = form.find('#' + id + '-name').val();

        if (address.length) {
            $.ajax({
                data: {
                    address: address
                },
                type: 'GET',
                url: $(this).attr('href'),
                dataType: 'json',
                success: function (data, textStatus) {
                    if (data.status == 'success') {
                        data = data.data;
                        form.find('#' + id + '-latitude').val(data.latitude);
                        form.find('#' + id + '-longitude').val(data.longitude);
                        gMap.setCenter(new google.maps.LatLng(data.latitude, data.longitude));
                        gMap.setZoom(id == 'region' ? 6 : 10);
                    } else {
                        if (id == 'locality') {
                            gMap.setCenter(new google.maps.LatLng(link.data('lat'), link.data('long')));
                            gMap.setZoom(6);
                        }
                    }
                }
            });
        }

        return false;
    });
});

function addOption() {
    if ($('[name="Service[name]"]').val().length > 0) {
        $('#modal-options').modal('show');
    }
}

function saveServiceType(e) {
    prevServiceType = $(e).val();
}

function confirmChars(e) {
    if (!prevServiceType) {
        getChars(e);
        // service.getCloneHtml();
        return false;
    }

    if (!$(e).val()) {
        $('#chars').html('');
        // service.getCloneHtml();
        return false;
    }

    $.SmartMessageBox({
        title : "Удаление записи",
        content : "При смене Категории предложения введенные данные будут удалены, подтверждаете?",
        buttons : '[Нет][Да]'
    }, function(ButtonPressed) {
        if (ButtonPressed === "Да") {
            getChars(e);
            // service.getCloneHtml();
        }
        else {
            $(e).val(prevServiceType);
        }
    });
}

function getChars(e) {
    $.ajax({
        url: '/admin/service/chars',
        method: 'GET',
        data: {
            id: $(e).val(),
            name: $(e).data('name'),
        }
    }).done(response => {
        $('#chars').html(response);
    });
}

function setLocalityCords(map, event) {
    let cords = gm.setCords(map, event),
        latitude = cords.lat(),
        longitude = cords.lng();

    if (!latitude || !longitude) {
        return false;
    }

    let latLng = new google.maps.LatLng(latitude, longitude);

    gMarker.setPosition(latLng);
    gMap.setCenter(latLng);

    $('[name="Locality[latitude]"]').val(latitude);
    $('[name="Locality[longitude]"]').val(longitude);
}

function setRegionCords(map, event) {
    let cords = gm.setCords(map, event),
        latitude = cords.lat(),
        longitude = cords.lng();

    if (!latitude || !longitude) {
        return false;
    }

    let latLng = new google.maps.LatLng(latitude, longitude);

    if(typeof gMarker !== 'undefined'){
        gMarker.setPosition(latLng);
    }
    gMap.setCenter(latLng);

    $('[name="Region[latitude]"]').val(latitude);
    $('[name="Region[longitude]"]').val(longitude);
}

/*Order script start*/
$(function(){
    $(document).on('change','.form-group.order-person-status .radiobox',function(e){

        console.log(Number(e.target.value));
        setTimeout(()=>{
          $('#person-class-block').hide(1000);
      showOrderBlock(Number(e.target.value));
        },500);
    });

  function showOrderBlock(type){
    switch (type){
      case 1:
        $(".individual-person").removeClass('hidden');
        $('.individual-person input:hidden[name="Order[type]"]').val(type);
        break;
      case 2:
        $(".law-person").removeClass('hidden');
        $('.law-person input:hidden[name="Order[type]"]').val(type);
        break;
    }
  }

  $(document).on('change','.order-region-f',(e)=>locality(e));
  $(document).on('change','.order-region-u',(e)=>locality(e));
  function locality(e){
    if(e.target.value){
      getLocality(e.target.value);
    }
  }

  function getLocality(id){
    $.ajax({
      url: '/admin/locality/'+ id +'/locality-by-region',
      method: 'GET',
    }).done(response => {
        buildLocalitySelect(response);
    });
  }

  function buildLocalitySelect(response) {
    let order_locality = $('.order-locality');
    const firstOption = order_locality.find('option')[0];
    order_locality.find('option').remove();
    order_locality.append(firstOption);
    $.each(response,function(key,value){
      order_locality.append("<option value='"+value.id+"'>"+ value.name +"</option>");
    });
  }
});

/*Order script end*/

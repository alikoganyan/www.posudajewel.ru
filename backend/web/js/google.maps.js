class GoogleMaps {
    constructor() {
        this.markers = [];
    }

    setCords(map, event) {
        let marker = new google.maps.Marker({
            position: event.latLng,
            map: map
        });

        this.clear();
        this.markers.push(marker);

        map.panTo(event.latLng);

        return event.latLng;
    }

    clear() {
        this.markers.forEach(marker => {
            marker.setMap(null);
        });
    }
}

let gm = new GoogleMaps();

<?php
namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    public $css = [
        'css/bootstrap.min.css',
        'css/smartadmin/smartadmin-production-plugins.min.css',
        'css/smartadmin/smartadmin-production.min.css',
        'css/smartadmin/smartadmin-skins.min.css',
        'css/smartadmin/smartadmin-rtl.min.css',

        'css/font-awesome.min.css',
        'css/site.css',
        'css/font.css',
        'css/custom.css',

        'css/demo.min.css',
    ];

    public $js = [
        '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',
        'js/app.config.js',
        'js/jquery.ui.touch-punch.min.js',
        'js/bootstrap.min.js',
        'js/SmartNotification.min.js',
        'js/jarvis.widget.min.js',
        'js/jquery.easy-pie-chart.min.js',
        'js/jquery.sparkline.min.js',
        'js/bootstrap-slider.min.js',
        'js/demo.min.js',
        'js/app.min.js',
        'js/jquery.mb.browser.min.js',
        'js/jquery.dataTables.min.js',
        'js/select2.min.js',
        'js/bootstrap-timepicker.min.js',
        'js/dropzone.min.js',

        'js/voicecommand.min.js',
        'js/smart-chat-ui/smart.chat.manager.min.js',
        'js/smart-chat-ui/smart.chat.ui.min.js',

        'js/google.maps.js',

        // custom
        'js/char.js',
        'js/constants.js',
        'js/service-option.js',
        'js/custom.js',

        //models
        'js/models/company.js',
        'js/models/user.js',
        'js/models/options.js',
        'js/models/profile.js',
        'js/models/service.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\widgets\notify\NotifyAsset',
    ];
}

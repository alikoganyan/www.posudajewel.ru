<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\QuickSearch;

class QuickSearchSearch extends QuickSearch {
    public function rules() {
        return [
            [['id'], 'integer'],
            [['name', 'image', 'serviceTypeName'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = QuickSearch::find()->joinWith(['serviceType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'service_type.name', $this->serviceTypeName])
              ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}

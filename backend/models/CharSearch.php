<?php
namespace backend\models;

use common\models\ServiceTypeChar;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Char;

class CharSearch extends Char {
    public $service_type_id;

    public function rules() {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Char::find();

        if ($this->service_type_id) {
            $query->innerJoin(ServiceTypeChar::tableName(), "service_type_id = {$this->service_type_id} and char_id = char.id");
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

<?php

namespace backend\models;

use Yii;
use yii\helpers\FileHelper;
use common\models\Settings;

/**
 * Настройки
 */
class SettingForm extends \yii\base\Model {

    public $info;
    public $url_info;
    public $about;
    public $banner;
    public $logo;
    public $file_banner;
    public $file_logo;
    public $index_title;
    public $index_description;
    public $index_keywords;
    public $soc1_name;
    public $soc1_link;
    public $soc2_name;
    public $soc2_link;
    public $soc3_name;
    public $soc3_link;
    public $soc4_name;
    public $soc4_link;

    /**
     * Список настроек
     * @var array 
     */
    protected $setting = [
        'info', 'url_info', 'about', 'banner', 'logo', 'index_title', 'index_description', 'index_keywords',
        'soc1_name', 'soc1_link', 'soc2_name', 'soc2_link',
        'soc3_name', 'soc3_link', 'soc4_name', 'soc4_link',
    ];

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['info', 'about'], 'string', 'max' => 30000],
            [['url_info', 'index_title', 'index_description', 'index_keywords', 'soc1_name', 'soc1_link', 'soc2_name', 'soc2_link', 'soc3_name', 'soc3_link', 'soc4_name', 'soc4_link'], 'string', 'max' => 255],
            [['file_banner', 'file_logo'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'checkExtensionByMimeType' => false, 'skipOnEmpty' => true, 'maxSize' => 73400320],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'info' => 'Информационная полоса',
            'url_info' => 'Url ссылки информационной полосы',
            'about' => 'О компании',
            'banner' => 'Баннер на главной странице',
            'logo' => 'Логотип',
            'file_banner' => 'Баннер на главной странице',
            'file_logo' => 'Логотип',
            'index_title' => 'Заголовок',
            'index_description' => 'Описание',
            'index_keywords' => 'Ключи',
            'soc1_name' => 'Название ссылки',
            'soc1_link' => 'Адрес ссылки',
            'soc2_name' => 'Название ссылки',
            'soc2_link' => 'Адрес ссылки',
            'soc3_name' => 'Название ссылки',
            'soc3_link' => 'Адрес ссылки',
            'soc4_name' => 'Название ссылки',
            'soc4_link' => 'Адрес ссылки',
        ];
    }

    /**
     * 
     */
    public function init() {
        parent::init();

        foreach ($this->setting as $key) {
            $this->$key = Settings::getSetting($key);
        }
    }

    /**
     * Сохранение настроек
     * @return boolean
     */
    public function save() {
        if ($this->validate()) {
            $path = Yii::getAlias('@frontend') . '/web/uploads/';

            if (!file_exists($path)) {
                FileHelper::createDirectory($path);
            }

            // Сохраняем баннер
            if ($this->file_banner instanceof \yii\web\UploadedFile) {
                $banner = Yii::$app->security->generateRandomString();

                if ($this->file_banner->saveAs($path . $banner)) {
                    $this->banner = $banner;
                } else {
                    $this->addError('file_banner', 'Не удалось сохранить файл.');
                }
            }

            // Сохраняем логотип
            if ($this->file_logo instanceof \yii\web\UploadedFile) {
                $logo = Yii::$app->security->generateRandomString();

                if ($this->file_logo->saveAs($path . $logo)) {
                    $this->logo = $logo;
                } else {
                    $this->addError('file_logo', 'Не удалось сохранить файл.');
                }
            }

            // Сохраняем настройки
            foreach ($this->setting as $key) {
                Settings::setSetting($key, $this->$key);
            }

            return true;
        }

        return false;
    }

}

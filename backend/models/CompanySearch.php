<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Company as CompanyModel;

class CompanySearch extends CompanyModel {
    public function rules() {
        return [
            [['id', 'user_id', 'locality_id', 'is_blocked'], 'integer'],
            [['name', 'regionName', 'localityName', 'userName', 'web', 'first_phone', 'main_phone', 'longitude', 'latitude', 'second_phone', 'email', 'address'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = CompanyModel::find()
            ->joinWith(['locality'])
            ->leftJoin(['r' => 'region'], 'r.id = locality.region_id')
            ->joinWith(['user']);

        /** @var User $user */
        $user = \Yii::$app->user->identity;
        if (!$user->isAdmin()) {
            $query->andWhere(['company.user_id' => $user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'user_id'     => $this->user_id,
            'locality_id' => $this->locality_id,
            'longitude'   => $this->longitude,
            'latitude'    => $this->latitude,
            'company.is_blocked'  => $this->is_blocked,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'locality.name', $this->localityName])
              ->andFilterWhere(['like', 'r.name', $this->regionName])
              ->andFilterWhere(['like', 'web', $this->web])
              ->andFilterWhere(['like', 'first_phone', $this->first_phone])
              ->andFilterWhere(['like', 'main_phone', $this->main_phone])
              ->andFilterWhere(['like', 'second_phone', $this->second_phone])
              ->andFilterWhere(['like', 'email', $this->email])
              ->andFilterWhere(['like', 'address', $this->address]);

        if (!$this->user && $this->userName) {
            $query->andFilterWhere(['like', 'user.first_name', $this->userName])
                  ->orFilterWhere(['like', 'user.last_name', $this->userName]);
        }

        return $dataProvider;
    }
}

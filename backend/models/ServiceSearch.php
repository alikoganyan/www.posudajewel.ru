<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Service;

class ServiceSearch extends Service {

    public $is_image;

    public function rules() {
        return [
            [['id', 'service_type_id', 'company_id', 'is_popular', 'price', 'is_blocked', 'unit_id', 'in_stock', 'is_image'], 'integer'],
            [['name', 'description', 'serviceTypeName', 'companyName', 'userName', 'image', 'meta_description', 'meta_keywords', 'code'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Service::find()->joinWith(['serviceType', 'company.user', 'images']);

        /** @var User $user */
        $user = \Yii::$app->user->identity;
        if (!$user->isAdmin()) {
            $query->andWhere(['company.user_id' => $user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'company_id' => $this->company_id,
            'service_type_id' => $this->service_type_id,
            'is_popular' => $this->is_popular,
            'service.is_blocked' => $this->is_blocked,
            'unit_id' => $this->unit_id,
            'in_stock' => $this->in_stock,
        ]);

        $query->andFilterWhere(['like', 'service.name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'service_type.name', $this->serviceTypeName])
                ->andFilterWhere(['like', 'company.name', $this->companyName])
                ->andFilterWhere(['like', 'image', $this->image])
                ->andFilterWhere(['like', 'meta_description', $this->meta_description])
                ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
                ->andFilterWhere(['like', 'code', $this->code]);

        // Фильтр по фото
        if ($this->is_image !== '') {
            if ($this->is_image === '0') {
                $query->andFilterWhere(['AND', 'service_image.id IS NULL']);
            }
            if ($this->is_image === '1') {
                $query->andFilterWhere(['AND', 'service_image.id IS NOT NULL']);
            }
            $query->groupBy('service.id');
        }

        if ((!$this->company) && $this->userName) {
            $query->andFilterWhere(['like', 'user.first_name', $this->userName])
                    ->orFilterWhere(['like', 'user.last_name', $this->userName]);
        }

        return $dataProvider;
    }

}

<?php
namespace backend\models;

use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\ServiceType;

class ServiceTypeSearch extends ServiceType {
    public function rules() {
        return [
            [['id'], 'integer'],
            [['name', 'is_mass'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = ServiceType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'is_mass'    => $this->is_mass,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Region;

class RegionSearch extends Region {
    public function rules() {
        return [
            [['id'], 'integer'],
            [['name', 'code', 'longitude', 'latitude'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Region::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'code', $this->code])
              ->andFilterWhere(['like', 'longitude', $this->longitude])
              ->andFilterWhere(['like', 'latitude', $this->latitude]);

        return $dataProvider;
    }
}

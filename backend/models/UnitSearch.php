<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Unit;

class UnitSearch extends Unit {
    public function rules() {
        return [
            [['id', 'code'], 'integer'],
            [['name', 'designation'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Unit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'code'       => $this->code,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'designation', $this->designation]);

        return $dataProvider;
    }
}

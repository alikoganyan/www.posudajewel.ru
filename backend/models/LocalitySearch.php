<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Locality;

class LocalitySearch extends Locality {
    public function rules() {
        return [
            [['id', 'region_id', 'gismeteo_id'], 'integer'],
            [['name', 'longitude', 'latitude'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Locality::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'region_id'   => $this->region_id,
            'gismeteo_id' => $this->gismeteo_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'longitude', $this->longitude])
              ->andFilterWhere(['like', 'latitude', $this->latitude]);

        return $dataProvider;
    }
}

<?php
namespace backend\models;

use common\models\CharValue;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Value;

class ValueSearch extends Value {
    public $charTypeId;

    public function rules() {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Value::find()->innerJoin(CharValue::tableName(), "char_id = {$this->charTypeId} and value_id = id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

<?php
namespace backend\models;

use backend\components\ActiveDataProvider;
use common\models\User;

class UserSearch extends User {
    public function rules() {
        return [
            [['id'], 'integer'],
            [['fullName', 'is_blocked', 'roleName', 'first_name', 'last_name', 'email', 'birthday', 'phone', 'auth_key', 'password_hash', 'password_reset_token', 'google_id',  'vk_id', 'fb', 'yandex'], 'safe'],
        ];
    }

    public function search($params) {
        $query = User::find()->joinWith(['role']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'birthday'   => $this->birthday,
            'is_blocked' => $this->is_blocked,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
              ->andFilterWhere(['like', 'last_name', $this->last_name])
              ->andFilterWhere(['like', 'email', $this->email])
              ->andFilterWhere(['like', 'phone', $this->phone])
              ->andFilterWhere(['like', 'auth_key', $this->auth_key])
              ->andFilterWhere(['like', 'password_hash', $this->password_hash])
              ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
              ->andFilterWhere(['like', 'google_id', $this->google_id])
              ->andFilterWhere(['like', 'vk_id', $this->vk_id])
              ->andFilterWhere(['like', 'fb', $this->fb])
              ->andFilterWhere(['like', 'role.name', $this->roleName])
              ->andFilterWhere(['like', 'yandex', $this->yandex]);

        if ($this->fullName) {
            $query->andFilterWhere(['like', 'first_name', $this->fullName])
                  ->orFilterWhere(['like', 'last_name', $this->fullName]);
        }

        return $dataProvider;
    }
}

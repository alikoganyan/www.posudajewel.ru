<?php
namespace backend\models;

use common\models\ServiceOption;
use yii\base\Model;
use backend\components\ActiveDataProvider;

class ServiceOptionSearch extends ServiceOption {
    public function rules() {
        return [
            [['id', 'price', 'service_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = ServiceOption::find()->andWhere(['service_id' => $this->service_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\Role;

class RoleSearch extends Role
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'permissions'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Role::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'permissions', $this->permissions]);

        return $dataProvider;
    }
}

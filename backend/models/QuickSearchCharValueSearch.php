<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use backend\components\ActiveDataProvider;
use common\models\QuickSearchCharValue;

class QuickSearchCharValueSearch extends QuickSearchCharValue {
    public function rules() {
        return [
            [['quick_search_id', 'char_id', 'value_id'], 'integer'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = QuickSearchCharValue::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'quick_search_id' => $this->quick_search_id,
            'char_id'         => $this->char_id,
            'value_id'        => $this->value_id,
        ]);

        return $dataProvider;
    }
}

<?php

namespace backend\models;

use common\models\Order;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'user_id', 'delivery_id', 'status_id'], 'integer'],
            [['cached_cart_id', 'fio', 'phone', 'email', 'address', 'comment', 'created_at', 'updated_at'], 'safe'],
            [['delivery_cost', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchUserOrders($params) {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id'      => $this->id,
            'user_id' => Yii::$app->user->id,
        ]);
        return $dataProvider;
    }
    public function search($params) {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'user_id'       => $this->user_id,
            'delivery_cost' => $this->delivery_cost,
            'total'         => $this->total,
            'delivery_id'   => $this->delivery_id,
            'status_id'     => $this->status_id,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'cached_cart_id', $this->cached_cart_id])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}

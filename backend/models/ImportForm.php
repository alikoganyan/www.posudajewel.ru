<?php

namespace backend\models;

use Yii;
use common\models\Company;
use common\models\Unit;
use common\models\Char;
use common\models\CharValue;
use common\models\Value;
use common\models\Service;
use common\models\ServiceType;
use common\models\ServiceTypeChar;
use common\models\ServiceCharValue;

/**
 * Экспорт предложений
 */
class ImportForm extends \yii\base\Model {

    public $file;
    public $start;
    public $end;
    protected $field = [
        'company' => false,
        'category' => false,
        'service_name' => false,
        'service_code' => null,
        'service_id' => false,
        'service_desc' => null,
        'service_price' => false,
        'service_wholesale_price' => false,
        'service_unit' => false,
        'property' => [],
    ];
    protected $index = 0;
    protected $company = [];
    protected $unit = [];
    protected $category = [];
    protected $property = [];
    protected $value = [];

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['file'], 'file', 'extensions' => ['csv'], 'checkExtensionByMimeType' => false, 'skipOnEmpty' => false, 'maxSize' => 73400320],
            [['start', 'end'], 'integer', 'min' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'file' => 'Файл',
            'start' => 'Экспортировать данные начиная со строки',
            'end' => 'Экспортировать данные до строки',
        ];
    }

    /**
     * Экспорт данных
     * @return boolean
     */
    public function import() {
        if ($this->validate() && $this->file instanceof \yii\web\UploadedFile) {
            $file = Yii::getAlias('@runtime/') . 'import.csv';

            if (file_exists($file)) {
                unlink($file);
            }

            if ($this->file->saveAs($file)) {
                // Открываем прайс
                $fp = fopen($file, "r");

                if ($fp) {
                    // Получим индексы колонок
                    if ($this->dataIndex($fp)) {

                        $this->company = Company::find()->select(['id', 'name'])->indexBy('name')->column();
                        $this->unit = Unit::find()->select(['id', 'designation'])->indexBy('designation')->column();
                        $this->category = ServiceType::find()->select(['id', 'name'])->indexBy('name')->column();

                        $property = Char::find()->
                                select(['t.service_type_id', 'c.id as char_id', 'c.name'])->
                                from(['c' => Char::tableName()])->
                                leftJoin(['t' => ServiceTypeChar::tableName()], 't.char_id=c.id')->
                                asArray()->
                                all();
                        $value = Value::find()->
                                select(['c.char_id', 'v.name', 'v.id as value_id'])->
                                from(['v' => Value::tableName()])->
                                leftJoin(['c' => CharValue::tableName()], 'c.value_id=v.id')->
                                asArray()->
                                all();

                        $this->property = [];
                        foreach ($property as $row) {
                            if (!isset($this->property[$row['name']])) {
                                $this->property[$row['name']] = [
                                    'id' => $row['char_id'],
                                    'service_type_id' => [],
                                ];
                            }

                            if (!empty($row['service_type_id'])) {
                                $this->property[$row['name']]['service_type_id'][] = $row['service_type_id'];
                            }
                        }

                        $this->value = [];
                        foreach ($value as $row) {
                            if (!isset($this->value[$row['name']])) {
                                $this->value[$row['name']] = [
                                    'id' => $row['value_id'],
                                    'char_id' => [],
                                ];
                            }

                            if (!empty($row['char_id'])) {
                                $this->value[$row['name']]['char_id'][] = $row['char_id'];
                            }
                        }

                        $transaction = \Yii::$app->db->beginTransaction();
                        try {
                            while (($data = $this->readFile($fp)) !== false) {
                                $this->insertData($data);
                            }

                            $transaction->commit();
                        } catch (\Exception $e) {
                            $this->addError('file', $e->getMessage());

                            $transaction->rollback();
                        }
                    }

                    fclose($fp);
                }
            }
        }

        return !$this->hasErrors();
    }

    protected function insertData($data) {
        if ((empty($this->start) || $this->start <= $this->index) && (empty($this->end) || $this->end >= $this->index)) {
            foreach ($this->field as $val) {
                if (is_int($val) && !isset($data[$val])) {
                    return false;
                }
            }

            $service = Service::findOne(['id_1c' => $data[$this->field['service_id']]]);

            if (!$service instanceof Service) {
                $service = new Service();
                $service->code = '';
                $service->description = '';
            }

            $service->saveOption = false;

            if (!is_null($this->field['service_code'])) {
                $service->code = $data[$this->field['service_code']];
            }
            if (!is_null($this->field['service_desc'])) {
                $service->description = $data[$this->field['service_desc']];
            }

            $service->company_id = $this->getCompany($data[$this->field['company']]);
            $service->service_type_id = $this->getCategory($data[$this->field['category']]);
            $service->name = $data[$this->field['service_name']];
            $service->price = $data[$this->field['service_price']];
            $service->wholesale = $data[$this->field['service_wholesale_price']];
            $service->unit_id = $this->getUnit($data[$this->field['service_unit']]);
            $service->id_1c = $data[$this->field['service_id']];

            if (!$service->save()) {
                throw new \Exception("Не удалось сохранить данные (строка {$this->index}).");
            }

            foreach ($this->field['property'] as $index => $name) {
                if (isset($data[$index])) {
                    $property = $this->getProperty($service->service_type_id, $name);

                    if (empty($data[$index])) {
                        // Удалим свойство
                        ServiceCharValue::deleteAll(['service_id' => $service->id, 'char_id' => $property]);
                    } else {
                        // Добавим/обновим
                        $value = $this->getValue($property, $data[$index]);


                        $charValue = ServiceCharValue::findOne(['service_id' => $service->id, 'char_id' => $property]);

                        if (!$charValue instanceof ServiceCharValue) {
                            $charValue = new ServiceCharValue();
                            $charValue->service_id = $service->id;
                            $charValue->char_id = $property;
                        }

                        $charValue->value_id = $value;

                        if (!$charValue->save()) {
                            throw new \Exception("Не удалось привязать свойство к предложению.");
                        }
                    }
                }
            }
        }
    }

    /**
     * Id свойства
     * @param int $category
     * @param string $name
     * @return int
     * @throws \Exception
     */
    protected function getProperty($category, $name) {

        if (!isset($this->property[$name])) {
            $char = new Char();
            $char->name = $name;
            $char->is_multiple = 0;

            if (!$char->save()) {
                throw new \Exception("Не удалось создать свойство $name.");
            }

            $this->property[$name] = [
                'id' => $char->id,
                'service_type_id' => [],
            ];
        }

        if (!in_array($category, $this->property[$name]['service_type_id'])) {
            $serviceChar = new ServiceTypeChar();
            $serviceChar->service_type_id = $category;
            $serviceChar->char_id = $this->property[$name]['id'];

            if (!$serviceChar->save()) {
                throw new \Exception("Не удалось привязать свойство $name к категории.");
            }

            $this->property[$name]['service_type_id'][] = $category;
        }

        return $this->property[$name]['id'];
    }

    /**
     * Id значения свойства
     * @param int $property
     * @param string $name
     * @return int
     * @throws \Exception
     */
    protected function getValue($property, $name) {
        if (!isset($this->value[$name])) {
            $value = new Value();
            $value->name = $name;

            if (!$value->save()) {
                throw new \Exception("Не удалось создать значение свойства $name.");
            }

            $this->value[$name] = [
                'id' => $value->id,
                'char_id' => [],
            ];
        }

        if (!in_array($property, $this->value[$name]['char_id'])) {
            $charValue = new CharValue();
            $charValue->char_id = $property;
            $charValue->value_id = $this->value[$name]['id'];

            if (!$charValue->save()) {
                throw new \Exception("Не удалось привязать значение $name к свойству.");
            }

            $this->value[$name]['char_id'][] = $property;
        }

        return $this->value[$name]['id'];
    }

    /**
     * Id компании
     * @param string $name
     * @return int
     * @throws \Exception
     */
    protected function getCompany($name) {
        if (isset($this->company[$name])) {
            return $this->company[$name];
        } else {
            throw new \Exception("Компания $name не найдена.");
        }
    }

    /**
     * Id категории
     * @param string $name
     * @return int
     * @throws \Exception
     */
    protected function getCategory($name) {

        if (!isset($this->category[$name])) {
            $category = new ServiceType();
            $category->name = $name;
            $category->is_mass = 0;

            if (!$category->save()) {
                throw new \Exception("Не удалось создать категорию $name.");
            }

            $this->category[$name] = $category->id;
        }

        return $this->category[$name];
    }

    /**
     * Id ед.измерения
     * @param string $name
     * @return int
     * @throws \Exception
     */
    protected function getUnit($name) {

        if (!isset($this->unit[$name])) {
            $unit = new Unit();
            $unit->name = $name;
            $unit->code = 0;
            $unit->designation = $name;

            if (!$unit->save()) {
                throw new \Exception("Не удалось создать ед.измерения $name.");
            }

            $this->unit[$name] = $unit->id;
        }

        return $this->unit[$name];
    }

    /**
     * Получение индексов колонок
     * @return boolean|array
     */
    protected function dataIndex($fp) {
        // Строка с названиями
        $columnTitle = $this->readFile($fp);

        // Названия полей
        $fieldName = $this->readFile($fp);

        // Статус заугрки
        $fieldStatus = $this->readFile($fp);

        if (is_array($fieldName) && is_array($fieldStatus)) {
            // Определим индексы
            foreach ($fieldName as $index => $name) {
                if (array_key_exists($name, $this->field) && isset($fieldStatus[$index]) && $fieldStatus[$index] == 'да') {
                    if (is_array($this->field[$name])) {
                        if (isset($columnTitle[$index])) {
                            $this->field[$name][$index] = $columnTitle[$index];
                        }
                    } else {
                        $this->field[$name] = $index;
                    }
                }
            }

            // Проверим обязательные поля
            foreach ($this->field as $key => $val) {
                if ($val === false) {
                    $this->addError('file', 'Отсутствует обязательный параметр ' . $key);
                }
            }
        }

        return !$this->hasErrors();
    }

    /**
     * Чтение файла
     * @param type $fp
     * @return mixed
     */
    public function readFile($fp) {
        $data = fgetcsv($fp, 100000, ';');

        if (is_array($data)) {
            foreach ($data as &$val) {
                $val = trim($this->conv($val));
            }
        }

        $this->index++;

        return $data;
    }

    /**
     * Перекодировка
     * @param string $str
     * @return string
     */
    public function conv($str) {
        return iconv('windows-1251', 'utf-8//IGNORE', $str);
    }

}

<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\ServiceType
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CharSearch
 * @var $allChars     []
 */

use yii\helpers\Html;
use backend\widgets\activeForm\ActiveForm;
use backend\widgets\formFooter\FormFooter;
use mihaildev\elfinder\ElFinder;
use mihaildev\ckeditor\CKEditor;


$this->title = 'Информация';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['service-type/index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая запись' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Информация</h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <? $form = ActiveForm::begin([
                'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form',
                ],
            ])
            ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name')->textInput(['class' => 'url-name']) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('url')): ?>
                    <section>
                        <?= $form->field($model, 'url')->textInput(['class' => 'url-translit']) ?>
                        <?= Html::a('Получить адрес', '#', ['class' => 'create-url']); ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('sitemap')): ?>
                    <section class="row form-checkbox">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'sitemap', ['permission' => $permission])->checkbox(['label' => 'Блокировать в sitemap.xml']) ?>
                        </div>
                    </section>
                <?php endif ?>

                <section>
                    <?= $form->field($model, 'meta_title')->label($model->getAttributeLabel('meta_title')) ?>
                </section>

                <section>
                    <?= $form->field($model, 'meta_description')->label($model->getAttributeLabel('meta_description')) ?>
                </section>

                <section>
                    <?= $form->field($model, 'meta_keywords')->label($model->getAttributeLabel('meta_keywords')) ?>
                </section>
                

                <?php if ($permission = $model->getFieldPermission('is_footer')): ?>
                    <section class="row">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'is_footer')->checkbox(['label' => 'Вывод в футере']) ?>
                        </div>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('content')): ?>
                    <section>
                        <?=
                        $form->field($model, 'content')->widget(CKEditor::className(), [
                            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                'editorOptions' => ElFinder::ckeditorOptions(['elfinder'])
                            ]),
                        ]);
                        ?>
                    </section>
                <?php endif ?>

            </fieldset>

            <footer>
                <?php if ($permission = $model->getFieldPermission('is_blocked')): ?>
                    <div>
                        <?= $form->field($model, 'is_blocked', ['permission' => $permission])->checkbox(['label' => $model->getAttributeLabel('is_blocked')]) ?>
                    </div>
                <?php endif ?>

                <?= FormFooter::widget([
                    'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                ]) ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

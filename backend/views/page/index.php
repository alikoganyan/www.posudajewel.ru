<?php

/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceTypeSearch
 */
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;

$this->title = 'Информация';
$this->params['breadcrumbs'][] = $this->title;
?>

<?=

GridView::widget([
    'caption' => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'hideColumns' => ['url', 'is_footer', 'is_blocked'],
    'columns' => [
        'name',
        'url',
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_footer',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
        return $model->is_footer ? 'Да' : 'Нет';
    },
        ],
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_blocked',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
        return $model->is_blocked ? 'Да' : 'Нет';
    },
        ],
    ],
])
?>

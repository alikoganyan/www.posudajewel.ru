<?php
/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\RegionSearch
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use \backend\widgets\gridView\GridView;

$this->title = 'ОКЕИ';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'hideColumns'  => ['code', 'designation'],
    'columns'      => [
        'name',
        'code',
        'designation',
    ],
]) ?>

<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Unit
 */

use \backend\widgets\activeForm\ActiveForm;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'ОКЕИ';
$this->params['breadcrumbs'][] = ['label' => 'ОКЕИ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая ед. измерения' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">ОКЕИ</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'unit-form',
                'options' => [
                    'class' => 'smart-form'
                ]
            ]) ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name', ['permission' => $permission])
                            ->label('Наименование ед. измерения') ?>
                    </section>
                <?php endif ?>

                <div class="row">
                    <?php if ($permission = $model->getFieldPermission('code')): ?>
                        <section class="col col-6">
                            <?= $form->field($model, 'code', ['permission' => $permission])->label($model->getAttributeLabel('code')) ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('designation')): ?>
                        <section class="col col-6">
                            <?= $form->field($model, 'designation', ['permission' => $permission])->label($model->getAttributeLabel('designation')) ?>
                        </section>
                    <?php endif ?>
                </div>
            </fieldset>

            <footer class="col col-md-12">
                <?= FormFooter::widget() ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<?php

use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\GridView;

$this->title = 'Каталог предложений';
$this->params['breadcrumbs'][] = $this->title;
?>

<?=

GridView::widget([
    'caption' => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => DataColumn::className(),
            'attribute' => 'name',
            'label' => 'Раздел',
        ],
    ],
])
?>

<?php

/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\ServiceType
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CharSearch
 * @var $allChars     []
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use karpoff\icrop\CropImageUpload;
use backend\widgets\activeForm\ActiveForm;
use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\html\HeaderButton;
use backend\widgets\formFooter\FormFooter;
use common\models\CatalogLink;

$this->title = 'Каталог предложений';
$this->params['breadcrumbs'][] = ['label' => 'Каталог предложений', 'url' => ['catalog/index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый раздел' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Каталог предложений</h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                        'id' => 'ctalog-form',
                        'enableAjaxValidation' => false,
                        'options' => [
                            'class' => 'smart-form',
                            'enctype' => 'multipart/form-data',
                        ]
                    ])
            ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name')->label('Наименование раздела') ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('sort')): ?>
                    <section>
                        <?= $form->field($model, 'sort')->label('Сортировка') ?>
                    </section>
                <?php endif ?>

                <?php if (!is_null($model->parent_id) && $permission = $model->getFieldPermission('is_hidden_subsections')): ?>
                    <section>
                        <?= $form->field($model, 'is_hidden_subsections')->checkbox(['label' => 'Не выводить подразделы в меню каталога']) ?>
                    </section>
                <?php endif ?>

                <?php if (is_null($model->parent_id)): ?>
                    <section>
                        <label class="label">Акционное предложение</label>
                        <?=
                        Select2::widget([
                            'name' => "Catalog[service_id]",
                            'data' => $listService,
                            'value' => $model->service_id,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['/service/search']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term, p: "code"}; }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function(e) { $('.catalog-service-edit').attr('href', '/admin/service/'+$('select[name=\"Catalog[service_id]\"]').val()+'/update').show(); }",
                            ]
                        ])
                        ?>
                        <?= Html::a('Изменить', ['/service/update', 'id' => $model->service_id], ['class' => 'link-input catalog-service-edit', 'style' => empty($model->service_id) ? 'display:none' : '']); ?>
                    </section>

                    <section>
                        <label class="label">Изображение</label>
                        <?=
                                $form->field($model, 'image')->label(false)->
                                widget(CropImageUpload::className(), ['clientOptions' => ['boxWidth' => 350, 'boxHeight' => 300]])
                        ?>
                    </section>
                <?php endif; ?>

            </fieldset>


            <footer>
                <?=
                FormFooter::widget([
                    'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                ])
                ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord): ?>
        <?php if (is_null($model->parent_id)): ?>
            <?=
            GridView::widget([
                'caption' => 'Подразделы',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'addUrlParams' => ['id' => $model->id],
                'columns' => [
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'name',
                        'label' => 'Подраздел',
                    ],
                ],
            ])
            ?>
        <?php else: ?>
            <?=
            $this->render('link', [
                'quickSearch' => $quickSearch,
                'model' => new CatalogLink(),
                'catalog' => $model,
            ])
            ?>

            <?=
            GridView::widget([
                'caption' => 'Подразделы',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'isAddVisible' => false,
                'actions' => [
                    'delete' => '/delete-link',
                ],
                'actionParams' => [
                    'parent_id' => $model->id,
                ],
                'headerButtons' => [
                    new HeaderButton('Добавить', "$('#modal-chars').modal('show')"),
                ],
                'columns' => [
                    'name',
                ],
            ])
            ?>
        <?php endif; ?>
    <?php endif; ?>
</div>

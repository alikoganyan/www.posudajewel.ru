<?php

/**
 * @var $this        \yii\web\View
 * @var $allChars    []
 * @var $serviceType \common\models\ServiceType
 * @var $model       \common\models\ServiceTypeChar
 */
use yii\helpers\Url;
use yii\bootstrap\Html;
use backend\widgets\activeForm\ActiveForm;
?>

<div class="modal fade" id="modal-chars" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Добавить ссылку быстрого поиска</h3>
            </div>
            <div class="modal-body">
                <?
                $form = ActiveForm::begin([
                            'id' => 'search-form',
                            'action' => Url::to(['catalog/save-link', 'id' => $catalog->id]),
                            'enableAjaxValidation' => false,
                            'options' => [
                                'class' => 'smart-form'
                            ]
                        ])
                ?>
                <div class="items smart-form">
                    <section>
                        <?php if (!$quickSearch): ?>
                            <div class="alert alert-info">Нет доступных ссылок</div>
                        <?php else: ?>
                            <label class="select">
                                <?=
                                        $form->field($model, 'search_id')
                                        ->dropDownList($quickSearch)->label('Выберите ссылку')
                                ?>
                            </label>
                        <?php endif ?>
                    </section>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <div class="modal-footer">
                <?=
                implode('', [
                    Html::submitButton('Сохранить', [
                        'class' => 'btn btn-primary save-button',
                        'form' => 'search-form',
                    ]),
                    Html::button('Закрыть', [
                        'class' => 'btn btn-default',
                        'data-dismiss' => 'modal',
                    ]),
                ])
                ?>
            </div>
        </div>
    </div>
</div>

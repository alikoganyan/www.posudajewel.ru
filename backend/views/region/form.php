<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\Region
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\LocalitySearch
 */

use yii\helpers\Html;
use \backend\widgets\gridView\GridView;
use \backend\widgets\formFooter\FormFooter;
use \backend\widgets\activeForm\ActiveForm;
use \dosamigos\google\maps\LatLng;
use \common\components\Map;
use \common\components\Marker;
use \dosamigos\google\maps\Event;
use \backend\widgets\gridView\DataColumn;

$this->title = 'Регионы';

$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый регион' : $model->name;

$showMap = !$model->latitude || !$model->longitude;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <link rel="stylesheet" href="https://api-maps.yandex.ru/1.1/_YMaps.css?v=1.1.21-58">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Регион</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                'id' => 'region-form',
            ])
            ?>
            <fieldset>
                <div class="row">
                    <?php if ($permission = $model->getFieldPermission('name')): ?>
                        <section class="col col-md-12">
                            <?= $form->field($model, 'name', [ 'permission' => $permission])
                                ->label('Наименование региона') ?>
                        </section>

                        <?= Html::a($showMap ? 'Скрыть карту' : 'Показать карту', 'javascript:void(0)', [
                            'id' => 'btn-map',
                            'class' => 'region-map',
                            'onclick' => 'company.toggleMap();',
                        ]); ?>
                    <?php endif ?>
                </div>

                <div class="row">
                    <?php if ($permission = $model->getFieldPermission('code')): ?>
                        <section class="col col-md-12">
                            <?= $form->field($model, 'code', ['permission' => $permission])->label($model->getAttributeLabel('code')) ?>
                        </section>
                    <?php endif ?>
                </div>

                <section id="map" style="<?= !$showMap ? 'display: none' : '' ?>">
                    <div class="row">
                        <?php if ($permission = $model->getFieldPermission('latitude')): ?>
                            <section class="col col-6">
                                <?= $form->field($model, 'latitude', ['permission' => $permission])->label($model->getAttributeLabel('latitude')) ?>
                            </section>
                        <?php endif ?>

                        <?php if ($permission = $model->getFieldPermission('longitude')): ?>
                            <section class="col col-6">
                                <?= $form->field($model, 'longitude', ['permission' => $permission])->label($model->getAttributeLabel('longitude')) ?>
                            </section>
                        <?php endif ?>

                        <div class="col col-6">
                            <label class="label">Место на карте</label>
                        </div>
                        <div class="col col-6 text-right">
                            <?= Html::a('Найти на карте', ['region/search'], ['id' => 'show-on-map', 'data-id' => 'region']) ?>
                        </div>
                    </div>
                    <label class="input">
                        <?php
                        if(is_null($model->latitude) || is_null($model->longitude)){
                            $model->latitude = 55.748517;
                            $model->longitude = 37.643373;
                        }

                        $coord = new LatLng([
                            'lat' => $model->latitude,
                            'lng' => $model->longitude,
                        ]);
                        $map = new Map([
                            'center' => $coord,
                            'zoom' => is_null($model->latitude) || is_null($model->longitude) ? 4 : 6,
                            'width' => '100%',
                        ]);

                        $map->addEvent(new Event([
                            'trigger' => 'click',
                            'js' => "setRegionCords(this, event)",
                        ]));

                        echo $map->display();
                        ?>
                    </label>
                </section>
            </fieldset>

            <footer>
                <?= FormFooter::widget([
                    'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                ])
                ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord && ($permission = $model->getFieldPermission('grid'))): ?>
        <?=
        GridView::widget([
            'caption' => 'Нас. пункты',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'controller' => Yii::$app->createControllerByID('locality'),
            'addUrlParams' => ['region_id' => $model->id],
            'columns' => [
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'name',
                    'label' => 'Нас. пункт',
                ],
            ],
        ])
        ?>
    <?php endif ?>
</div>

<script        type="text/javascript">
    pageSetUp();
</script>

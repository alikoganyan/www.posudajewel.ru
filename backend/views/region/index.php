<?php
/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\RegionSearch
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\html\HeaderLinkButton;
use \yii\helpers\Url;

$this->title = 'Регионы';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'hideColumns'  => ['code'],
    'columns'      => [
        'name',
        'code',
    ],
]) ?>

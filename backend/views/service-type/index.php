<?php
/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceTypeSearch
 */

use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\DataColumn;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'hideColumns'  => ['is_mass'],
    'columns'      => [
        'name',
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_mass',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
                return $model->is_mass ? 'Да' : 'Нет';
            },
            'label' => 'Массовая',
        ],
    ],
]) ?>

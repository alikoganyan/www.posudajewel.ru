<?php
/**
 * @var $this        \yii\web\View
 * @var $allChars    []
 * @var $serviceType \common\models\ServiceType
 * @var $model       \common\models\ServiceTypeChar
 */

use \yii\bootstrap\Html;
use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\Url;
?>

<div class="modal fade" id="modal-chars" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Добавить свойство</h3>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id'      => 'values-form',
                    'action'  => Url::to(['service-type/save-char', 'id' => $serviceType->id]),
                    'enableAjaxValidation' => false,
                    'options' => [
                        'class' => 'smart-form'
                    ]
                ]) ?>
                <div class="items smart-form">
                    <section>
                        <?php if (!$allChars): ?>
                            <div class="alert alert-info">Нет доступных характеристик</div>
                        <?php else: ?>
                            <label class="select">
                                <?= $form->field($model, 'char_id')
                                    ->dropDownList($allChars)->label('Выберите свойство') ?>
                            </label>
                        <?php endif ?>
                    </section>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <div class="modal-footer">
                <?= implode('', [
                    Html::submitButton('Сохранить', [
                        'class' => 'btn btn-primary save-button',
                        'form'  => 'values-form',
                    ]),
                    Html::button('Закрыть', [
                        'class'        => 'btn btn-default',
                        'data-dismiss' => 'modal',
                    ]),
                ]) ?>
            </div>
        </div>
    </div>
</div>

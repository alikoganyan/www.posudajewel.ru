<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\ServiceType
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CharSearch
 * @var $allChars     []
 */

use \backend\widgets\activeForm\ActiveForm;
use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\html\HeaderButton;
use \common\models\ServiceTypeChar;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['service-type/index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая категория' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Категория</h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'servicetype-form',
                            'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form'
                ]
            ]) ?>
                <fieldset>
                    <?php if ($permission = $model->getFieldPermission('name')): ?>
                        <section>
                            <?= $form->field($model, 'name')->label('Наименование категории') ?>
                        </section>
                    <?php endif ?>
                </fieldset>

                <footer>
                    <?php if ($permission = $model->getFieldPermission('is_mass')): ?>
                        <?= $form->field($model, 'is_mass', ['permission' => $permission])
                            ->checkbox(['label' => 'Массовое предложение']) ?>
                    <?php endif ?>

                    <?= FormFooter::widget([
                        'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                    ]) ?>
                </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord && ($permission = $model->getFieldPermission('grid'))): ?>

        <?= GridView::widget([
            'caption'       => 'Свойства',
            'dataProvider'  => $dataProvider,
            'filterModel'   => $searchModel,
            'isAddVisible'  => false,
            'controller'    => Yii::$app->createControllerByID('service-type'),
            'actions'       => [
                'delete' => '/delete-char',
            ],
            'actionParams' => [
                'serviceTypeId' => $model->id,
            ],
            'headerButtons' => [
                new HeaderButton('Добавить', "$('#modal-chars').modal('show')"),
            ],
            'columns'       => [
                'name',
            ],
        ]) ?>
    <?php endif ?>
</div>

<? if (!$model->isNewRecord): ?>
<?= $this->render("chars", [
    'serviceType' => $model,
    'allChars' => $allChars,
    'model' => new ServiceTypeChar()
])?>
<? endif ?>
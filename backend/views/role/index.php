<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use \backend\widgets\gridView\GridView;

$this->title = 'Права доступа';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'columns'      => [
        'name',
    ]
]) ?>


<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Role
 * @var $entities []
 * @var $userPermissions []
 */

use backend\widgets\activeForm\ActiveForm;
use \yii\bootstrap\Tabs;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'Права доступа';

$items = [];
foreach ($entities as $key => $entity) {
    if ($model->is_undeleted == 1 && $key == 'role') {
        continue;
    }

    $items[] = [
        'label' => $entity['label'],
        'content' => $this->render('tab-item', [
            'fields' => $entity['fields'],
            'key' => $key,
            'userPermissions' => isset($userPermissions[$key]) ? $userPermissions[$key] : [],
        ]),
        'options' => [
            'style' => 'padding: 10px; font-size: 20px;',
        ],
    ];
}
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form'
                ],
            ]) ?>

            <fieldset>
                <section>
                    <?= $form->field($model, 'name')->label($model->getAttributeLabel('name')) ?>
                </section>

                <section class="col-md-12">
                    <?= Tabs::widget([
                        'items' => $items,
                    ]); ?>
                </section>

                <section class="col-md-12">
                    <footer>
                        <?= FormFooter::widget() ?>
                    </footer>
                </section>
            </fieldset>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

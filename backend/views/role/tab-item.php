<?php
/**
 * @var $this \yii\web\View
 * @var $fields []
 * @var $key string
 * @var $userPermissions []
 */

use \yii\bootstrap\Html;
?>

<?php foreach ($fields as $value => $label): ?>
    <section class="col-md-12">
        <label class="label col-md-2"><?= $label ?></label>
        <label class="select col-md-2">
            <?= Html::dropDownList($key."[$value]", isset($userPermissions[$value]) ? $userPermissions[$value] : 0,
                in_array($value, ['add', 'delete', 'access']) ? ['Нет', 'Да'] : ['Скрытое', 'Только для чтения', 'Для записи']
            ) ?>
            <i></i>
        </label>
    </section>
<?php endforeach ?>

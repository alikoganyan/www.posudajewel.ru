<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Value
 */

use \backend\widgets\activeForm\ActiveForm;
use \backend\widgets\formFooter\FormFooter;


$this->title = 'Значение свойства';
$this->params['breadcrumbs'][] = ['label' => 'Свойства', 'url' => ['char/index']];
$this->params['breadcrumbs'][] = ['label' => $char->name, 'url' => ['char/update', 'id' => $char->id]];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новое значение' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Значение</h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'value-form',
                'options' => [
                    'class' => 'smart-form'
                ]
            ]) ?>

            <fieldset>
                <section>
                    <?= $form->field($model, 'name')->label('Наименование значения') ?>
                </section>
            </fieldset>

            <section>
                <footer>
                    <?= FormFooter::widget([
                        'resourceId' => $model->id,
                    ]) ?>
                </footer>
            </section>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

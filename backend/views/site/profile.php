<?php
$this->title = 'Профиль пользователя';

use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;
$this->params['breadcrumbs'][] = $this->title;
?>

<header>
    <h3><?= Html::encode($this->title) ?></h3>
</header>

<img id="profile-banner" src="/admin/images/profile_banner.jpg">

<div class="row">
    <article class="col-sm-3 profile-left">
        <div class="text-center buttons">
            <h3></h3>
            <div class="profile-left-info">
                <div class="profile-left-img">
                <?php $image = $user->getUrlPrev();?>
                <?= Html::img($image ? $image : '/admin/images/no_avatar.jpg')?>
                </div>
            </div>

            <?= Html::a('Сменить пароль', '#', ['class' => 'btn btn-default', ' data-toggle' => 'modal', 'data-target' => '#change-password-modal']) ?>
            <?= Html::hiddenInput('User[email]', $user->email) ?>
        </div>
    </article>
    <article class="col-sm-9 buttons profile-right">
     <div class="profile-cont">
        <h3 class="clearfix flex">
            <span class="nowrap-ellipsis"><?= Html::encode($user->fullName) ?></span>
            <?= Html::a('Изменить', ['/user/update', 'id' => $user->id], ['class' => 'btn btn-default pull-right']) ?>
        </h3>

        <?php if (!empty($user->email)): ?>
            <div class="profile-user-email nowrap-ellipsis"><span class="glyphicon glyphicon-envelope"></span> <?= Html::encode($user->email) ?></div>
        <?php endif; ?>

        <?php if (!empty($user->phone)): ?>
            <div><span class="glyphicon glyphicon-earphone"></span> <?= Html::encode($user->phone) ?></div>
        <?php endif; ?>
    </div>
</article>
</div>

<?= $this->render('//user/modal_password') ?>


<?=

GridView::widget([
    'caption' => 'Заказы',
    'actions'  => [
        'update' => ['/order/user-update']
    ],
    'dataProvider' => $ordersProvider,
    'filterModel' => $ordersSearch,
    'hideColumns' => ['type', 'date', 'is_blocked'],
    'columns' => [
        'id',
        // 'cached_cart_id',
        'fio',
        'phone',
        // 'email:email',
        'address:ntext',
        // 'comment:ntext',
        // 'delivery_cost',
        // 'total',
        // 'delivery_id',
        [
            'class' => DataColumn::className(),
            'attribute' => 'status_id',
            'filter' => $statuses_list,
            'value' => function ($model) {
                return $model->status->name;
            },
        ],
        'created_at',
        'updated_at',
    ],
])
?>

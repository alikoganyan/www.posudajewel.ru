<?php

/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\Locality
 * @var $region       \common\models\Region
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\LocalitySearch
 */
use yii\helpers\Html;
use \backend\widgets\activeForm\ActiveForm;
use \dosamigos\google\maps\LatLng;
use \common\components\Map;
use \common\components\Marker;
use \dosamigos\google\maps\Event;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'Нас. пункты';

$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['region/index']];
$this->params['breadcrumbs'][] = ['label' => $region->name, 'url' => ['region/update', 'id' => $region->id]];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый нас. пункт' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <link rel="stylesheet" href="https://api-maps.yandex.ru/1.1/_YMaps.css?v=1.1.21-58">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Нас. пункт</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                'id' => 'locality-form',
            ])
            ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name', ['permission' => $permission])
                            ->label('Наименование нас. пункта') ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('is_quick_choice')): ?>
                    <section class="row">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'is_quick_choice', ['permission' => $permission])->checkbox(['label' => 'Быстрый выбор']) ?>
                        </div>
                    </section>
                <?php endif ?>


                <div class="row">
                    <?php if ($permission = $model->getFieldPermission('latitude')): ?>
                        <section class="col col-6">
                            <?= $form->field($model, 'latitude', ['permission' => $permission])->label($model->getAttributeLabel('latitude')) ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('longitude')): ?>
                        <section class="col col-6">
                            <?= $form->field($model, 'longitude', ['permission' => $permission])->label($model->getAttributeLabel('longitude')) ?>
                        </section>
                    <?php endif ?>
                </div>

                <section>
                    <div class="row">
                        <div class="col col-6">
                            <label class="label">Место на карте</label>
                        </div>
                        <div class="col col-6 text-right">
                            <?= Html::a('Найти на карте', ['region/search'], ['id' => 'show-on-map', 'data-id' => 'locality', 'data-lat' => $region->latitude, 'data-long' => $region->longitude]) ?>
                        </div>
                    </div>
                    <label class="input">
                        <?php
                        $zoom = 10;

                        if (empty($model->latitude) || empty($model->longitude))
                        {
                            if ( ! empty($region->latitude) && ! empty($region->longitude))
                            {
                                $model->latitude = $region->latitude;
                                $model->longitude = $region->longitude;

                                $zoom = 6;
                            }
                        }

                        $coord = new LatLng([
                            'lat' => $model->latitude ? $model->latitude : 55.748517,
                            'lng' => $model->longitude ? $model->longitude : 37.643373,
                        ]);
                        $map = new Map([
                            'center' => $coord,
                            'zoom' => $zoom,
                            'width' => '100%',
                        ]);

                        $map->addEvent(new Event([
                            'trigger' => 'click',
                            'js' => "setLocalityCords(this, event)",
                        ]));

                        echo $map->display();
                        ?>
                    </label>
                </section>
            </fieldset>

                <footer>
                    <?= FormFooter::widget([
                        'resourceId' => $model->id,
                    ]) ?>
                </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    pageSetUp();
</script>

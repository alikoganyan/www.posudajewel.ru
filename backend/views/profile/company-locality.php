<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Company
 */

use \yii\bootstrap\Html;
use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\ArrayHelper;
use \common\models\Locality;
use \dosamigos\google\maps\LatLng;
use \dosamigos\google\maps\Event;
use \common\components\Map;
use \common\components\Marker;
use \yii\helpers\Url;

$this->title = 'Адрес компании';

$localityQuery = Locality::find()->orderBy('name');
$localityList = ArrayHelper::map($localityQuery->all(), 'id', 'name');
?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'company-form',
        ]) ?>

        <section>
            <label class="label">Регион</label>
            <label class="select">
                <?= Html::dropDownList('region', $model->locality ? $model->locality->region_id : null, $model->getRegionList(), [
                    'onchange' => 'company.getLocalities(this)',
                    'prompt' => 'Выберите значение',
                ]) ?>
                <i></i>
            </label>
        </section>

        <section onchange="company.getLocalityCoords(this)" id="locality">
            <?= $form->field($model, 'locality_id')
                     ->dropDownList(['' => 'Выберите значение'] + $localityList)
                     ->label($model->getAttributeLabel('locality_id')) ?>
        </section>

        <section>
            <?= $form->field($model, 'address')->label($model->getAttributeLabel('address')) ?>
        </section>

        <?= $form->field($model, 'latitude')->label(false)->hiddenInput() ?>
        <?= $form->field($model, 'longitude')->label(false)->hiddenInput() ?>

        <section id ="map">
            <label class="input">
                <?
                $coord = new LatLng([
                    'lat' => $model->latitude,
                    'lng' => $model->longitude,
                ]);
                $map = new Map([
                    'center' => $coord,
                    'zoom'   => 10,
                    'width'  => '100%',
                ]);

                $marker = new Marker([
                    'position' => $coord,
                ]);
                $map->addOverlay($marker);

                $map->addEvent(new Event([
                    'trigger' => 'click',
                    'js'      => "setCords(this, event)",
                ]));

                echo $map->display();
                ?>
            </label>
        </section>

        <footer>
            <button class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/company-create']) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\User
 */

use \backend\widgets\activeForm\ActiveForm;

$this->title = 'Регистрация пользователя';
?>

<?= \backend\widgets\formWizard\FormWizard::widget() ?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'user-form',
        ]) ?>

        <section>
            <?= $form->field($model, 'email')->label($model->getAttributeLabel('email')) ?>
        </section>

        <section>
            <?= $form->field($model, 'password')->passwordInput()->label($model->getAttributeLabel('password')) ?>
        </section>

        <section>
            <?= $form->field($model, 'repeat_password')->passwordInput()->label($model->getAttributeLabel('repeat_password')) ?>
        </section>

        <footer>
            <button type="submit" class="btn btn-default">Далее</button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

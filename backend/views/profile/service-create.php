<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Service
 * @var $serviceList \common\models\Service[]
 */

use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\Url;

$this->title = 'Сведения о предложении';
?>

<div class="form-container">
    <header class="row">
        <h3 class="col-md-9"><?= $this->title ?></h3>
        <section class="col-md-3 pull-right">
            <div class="form-group field-service-unit_id has-success">
                <label class="select">
                    <select class="form-control" name="clone" onchange="profile.cloneChars(this)">
                        <option value="">Скопировать из услуги</option>
                        <?php foreach ($serviceList as $service): ?>
                            <?php if ($service->chars == null) continue; ?>
                            <option value="<?= $service->id ?>"><?= $service->name ?></option>
                        <?php endforeach ?>
                    </select>
                    <i></i>
                </label>
            </div>
        </section>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'user-form',
        ]) ?>

        <section>
            <?= $form->field($model, 'name')->label($model->getAttributeLabel('name')) ?>
        </section>

        <section>
            <?= $form->field($model, 'service_type_id')
                     ->dropDownList(['' => 'Выберите значение'] + $model->getServiceTypesList(),
                         [
                             'onmousedown' => 'saveServiceType(this)',
                             'onchange' => "confirmChars(this)",
                         ]
                     )->label($model->getAttributeLabel('service_type_id')) ?>
        </section>

        <div id="chars">
            <?php if (isset($model->serviceType)): ?>
                <?= $this->render('//service/chars', [
                    'model' => $model,
                    'serviceType' => $model->serviceType,
                ]) ?>
            <?php endif ?>
        </div>

        <footer>
            <button type="submit" class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/company-locality']) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

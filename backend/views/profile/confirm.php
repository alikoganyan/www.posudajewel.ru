<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\User
 */

use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\Url;

$this->title = 'Завершение регистрации';
?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'user-form',
        ]) ?>

        <section>
            <?= $form->field($model, 'code')->label($model->getAttributeLabel('code')) ?>
        </section>

        <footer>
            <button class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/index']) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\User
 */

use \backend\widgets\activeForm\ActiveForm;
use \kartik\daterange\DateRangePicker;
use \yii\helpers\Url;

$this->title = 'Сведения о пользователе';
?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'user-form',
        ]) ?>

        <section>
            <?= $form->field($model, 'last_name')->label($model->getAttributeLabel('last_name')) ?>
        </section>

        <section>
            <?= $form->field($model, 'first_name')->label($model->getAttributeLabel('first_name')) ?>
        </section>

        <section>
            <section>
                <div class="form-group input-group drp-container col-md-12">
                    <label class="label">Дата рождения</label>
                    <label class="input">
                        <?= DateRangePicker::widget([
                            'model'         => $model,
                            'attribute'     => 'birthday',
                            'useWithAddon'  => true,
                            'pluginOptions' => [
                                'singleDatePicker' => true,
                                'showDropdowns'    => true,
                                'locale'=>[
                                    'format'=>'D.MM.Y',
                                ],
                            ],
                        ]);
                        ?>
                    </label>
                </div>
            </section>
        </section>

        <footer>
            <button type="submit" class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/index']) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

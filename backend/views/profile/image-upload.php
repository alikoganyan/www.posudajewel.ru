<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Service
 */

use \backend\widgets\activeForm\ActiveForm;
use \kartik\file\FileInput;
use \yii\helpers\Url;

$this->title = 'Загрузка фотографий';
?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'company-form',
            'options' => [
                'class' => 'smart-form',
                'enctype' => 'multipart/form-data',
            ],
        ]) ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
        <section>
            <?= $form->field($model, 'image')->widget(\backend\widgets\cropper\Cropper::className(), [
                'options' => [
                    'height' => 300,
                    'width' => 500,
                ],
            ]) ?>
<!--            --><?//= $form->field($model, 'image')->widget(FileInput::className(), [
//                'options' => [
//                    'multiple' => true
//                ],
//                'pluginOptions' => [
//                    'dropZoneTitle' => 'Загрузите фотографию с компьютера или смартфона...',
//                    'overwriteInitial'=>false,
//                    'uploadUrl' => Url::to(['service/image-upload', 'id' => $model->id]),
//                    'maxFileCount' => 10,
//                    'autoReplace' => false,
//                    'initialPreview' => $model->getGalleryImages(),
//                    'initialPreviewConfig' => $model->getGalleryImagesConfig(),
//                    'initialPreviewAsData' => true,
//                    'maxFileSize' => 7000, // TODO set server setting for max upload file size
//                    'msgSizeTooLarge' => 'Максимальны размер загружаемого файла 5 МБ',
//                ]
//            ]) ?>
        </section>

        <footer>
            <button type="button" class="btn btn-primary pull-left" onclick="service.addImage(this)">Добавить фото</button>
            <button class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/service-price', 'id' => $model->id]) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

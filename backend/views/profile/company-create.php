<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Company
 */

use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\Url;

$this->title = 'Сведения о компании';
?>

<?= \backend\widgets\formWizard\FormWizard::widget() ?>

<div class="form-container">
    <header>
        <h3><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'company-form',
        ]) ?>

        <section>
            <?= $form->field($model, 'name')->label($model->getAttributeLabel('name')) ?>
        </section>

        <section>
            <?= $form->field($model, 'main_phone')->label($model->getAttributeLabel('main_phone')) ?>
        </section>

        <section>
            <?= $form->field($model, 'first_phone')->label($model->getAttributeLabel('first_phone')) ?>
        </section>

        <section>
            <?= $form->field($model, 'second_phone')->label($model->getAttributeLabel('second_phone')) ?>
        </section>

        <section>
            <?= $form->field($model, 'web')->label($model->getAttributeLabel('web')) ?>
        </section>

        <section>
            <?= $form->field($model, 'email')->label($model->getAttributeLabel('email')) ?>
        </section>

        <footer>
            <button class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/main-info']) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

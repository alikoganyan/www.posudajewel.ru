<?php
/**
 * @var $this \yii\web\View
 */

use \yii\helpers\Url;

$this->title = 'Завершение';
?>

<a class="btn btn-default" href="<?= Url::to(['profile/service-create']) ?>">Добавить новую услугу</a>
<a class="btn btn-default" href="<?= Url::to(['index']) ?>">Закрыть</a>

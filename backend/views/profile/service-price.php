<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Service
 * @var $serviceList \common\models\Service[]
 */

use \backend\widgets\activeForm\ActiveForm;
use \backend\widgets\formWizard\FormWizard;
use \yii\helpers\Url;

$this->title = 'Стоимость предложения';
?>

<div class="form-container">
    <header class="row">
        <h3 class="col-md-9"><?= $this->title ?></h3>
        <section class="col-md-3 pull-right">
            <div class="form-group field-service-unit_id has-success">
                <label class="select">
                    <select class="form-control" name="clone" onchange="profile.cloneOptions(this)">
                        <option value="">Скопировать из услуги</option>
                        <?php foreach ($serviceList as $service): ?>
                            <?php if ($service->optionGroups == null) continue; ?>
                            <option value="<?= $service->id ?>"><?= $service->name ?></option>
                        <?php endforeach ?>
                    </select>
                    <i></i>
                </label>
            </div>
        </section>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>

        <?php $form = ActiveForm::begin([
            'id' => 'company-form',
        ]) ?>

        <div class="row">
            <section class="col col-10">
                <?= $form->field($model, 'price')->label('Стоимость предложения (руб.)') ?>
            </section>

            <section class="col col-2">
                <?= $form->field($model, 'unit_id')
                         ->dropDownList($model->getUnitList())
                         ->label($model->getAttributeLabel('unit_id')) ?>
            </section>
        </div>

        <?= $this->render('//service/options', [
            'model' => $model,
        ]) ?>

        <footer>
            <button class="btn btn-default">Далее</button>
            <button class="btn btn-default">
                <a href="<?= Url::to(['profile/service-create', 'id' => $model->id]) ?>">Назад</a>
            </button>
        </footer>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $model        \common\models\Company
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CompanySearch
 */

use \backend\widgets\gridView\GridView;
?>

<?= GridView::widget([
    'caption'      => 'Услуги',
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'controller'   => Yii::$app->createControllerByID('service'),
    'actionParams' => [
        'serviceTypeId' => $model->id,
    ],
    'columns'      => [
        'name',
        'price',
    ],
]) ?>

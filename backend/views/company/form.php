<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\Company
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CompanySearch
 */

use \backend\widgets\activeForm\ActiveForm;
use \common\models\Locality;
use \yii\helpers\ArrayHelper;
use \yii\bootstrap\Html;
use \dosamigos\google\maps\LatLng;
use \common\components\Map;
use \common\components\Marker;
use \dosamigos\google\maps\Event;
use \backend\widgets\formFooter\FormFooter;

$localityQuery = Locality::find()->orderBy('name');
if ($model->locality) {
    $localityQuery->where(['region_id' => $model->locality->region->id]);
}
$localityList = ArrayHelper::map($localityQuery->all(), 'id', 'name');

$this->title = 'Компании';
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая компания' : $model->name;

if (!$model->locality_id) {
    $this->registerJs("$('[name=region]').change()");
}
?>

<header>
    <h3 style="margin-top: 0; margin-bottom: 10px;">Компания</h3>
</header>

<div>
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body no-padding">
        <?php $form = ActiveForm::begin([
            'id' => 'company-form',
        ]) ?>
        <fieldset>
            <?= $form->field($model, 'id')->label(false)->hiddenInput() ?>

            <?php if ($permission = $model->getFieldPermission('user_id')): ?>
                <section>
                    <?= $form->field($model, 'user_id', ['permission' => $permission])
                        ->dropDownList($model->userList)
                        ->label($model->getAttributeLabel('user_id')) ?>
                </section>
            <?php endif ?>

            <?php if ($permission = $model->getFieldPermission('name')): ?>
                <section>
                    <?= $form->field($model, 'name', ['permission' => $permission])->label('Наименование компании') ?>
                </section>
            <?php endif ?>

            <div class="row">
                <?php if ($permission = $model->getFieldPermission('main_phone')): ?>
                    <section class="col col-6">
                        <?= $form->field($model, 'main_phone')->label($model->getAttributeLabel('main_phone')) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('first_phone')): ?>
                    <section class="col col-6">
                        <?= $form->field($model, 'first_phone')->label($model->getAttributeLabel('first_phone')) ?>
                    </section>
                <?php endif ?>
            </div>
            <div class="row">
                <?php if ($permission = $model->getFieldPermission('second_phone')): ?>
                    <section class="col col-6">
                        <?= $form->field($model, 'second_phone')->label($model->getAttributeLabel('second_phone')) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('email')): ?>
                    <section class="col col-6">
                        <?= $form->field($model, 'email', ['permission' => $permission])
                                 ->label($model->getAttributeLabel('email')) ?>
                    </section>
                <?php endif ?>
            </div>

            <?php if ($permission = $model->getFieldPermission('web')): ?>
                <section>
                    <?= $form->field($model, 'web', ['permission' => $permission])->label($model->getAttributeLabel('web')) ?>
                </section>
            <?php endif ?>

            <?php if ($permission = $model->getFieldPermission('region')): ?>
                <section>
                    <label class="label">Регион</label>
                    <label class="select">
                        <?= Html::dropDownList('region', $model->locality ? $model->locality->region_id : null, $model->getRegionList(), [
                            'onchange' => 'company.getLocalities(this)',
                            'disabled' => $permission == 1,
                            'prompt' => 'Выберите значение',
                        ]) ?>
                        <i></i>
                    </label>
                </section>
            <?php endif ?>

            <?php if ($permission = $model->getFieldPermission('locality_id')): ?>
                <section onchange="company.getLocalityCoords(this)" id="locality">
                    <?= $form->field($model, 'locality_id', ['permission' => $permission])
                         ->dropDownList(['' => 'Выберите значение'] +  $localityList)
                         ->label($model->getAttributeLabel('locality_id')) ?>
                </section>
            <?php endif ?>
            <div class="row">
            <?php if ($permission = $model->getFieldPermission('address')): ?>
                <section class="col col-md-12">
                    <?= $form->field($model, 'address', ['permission' => $permission])
                         ->label($model->getAttributeLabel('address')) ?>

                    <?= Html::a('Скрыть карту', 'javascript:void(0)', [
                        'id' => 'btn-map',
                        'onclick' => 'company.toggleMap();'
                    ]); ?>
                </section>
            <?php endif ?>
            </div>

            <?php if ($permission = $model->getFieldPermission('map')): ?>
                <section id ="map">
                    <label class="input label map">
                        <span>Место на карте</span>
                        <?= $form->field($model, 'longitude')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'latitude')->hiddenInput()->label(false)->hint(false) ?>
                        <?
                        if(is_null($model->latitude) || is_null($model->longitude)){
                            $model->latitude = 55.748517;
                            $model->longitude = 37.643373;
                        }

                        $coord = new LatLng([
                            'lat' => $model->latitude,
                            'lng' => $model->longitude,
                        ]);
                        $map = new Map([
                            'center' => $coord,
                            'zoom'   => 10,
                            'width'  => '100%',
                        ]);

                        if (!$model->isNewRecord) {
                            $marker = new Marker([
                                'position' => $coord,
                            ]);
                            $map->addOverlay($marker);
                        }

                        $map->addEvent(new Event([
                            'trigger' => 'click',
                            'js'      => "setCords(this, event)",
                        ]));

                        echo $map->display();
                        ?>
                    </label>
                </section>
            <?php endif ?>
        </fieldset>

        <footer>
            <?php if ($permission = $model->getFieldPermission('is_blocked')): ?>
                <div>
                    <?= $form->field($model, 'is_blocked', ['permission' => $permission])
                        ->checkbox(['label' => $model->getAttributeLabel('is_blocked')]) ?>
                </div>
            <?php endif ?>

            <?= FormFooter::widget() ?>
        </footer>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\CompanySearch
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\DataColumn;

$this->title = 'Компании';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'       => $this->title,
    'dataProvider'  => $dataProvider,
    'filterModel'   => $searchModel,
    'hideColumns'  => ['regionName', 'localityName', 'userName', 'main_phone', 'is_blocked'],
    'columns'       => [
        'name',
        'regionName',
        'localityName',
        'userName',
        [
            'class' => DataColumn::className(),
            'attribute' => 'main_phone',
            'label' => 'Телефон',
        ],
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_blocked',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
                return $model->is_blocked ? 'Да' : 'Нет';
            },
        ],
    ],
]) ?>

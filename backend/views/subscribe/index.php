<?php

/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceTypeSearch
 */
use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;


$this->params['listUser'] = $listUser;
?>

<?=

GridView::widget([
    'caption' => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'hideColumns' => ['time_create', 'status'],
    'buttons' => [],
    'columns' => [
        [
            'class' => DataColumn::className(),
            'attribute' => 'email',
            'content' => function ($model) {
                $email = Html::encode($model->email);

                return isset($this->params['listUser'][$model->email]) ? Html::a($email, ['/user/update', 'id' => $this->params['listUser'][$model->email]]) : $email;
            },
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'time_create',
                    'value' => function ($model) {
                        return date('Y-m-d H:i:s', $model->time_create);
                    },
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'status',
                    'filter' => [0 => 'Нет', 1 => 'Да'],
                    'value' => function ($model) {
                return $model->getStatus();
            },
                ],
            ],
        ])
        ?>

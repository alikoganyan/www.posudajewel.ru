<?php

/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceTypeSearch
 */
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>

<?=

GridView::widget([
    'caption' => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'hideColumns' => ['type', 'date', 'is_blocked'],
    'columns' => [
        'name',
        [
            'class' => DataColumn::className(),
            'attribute' => 'type',
            'filter' => $listType,
            'value' => function ($model) {
                return $model->getTypeName();
            },
        ],
        'date',
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_blocked',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
        return $model->is_blocked ? 'Да' : 'Нет';
    },
        ],
    ],
])
?>

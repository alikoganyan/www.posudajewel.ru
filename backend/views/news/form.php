<?php

/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\ServiceType
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\CharSearch
 * @var $allChars     []
 */
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\file\FileInput;
use kartik\daterange\DateRangePicker;
use backend\widgets\activeForm\ActiveForm;
use backend\widgets\formFooter\FormFooter;
use mihaildev\elfinder\ElFinder;
use mihaildev\ckeditor\CKEditor;


$this->title = 'Новости';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['service-type/index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая запись' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Новости</h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php
            $form = ActiveForm::begin([
                        'enableAjaxValidation' => false,
                        'options' => [
                            'class' => 'smart-form',
                            'enctype' => 'multipart/form-data',
                        ]
                    ])
            ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('type')): ?>
                    <section>
                        <?= $form->field($model, 'type')->dropDownList($listType) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name')->textInput(['class' => 'url-name']) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('url')): ?>
                    <section>
                        <?= $form->field($model, 'url')->textInput(['class' => 'url-translit']) ?>
                        <?= Html::a('Получить адрес', '#', ['class' => 'create-url']); ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('sitemap')): ?>
                    <section class="row form-checkbox">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'sitemap', ['permission' => $permission])->checkbox(['label' => 'Блокировать в sitemap.xml']) ?>
                        </div>
                    </section>
                <?php endif ?>

                <section>
                    <?= $form->field($model, 'meta_title')->label($model->getAttributeLabel('meta_title')) ?>
                </section>

                <section>
                    <?= $form->field($model, 'meta_description')->label($model->getAttributeLabel('meta_description')) ?>
                </section>

                <section>
                    <?= $form->field($model, 'meta_keywords')->label($model->getAttributeLabel('meta_keywords')) ?>
                </section>



                <?php if ($permission = $model->getFieldPermission('description')): ?>
                    <section>
                        <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('content')): ?>
                    <section>
                        <?=
                        $form->field($model, 'content')->widget(CKEditor::className(), [
                            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                'editorOptions' => ElFinder::ckeditorOptions(['elfinder'])
                            ]),
                        ]);
                        ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('image')): ?>
                    <section>
                        <label class="label">Изображение</label>
                        <label class="input">
                            <?=
                            FileInput::widget([
                                'name' => 'News[file]',
                                'pluginOptions' => [
                                    'overwriteInitial' => false,
                                    'autoReplace' => true,
                                    'initialPreview' => !empty($model->image) ? [$model->getUrlImage()] : [],
                                    'initialPreviewConfig' => [['url' => Url::to(['remove-image', 'id' => $model->id])]],
                                    'initialPreviewAsData' => true,
                                ]
                            ])
                            ?>
                        </label>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('date')): ?>
                    <section>
                        <div class="form-group input-group drp-container col-md-12">
                            <label class="label">Дата публикации</label>
                            <label class="input">
                                <?=
                                DateRangePicker::widget([
                                    'model' => $model,
                                    'attribute' => 'date',
                                    'useWithAddon' => true,
                                    'pluginOptions' => [
                                        'singleDatePicker' => true,
                                        'showDropdowns' => true,
                                        'locale' => [
                                            'format' => 'YYYY-MM-DD',
                                        ],
                                    ],
                                ]);
                                ?>
                            </label>
                        </div>
                    </section>
                <?php endif ?>
            </fieldset>

            <footer>
                <?php if ($permission = $model->getFieldPermission('is_blocked')): ?>
                    <div>
                        <?= $form->field($model, 'is_blocked', ['permission' => $permission])->checkbox(['label' => $model->getAttributeLabel('is_blocked')]) ?>
                    </div>
                <?php endif ?>

                <?=
                FormFooter::widget([
                    'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                ])
                ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

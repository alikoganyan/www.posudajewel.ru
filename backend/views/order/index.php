<?php

use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\html\HeaderLinkButton;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid-service">
    <?php echo GridView::widget([
        'caption' => $this->title,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFilter' => true,
        'columns' => [
            // 'id',
            [
                'class' => DataColumn::className(),
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->fullName;
                },
            ],
            // 'cached_cart_id',
            'fio',
            'email:email',
            'phone',
            'address',
            'created_at',
            'updated_at',

        ],
    ])?>
</div>

<?php

use yii\helpers\Html;
use backend\widgets\activeForm\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelU common\models\Order */
/* @var $formU yii\widgets\ActiveForm */
?>
<div>
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body no-padding">

        <?php $formU = ActiveForm::begin([
            'id' => 'order-form-u',
            'enableAjaxValidation' => false,
            'options' => [
                'class' => 'smart-form',
            ],
        ])?>
        <fieldset style="margin-left: 13px;">
            <?= $formU->errorSummary($modelU);?>
        </fieldset>
        <fieldset>
            <section>
                <?= $formU->field($modelU, 'fio')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'phone')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'email')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'notification')->checkbox(['label'=>$modelU->getAttributeLabel('notification')]) ?>

                <br>

                <?= $formU->field($modelU, 'postcode')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'region_id')->dropDownList($regions,['prompt'=>'Выберите регион','class'=>'order-region-u','id' => 'order_dropdown_region-u']) ?>

                <?= $formU->field($modelU, 'locality_id')->dropDownList([],['prompt'=>'Выберите нас. пункт','class'=>'order-locality']) ?>

                <?= $formU->field($modelU, 'address')->textarea(['rows' => 6]) ?>

                <?= $formU->field($modelU, 'delivery_id')->dropDownList($delivery_services,['prompt'=>'Выберите службу доставки']) ?>

                <?= $formU->field($modelU, 'comment')->textarea(['rows' => 6]) ?>

                <?= $formU->field($modelU, 'organization' )->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'inn')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'kpp')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'law_address')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'actual_address')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'account_number')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'bank')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'corr_number')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'bik')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'bank_city')->textInput(['maxlength' => true]) ?>

                <?= $formU->field($modelU, 'type')->hiddenInput()->label(false);?>
            </section>
        </fieldset>
        <footer>
            <?php echo $this->render('/order/_cart-list', ['dataProvider' => $cartProvider,'cart_total' => $cart_total]) ?>
            <?= Html::hiddenInput('open', 0) ?>

            <div class="buttons-block-ch">
                <div>
                    <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                    <div class="">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-footer']) ?>
                    </div>
                </div>
            </div>
        </footer>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php

/**
 * @var $this                \yii\web\View
 * @var $model               \common\models\Discount
 * @var $searchModel         \backend\models\DiscountSearch
 * @var $dataProvider        \backend\components\ActiveDataProvider
 */


use yii\helpers\Html;
use yii\helpers\StringHelper;
use backend\widgets\activeForm\ActiveForm;


$this->title = 'Заказы';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый заказ' : 'Заказ №' . $model->id;
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?=$this->title?></h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form',
                ],
            ])?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('user_id')): ?>
                    <section>
                        <?= $form->field($model, 'user_id')->dropDownList($users_list,['prompt'=>'Выберите пользователя']) ?>
                    </section>
                <?php endif?>

                <?php if ($permission = $model->getFieldPermission('fio')): ?>
                    <section>
                        <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('phone')): ?>
                    <section>
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('email')): ?>
                    <section>
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('address')): ?>
                    <section>
                        <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('comment')): ?>
                    <section>
                        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('delivery_cost')): ?>
                    <section>
                        <?= $form->field($model, 'delivery_cost')->textInput(['maxlength' => true]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('total')): ?>
                    <section>
                        <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('delivery_id')): ?>
                    <section>
                        <?= $form->field($model, 'delivery_id')->dropDownList($delivery_services,['prompt'=>'Выберите службу доставки']) ?>
                    </section>
                <?php endif;?>

                <?php if ($permission = $model->getFieldPermission('status_id')): ?>
                    <section>
                        <?= $form->field($model, 'status_id')->dropDownList($statuses_list,['prompt'=>'Выберите статус заказа']) ?>
                    </section>
                <?php endif;?>

            </fieldset>

            <footer>

                <?php echo $this->render('/order/_cart-list', ['dataProvider' => $cartProvider, 'cart_total' => $cart_total]) ?>

                <div class="buttons-block-ch">
                    <div>
                        <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                        <div class="btn-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-dropdown']) ?>
                            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><?= Html::a('Применить', '#', ['class' => 'service-save']) ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

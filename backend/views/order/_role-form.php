<?php

use yii\helpers\Html;
use backend\widgets\activeForm\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>
<div>
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body no-padding">

        <?php $form = ActiveForm::begin([
            'id' => 'order-form',
            'enableAjaxValidation' => false,
            'options' => [
                'class' => 'smart-form',
            ],
        ])?>
        <!--<fieldset style="margin-left: 13px;">
            <?/*= $form->errorSummary($model);*/?>
        </fieldset>-->
        <fieldset>


            <div class="form-group order-person-status">
<!--                <label class="col-md-2 control-label">Статус</label>-->
                <div class="col-md-12">
                    <div class="radio">
                        <label>
                            <input class="radiobox style-1" value="1" name="style-0" type="radio">
                            <span>Физическое лицо </span>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input class="radiobox style-1" value="2" name="style-0" type="radio">
                            <span>Юридическое лицо</span>
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>
        <?php ActiveForm::end() ?>
    </div>
</div>

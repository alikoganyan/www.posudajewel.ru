<?php
use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\html\HeaderLinkButton;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// var_dump($dataProvider->models)

?>

<div class="grid-service">
    <?php echo GridView::widget([
        'caption' => 'Содержимое корзины',
        'dataProvider' => $dataProvider,
        'showFilter' => false,
        'actions'=>false,
        'showFooter' => true,
        'columns' => [
            'name',
            [
                'class' => DataColumn::className(),
                'label' => 'Цена за ед.',
                'attribute' => 'price',
                'value' => function ($model) {
                    return Yii::$app->formatter->asCurrency($model->getPrice(), 'RUB');
                },
            ],
            // 'price:currency',
            'quantity',
            // 'cost:currency',
            [
                'class' => DataColumn::className(),
                'attribute'=>'cost',
                'format' => 'currency',
                'footer' => Yii::$app->formatter->asCurrency($cart_total, 'RUB'),
            ]
        ],
    ])?>
</div>

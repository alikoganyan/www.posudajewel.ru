<?php

use yii\helpers\Html;
use backend\widgets\activeForm\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>
<div>
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body no-padding">

        <?php $formF = ActiveForm::begin([
            'id' => 'order-form-f',
            'enableAjaxValidation' => false,
            'options' => [
                'class' => 'smart-form',
            ],
        ])?>
        <fieldset style="margin-left: 13px;">
            <?= $formF->errorSummary($modelF);?>
        </fieldset>
        <fieldset>
            <section>
            <?= $formF->field($modelF, 'fio')->textInput(['maxlength' => true]) ?>

            <?= $formF->field($modelF, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $formF->field($modelF, 'email')->textInput(['maxlength' => true]) ?>

            <?= $formF->field($modelF, 'notification')->checkbox(['label'=>$modelF->getAttributeLabel('notification')]) ?>

            <br/>

            <?= $formF->field($modelF, 'postcode')->textInput(['maxlength' => true]) ?>

            <?= $formF->field($modelF, 'region_id')->dropDownList($regions,['prompt'=>'Выберите регион','class'=>'order-region-f','id' => 'order_dropdown_region-f']) ?>

            <?= $formF->field($modelF, 'locality_id')->dropDownList([],['prompt'=>'Выберите нас. пункт','class'=>'order-locality']) ?>

            <?= $formF->field($modelF, 'address')->textarea(['rows' => 6]) ?>

            <?= $formF->field($modelF, 'delivery_id')->dropDownList($delivery_services,['prompt'=>'Выберите службу доставки']) ?>

            <?= $formF->field($modelF, 'comment')->textarea(['rows' => 6]) ?>

            <?= $formF->field($modelF, 'type')->hiddenInput()->label(false);?>

            </section>
        </fieldset>
        <footer>
            <?php echo $this->render('/order/_cart-list', ['dataProvider' => $cartProvider,'cart_total' => $cart_total]) ?>
            <?= Html::hiddenInput('open', 0) ?>
            <div class="buttons-block-ch">
                <div>
                    <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                    <div class="">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-footer']) ?>
                    </div>
                </div>
            </div>
        </footer>
        <?php ActiveForm::end() ?>
    </div>
</div>

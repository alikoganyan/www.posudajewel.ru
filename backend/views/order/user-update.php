<?php

/**
 * @var $this                \yii\web\View
 * @var $model               \common\models\Discount
 * @var $searchModel         \backend\models\DiscountSearch
 * @var $dataProvider        \backend\components\ActiveDataProvider
 */


use yii\helpers\Html;
use yii\helpers\StringHelper;
use backend\widgets\activeForm\ActiveForm;


$this->title = 'Заказы';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = 'Заказ №' . $model->id;
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?=$this->title?></h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form',
                ],
            ])?>
            <fieldset>

                <?php if ($permission = $model->getFieldPermission('comment')): ?>
                    <section>
                        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                    </section>
                <?php endif;?>

            </fieldset>

            <footer>
                <?php echo $this->render('/order/_cart-list', ['dataProvider' => $cartProvider,'cart_total' => $cart_total]) ?>

                <div class="buttons-block-ch">
                    <div>
                        <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                        <div class="btn-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-dropdown']) ?>
                            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><?= Html::a('Применить', '#', ['class' => 'service-save']) ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

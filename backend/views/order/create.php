<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = ['label' => 'Профиль пользователя', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;
//\yii\helpers\VarDumper::dump($modelF->type,10,true);die;
?>
<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?=$this->title?></h3>
    </header>
    <div class="person-class-block <?= $modelF->type !== null && $modelU->type !== null? 'hidden' : ''?>" id="person-class-block">
        <?= $this->render('_role-form', [
//            'model' => $model,
            'delivery_services' => $delivery_services,
            'cartProvider'  => $cartProvider,
            'cart_total' => $cart_total
        ]);
        ?>
    </div>
    <div class="individual-person <?= $modelF->type != 1 ? 'hidden' : ''?>">
        <?= $this->render('_form', [
            'modelF' => $modelF,
            'regions'=>$regions,
            'delivery_services' => $delivery_services,
            'cartProvider'  => $cartProvider,
            'cart_total' => $cart_total
        ]);
        ?>
    </div>
    <div class="law-person <?= $modelU->type != 2 ? 'hidden' : ''?>">
        <?= $this->render('_law-form', [
            'modelU' => $modelU,
            'regions'=>$regions,
            'delivery_services' => $delivery_services,
            'cartProvider'  => $cartProvider,
            'cart_total' => $cart_total
        ]);
        ?>
    </div>


</div>

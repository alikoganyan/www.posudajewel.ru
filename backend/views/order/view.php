<?php

use yii\helpers\Html;
use backend\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = 'Информация про заказ #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Профиль пользователя', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'fio',
            'phone',
            'email:email',
            'address:ntext',
            'comment:ntext',
            'delivery_cost',
            'total',
            'delivery_id',
            'status_id',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>

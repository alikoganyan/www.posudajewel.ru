<?php
/**
 * @var $this \yii\web\View
 * @var $settingsGroup \common\models\SettingsGroup[]
 */

use kartik\editable\Editable;
use kartik\popover\PopoverX;
use \backend\widgets\settingsField\SettingsField;

$this->title = 'Настройки сайта';
?>

<div id="settings" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?= $this->title ?></h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <th>Параметр</th>
                    <th>Значение</th>
                </thead>
                <?php foreach ($settingsGroup as $group): ?>
                    <tr class="group">
                        <td colspan="2"><?= $group->name ?></td>
                    </tr>
                    <?php foreach ($group->settings as $key => $setting): ?>
                        <tr>
                            <td><?= $setting->title ?></td>
                            <td>
                                <?
                                $editable = Editable::begin([
                                    'name' => '',
                                    'asPopover' => false,
                                    'displayValue' => 'Изменить',
                                    'valueIfNull' => 'Изменить',
                                    'size' => PopoverX::SIZE_LARGE,
                                    'placement' => PopoverX::ALIGN_BOTTOM,
                                    'submitOnEnter' => false,
                                    'ajaxSettings' => [
                                        'url' => '/admin/settings/save',
                                    ],
                                ]);
                                ?>
                                <?= SettingsField::widget([
                                    'setting' => $setting,
                                    'editable' => $editable,
                                ]) ?>
                                <?php Editable::end() ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php endforeach ?>
            </table>
        </div>
    </div>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $services \common\models\Service[]
 */
?>

<section>
    <div class="form-group">
        <label class="label" for="service-company_id">Клонировать услугу</label>
        <label class="select">
            <select class="form-control" onchange="service.clone(this)">
                <?php foreach ($services as $service): ?>
                    <option value="<?= $service->id ?>"><?= $service->name ?></option>
                <?php endforeach ?>
            </select><i></i>
        </label>
    </div>
</section>

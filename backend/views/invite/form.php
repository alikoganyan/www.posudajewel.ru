<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Invite
 */

use \backend\widgets\activeForm\ActiveForm;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'Приглашение';
$this->params['breadcrumbs'][] = ['label' => 'Приглашение', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новое Приглашение' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Приглашение</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'smart-form'
                ]
            ]) ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name', ['permission' => $permission])->label($model->getAttributeLabel('name')) ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('description')): ?>
                    <section>
                        <?= $form->field($model, 'description', ['permission' => $permission])->textarea(['rows' => 6])->label($model->getAttributeLabel('description')) ?>
                    </section>
                <?php endif ?>
            </fieldset>

            <footer>
                <?= FormFooter::widget() ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

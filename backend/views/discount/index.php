<?php

/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceSearch
 */
use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\html\HeaderLinkButton;

$this->title = 'Типы скидок';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid-service">
    <?php echo GridView::widget([
        'caption' => $this->title,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFilter' => true,
        'columns' => [
            'name',
            'discount:percent',
        ],
    ])?>
</div>

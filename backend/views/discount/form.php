<?php

/**
 * @var $this                \yii\web\View
 * @var $model               \common\models\Discount
 * @var $searchModel         \backend\models\DiscountSearch
 * @var $dataProvider        \backend\components\ActiveDataProvider
 */


use yii\helpers\Html;
use yii\helpers\StringHelper;
use backend\widgets\activeForm\ActiveForm;


$this->title = 'Типы скидок';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новая скидка' : 'Скидка: ' . $model->name;
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Предложение</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'discount-form',
                'enableAjaxValidation' => false,
                'options' => [
                    'class' => 'smart-form',
                ],
            ])?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name')->label('Наименнование скидки') ?>
                    </section>
                <?php endif ?>
                <?php if ($permission = $model->getFieldPermission('discount')): ?>
                    <section>
                        <?= $form->field($model, 'discount')->label('Скидка (%)');?>
                    </section>
                <?php endif ?>
            </fieldset>

            <footer>

                <?= Html::hiddenInput('open', 0) ?>

                <div class="buttons-block-ch">
                    <div>
                        <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                        <div class="btn-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-dropdown']) ?>
                            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><?= Html::a('Применить', '#', ['class' => 'service-save']) ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

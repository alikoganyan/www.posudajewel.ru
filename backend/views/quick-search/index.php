<?php
/**
 * @var $this \yii\web\View
 * @var $searchModel  \backend\models\QuickSearchSearch
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\DataColumn;

$this->title = 'Быстрый поиск';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'hideColumns'  => ['serviceTypeName'],
    'columns'      => [
        'name',
        'serviceTypeName',
    ],
]) ?>

<?php

/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\QuickSearch
 * @var $searchModel  \common\models\QuickSearchCharValue
 * @var $dataProvider \backend\components\ActiveDataProvider
 */
use \backend\widgets\activeForm\ActiveForm;
use \kartik\file\FileInput;
use \yii\helpers\Url;
use \yii\bootstrap\Html;
use \yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use \backend\widgets\formFooter\FormFooter;
use backend\widgets\editor\EditorWidget;

EditorWidget::widget(['id' => 'quicksearch-content']);

$this->title = 'Быстрый поиск';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый Поиск' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                        'id' => 'quick-search-form',
                        'options' => [
                            'class' => 'smart-form',
                            'enctype' => 'multipart/form-data',
                        ]
                    ])
            ?>
            <fieldset>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?=
                                $form->field($model, 'name', ['permission' => $permission])
                                ->label('Наименование ссылки')->textInput(['class' => 'url-name'])
                        ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('service_type_id')): ?>
                    <section>
                        <?=
                                $form->field($model, 'service_type_id', ['permission' => $permission])
                                ->dropDownList(['' => 'Выберите значение'] + $model->getServiceTypeList(), [
                                    'onmousedown' => 'saveServiceType(this)',
                                    'onchange' => "confirmChars(this)",
                                    'data-name' => StringHelper::basename(get_class($model)),
                                        ]
                                )
                                ->label($model->getAttributeLabel('service_type_id'))
                        ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('image')): ?>
                    <section>
                        <?=
                        $form->field($model, 'imageFile', ['enableAjaxValidation' => true])->widget(FileInput::className(), [
                            'pluginOptions' => [
                                'overwriteInitial' => false,
                                'autoReplace' => false,
                                'initialPreview' => $model->getImg(),
                                'initialPreviewConfig' => [['url' => Url::to(['quick-search/remove-image', 'id' => $model->id])]],
                                'initialPreviewAsData' => true,
                            ]
                        ])->label('Выберите картинку')
                        ?>
                    </section>
                <?php endif ?>

                <div id="chars">
                    <?php if ($permission = $model->getFieldPermission('chars') && !$model->isNewRecord): ?>
                        <?=
                        $this->render('//service/chars', [
                            'model' => $model,
                            'serviceType' => $model->serviceType,
                        ])
                        ?>
                    <?php endif ?>
                </div>

                <section>
                    <?= $form->field($model, 'url')->textInput(['class' => 'url-translit']) ?>
                    <?= Html::a('Получить адрес', '#', ['class' => 'create-url']); ?>
                </section>

                <?php if ($permission = $model->getFieldPermission('sitemap')): ?>
                    <section class="row form-checkbox">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'sitemap', ['permission' => $permission])->checkbox(['label' => 'Блокировать в sitemap.xml']) ?>
                        </div>
                    </section>
                <?php endif ?>

                <section>
                    <?= $form->field($model, 'title') ?>
                </section>

                <section>
                    <?= $form->field($model, 'keywords') ?>
                </section>

                <section>
                    <?= $form->field($model, 'description') ?>
                </section>

                <section>
                    <?= $form->field($model, 'content')->textarea(['rows' => 7]) ?>
                </section>

                <section class="row">
                    <div class="col col-md-12">
                        <?= $form->field($model, 'is_show')->checkbox(['label' => 'Вывести на главной странице']) ?>
                    </div>
                </section>
            </fieldset>

                <footer>
                    <?= FormFooter::widget() ?>
                </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

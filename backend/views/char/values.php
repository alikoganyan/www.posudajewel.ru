<?php
/**
 * @var $this        \yii\web\View
 * @var $char        \common\models\Char
 * @var $serviceType \common\models\ServiceType
 */

use \yii\helpers\Url;
use \backend\widgets\activeForm\ActiveForm;
?>

<label class="label">Значения</label>
<?php $form = ActiveForm::begin([
    'id'      => 'values-form',
    'action'  => Url::to(['service-type/char-value', 'id' => $char->id]),
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'smart-form'
    ]
]) ?>
    <?php foreach ($char->values as $key => $value): ?>
        <label class="checkbox">
            <input type="hidden" name="ServiceTypeChar[service_type_id]" value="<?= $serviceType->id ?>">
            <input type="hidden" name="ServiceTypeChar[char_id]" value="<?= $char->id ?>">

            <input type="<?= $char->is_multiple ? 'checkbox' : 'radio' ?>" value="<?= $value->id ?>" name="ServiceTypeChar[value_id][]"<?php if($key == 0): ?> checked<?php endif ?>>
            <i></i>
            <?= $value->name ?>
        </label>
    <?php endforeach ?>
<?php ActiveForm::end() ?>

<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \common\models\Char
 * @var $searchModel  \backend\models\ValueSearch
 * @var $dataProvider \backend\components\ActiveDataProvider
 */

use backend\widgets\activeForm\ActiveForm;
use backend\widgets\gridView\GridView;
use \backend\widgets\formFooter\FormFooter;

$this->title = 'Свойство';
$this->params['breadcrumbs'][] = ['label' => 'Свойства', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Добавить свойство' : $model->name;
?>

<div class="" id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;"><?= $this->title ?></h3>
    </header>

    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'char-form',
                'options' => [
                    'class' => 'smart-form'
                ]
            ]) ?>

            <fieldset>
            <section>
                <?php if ($permission = $model->getFieldPermission('name')): ?>
                    <section>
                        <?= $form->field($model, 'name', ['permission' => $permission])
                            ->label('Наименование свойства') ?>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('sort')): ?>
                    <section>
                        <?= $form->field($model, 'sort', ['permission' => $permission])
                            ->label('Сортировка') ?>
                    </section>
                <?php endif ?>
            </section>

            <section class="options">
                <?php if ($permission = $model->getFieldPermission('is_multiple')): ?>
                    <label class="label">Варианты использования</label>
                    <div class="form-group field-servicetype-is_mass">
                        <label class="checkbox">
                            <input type="radio" name="Char[is_multiple]" <?if($permission==1):?>disabled<?endif?>  value="0" <?if($model->is_multiple==0):?>checked<?endif?>>
                            <i></i>Одно значение
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="Char[is_multiple]" <?if($permission==1):?>disabled<?endif?>  value="1" <?if($model->is_multiple==1):?>checked<?endif?>>
                            <i></i>Мультивыбор
                        </label>
                    </div>
                <?php endif ?>
            </section>

                <?php if ($permission = $model->getFieldPermission('is_filter')): ?>
                    <section class="row">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'is_filter', ['permission' => $permission])->checkbox(['label' => 'Выводить в фильтр']) ?>
                        </div>
                    </section>
                <?php endif ?>

                <?php if ($permission = $model->getFieldPermission('is_show')): ?>
                    <section class="row">
                        <div class="col col-md-12">
                            <?= $form->field($model, 'is_show', ['permission' => $permission])->checkbox(['label' => 'Выводить в карточку товара']) ?>
                        </div>
                    </section>
                <?php endif ?>

            </fieldset>

            <section>
                <footer>
                    <?= FormFooter::widget([
                        'createButtonLabel' => $model->isNewRecord ? 'Создать' : 'Сохранить',
                    ]) ?>
                </footer>
            </section>
            <?php ActiveForm::end() ?>
        </div>

        <?php if (!$model->isNewRecord && ($permission = $model->getFieldPermission('grid'))): ?>
            <?= GridView::widget([
                'caption'      => 'Значения',
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'controller'   => Yii::$app->createControllerByID('value'),
                'addUrlParams' => ['char_id' => $model->id],
                'columns'      => [
                    'name',
                ]
            ]) ?>
        <?php endif ?>
    </div>
</div>

<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\User
 */

use \backend\widgets\activeForm\ActiveForm;
use \yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'id' => 'change-password-form',
    'enableAjaxValidation' => false,
    'validationUrl' => Url::toRoute(['validate']),
]) ?>
    <?= $form->field($model, 'email')->label(false)->hiddenInput() ?>
    <?= $form->field($model, 'id')->label(false)->hiddenInput() ?>

    <section>
        <?= $form->field($model, 'password')
                 ->passwordInput()
                 ->label($model->getAttributeLabel('password')) ?>
    </section>

    <section>
        <?= $form->field($model, 'repeat_password')
                 ->passwordInput()
                 ->label($model->getAttributeLabel('repeat_password')) ?>
    </section>
<?php ActiveForm::end() ?>

<?php
/**
 * @var $this \yii\web\View
 * @var $model               \common\models\User
 * @var $companyDataProvider \backend\components\ActiveDataProvider
 * @var $companySearchModel  \backend\models\CompanySearch
 */

use \backend\widgets\gridView\GridView;
?>

<?= GridView::widget([
    'caption'       => 'Компании',
    'dataProvider'  => $companyDataProvider,
    'filterModel'   => $companySearchModel,
    'controller'    => Yii::$app->createControllerByID('company'),
    'addUrlParams'  => ['userId' => $model->id],
    'columns'       => [
        'name',
    ],
]) ?>

<?php

/**
 * @var $this                \yii\web\View
 * @var $model               \common\models\User
 * @var $companyDataProvider \backend\components\ActiveDataProvider
 * @var $companySearchModel  \backend\models\CompanySearch
 */
use kartik\daterange\DateRangePicker;
use karpoff\icrop\CropImageUpload;
use backend\widgets\activeForm\ActiveForm;
use backend\widgets\formFooter\FormFooter;

$this->title = ($model->first_name && $model->last_name) ? $model->fullName : (($model->email) ? $model->email : 'Новый пользователь');

$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Пользователь</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>

        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                        'id' => 'user-form',
                        'enableAjaxValidation' => false,
                        'options' => [
                            'class' => 'smart-form',
                            'enctype' => 'multipart/form-data',
                        ],
                    ])
            ?>
            <fieldset>
                <div>
                    <section>
                        <?php if ($permission = $model->getFieldPermission('email')): ?>
                            <section>
                                <?=
                                        $form->field($model, 'email', ['permission' => $permission])
                                        ->label($model->getAttributeLabel('email'))
                                ?>

                                <?php if (!$model->isNewRecord): ?>
                                    <div id="password-form-modal">
                                        <a href="#" data-toggle="modal" data-target="#change-password-modal">Сменить пароль</a>
                                    </div>
                                <?php endif ?>
                            </section>
                        <?php endif ?>

                        <div>
                            <?php if ($model->isNewRecord): ?>
                                <?=
                                $this->render('password-form', [
                                    'model' => $model,
                                    'form' => $form,
                                ])
                                ?>
                            <?php endif ?>
                        </div>

                        <?php if ($permission = $model->getFieldPermission('role_id')): ?>
                            <section>
                                <?=
                                        $form->field($model, 'role_id', ['permission' => $permission])
                                        ->dropDownList($model->getRoleList())
                                        ->label($model->getAttributeLabel('role_id'))
                                ?>
                            </section>
                        <?php endif ?>

                        <div>
                            <?php if ($permission = $model->getFieldPermission('last_name')): ?>
                                <section>
                                    <?=
                                            $form->field($model, 'last_name', ['permission' => $permission])
                                            ->label($model->getAttributeLabel('last_name'))
                                    ?>
                                </section>
                            <?php endif ?>

                            <?php if ($permission = $model->getFieldPermission('first_name')): ?>
                                <section>
                                    <?=
                                            $form->field($model, 'first_name', ['permission' => $permission])
                                            ->label($model->getAttributeLabel('first_name'))
                                    ?>
                                </section>
                            <?php endif ?>

                            <?php if (!$model->isNewRecord && $permission = $discountModel->getFieldPermission('name')): ?>
                                <section>
                                    <?=$form->field($discountUserModel, 'discount_id', [
                                        'permission' => $permission
                                    ])->dropdownList($discountTypesList,[
                                        'prompt' => Yii::t('app', 'Выберите скидку для пользователя')
                                    ])->label('Тип скидки')?>
                                </section>
                            <?php endif ?>

                            <section>
                                <label class="label">Изображение</label>
                                <?=
                                        $form->field($model, 'image')->label(false)->
                                        widget(CropImageUpload::className(), ['clientOptions' => ['boxWidth' => 350, 'boxHeight' => 300]])
                                ?>
                            </section>

                            <?php if ($permission = $model->getFieldPermission('birthday')): ?>
                                <section>
                                    <div class="form-group input-group drp-container col-md-12">
                                        <label class="label">Дата рождения</label>
                                        <label class="input">
                                            <?=
                                            DateRangePicker::widget([
                                                'model' => $model,
                                                'attribute' => 'birthday',
                                                'useWithAddon' => true,
                                                'options' => [
                                                    'readonly' => '',
                                                ],
                                                'pluginOptions' => [
                                                    'singleDatePicker' => true,
                                                    'showDropdowns' => true,
                                                    'locale' => [
                                                        'format' => 'D.MM.Y',
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                        </label>
                                    </div>
                                </section>
                            <?php endif ?>

                            <?php if ($permission = $model->getFieldPermission('phone')): ?>
                                <section>
                                    <?= $form->field($model, 'phone')->label($model->getAttributeLabel('phone')) ?>
                                </section>
                            <?php endif ?>
                        </div>

                    </section>
                </div>

                <div>
                    <?php if ($permission = $model->getFieldPermission('vk_id')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'vk_id', ['permission' => $permission])
                                    ->label($model->getAttributeLabel('vk_id'))
                            ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('yandex')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'yandex', ['permission' => $permission])
                                    ->label($model->getAttributeLabel('yandex'))
                            ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('fb')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'fb', ['permission' => $permission])
                                    ->label($model->getAttributeLabel('fb'))
                            ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('google_id')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'google_id', ['permission' => $permission])
                                    ->label($model->getAttributeLabel('google_id'))
                            ?>
                        </section>
                    <?php endif ?>
                </div>
            </fieldset>

            <footer>
                <?php if ($permission = $model->getFieldPermission('is_blocked')): ?>
                    <div>
                        <?=
                                $form->field($model, 'is_blocked', ['permission' => $permission])
                                ->checkbox(['label' => $model->getAttributeLabel('is_blocked')])
                        ?>
                    </div>
                <?php endif ?>

                <?= FormFooter::widget() ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>

        <script>
            $(document).ready(function () {
                $('.page-size-button ul li a').click(function () {
                    $('.dataTables_length select').val($(this).attr('id')).change();
                });
                $('.smart-form .fa-eye').click(function () {
                    if ($(this).parent().find('input').attr('type') == 'password') {
                        $(this).parent().find('input').attr('type', 'text');
                    } else {
                        $(this).parent().find('input').attr('type', 'password');
                    }

                })
            })
        </script>
    </div>
</div>

<?= $this->render('modal_password'); ?>

<!-- end widget -->
<script type="text/javascript">
    pageSetUp();
</script>

<?php
/**
 * @var $this  \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel \backend\models\UserSearch
 */

use \backend\widgets\gridView\GridView;
use \backend\widgets\gridView\DataColumn;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'caption'      => $this->title,
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'hideColumns'  => ['fullName', 'roleName', 'birthday', 'phone', 'is_blocked'],
    'columns'      => [
        'email',
        'fullName',
        'roleName',
        [
            'class' => DataColumn::className(),
            'attribute' => 'birthday',
        ],
        'phone',
        [
            'class' => DataColumn::className(),
            'attribute' => 'is_blocked',
            'filter' => [0 => 'Нет', 1 => 'Да'],
            'value' => function ($model) {
                return $model->is_blocked ? 'Да' : 'Нет';
            },
        ],
    ],
]) ?>

<?php
/**
 * @var $this  \yii\web\View
 * @var $form  \backend\widgets\activeForm\ActiveForm
 * @var $model \common\models\User
 */
?>

<?php if ($permission = $model->getFieldPermission('password')): ?>
    <section>
        <?= $form->field($model, 'password', ['permission' => $permission])
                 ->passwordInput()
                 ->label($model->getAttributeLabel('password')) ?>
    </section>
<?php endif ?>

<?php if ($permission = $model->getFieldPermission('repeat_password')): ?>
    <section>
        <?= $form->field($model, 'repeat_password', ['permission' => $permission])
                 ->passwordInput()
                 ->label($model->getAttributeLabel('repeat_password')) ?>
    </section>
<?php endif ?>

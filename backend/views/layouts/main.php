<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use backend\widgets\header\Header;
use backend\widgets\sidebar\Sidebar;
use backend\widgets\notify\Notify;
use \common\models\Settings;

AppAsset::register($this);

if (Yii::$app->user->identity != null && Yii::$app->user->identity->panel_settings) {
    $panelSettings = Yii::$app->user->identity->panel_settings;

    $this->registerJs(<<<JS
        document.panelSettings = $panelSettings;
JS
            , View::POS_HEAD);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <?php $this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/png', 'href' => '/uploads/' . Settings::getSetting('favicon')]); ?>
        <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/uploads/' . Settings::getSetting('favicon')]); ?>

        <link rel="apple-touch-icon" href="/images/splash/sptouch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/splash/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/splash/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/splash/touch-icon-ipad-retina.png">

        <link rel="apple-touch-startup-image" href="/images/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
        <link rel="apple-touch-startup-image" href="/images/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="/images/splash/iphone.png" media="screen and (max-device-width: 320px)">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="smart-style-0">
        <?php $this->beginBody() ?>

        <?= Header::widget() ?>
        <?= Sidebar::widget() ?>

        <div id="main" role="main">
            <div id="ribbon">
                <span class="ribbon-button-alignment">
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Внимание! Это сбросить все настройки личного кабинета." data-html="true" data-reset-msg="Вы действительно хотите сбросить настройки личного кабинета?"><i class="fa fa-refresh"></i></span>
                </span>

                <?=
                Breadcrumbs::widget([
                    'homeLink' => false,
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
            </div>

            <div id="content">
                <?= $content ?>
            </div>
        </div>

        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?
//            <span class="txt-color-white">Активный пользователей на сайте: ххх <span class="hidden-xs"></span></span>
                    ?>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>


        <?=
        Notify::widget([
            'message' => Yii::$app->session->getFlash('notify'),
        ])
        ?>
        <script src="https://yastatic.net/browser-updater/v1/script.js" charset="utf-8"></script><script>var yaBrowserUpdater = new ya.browserUpdater.init({"lang": "ru", "browsers": {"yabrowser": "15.12", "chrome": "54", "ie": "10", "opera": "41", "safari": "8", "fx": "49", "iron": "35", "flock": "Infinity", "palemoon": "25", "camino": "Infinity", "maxthon": "4.5", "seamonkey": "2.3"}, "theme": "yellow"});</script>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

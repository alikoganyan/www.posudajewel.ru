<?php

/**
 * @var $this                \yii\web\View
 * @var $model               \common\models\Service
 * @var $optionsModel        \common\models\ServiceOption
 * @var $optionsDataProvider \backend\components\ActiveDataProvider
 * @var $image               \common\models\ServiceImage
 */
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\JsExpression;
use backend\widgets\activeForm\ActiveForm;
use mihaildev\elfinder\ElFinder;
use mihaildev\ckeditor\CKEditor;

$this->title = 'Предложения';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новое предложение' : $model->name;
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <?php if ($model->getCompaniesList() == null): ?>
        <h2>
            У вас нет ни одной компании. Для создания услуги нужно сперва
            <a href="<?= Url::to(['company/create']) ?>">создать компанию</a>
        </h2>
    <?php else: ?>
        <header>
            <h3 style="margin-top: 0; margin-bottom: 10px;">Предложение</h3>
        </header>
        <div>
            <div class="jarviswidget-editbox"></div>
            <div class="widget-body no-padding">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'service-form',
                            'enableAjaxValidation' => false,
                            'options' => [
                                'class' => 'smart-form',
                                'enctype' => 'multipart/form-data',
                            ],
                        ])
                ?>
                <fieldset>
                    <?= $form->field($model, 'id')->label(false)->hiddenInput() ?>

                    <?php if ($permission = $model->getFieldPermission('company_id')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'company_id', ['permission' => $permission])
                                    ->dropDownList(['' => 'Выберите значение'] + $model->getCompaniesList(), ['onchange' => 'service.getCloneHtml()'])
                                    ->label('Выберите компанию из списка')
                            ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('service_type_id')): ?>
                        <section>
                            <?=
                                    $form->field($model, 'service_type_id', ['permission' => $permission])
                                    ->dropDownList(['' => 'Выберите значение'] + $model->getServiceTypesList(), [
                                        'onmousedown' => 'saveServiceType(this)',
                                        'onchange' => "confirmChars(this)",
                                        'data-name' => StringHelper::basename(get_class($model)),
                                            ]
                                    )->label('Выберите категорию')
                            ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('name')): ?>
                        <section>
                            <?= $form->field($model, 'name', ['permission' => $permission])->label('Наименование предложения')->textInput(['class' => 'url-name']) ?>
                        </section>
                    <?php endif ?>

                    <section class="row">
                        <?php if ($permission = $model->getFieldPermission('code')): ?>
                            <section class="col col-6">
                                <?= $form->field($model, 'code', ['permission' => $permission]) ?>
                            </section>
                        <?php endif ?>
                        <?php if ($permission = $model->getFieldPermission('id_1c')): ?>
                            <section class="col col-6">
                                <?= $form->field($model, 'id_1c', ['permission' => $permission]) ?>
                            </section>
                        <?php endif ?>
                    </section>

                    <?php if ($permission = $model->getFieldPermission('is_popular')): ?>
                        <section class="row">
                            <div class="col col-md-12">
                                <?= $form->field($model, 'is_popular', ['permission' => $permission])->checkbox(['label' => 'Популярное предложение']) ?>
                            </div>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('is_new')): ?>
                        <section class="row">
                            <div class="col col-md-12">
                                <?= $form->field($model, 'is_new', ['permission' => $permission])->checkbox(['label' => 'Новое предложение']) ?>
                            </div>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('in_stock')): ?>
                        <section class="row">
                            <div class="col col-md-12">
                                <?= $form->field($model, 'in_stock', ['permission' => $permission])->checkbox(['label' => 'В наличии на складе']) ?>
                            </div>
                        </section>
                    <?php endif ?>

                    <div id="chars">
                        <?php if ($permission = $model->getFieldPermission('chars') && !$model->isNewRecord): ?>
                            <?=
                            $this->render('chars', [
                                'model' => $model,
                                'serviceType' => $model->serviceType,
                            ])
                            ?>
                        <?php endif ?>
                    </div>

                    <?php if ($permission = $model->getFieldPermission('description')): ?>
                        <section>
                            <?=
                                $form->field($model, 'description', ['permission' => $permission])
                                    ->label($model->getAttributeLabel('description'))
                                    ->widget(CKEditor::className(), [
                                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                                        'editorOptions' => ElFinder::ckeditorOptions(['elfinder'])
                                    ]),
                                ]);
                            ?>
                        </section>
                    <?php endif ?>

                    <div class="row">
                        <?php if ($permission = $model->getFieldPermission('price')): ?>
                            <section class="col col-5">
                                <?=
                                        $form->field($model, 'price', ['permission' => $permission])
                                        ->label('Розничная цена (руб.)')
                                ?>
                            </section>

                            <section class="col col-5">
                                <?=
                                $form->field($model, 'wholesale', ['permission' => $permission])
                                    ->label('Оптовая цена (руб.)')
                                ?>
                            </section>
                        <?php endif ?>

                        <?php if ($permission = $model->getFieldPermission('unit_id')): ?>
                            <section class="col col-2">
                                <?=
                                        $form->field($model, 'unit_id', ['permission' => $permission])
                                        ->dropDownList(['' => 'Выберите значение'] + $model->getUnitList())
                                        ->label($model->getAttributeLabel('unit_id'))
                                ?>
                            </section>
                        <?php endif ?>
                    </div>
                </fieldset>

                <section class="col col-md-12 options">
                    <?=
                    $this->render('//service/options', [
                        'model' => $model,
                    ])
                    ?>
                </section>

                <fieldset class="image">
                    <section>
                        <label class="label">Фотографии</label>
                        <div class="service-image-items">
                            <?php foreach ($model->images as $img): ?>
                                <?= $this->render('_image', ['img' => $img]); ?>
                            <?php endforeach; ?>
                        </div>
                        <div>
                            <?= $form->field($image, 'name')->label(false)->widget(\karpoff\icrop\CropImageUpload::className()) ?>
                            <div>
                                <a href="<?= Url::to(['image-upload']) ?>" class="btn btn-default service-image-save">Сохранить</a>
                                <a href="#" class="btn btn-default service-image-cancel">Отменить</a>
                            </div>
                        </div>
                        <br>
                    </section>
                </fieldset>
                <fieldset>

                    <?php if ($permission = $model->getFieldPermission('url')): ?>
                        <section>
                            <?= $form->field($model, 'url', ['permission' => $permission])->textInput(['class' => 'url-translit']) ?>
                            <?= Html::a('Получить адрес', '#', ['class' => 'create-url']); ?>
                        </section>
                    <?php endif ?>

                    <?php if ($permission = $model->getFieldPermission('sitemap')): ?>
                        <section class="row form-checkbox">
                            <div class="col col-md-12">
                                <?= $form->field($model, 'sitemap', ['permission' => $permission])->checkbox(['label' => 'Блокировать в sitemap.xml']) ?>
                            </div>
                        </section>
                    <?php endif ?>

                    <section>
                        <?= $form->field($model, 'meta_title')->label($model->getAttributeLabel('meta_title')) ?>
                    </section>

                    <section>
                        <?= $form->field($model, 'meta_description')->label($model->getAttributeLabel('meta_description')) ?>
                    </section>

                    <section>
                        <?= $form->field($model, 'meta_keywords')->label($model->getAttributeLabel('meta_keywords')) ?>
                    </section>

                    <section>
                        <label class="label">С этим товаром покупают</label>
                        <?=
                        Select2::widget([
                            'name' => "Service[buy]",
                            'data' => $serviceBuy,
                            'value' => $model->buy,
                            'options' => [
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['/service/search']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term, p: "code"}; }')
                                ],
                            ]
                        ])
                        ?>
                    </section>

                    <?php if ($permission = $model->getFieldPermission('sort')): ?>
                        <section>
                            <?= $form->field($model, 'sort')->label('Сортировка') ?>
                        </section>
                    <?php endif ?>
                </fieldset>

                <footer>
                    <?php if ($permission = $model->getFieldPermission('is_blocked')): ?>
                        <div>
                            <?= $form->field($model, 'is_blocked', ['permission' => $permission])->checkbox(['label' => $model->getAttributeLabel('is_blocked')]) ?>
                        </div>
                    <?php endif ?>

                    <?= Html::hiddenInput('open', 0) ?>

                    <div class="buttons-block-ch">
                        <div>
                            <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-default btn-footer']) ?>
                            <div class="btn-group">
                                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-dropdown']) ?>
                                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><?= Html::a('Применить', '#', ['class' => 'service-save']) ?></li>
                                    <li><?= Html::a('Открыть', $model->getUrl(), ['target' => '_blank']) ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    <?php endif ?>
</div>

<?php

use yii\helpers\Url;
?>
<div class="service-image">
    <img src="<?= $img->getUrlPrev() ?>">
    <a href="<?= Url::to(['image-remove', 'id' => $img->id]) ?>"><span class="fa fa-trash-o"></span></a>
    <?php if(is_null($img->service_id)): ?>
        <input type="hidden" name="Service[image][]" value="<?= $img->id ?>">
    <?php endif; ?>
</div>

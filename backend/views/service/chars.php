<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\models\Service
 * @var $serviceType \common\models\ServiceType
 * @var $permission []
 */
use yii\helpers\StringHelper;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

if (isset($model)) {
    $serviceType = $model->serviceType;
    $name = StringHelper::basename(get_class($model));
}
?>

<?php foreach ($serviceType->chars as $item): ?>
    <section>
        <label class="label"><?= Html::encode($item->name) ?></label>
        <?=
        Select2::widget([
            'name' => "{$name}[char][{$item->id}][]",
            'data' => ['' => ''] + ArrayHelper::map($item->values, 'id', 'name'),
            'value' => isset($model) ? ArrayHelper::getColumn($model->getCharValues($item->id), 'id') : null,
            'options' => [
                'multiple' => $item->is_multiple == 1,
            ],
        ])
        ?>
    </section>
<?php endforeach ?>

<?php

/**
 * @var $this         \yii\web\View
 * @var $dataProvider \backend\components\ActiveDataProvider
 * @var $searchModel  \backend\models\ServiceSearch
 */
use yii\helpers\Html;
use backend\widgets\gridView\GridView;
use backend\widgets\gridView\DataColumn;
use backend\widgets\gridView\html\HeaderLinkButton;

$this->title = 'Предложения';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid-service">
    <?=
    GridView::widget([
        'caption' => $this->title,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerButtons' => [
            new HeaderLinkButton('Удалить', ['remove'], 'grid-service-remove'),
            new HeaderLinkButton('Импорт', ['import']),
            new HeaderLinkButton('Экспорт', ['export']),
        ],
        'buttons' => ['update', 'copy', 'delete'],
        'hideColumns' => [
            'company.user.fullName', 'is_popular', 'serviceTypeName', 'companyName',
            'userName', 'price', 'code', 'is_image', 'in_stock', 'is_blocked',
            'time_create', 'time_update', 'user_update',
        ],
        'showFilter' => true,
        'columns' => [
            [
                'class' => DataColumn::className(),
                'content' => function($model) {
                    return '<label class="checkbox">' . Html::checkbox('grid_id[]', false, ['value' => $model->id]) . '<i></i></label>';
                },
                        'contentOptions' => ['style' => 'width: 40px;'],
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'name',
                        'contentOptions' => ['style' => 'max-width: 200px;']
                    ],
                    'serviceTypeName',
                    'companyName',
                    'userName',
                    'price',
                    'code',
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'is_image',
                        'label' => 'Фото',
                        'filter' => [0 => 'Нет', 1 => 'Да'],
                        'value' => function ($model) {
                            /** @var $model \common\models\Service */
                            return count($model->images) > 0 ? 'Да' : 'Нет';
                }
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'in_stock',
                        'label' => 'В наличии',
                        'filter' => [0 => 'Нет', 1 => 'Да'],
                        'value' => function ($model) {
                    return $model->in_stock ? 'Да' : 'Нет';
                }
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'is_popular',
                        'label' => 'Популярное',
                        'filter' => [0 => 'Нет', 1 => 'Да'],
                        'value' => function ($model) {
                    return $model->is_popular ? 'Да' : 'Нет';
                },
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'is_blocked',
                        'filter' => [0 => 'Нет', 1 => 'Да'],
                        'value' => function ($model) {
                    return $model->is_blocked ? 'Да' : 'Нет';
                },
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'time_create',
                        'value' => function ($model) {
                            return date('Y-m-d H:i:s', $model->time_create);
                        },
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'time_update',
                        'value' => function ($model) {
                            return date('Y-m-d H:i:s', $model->time_update);
                        },
                    ],
                    [
                        'class' => DataColumn::className(),
                        'attribute' => 'user_update',
                        'value' => function ($model) {
                            /** @var $model \common\models\Service */
                            $user = $model->userLastUpdated;
                            return $user ? $user->email : '';
                        },
                    ],
                ],
            ])
            ?>
</div>
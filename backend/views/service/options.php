<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\models\Service
 */
use yii\helpers\Html;
?>

<div class="service-option">
    <div class="option-list">
        <?php foreach ($model->optionGroups as $index => $group): ?>
            <div class="option-group" data-id="<?= $group->id ?>">
                <div class="option-group-title">
                    <span><?= Html::encode($group->name) ?></span>
                    <input value="<?= Html::encode($group->name) ?>" type="hidden" name="Service[groups][<?= $group->id ?>][name]"/>
                    <input value="<?= $group->is_multiple ? 0 : 1 ?>" type="hidden" name="Service[groups][<?= $group->id ?>][one]"/>
                    <div class="pull-right">
                        <a href="#" class="option-group-update">Изменить</a> | <a href="#" class="option-group-remove">Удалить</a>
                    </div>
                </div>

                <div class="option-items">
                    <?php foreach ($group->options as $key => $option): ?>
                        <?
                        $price = round($option->price, 2);
                        if ($price < 0) {
                            $sign = '-';
                            $price *= -1;
                        } else {
                            $sign = '+';
                        }
                        ?>
                        <div class="option-item" data-id="<?= $option->id ?>">
                            <span class="fa fa-remove option-item-remove"></span> <?= Html::encode($option->name) ?> (<?= $sign . $price ?> руб.) - (<?= $sign ?>) <?= $price ?> руб
                            <input value="<?= Html::encode($option->name) ?>" type="hidden" name="Service[groups][<?= $group->id ?>][options][<?= $option->id ?>][name]">
                            <input value="<?= round($option->price, 2) ?>" type="hidden" name="Service[groups][<?= $group->id ?>][options][<?= $option->id ?>][price]">
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="option-add">
                    <a href="#" class="option-item-add">Добавить новое условие...</a>
                </div>
            </div>
        <?php endforeach; ?>

        <div class="option-group option-group-form">
            <div class="option-add">
                <section>
                    <label class="input">
                        <input type="text" name="title" class="form-control" value="" placeholder="Введите название группы условий изменения стоимости">
                    </label>
                </section>
                <section>
                    <label class="checkbox">
                        <input type="checkbox" name="one" value="1" checked> Только одно значение
                        <i></i>
                    </label>
                </section>
                <div class="option-add-btn">
                    <button type="button" class="btn btn-default option-group-save">Сохранить</button>
                    <button type="button" class="btn btn-default option-group-close">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#" class="option-group-add">Добавить условия изменения стоимости...</a>
</div>

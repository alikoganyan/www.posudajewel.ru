<?php

use yii\helpers\Url;
use kartik\file\FileInput;
use backend\widgets\activeForm\ActiveForm;
use backend\widgets\formFooter\FormFooter;

$this->title = 'Предложения';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Экспорт';
?>

<div id="wid-id-1" data-widget-editbutton="false">
    <header>
        <h3 style="margin-top: 0; margin-bottom: 10px;">Экспорт</h3>
    </header>
    <div>
        <div class="jarviswidget-editbox"></div>
        <div class="widget-body no-padding">
            <?
            $form = ActiveForm::begin([
                        'id' => 'export-form',
                        'enableAjaxValidation' => false,
                        'options' => [
                            'class' => 'smart-form',
                            'enctype' => 'multipart/form-data',
                        ],
                    ])
            ?>
            <fieldset>
                <section>
                    <label class="input">
                        <?= $form->field($model, 'file')->fileInput() ?>
                    </label>
                </section>

                <section>
                    <?= $form->field($model, 'start') ?>
                </section>

                <section>
                    <?= $form->field($model, 'end') ?>
                </section>
            </fieldset>

            <footer>
                <?= FormFooter::widget() ?>
            </footer>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

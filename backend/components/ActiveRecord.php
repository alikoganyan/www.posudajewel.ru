<?php
namespace backend\components;

use common\models\Entity;
use common\models\User;
use yii\helpers\Json;

class ActiveRecord extends \yii\db\ActiveRecord {
    public function getFieldPermission($fieldName) {
        $ar = explode('\\', self::className());
        $className = strtolower(array_pop($ar));

        if (!isset(Entity::$collection[$className]['fields'])) {
            return false;
        }

        /** @var User $user */
        $user = \Yii::$app->user->identity;
        $permissions = Json::decode($user->role->permissions);

        if (!$permissions || !isset($permissions[$className][$fieldName])) {
            return false;
        }

        return $permissions[$className][$fieldName];
    }
}

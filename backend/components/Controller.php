<?php
namespace backend\components;

use Yii;
use common\models\User;
use yii\web\ForbiddenHttpException;

class Controller extends \common\components\Controller {
    protected $allowUserControllers = [
        'user',
        'company',
        'service',
        'region',
        'locality',
        'profile',
        'html',
    ];

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action) {
        if (!Yii::$app->user->isGuest && ($user = Yii::$app->user->identity)) {
            /** @var User $user */
            $controllerId = strtolower($action->controller->id);

            if (!$user->isActivated()) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }

            $permissions = $user->role->getParsedPermissions();
            $entityName = strtolower(str_replace('-', '', $controllerId));

            if ($controllerId != 'site' && !in_array($controllerId, $this->allowUserControllers)) {
                $deny = false;
                if (isset($permissions[$entityName])) {
                    foreach ($permissions[$entityName] as $field) {
                        if ($field > 0) {
                            $deny = true;
                            break;
                        }
                    }
                }

                if (!$deny && $user->role_id != 5) {
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }

            return parent::beforeAction($action);
        } else {
            $this->redirect('/', 302);
            return false;
        }
    }
}

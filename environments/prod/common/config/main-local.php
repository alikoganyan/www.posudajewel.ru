<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=admin_weekend',
            'username' => 'admin_root',
            'password' => '5T1UTR1l1d',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
    ],
];

Как развернуть проект
===============================

1. Склонировать проект
2. Выполнить composer install (если его нет - http://google.com)
3. Выполнить команду php init из корня проекта и выбрать DEV-режим
4. Применить миграции
5. Так же не забыть про включение коротких тегов

Перед запуском миграций сперва следует выполнить эту:
--
```
yii migrate --migrationPath=@yii/log/migrations/
```

Для работы корзины следуте применить эту миграцию:
--
```
yii migrate --migrationPath=@hscstudio/cart/migrations
```

Apache config
-------------------

```
<VirtualHost *:80 *:8080>
ServerName weekend.loc
ServerAdmin webmaster@localhost
DocumentRoot /var/www/development.e-ccs.ru
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined

RewriteEngine on
    # the main rewrite rule for the frontend application
    RewriteCond %{REQUEST_URI} !^/(backend/web|admin)
    RewriteRule !^/frontend/web /frontend/web%{REQUEST_URI} [L]
    # redirect to the page without a trailing slash (uncomment if necessary)
    #RewriteCond %{REQUEST_URI} ^/admin/$
    #RewriteRule ^(/admin)/ $1 [L,R=301]
    # disable the trailing slash redirect
    RewriteCond %{REQUEST_URI} ^/admin$
    RewriteRule ^/admin /backend/web/index.php [L]
    # the main rewrite rule for the backend application
    RewriteCond %{REQUEST_URI} ^/admin
    RewriteRule ^/admin(.*) /backend/web$1 [L]

<Directory "/var/www/development.e-ccs.ru/">
   Order allow,deny
   Allow from all
   # New directive needed in Apache 2.4.3: 
   Require all granted
</Directory>

<Directory /var/www/development.e-ccs.ru/frontend/web>
        RewriteEngine on
        # if a directory or a file exists, use the request directly
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        # otherwise forward the request to index.php
        RewriteRule . index.php

        Order Allow,Deny
        Allow from all
    </Directory>
    <Directory /var/www/development.e-ccs.ru/backend/web/>
        RewriteEngine on
        # if a directory or a file exists, use the request directly
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        # otherwise forward the request to index.php
        RewriteRule . index.php

        Order Allow,Deny
        Allow from all
    </Directory>

</VirtualHost>
```

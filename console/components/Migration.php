<?php
namespace console\components;

class Migration extends \yii\db\Migration {
    protected $dbName;

    public function init() {
        parent::init();

        $this->dbName = $this->db->createCommand("SELECT DATABASE()")->queryScalar();
    }

    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null) {
        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, 'CASCADE', $update);
    }

    public function dropColumn($table, $column) {
        echo "    > drop column $column from table $table ...";
        if ($this->isColumnExist($table, $column)) {
            $time = microtime(true);
            $this->db->createCommand()->dropColumn($table, $column)->execute();
            echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
        }
        else {
            echo "column $column does not exist in table $table\n";
        }
    }

    public function dropForeignKey($name, $table) {
        echo "    > drop foreign key $name from table $table ...";
        if ($this->isForeignKeyExist($table, $name)) {
            $time = microtime(true);
            $this->db->createCommand()->dropForeignKey($name, $table)->execute();
            echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
        }
        else {
            echo "foreign key $name does not exist in table $table\n";
        }
    }

    protected function isForeignKeyExist($table, $fkName) {
        $query = "SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = DATABASE() AND TABLE_NAME = '$table' AND CONSTRAINT_NAME = '$fkName' AND CONSTRAINT_TYPE = 'FOREIGN KEY'";

        return count($this->db->createCommand($query)->queryAll()) > 0;
    }

    protected function isColumnExist($table, $column) {
        $query = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = '$table' AND COLUMN_NAME = '$column'";

        return count($this->db->createCommand($query)->queryAll()) > 0;
    }
}

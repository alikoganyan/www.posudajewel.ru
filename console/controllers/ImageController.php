<?php

namespace console\controllers;

use Yii;
use common\models\ServiceImage;
use common\components\ImageHelper;

/**
 * Создание превью для старых изображений
 */
class ImageController extends \yii\console\Controller {

    /**
     * 
     */
    public function actionIndex() {
        $images = ServiceImage::find()->all();
        $path = ServiceImage::getPath();

        foreach ($images as $img) {
            echo "id={$img->id}\n";
            
            $source = $path . '/' . $img->name;
            $thumb = $path . '/thumb_' . $img->name;

            if (file_exists($source) && !file_exists($thumb)) {
                ImageHelper::thumbnail($source, $thumb, 590, 590, ImageHelper::RESIZE_PROPORTION);
            }
        }
    }

}

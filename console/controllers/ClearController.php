<?php

namespace console\controllers;

use Yii;
use common\models\Catalog;
use common\models\CatalogLink;
use common\models\Company;
use common\models\Locality;
use common\models\QuickSearch;
use common\models\QuickSearchCharValue;
use common\models\Char;
use common\models\CharValue;
use common\models\Value;
use common\models\User;
use common\models\Unit;
use common\models\Region;
use common\models\Service;
use common\models\ServiceBuy;
use common\models\ServiceType;
use common\models\ServiceImage;
use common\models\ServiceCharValue;
use common\models\ServiceOption;
use common\models\ServiceOptionGroup;
use common\models\ServiceTypeChar;

/**
 * Очистка данных
 */
class ClearController extends \yii\console\Controller {

    /**
     * 
     */
    public function actionIndex() {
        // Каталог
        $catalog = Catalog::find()->select(['id'])->column();
        Catalog::deleteAll(['AND', 'parent_id IS NOT NULL', ['NOT IN', 'parent_id', $catalog]]);
        CatalogLink::deleteAll(['AND', ['NOT IN', 'parent_id', $catalog]]);


        // Нас. пункты
        $localities = Locality::find()->select(['id'])->column();
        Company::deleteAll(['AND', ['NOT IN', 'locality_id', $localities]]);


        // Пользователи
        $users = User::find()->select(['id'])->column();
        Company::deleteAll(['AND', ['NOT IN', 'user_id', $users]]);
        Service::updateAll(['user_update' => null], ['AND', ['NOT IN', 'user_update', $users]]);


        // Быстрый поиск
        $search = QuickSearch::find()->select(['id'])->column();
        CatalogLink::deleteAll(['AND', ['NOT IN', 'search_id', $search]]);
        QuickSearchCharValue::deleteAll(['AND', ['NOT IN', 'quick_search_id', $search]]);


        // Свойства 
        $char = Char::find()->select(['id'])->column();
        CharValue::deleteAll(['AND', ['NOT IN', 'char_id', $char]]);
        QuickSearchCharValue::deleteAll(['AND', ['NOT IN', 'char_id', $char]]);
        ServiceCharValue::deleteAll(['AND', ['NOT IN', 'char_id', $char]]);
        ServiceTypeChar::deleteAll(['AND', ['NOT IN', 'char_id', $char]]);


        // Значения свойств 
        $value = Value::find()->select(['id'])->column();
        CharValue::deleteAll(['AND', ['NOT IN', 'value_id', $value]]);
        QuickSearchCharValue::deleteAll(['AND', ['NOT IN', 'value_id', $value]]);
        ServiceCharValue::deleteAll(['AND', ['NOT IN', 'value_id', $value]]);


        // Значения свойств 
        $region = Region::find()->select(['id'])->column();
        Locality::deleteAll(['AND', ['NOT IN', 'region_id', $region]]);


        // Категории
        $serviceType = ServiceType::find()->select(['id'])->column();
        QuickSearch::deleteAll(['AND', ['NOT IN', 'service_type_id', $serviceType]]);
        Service::deleteAll(['AND', ['NOT IN', 'service_type_id', $serviceType]]);
        ServiceTypeChar::deleteAll(['AND', ['NOT IN', 'service_type_id', $serviceType]]);


        // Предложения
        $service = Service::find()->select(['id'])->column();
        ServiceBuy::deleteAll(['OR', ['NOT IN', 'service_id', $service], ['NOT IN', 'buy_id', $service]]);
        ServiceCharValue::deleteAll(['AND', ['NOT IN', 'service_id', $service]]);
        ServiceImage::deleteAll(['AND', ['NOT IN', 'service_id', $service]]);
        ServiceOptionGroup::deleteAll(['AND', ['NOT IN', 'service_id', $service]]);


        // Категории
        $company = Company::find()->select(['id'])->column();
        Service::deleteAll(['AND', ['NOT IN', 'company_id', $company]]);


        // Ед. измерения
        $unit = Unit::find()->select(['id'])->column();
        Service::updateAll(['unit_id' => null], ['AND', ['NOT IN', 'unit_id', $unit]]);


        // Дополнительные параметры
        $optionGroup = ServiceOptionGroup::find()->select(['id'])->column();
        ServiceOption::deleteAll(['AND', ['NOT IN', 'group_id', $optionGroup]]);



        echo "ok\n";
    }

}

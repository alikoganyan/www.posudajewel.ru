<?php
use \console\components\Migration;

class m170205_105836_regions extends Migration {
    public function safeUp() {
        $this->createTable('region', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string()->notNull(),
            'code'      => $this->string()->notNull(),
            'longitude' => $this->integer()->notNull(),
            'latitude'  => $this->integer()->notNull(),
        ]);

        $this->createTable('locality', [
            'id'          => $this->primaryKey(),
            'region_id'   => $this->integer(),
            'name'        => $this->string(),
            'longitude'   => $this->string(),
            'latitude'    => $this->string(),
            'gismeteo_id' => $this->integer(),
        ]);
        $this->createIndex('region', 'locality', ['region_id']);
    }

    public function safeDown() {
        $this->dropTable('locality');
        $this->dropTable('region');
    }
}

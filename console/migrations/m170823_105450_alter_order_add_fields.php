<?php

use yii\db\Migration;

class m170823_105450_alter_order_add_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'notification', $this->boolean());
        $this->addColumn('order', 'postcode', $this->integer(10));
        $this->addColumn('order', 'region_id', $this->integer()->notNull());
        $this->addColumn('order', 'locality_id', $this->integer()->notNull());

        $this->addColumn('order', 'organization', $this->char(255));
        $this->addColumn('order', 'inn', $this->char(255));
        $this->addColumn('order', 'kpp', $this->char(255));
        $this->addColumn('order', 'law_address', $this->char(255));
        $this->addColumn('order', 'actual_address', $this->char(255));
        $this->addColumn('order', 'account_number', $this->bigInteger());
        $this->addColumn('order', 'bank', $this->char(255));
        $this->addColumn('order', 'corr_number', $this->bigInteger());
        $this->addColumn('order', 'bik', $this->integer());
        $this->addColumn('order', 'bank_city', $this->char(255));
        $this->createIndex('region', 'order', 'region_id');
        $this->createIndex('locality', 'order', 'locality_id');
        $this->addForeignKey(
            'fk-region_id',
            'order',
            'region_id',
            'region',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-locality_id',
            'order',
            'locality_id',
            'locality',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'notification', $this->boolean());
        $this->dropColumn('order', 'postcode', $this->integer(10));
        $this->dropColumn('order', 'region_id', $this->integer()->notNull());
        $this->dropColumn('order', 'locality_id', $this->integer()->notNull());

        $this->dropColumn('order', 'organization', $this->char(255));
        $this->dropColumn('order', 'inn', $this->char(255));
        $this->dropColumn('order', 'kpp', $this->char(255));
        $this->dropColumn('order', 'law_address', $this->char(255));
        $this->dropColumn('order', 'actual_address', $this->char(255));
        $this->dropColumn('order', 'account_number', $this->bigInteger());
        $this->dropColumn('order', 'bank', $this->char(255));
        $this->dropColumn('order', 'corr_number', $this->bigInteger());
        $this->dropColumn('order', 'bik', $this->integer());
        $this->dropColumn('order', 'bank_city', $this->char(255));

        $this->dropForeignKey(
            'fk-region_id',
            'order'
        );

        $this->dropForeignKey(
            'fk-locality_id',
            'order'
        );
    }
}

<?php

use yii\db\Migration;

class m170608_094216_update_column_price extends Migration {

    public function safeUp() {
        $this->alterColumn('service', 'price', $this->decimal(10, 2)->comment('Стоимость'));
    }

    public function safeDown() {
        $this->alterColumn('service', 'price', $this->integer());
    }

}

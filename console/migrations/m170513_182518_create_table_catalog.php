<?php

use yii\db\Migration;

class m170513_182518_create_table_catalog extends Migration {

    public function safeUp() {
        $this->createTable('catalog', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(11)->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->integer(11),
        ]);
        $this->createTable('catalog_link', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(11),
            'search_id' => $this->integer(11),
            'sort' => $this->integer(11),
        ]);
    }

    public function safeDown() {
        $this->dropTable('catalog');
        $this->dropTable('catalog_link');
    }

}

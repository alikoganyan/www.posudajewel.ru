<?php

class m170422_140316_alter_options extends \console\components\Migration {
    public function safeUp() {
        $this->createTable('service_option_group', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer(),
            'name' => $this->string(),
            'is_multiple' => $this->integer(1)->defaultValue(0),
        ]);

        $this->addColumn('service_option', 'group_id', $this->integer());
        $this->dropColumn('service_option', 'service_id');
    }

    public function safeDown() {
        $this->dropColumn('service_option', 'group_id');
        $this->dropTable('service_option_group');
        $this->addColumn('service_option', 'service_id', $this->integer());
    }
}

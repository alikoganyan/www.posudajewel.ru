<?php

use yii\db\Migration;

class m170608_102410_create_table_page extends Migration {

    public function safeUp() {

        $this->createTable('page', [
            'id' => $this->primaryKey()->comment('Id'),
            'name' => $this->string(255)->notNull()->comment('Наименование'),
            'url' => $this->string(255)->notNull()->comment('Url'),
            'content' => $this->text()->comment('Описание'),
            'is_footer' => $this->smallInteger()->defaultValue(0)->comment('Вывод в футере'),
            'is_blocked' => $this->smallInteger()->defaultValue(0)->comment('Блокировка'),
            'time_create' => $this->integer(11)->defaultValue(0)->comment('Время добавления'),
            'time_update' => $this->integer(11)->defaultValue(0)->comment('Время редактирования'),
            'user_create' => $this->integer(11)->defaultValue(null)->comment('Кто добавил'),
            'user_update' => $this->integer(11)->defaultValue(null)->comment('Кто изменил'),
        ]);
        
        $this->createIndex('user_create', 'page', 'user_create');
        $this->addForeignKey('page_user_create_fk', 'page', 'user_create', 'user', 'id', 'SET NULL', 'SET NULL');
        
        $this->createIndex('user_update', 'page', 'user_update');
        $this->addForeignKey('page_user_update_fk', 'page', 'user_update', 'user', 'id', 'SET NULL', 'SET NULL');
    }

    public function safeDown() {
        $this->dropTable('page');
    }

}

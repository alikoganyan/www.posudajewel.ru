<?php

class m170506_144952_add_service_meta_title extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('service', 'meta_title', $this->string());
    }

    public function safeDown() {
        $this->dropColumn('service', 'meta_title');
    }
}

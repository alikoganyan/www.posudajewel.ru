<?php

use yii\db\Migration;

class m170526_061019_add_catalog_image extends Migration {

    public function safeUp() {
        $this->addColumn('catalog', 'image', $this->string());
        $this->addColumn('catalog', 'image_crop', $this->string());
        $this->addColumn('catalog', 'service_id', $this->integer());
    }

    public function safeDown() {
        $this->dropColumn('catalog', 'image');
        $this->dropColumn('catalog', 'image_crop');
        $this->dropColumn('catalog', 'service_id');
    }

}

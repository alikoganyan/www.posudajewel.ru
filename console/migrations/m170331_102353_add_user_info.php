<?php

class m170331_102353_add_user_info extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('user', 'gender', $this->string());
        $this->addColumn('user', 'vk_id', $this->integer());
        $this->alterColumn('user', 'email', $this->string()->null());
    }

    public function safeDown() {
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'vk_id');
        $this->alterColumn('user', 'email', $this->string()->notNull());
    }
}

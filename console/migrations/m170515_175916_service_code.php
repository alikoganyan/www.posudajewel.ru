<?php

use yii\db\Migration;

class m170515_175916_service_code extends Migration {

    public function safeUp() {
        $this->addColumn('service', 'in_stock', $this->integer(4)->defaultValue(0));
        $this->addColumn('service', 'code', $this->string(255));
    }

    public function safeDown() {
        $this->dropColumn('service', 'in_stock');
        $this->dropColumn('service', 'code');
    }

}

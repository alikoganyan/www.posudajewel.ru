<?php

class m170304_202337_alter_company_geo extends \console\components\Migration {
    public function up() {
        $this->alterColumn('company', 'longitude', $this->string());
        $this->alterColumn('company', 'latitude', $this->string());
    }

    public function down() {
        $this->alterColumn('company', 'longitude', $this->integer());
        $this->alterColumn('company', 'latitude', $this->integer());
    }
}

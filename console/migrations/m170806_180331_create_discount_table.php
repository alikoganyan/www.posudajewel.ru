<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount`.
 */
class m170806_180331_create_discount_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('discount', [
            'id'       => $this->primaryKey(),
            'discount' => $this->float()->null(),
            'name'     => $this->string()->notNull(),
        ]);
        $this->createTable('discount_to_user', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer()->notNull(),
            'discount_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('user', 'discount_to_user', 'user_id');
        $this->createIndex('discount', 'discount_to_user', 'discount_id');
        // name, table, columns, refTable, refColumns, delete, update
        $this->addForeignKey(
            'fk-doscount_to_user-user_id',
            'discount_to_user',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-doscount_to_user-discount_id',
            'discount_to_user',
            'discount_id',
            'discount',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropForeignKey(
            'fk-doscount_to_user-discount_id',
            'discount_to_user'
        );
        $this->dropForeignKey(
            'fk-doscount_to_user-user_id',
            'discount_to_user'
        );
        $this->dropIndex(
            'user',
            'discount_to_user'
        );
        $this->dropIndex(
            'discount',
            'discount_to_user'
        );

        $this->dropTable('discount');
        $this->dropTable('discount_to_user');
    }
}

<?php

use yii\db\Migration;

class m170512_183352_quick_search_main extends Migration {

    public function safeUp() {
        $this->addColumn('quick_search', 'is_show', $this->integer(4)->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('quick_search', 'is_show');
    }

}

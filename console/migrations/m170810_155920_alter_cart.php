<?php

use yii\db\Migration;

/**
 * Class m170810_155920_alter_cart
 */
class m170810_155920_alter_cart extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addColumn('cart', 'created_at', $this->dateTime());
        $this->addColumn('cart', 'updated_at', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        $this->dropColumn('cart', 'created_at', $this->dateTime());
        $this->dropColumn('cart', 'updated_at', $this->dateTime());
    }

}

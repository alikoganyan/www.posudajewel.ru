<?php
use \console\components\Migration;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id'                   => $this->primaryKey(),
            'first_name'           => $this->string()->notNull(),
            'last_name'            => $this->string()->notNull(),
            'email'                => $this->string()->notNull()->unique(),
            'birthday'             => $this->date(),
            'phone'                => $this->string(),
            'auth_key'             => $this->string(32),
            'password_hash'        => $this->string(),
            'password_reset_token' => $this->string()->unique(),

            'ok'                   => $this->string(),
            'vk'                   => $this->string(),
            'fb'                   => $this->string(),
            'mail_ru'              => $this->string(),
            'yandex'               => $this->string(),
            'img'                  => $this->string(),

            'is_active'            => $this->integer()->defaultValue(1),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);
        $this->createIndex('active', 'user', ['is_active']);

        $this->insert('user', [
            'email'         => 'test@mail.ru',
            'password_hash' => Yii::$app->security->generatePasswordHash('123'),
            'first_name'    => 'test_first_name',
            'last_name'     => 'test_last_name',
            'created_at'    => microtime(true),
            'updated_at'    => microtime(true),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}

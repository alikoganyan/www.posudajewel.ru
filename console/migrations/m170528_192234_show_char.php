<?php

use yii\db\Migration;

class m170528_192234_show_char extends Migration {

    public function safeUp() {
        $this->addColumn('char', 'is_show', $this->smallInteger()->defaultValue(1));
        $this->addColumn('char', 'sort', $this->integer());
    }

    public function safeDown() {
        $this->dropColumn('char', 'is_show');
        $this->dropColumn('char', 'sort');
    }

}

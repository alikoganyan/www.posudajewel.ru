<?php

class m170417_121823_drop_is_deleted extends \console\components\Migration {
    public function safeUp() {
        $this->dropColumn('service', 'is_deleted');
        $this->dropColumn('char', 'is_deleted');
        $this->dropColumn('char_value', 'is_deleted');
        $this->dropColumn('company', 'is_deleted');
        $this->dropColumn('invite', 'is_deleted');
        $this->dropColumn('invite_user', 'is_deleted');
        $this->dropColumn('locality', 'is_deleted');
        $this->dropColumn('region', 'is_deleted');
        $this->dropColumn('user', 'is_deleted');
        $this->dropColumn('quick_search', 'is_deleted');
        $this->dropColumn('quick_search_char_value', 'is_deleted');
        $this->dropColumn('role', 'is_deleted');
        $this->dropColumn('service_char_value', 'is_deleted');
        $this->dropColumn('service_option', 'is_deleted');
        $this->dropColumn('service_image', 'is_deleted');
        $this->dropColumn('service_type_char', 'is_deleted');
        $this->dropColumn('service_type', 'is_deleted');
        $this->dropColumn('settings', 'is_deleted');
        $this->dropColumn('value', 'is_deleted');

        $this->dropForeignKey('F4', 'service');
    }

    public function safeDown() {
        $this->addColumn('service', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('char', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('char_value', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('company', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('invite', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('invite_user', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('locality', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('region', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('user', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('quick_search', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('quick_search_char_value', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('role', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('service_char_value', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('service_option', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('service_image', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('service_type_char', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('service_type', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('settings', 'is_deleted', $this->integer(1)->defaultValue(0));
        $this->addColumn('value', 'is_deleted', $this->integer(1)->defaultValue(0));

        $this->addForeignKey('F4', 'service', ['service_type_id'], 'service_type', ['id']);
    }
}

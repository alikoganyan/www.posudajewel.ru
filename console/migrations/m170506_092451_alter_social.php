<?php

class m170506_092451_alter_social extends \console\components\Migration {
    public function safeUp() {
        $this->dropColumn('user', 'ok');
        $this->dropColumn('user', 'vk');
        $this->dropColumn('user', 'mail_ru');

        $this->alterColumn('user', 'google_id', $this->string());
    }

    public function safeDown() {
        $this->addColumn('user', 'ok', $this->string());
        $this->addColumn('user', 'vk', $this->string());
        $this->addColumn('user', 'mail_ru', $this->string());

        $this->alterColumn('user', 'google_id', $this->integer());
    }
}

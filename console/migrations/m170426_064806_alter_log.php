<?php

class m170426_064806_alter_log extends \console\components\Migration {
    public function safeUp() {
        $this->dropColumn('log', 'log_time');
        $this->addColumn('log', 'date', $this->dateTime());
        $this->addColumn('log', 'url', $this->string());

        $this->createIndex('date', 'log', ['date']);
    }

    public function safeDown() {
        $this->addColumn('log', 'log_time', $this->double()->null());
        $this->dropColumn('log', 'date');
        $this->dropColumn('log', 'url');
    }
}

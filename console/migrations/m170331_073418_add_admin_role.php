<?php

use \yii\helpers\Json;
use \common\models\Entity;

class m170331_073418_add_admin_role extends \console\components\Migration {
    public function up() {
        $permissions = [];

        foreach (Entity::$collection as $key => $entity) {
            $item = [];

            foreach ($entity['fields'] as $k => $v) {
                $item[$k] = 2;
            }

            $permissions[$key] = $item;
        }

        $this->insert('role', ['name' => 'Администратор', 'permissions' => Json::encode($permissions)]);
    }

    public function down() {
        $this->delete('role', ['name' => 'Администратор']);
    }
}

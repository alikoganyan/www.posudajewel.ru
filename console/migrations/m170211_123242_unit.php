<?php

class m170211_123242_unit extends \console\components\Migration {
    public function safeUp() {
        $this->createTable('unit', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(),
            'code'        => $this->integer(),
            'designation' => $this->text(),
        ]);
    }

    public function safeDown() {
        $this->dropTable('unit');
    }
}

<?php

use yii\db\Migration;

class m170505_073917_image_crop extends Migration {

    public function safeUp() {
        $this->addColumn('service_image', 'name_crop', $this->string(255));

        \Yii::$app->db->createCommand()->
                update('service_image', ['name_crop' => new \yii\db\Expression('name')])->
                execute();
    }

    public function safeDown() {
        $this->dropColumn('service_image', 'name_crop');
    }

}

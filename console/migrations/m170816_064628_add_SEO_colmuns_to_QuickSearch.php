<?php

use yii\db\Migration;

class m170816_064628_add_SEO_colmuns_to_QuickSearch extends Migration
{
    public function safeUp()
    {
        $this->addColumn('quick_search', 'sitemap', $this->boolean()->null()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('quick_search', 'sitemap');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170816_064628_add_SEO_colmuns_to_QuickSearch cannot be reverted.\n";

        return false;
    }
    */
}

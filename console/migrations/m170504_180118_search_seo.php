<?php

use yii\db\Migration;

class m170504_180118_search_seo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('quick_search', 'url', $this->string(255));
        $this->addColumn('quick_search', 'title', $this->string(255));
        $this->addColumn('quick_search', 'keywords', $this->string(255));
        $this->addColumn('quick_search', 'description', $this->string(500));
        $this->addColumn('quick_search', 'content', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('quick_search', 'url');
        $this->dropColumn('quick_search', 'title');
        $this->dropColumn('quick_search', 'keywords');
        $this->dropColumn('quick_search', 'description');
        $this->dropColumn('quick_search', 'content');
    }
}

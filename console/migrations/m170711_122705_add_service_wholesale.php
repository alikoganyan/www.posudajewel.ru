<?php
use yii\db\Migration;

class m170711_122705_add_service_wholesale extends Migration {
    public function safeUp() {
        $this->addColumn('service', 'wholesale', $this->decimal(10, 2));
    }

    public function safeDown() {
        $this->dropColumn('service', 'wholesale');
    }
}

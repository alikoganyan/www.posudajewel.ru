<?php

use yii\db\Migration;
use yii\db\Query;

class m170729_225652_insert_settings_and_settings_group_a_row extends Migration
{
    public function safeUp()
    {
        $order = (new Query())->from('settings_group')->max('`order`');

        $this->insert('settings_group', [
            'name' => 'Регистрация',
            'order' => $order+1,
        ]);

        $group_id = (new Query())->from('settings_group')->max('`id`');

        $this->insert('settings', [
            'key' => 'condition_text',
            'value' => 'User agreement content',
            'group_id' => $group_id,
            'title' =>'&nbsp;&nbsp;&nbsp;Пользовательское соглашение',
            //'order' => '',
            'type' => 'text',
        ]);
    }

    public function safeDown()
    {        
		$group_id = (new Query())->from('settings')->where(['key'=>'condition_text'])->select(['group_id'])->one();
		$this->delete('settings',['key'=>'condition_text']);
        $this->delete('settings_group',['id'=>$group_id]);

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
		
    }

    public function down()
    {
        echo "m170729_225652_insert_settings_and_settings_group_a_row cannot be reverted.\n";

        return false;
    }
    */
}

<?php

class m170308_160431_tmp_settings extends \console\components\Migration {
    public function up() {
        $this->createTable('settings', [
            'key'   => $this->string(),
            'value' => $this->string(),
        ]);
    }

    public function down() {
        $this->dropTable('settings');
    }
}

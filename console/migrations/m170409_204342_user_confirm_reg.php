<?php

class m170409_204342_user_confirm_reg extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('user', 'is_activated', $this->integer(1)->defaultValue(0));
        $this->addColumn('user', 'confirm_token', $this->string());
    }

    public function safeDown() {
        $this->dropColumn('user', 'is_activated');
        $this->dropColumn('user', 'confirm_token');
    }
}

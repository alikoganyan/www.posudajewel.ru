<?php

class m170327_182229_alter_auth extends \console\components\Migration {
    public function safeUp() {
        $this->alterColumn('user', 'first_name', $this->string()->null()->defaultValue(''));
        $this->alterColumn('user', 'last_name', $this->string()->null()->defaultValue(''));
    }

    public function safeDown() {
        $this->alterColumn('user', 'first_name', $this->string()->notNull());
        $this->alterColumn('user', 'last_name', $this->string()->notNull());
    }
}

<?php

class m170304_115704_alter_company_phones extends \console\components\Migration {
    public function up() {
        $this->renameColumn('company', 'phone', 'first_phone');
    }

    public function down() {
        $this->renameColumn('company', 'first_phone', 'phone');
    }
}

<?php

use yii\db\Migration;

class m170605_191234_add_column_sitemap extends Migration {

    public function safeUp() {
        $this->addColumn('service', 'sitemap', $this->smallInteger()->defaultValue(0)->comment('Блокировать в sitemap.xml: 1-да, 0-нет'));
    }

    public function safeDown() {
        $this->dropColumn('service', 'sitemap');
    }

}

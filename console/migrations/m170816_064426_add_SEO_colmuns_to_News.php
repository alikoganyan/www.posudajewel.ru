<?php

use yii\db\Migration;

class m170816_064426_add_SEO_colmuns_to_News extends Migration
{
    public function safeUp()
    {
        $this->addColumn('news', 'sitemap', $this->boolean()->null()->defaultValue(false));
        $this->addColumn('news', 'meta_description', $this->string());
        $this->addColumn('news', 'meta_keywords', $this->string());
        $this->addColumn('news', 'meta_title', $this->string());

    }

    public function safeDown()
    {
        $this->dropColumn('news', 'sitemap');
        $this->dropColumn('news', 'meta_description');
        $this->dropColumn('news', 'meta_keywords');
        $this->dropColumn('news', 'meta_title');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170816_064426_add_SEO_colmuns_to_News cannot be reverted.\n";

        return false;
    }
    */
}

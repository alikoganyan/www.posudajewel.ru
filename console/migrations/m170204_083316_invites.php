<?php
use \console\components\Migration;

class m170204_083316_invites extends Migration {
    public function safeUp() {
        $this->createTable('invite', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
        ]);

        $this->createTable('invite_user', [
            'invite_id'     => $this->integer(),
            'user_id'       => $this->integer(),
            'email'         => $this->string(),
            'phone'         => $this->string(),
            'description'   => $this->text(),
            'is_add_number' => $this->integer(1),
            'send_sms'      => $this->integer(1),
            'send_email'    => $this->integer(1),
            'is_accepted'   => $this->integer(1),

            'UNIQUE INDEX `invite_user` (`invite_id`, `user_id`)',
        ]);
    }

    public function safeDown() {
        $this->dropTable('invite');
        $this->dropTable('invite_user');
    }
}

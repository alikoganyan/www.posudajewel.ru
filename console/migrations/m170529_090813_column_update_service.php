<?php

use yii\db\Migration;

class m170529_090813_column_update_service extends Migration {

    public function safeUp() {

        $this->addColumn('service', 'time_create', $this->integer()->comment('Время создания'));
        $this->addColumn('service', 'time_update', $this->integer()->comment('Время редактирования'));
        $this->addColumn('service', 'user_update', $this->integer()->comment('Пользователь изменивший запись'));
        $this->addColumn('service', 'sort', $this->integer());
    }

    public function safeDown() {
        $this->dropColumn('service', 'time_create');
        $this->dropColumn('service', 'time_update');
        $this->dropColumn('service', 'user_update');
        $this->dropColumn('service', 'sort');
    }

}

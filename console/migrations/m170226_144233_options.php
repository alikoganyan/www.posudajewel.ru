<?php

class m170226_144233_options extends \console\components\Migration {
    public function up() {
        $this->createTable('service_option', [
            'id'         => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
            'price'      => $this->integer()->defaultValue(0),
        ]);
    }

    public function down() {
        $this->dropTable('service_option');
    }
}

<?php

use yii\db\Migration;

class m170708_084733_alter_settings_type extends Migration {
    public function safeUp() {
        $this->alterColumn('settings', 'type', "ENUM('image', 'text', 'json')");

        $this->delete('settings', ['key' => [
            'url_info', 'soc1_name', 'soc1_link', 'soc2_name', 'soc2_link', 'soc3_name', 'soc3_link', 'soc4_name', 'soc4_link',
        ]]);

        $this->insert('settings', ['key' => 'vk', 'title' => 'Вконтакте', 'group_id' => 1, 'type' => 'json']);
        $this->insert('settings', ['key' => 'ok', 'title' => 'Одноклассники', 'group_id' => 1, 'type' => 'json']);
        $this->insert('settings', ['key' => 'fb', 'title' => 'Facebook', 'group_id' => 1, 'type' => 'json']);
        $this->insert('settings', ['key' => 'google', 'title' => 'Google+', 'group_id' => 1, 'type' => 'json']);
    }

    public function safeDown() {
        $this->alterColumn('settings', 'type', $this->string());

        $this->insert('settings', ['key' => 'soc1_name', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc1_link', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc2_name', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc2_link', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc3_name', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc3_link', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc4_name', 'type' => 'text']);
        $this->insert('settings', ['key' => 'soc4_link', 'type' => 'text']);
    }
}

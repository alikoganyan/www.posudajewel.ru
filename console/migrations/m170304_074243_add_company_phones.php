<?php

class m170304_074243_add_company_phones extends \console\components\Migration {
    public function up() {
        $this->addColumn('company', 'main_phone', $this->string());
        $this->addColumn('company', 'second_phone', $this->string());

        $this->addColumn('service', 'unit_id', $this->integer());
    }

    public function down() {
        $this->dropColumn('company', 'main_phone');
        $this->dropColumn('company', 'second_phone');

        $this->dropColumn('service', 'unit_id');
    }
}

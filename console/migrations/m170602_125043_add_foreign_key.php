<?php

use yii\db\Migration;

class m170602_125043_add_foreign_key extends Migration {

    /**
     * Ключи
     * @var array 
     */
    protected $fk = [
        'catalog' => [
            'parent_id' => [
                'table' => 'catalog', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
            'service_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'SET NULL', 'index' => true,
            ],
        ],
        'catalog_link' => [
            'parent_id' => [
                'table' => 'catalog', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
            'search_id' => [
                'table' => 'quick_search', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
        ],
        'char_value' => [
            'char_id' => [
                'table' => 'char', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'value_id' => [
                'table' => 'value', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
        ],
        'company' => [
            'user_id' => [
                'table' => 'user', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'locality_id' => [
                'table' => 'locality', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'RESTRICT', 'index' => false,
            ],
        ],
        'locality' => [
            'region_id' => [
                'table' => 'region', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'RESTRICT', 'index' => false,
            ],
        ],
        'quick_search' => [
            'service_type_id' => [
                'table' => 'service_type', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'RESTRICT', 'index' => true,
            ],
        ],
        'quick_search_char_value' => [
            'quick_search_id' => [
                'table' => 'quick_search', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'char_id' => [
                'table' => 'char', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'value_id' => [
                'table' => 'value', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
        ],
        'service' => [
            'company_id' => [
                'table' => 'company', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'RESTRICT', 'index' => false,
            ],
            'service_type_id' => [
                'table' => 'service_type', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'unit_id' => [
                'table' => 'unit', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'RESTRICT', 'index' => true,
            ],
            'user_update' => [
                'table' => 'user', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'SET NULL', 'index' => true,
            ],
        ],
        'service_buy' => [
            'service_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'buy_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
        ],
        'service_char_value' => [
            'service_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'char_id' => [
                'table' => 'char', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'value_id' => [
                'table' => 'value', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
        ],
        'service_image' => [
            'service_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
        ],
        'service_option' => [
            'group_id' => [
                'table' => 'service_option_group', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
        ],
        'service_option_group' => [
            'service_id' => [
                'table' => 'service', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => true,
            ],
        ],
        'service_type_char' => [
            'service_type_id' => [
                'table' => 'service_type', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
            'char_id' => [
                'table' => 'char', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE', 'index' => false,
            ],
        ],
    ];

    public function safeUp() {

        foreach ($this->fk as $table => $columns) {
            foreach ($columns as $column => $ref) {
                // Добавляем индекс
                if ($ref['index']) {
                    $this->createIndex($column, $table, $column);
                }

                // Добавляем внешний ключ
                $this->addForeignKey($table . '_' . $column . '_fk', $table, $column, $ref['table'], $ref['column'], $ref['delete'], $ref['update']);
            }
        }
    }

    public function safeDown() {

        foreach ($this->fk as $table => $columns) {

            foreach ($columns as $column => $ref) {

                // Удаляем внешний ключ
                $this->dropForeignKey($table . '_' . $column . '_fk', $table);

                // Удаляем индекс
                if ($ref['index']) {
                    $this->dropIndex($column, $table);
                }
            }
        }
    }

}

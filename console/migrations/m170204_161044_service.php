<?php
use \console\components\Migration;

class m170204_161044_service extends Migration {
    public function safeUp() {
        $this->createTable('service_type', [
            'id'      => $this->primaryKey(),
            'name'    => $this->string()->notNull(),
            'is_mass' => $this->boolean()->defaultValue(false),
        ]);

        $this->createTable('char', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'is_multiple' => $this->boolean()->defaultValue(false),
        ]);

        $this->createTable('value', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('char_value', [
            'char_id'  => $this->integer()->notNull(),
            'value_id' => $this->integer()->notNull(),
            'PRIMARY KEY (char_id, value_id)',
        ]);

        $this->createTable('service_char_value', [
            'service_id' => $this->integer()->notNull(),
            'char_id'    => $this->integer()->notNull(),
            'value_id'   => $this->integer()->notNull(),
            'PRIMARY KEY (service_id, char_id, value_id)',
        ]);

        $this->createTable('service_type_char', [
            'service_type_id' => $this->integer()->notNull(),
            'char_id'         => $this->integer()->notNull(),
            'PRIMARY KEY (service_type_id, char_id)',
        ]);

        $this->createTable('service', [
            'id'              => $this->primaryKey(),
            'company_id'      => $this->integer()->notNull(),
            'service_type_id' => $this->integer()->notNull(),
            'name'            => $this->string()->notNull(),
            'description'     => $this->text(),
            'is_popular'      => $this->integer(1),
            'is_active'       => $this->integer(1)->defaultValue(1),
            'price'           => $this->integer()->null(),

            'meta_description' => $this->text(),
            'meta_keywords'    => $this->text(),
        ]);
        $this->createIndex('service_type', 'service', 'service_type_id');
        $this->createIndex('company', 'service', 'company_id');
    }

    public function safeDown() {
        $this->dropTable('service');
        $this->dropTable('service_type_char');
        $this->dropTable('service_char_value');
        $this->dropTable('char_value');
        $this->dropTable('value');
        $this->dropTable('char');
        $this->dropTable('service_type');
    }
}

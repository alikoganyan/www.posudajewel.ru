<?php

class m170415_163159_alter_region_geo extends \console\components\Migration {
    public function safeUp() {
        $this->alterColumn('region', 'longitude', $this->string());
        $this->alterColumn('region', 'latitude', $this->string());
    }

    public function safeDown() {
        $this->alterColumn('region', 'longitude', $this->integer());
        $this->alterColumn('region', 'latitude', $this->integer());
    }
}

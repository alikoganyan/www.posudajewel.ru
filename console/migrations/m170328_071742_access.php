<?php

class m170328_071742_access extends \console\components\Migration {
    public function safeUp() {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'permissions' => $this->text(),
        ]);

        $this->addColumn('user', 'role_id', $this->integer()->notNull());
        $this->createIndex('role_id', 'user', ['role_id']);

        $this->insert('role', ['name' => 'Пользователь']);
    }

    public function safeDown() {
        $this->dropColumn('user', 'role_id');
        $this->dropTable('role');
    }
}

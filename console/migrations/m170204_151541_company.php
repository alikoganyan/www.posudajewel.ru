<?php
use \console\components\Migration;

class m170204_151541_company extends Migration {
    public function safeUp() {
        $this->createTable('company', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer(),
            'locality_id' => $this->integer()->notNull(),
            'name'        => $this->string()->notNull(),
            'web'         => $this->string(),
            'phone'       => $this->string(),
            'email'       => $this->string(),
            'address'     => $this->string(),
            'longitude'   => $this->integer(),
            'latitude'    => $this->integer(),
            'is_active'   => $this->integer(1),
        ]);
        $this->createIndex('user', 'company', ['user_id']);
    }

    public function safeDown() {
        $this->dropTable('company');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170811_094304_create_order_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('order', [
            'id'             => $this->primaryKey(),
            'user_id'        => $this->integer()->notNull(),
            'cached_cart_id' => $this->string()->notNull(),
            'fio'            => $this->string()->notNull(),
            'phone'          => $this->string()->notNull(),
            'email'          => $this->string()->notNull(),
            'address'        => $this->text()->notNull(),
            'comment'        => $this->text(),
            'delivery_cost'  => $this->money(10, 2),
            'total'          => $this->money(10, 2),
            'delivery_id'    => $this->integer()->notNull(),
            'status_id'      => $this->integer(),
            'created_at'     => $this->dateTime(),
            'updated_at'     => $this->dateTime(),
        ]);
        $this->createTable('status', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(),
        ]);
        $this->createTable('delivery_service', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(),
        ]);
        $this->createIndex('idx-user', 'order', 'user_id');
        $this->createIndex('idx-delivery', 'order', 'delivery_id');
        $this->createIndex('idx-status', 'order', 'status_id');
        $this->addForeignKey(
            'fk-order-user',
            'order',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-order-delivery',
            'order',
            'delivery_id',
            'delivery_service',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-order-status',
            'order',
            'status_id',
            'status',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        $this->dropForeignKey('fk-order-user', 'order');
        $this->dropForeignKey('fk-order-delivery', 'order');
        $this->dropForeignKey('fk-order-status', 'order');
        $this->dropIndex('idx-user', 'order');
        $this->dropIndex('idx-delivery', 'order');
        $this->dropIndex('idx-status', 'order');

        $this->dropTable('delivery');
        $this->dropTable('status');
        $this->dropTable('order');
    }
}

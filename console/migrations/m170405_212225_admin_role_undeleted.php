<?php

class m170405_212225_admin_role_undeleted extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('role', 'is_undeleted', $this->integer(1)->defaultValue(0));

    }

    public function safeDown() {
        $this->dropColumn('role', 'is_undeleted');
    }
}

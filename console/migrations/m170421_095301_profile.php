<?php

class m170421_095301_profile extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('user', 'confirm_code', $this->string());

        $this->alterColumn('company', 'locality_id', $this->integer()->null());
    }

    public function safeDown() {
        $this->alterColumn('company', 'locality_id', $this->integer());

        $this->dropColumn('user', 'confirm_code');
    }
}

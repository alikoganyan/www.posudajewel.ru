<?php

use yii\db\Migration;

class m170518_132523_alter_setting extends Migration {

    public function safeUp() {
        $this->alterColumn('settings', 'value', $this->text());
    }

    public function safeDown() {
        $this->alterColumn('settings', 'value', $this->string());
    }

}

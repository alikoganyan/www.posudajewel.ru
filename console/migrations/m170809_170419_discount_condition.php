<?php

use yii\db\Migration;

/**
 * Class m170809_170419_discount_condition
 */
class m170809_170419_discount_condition extends Migration {
    public function safeUp() {
        $group_id = 1; // Другие настройки сайта
        $this->insert('settings', [
            'key'      => 'discount_condition_text',
            'value'    => 'Discount condition text',
            'group_id' => $group_id,
            'title'    => '&nbsp;&nbsp;&nbsp;Условия получения пользоательской скидки',
            'type'     => 'text',
        ]);
    }
    public function safeDown() {
        $this->delete('settings', ['key' => 'discount_condition_text']);
    }
}

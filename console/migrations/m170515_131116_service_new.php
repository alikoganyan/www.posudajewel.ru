<?php

use yii\db\Migration;

class m170515_131116_service_new extends Migration {

    public function safeUp() {
        $this->addColumn('service', 'is_new', $this->integer(4)->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('service', 'is_new');
    }

}

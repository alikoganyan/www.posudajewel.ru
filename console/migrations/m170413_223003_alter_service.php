<?php

class m170413_223003_alter_service extends \console\components\Migration {
    public function safeUp() {
        $this->alterColumn('service', 'price', $this->integer()->null());
    }

    public function safeDown() {
        $this->dropColumn('service', 'price');
    }
}

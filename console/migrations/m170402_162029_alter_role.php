<?php

class m170402_162029_alter_role extends \console\components\Migration {
    public function safeUp() {
        $this->createIndex('name', 'role', ['name'], true);
    }

    public function safeDown() {
        $this->dropIndex('name', 'role');
    }
}

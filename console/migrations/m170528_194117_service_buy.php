<?php

use yii\db\Migration;

class m170528_194117_service_buy extends Migration {

    public function safeUp() {

        $this->createTable('service_buy', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer(11),
            'buy_id' => $this->integer(11),
        ]);
        $this->createIndex('service_id', 'service_buy', 'service_id');
    }

    public function safeDown() {
        $this->dropTable('service_buy');
    }

}

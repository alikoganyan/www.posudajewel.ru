<?php

use yii\db\Migration;

class m170524_185858_add_columns_service_and_char extends Migration {

    public function safeUp() {
        $this->addColumn('service', 'url', $this->string());
        $this->addColumn('char', 'is_filter', $this->smallInteger()->defaultValue(0));


        \Yii::$app->db->createCommand()->
                update('service', ['url' => new \yii\db\Expression('id')])->
                execute();
    }

    public function safeDown() {
        $this->dropColumn('service', 'url');
        $this->dropColumn('char', 'is_filter');
    }

}

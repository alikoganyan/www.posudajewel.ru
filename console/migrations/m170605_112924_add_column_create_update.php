<?php

use yii\db\Migration;

class m170605_112924_add_column_create_update extends Migration {

    protected $column = [
        'catalog' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'catalog_link' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'char' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'company' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'locality' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'news' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'quick_search' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'region' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'role' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'service' => [ 'user_create'], 
        'service_type' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'subscribe' => ['user_create', 'user_update'],
        'unit' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'user' => ['time_create', 'time_update', 'user_create', 'user_update'],
        'value' => ['time_create', 'time_update', 'user_create', 'user_update'],
    ];
    protected $comment = [
        'time_create' => 'Время добавления',
        'time_update' => 'Время редактирования',
        'user_create' => 'Кто добавил',
        'user_update' => 'Кто изменил',
    ];

    public function safeUp() {
        foreach ($this->column as $table => $columns) {
            foreach ($columns as $column) {

                if (in_array($column, ['user_create', 'user_update'])) {

                    $this->addColumn($table, $column, $this->integer(11)->defaultValue(null)->comment($this->comment[$column]));
                    $this->createIndex($column, $table, $column);
                    $this->addForeignKey($table . '_' . $column . '_fk', $table, $column, 'user', 'id', 'SET NULL', 'SET NULL');
                } else {

                    $this->addColumn($table, $column, $this->integer(11)->defaultValue(0)->comment($this->comment[$column]));
                }
            }
        }
    }

    public function safeDown() {
        foreach ($this->column as $table => $columns) {
            foreach ($columns as $column) {
                if (in_array($column, ['user_create', 'user_update'])) {
                    $this->dropForeignKey($table . '_' . $column . '_fk', $table);
                }

                $this->dropColumn($table, $column);
            }
        }
    }

}

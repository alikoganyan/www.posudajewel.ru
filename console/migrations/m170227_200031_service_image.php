<?php

class m170227_200031_service_image extends \console\components\Migration {
    public function safeUp() {
        $this->createTable('service_image', [
            'id'         => $this->primaryKey(),
            'service_id' => $this->integer(),
            'name'       => $this->string(),
        ]);

        $this->createIndex('service_id', 'service_image', ['service_id']);
    }

    public function safeDown() {
        $this->dropTable('service_image');
    }
}

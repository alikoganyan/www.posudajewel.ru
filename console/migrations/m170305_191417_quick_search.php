<?php

class m170305_191417_quick_search extends \console\components\Migration {
    public function up() {
        $this->createTable('quick_search', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'image' => $this->string(),
        ]);

        $this->createTable('quick_search_char_value', [
            'quick_search_id' => $this->integer(),
            'char_id'         => $this->integer(),
            'value_id'        => $this->integer(),
            'PRIMARY KEY (quick_search_id, char_id, value_id)',
        ]);
    }

    public function down() {
        $this->dropTable('quick_search_char_value');
        $this->dropTable('quick_search');
    }
}

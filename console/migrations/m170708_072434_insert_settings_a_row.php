<?php

use yii\db\Migration;
use yii\db\Query;

class m170708_072434_insert_settings_a_row extends Migration
{
    public function safeUp()
    {
        $order = (new Query())->from('settings')->max('`order`');
        $this->insert('settings', [
            'key' => 'analytics_scripts',
            'value' => '',
            'group_id' => 1,
            'title' =>'Скрипты аналитики',
            'order' => $order+1,
            'type' => 'text',
        ]);
    }

    public function safeDown()
    {
        $this->delete('settings',['key'=>'analytics_scripts']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170708_072434_insert_settings_a_row cannot be reverted.\n";

        return false;
    }
    */
}

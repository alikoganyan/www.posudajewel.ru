<?php

use yii\db\Migration;

class m170601_192808_create_table_subscribe extends Migration {

    public function safeUp() {

        $this->createTable('subscribe', [
            'id' => $this->primaryKey()->comment('Id'),
            'email' => $this->string(255)->notNull()->unique()->comment('E-mail'),
            'hash' => $this->char(32)->notNull()->unique()->comment('Hash'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('Статус: 1-подписан, 0-отписан'),
            'time_create' => $this->integer(11)->comment('Время подписки'),
            'time_update' => $this->integer(11)->comment('Время изменения'),
        ]);
    }

    public function safeDown() {
        $this->dropTable('subscribe');
    }

}

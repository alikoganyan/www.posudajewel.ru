<?php

class m170311_063423_alter_quick_search extends \console\components\Migration {
    public function up() {
        $this->addColumn('quick_search', 'service_type_id', $this->integer());
    }

    public function down() {
        $this->dropColumn('quick_search', 'service_type_id');
    }
}

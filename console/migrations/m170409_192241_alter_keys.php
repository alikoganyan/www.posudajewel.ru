<?php

class m170409_192241_alter_keys extends \console\components\Migration {
    public function safeUp() {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->addForeignKey('F18', 'service', ['company_id'], 'company', ['id']);
        $this->execute('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function safeDown() {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->dropForeignKey('F18', 'service');
        $this->execute('SET FOREIGN_KEY_CHECKS=1;');
    }
}

<?php

use yii\db\Migration;

class m170814_004302_alter_catalog_add_column_is_hidden_subsections extends Migration
{
    public function safeUp()
    {
        $this->addColumn('catalog', 'is_hidden_subsections', $this->boolean()->null()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('catalog', 'is_hidden_subsections');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_004302_alter_catalog_add_column_is_hidden_subsections cannot be reverted.\n";

        return false;
    }
    */
}

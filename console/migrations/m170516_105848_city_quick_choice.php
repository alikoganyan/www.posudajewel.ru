<?php

use yii\db\Migration;

class m170516_105848_city_quick_choice extends Migration {

    public function safeUp() {
        $this->addColumn('locality', 'is_quick_choice', $this->integer(4)->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('locality', 'is_quick_choice');
    }

}

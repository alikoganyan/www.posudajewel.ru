<?php

class m170304_072842_alter_active extends \console\components\Migration {
    public function safeUp() {
        $this->dropColumn('user', 'is_active');
        $this->dropColumn('company', 'is_active');
        $this->dropColumn('service', 'is_active');

        $this->addColumn('user', 'is_blocked', $this->integer(1)->defaultValue(0));
        $this->addColumn('company', 'is_blocked', $this->integer(1)->defaultValue(0));
        $this->addColumn('service', 'is_blocked', $this->integer(1)->defaultValue(0));
    }

    public function safeDown() {
        $this->addColumn('user', 'is_active', $this->integer(1)->defaultValue(1));
        $this->addColumn('company', 'is_active', $this->integer(1)->defaultValue(1));
        $this->addColumn('service', 'is_active', $this->integer(1)->defaultValue(1));

        $this->createIndex('active', 'user', ['is_active']);
    }
}

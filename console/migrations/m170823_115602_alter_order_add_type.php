<?php

use yii\db\Migration;

class m170823_115602_alter_order_add_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'type', $this->smallInteger(1));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'type', $this->smallInteger(1));
    }

}

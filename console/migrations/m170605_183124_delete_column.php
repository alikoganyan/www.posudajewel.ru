<?php

use yii\db\Migration;

class m170605_183124_delete_column extends Migration {

    public function safeUp() {
        $this->dropColumn('user', 'created_at');
        $this->dropColumn('user', 'updated_at');
    }

    public function safeDown() {
        $this->addColumn('user', 'created_at', $this->integer());
        $this->addColumn('user', 'updated_at', $this->integer());
    }

}

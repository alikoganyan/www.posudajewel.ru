<?php

use yii\db\Migration;

class m170515_181621_create_news extends Migration {

    public function safeUp() {

        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(4)->defaultValue(10),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'image' => $this->string(255),
            'description' => $this->string(500),
            'content' => $this->text(),
            'date' => $this->date(),
            'is_blocked' => $this->integer(4)->defaultValue(0),
        ]);
    }

    public function safeDown() {
        $this->dropTable('news');
    }

}

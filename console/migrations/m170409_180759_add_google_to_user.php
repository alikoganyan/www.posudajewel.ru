<?php

class m170409_180759_add_google_to_user extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('user', 'google_id', $this->integer());
    }

    public function safeDown() {
        $this->dropColumn('user', 'google_id');
    }
}

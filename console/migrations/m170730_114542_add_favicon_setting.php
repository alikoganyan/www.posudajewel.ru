<?php

use yii\db\Migration;
use yii\db\Query;

class m170730_114542_add_favicon_setting extends Migration {
    public function safeUp() {
        $order = (new Query())->from('settings')->max('`order`');

        $this->insert('settings', [
            'key' => 'favicon',
            'order' => $order + 1,
            'group_id' => 1,
            'title' => 'Favicon',
            'type' => 'image',
        ]);
    }

    public function safeDown() {
        $this->delete('settings', ['key' => 'favicon']);
    }
}

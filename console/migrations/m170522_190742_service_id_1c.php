<?php

use yii\db\Migration;

class m170522_190742_service_id_1c extends Migration {

    public function safeUp() {
        $this->addColumn('service', 'id_1c', $this->string());
    }

    public function safeDown() {
        $this->dropColumn('service', 'id_1c');
    }

}

<?php

class m170502_000000_alter_user extends \console\components\Migration {
    public function safeUp() {
        $this->addColumn('user', 'panel_settings', $this->string(255));
    }

    public function safeDown() {
        $this->dropColumn('user', 'panel_settings');
    }
}

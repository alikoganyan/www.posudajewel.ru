<?php

use yii\db\Migration;

class m170816_063940_add_SEO_colmuns_to_Page extends Migration
{
    public function safeUp()
    {
        $this->addColumn('page', 'sitemap', $this->boolean()->null()->defaultValue(false));
        $this->addColumn('page', 'meta_description', $this->string());
        $this->addColumn('page', 'meta_keywords', $this->string());
        $this->addColumn('page', 'meta_title', $this->string());

    }

    public function safeDown()
    {
        $this->dropColumn('page', 'sitemap');
        $this->dropColumn('page', 'meta_description');
        $this->dropColumn('page', 'meta_keywords');
        $this->dropColumn('page', 'meta_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170816_063940_add_SEO_colmuns_to_Page cannot be reverted.\n";

        return false;
    }
    */
}

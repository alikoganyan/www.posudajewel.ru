<?php

use yii\db\Migration;
use yii\db\Query;

class m170616_114204_table_settings extends Migration {

    protected $setting = [
        'info' => ['title' => 'Информационная полоса', 'type' => 'text'],
        'url_info' => ['title' => 'Url ссылки информационной полосы', 'type' => 'text'],
        'about' => ['title' => 'О компании', 'type' => 'text'],
        'banner' => ['title' => 'Баннер на главной странице', 'type' => 'image'],
        'logo' => ['title' => 'Логотип', 'type' => 'image'],
        'index_title' => ['title' => 'Заголовок', 'type' => 'text'],
        'index_description' => ['title' => 'Описание', 'type' => 'text'],
        'index_keywords' => ['title' => 'Ключи', 'type' => 'text'],
        'soc1_name' => ['title' => 'Название ссылки', 'type' => 'text'],
        'soc1_link' => ['title' => 'Адрес ссылки', 'type' => 'text'],
        'soc2_name' => ['title' => 'Название ссылки', 'type' => 'text'],
        'soc2_link' => ['title' => 'Адрес ссылки', 'type' => 'text'],
        'soc3_name' => ['title' => 'Название ссылки', 'type' => 'text'],
        'soc3_link' => ['title' => 'Адрес ссылки', 'type' => 'text'],
        'soc4_name' => ['title' => 'Название ссылки', 'type' => 'text'],
        'soc4_link' => ['title' => 'Адрес ссылки', 'type' => 'text'],
    ];

    public function safeUp() {

        $currentSetting = (new Query())->
                from('settings')->
                indexBy('key')->
                all();

        $this->createTable('settings_group', [
            'id' => $this->primaryKey()->comment('Id'),
            'name' => $this->string()->comment('Название'),
            'order' => $this->integer()->comment('Сортировка'),
        ]);

        $this->insert('settings_group', [
            'id' => 1,
            'name' => 'Главная страница сайта',
            'order' => 1,
        ]);


        $this->dropTable('settings');
        $this->createTable('settings', [
            'id' => $this->primaryKey()->comment('Id'),
            'key' => $this->string()->unique()->comment('Ключ'),
            'value' => $this->text()->comment('Значение настройки'),
            'group_id' => $this->integer()->comment('Группа'),
            'title' => $this->string()->comment('Наименование'),
            'order' => $this->integer()->comment('Сортировка'),
            'type' => $this->string()->comment('Тип: json, text, image, textarea'),
        ]);
        $this->createIndex('group_id', 'settings', 'group_id');
        $this->addForeignKey('settings_group_id_fk', 'settings', 'group_id', 'settings_group', 'id', 'RESTRICT', 'RESTRICT');

        $order = 10001;
        foreach ($this->setting as $key => $setting) {

            $this->insert('settings', [
                'key' => $key,
                'value' => isset($currentSetting[$key]) ? $currentSetting[$key]['value'] : '',
                'group_id' => 1,
                'title' => $setting['title'],
                'order' => $order,
                'type' => $setting['type'],
            ]);

            $order ++;
        }
    }

    public function safeDown() {
        $this->dropTable('settings');
        $this->dropTable('settings_group');

        $this->createTable('settings', [
            'key' => $this->string(),
            'value' => $this->text(),
        ]);
    }

}

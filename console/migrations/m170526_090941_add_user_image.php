<?php

use yii\db\Migration;

class m170526_090941_add_user_image extends Migration {

    public function safeUp() {
        $this->addColumn('user', 'image', $this->string());
        $this->addColumn('user', 'image_crop', $this->string());
    }

    public function safeDown() {
        $this->dropColumn('user', 'image');
        $this->dropColumn('user', 'image_crop');
    }

}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    public $css = [
        'http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic',
        'css/fonts.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'css/reset.css',
        'css/default.css',
        'css/slick.css',
        'css/nouislider.min.css',
        'css/jquery.formstyler.css',
        'css/main.css?ver=1704022118',
        'css/media-querries.css?ver=1704022118',
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
        'js/bootstrap.min.js',
        'js/slick.min.js',
        'js/jquery.formstyler.min.js',
        'js/jquery.nicescroll.min.js',
        'js/nouislider.min.js',
        'js/wNumb.js',
        'js/main.js?ver=1704022118',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}

<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use common\components\Controller;
use common\models\Subscribe;

/**
 * Подписка
 */
class SubscribeController extends Controller {

    /**
     * Оформление подписки
     * @return type
     */
    public function actionCreate() {
        $email = (string) Yii::$app->getRequest()->post('email');

        if (!empty($email)) {
            $subscribe = Subscribe::findOne(['email' => $email]);

            if (!$subscribe instanceof Subscribe) {
                $subscribe = new Subscribe();
                $subscribe->email = $email;
            }

            $subscribe->updateScenario();
            $subscribe->status = 1;


            if ($subscribe->save()) {
                return Json::encode(['status' => 'success', 'message' => 'Вы подписались на новостную рассылку. Благодарим за то, что Вы с нами.']);
            }
        }

        return Json::encode(['status' => 'error', 'message' => 'Укажите правильный e-mail']);
    }

    /**
     * Отписка от рассылки
     * @return type
     */
    public function actionCancel() {
        $hash = (string) Yii::$app->getRequest()->get('hash');

        if (!empty($hash)) {
            $subscribe = Subscribe::findOne(['hash' => $hash]);

            if ($subscribe instanceof Subscribe) {
                $subscribe->status = 0;
                $subscribe->save(false, ['status']);

                Yii::$app->session->setFlash('notify', 'Вы успешно отписались от рассылки.');
            }
        }

        return $this->redirect(['/']);
    }

}

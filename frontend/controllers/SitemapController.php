<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Controller;
use common\models\QuickSearch;
use common\models\Service;
use common\models\Page;
use common\models\News;

/**
 * Карта сайта
 */
class SitemapController extends Controller {

    /**
     * Sitemap.xml
     * @return type
     */
    public function actionIndex() {

        $sitemap = Yii::$app->cache->get('sitemap');

        if (empty($sitemap)) {

            $sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
                    "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";

            // Главная страница
            $sitemap .= $this->createUrlSitemap(['/'], time(), 'daily', 1);


            // Быстрый поиск
            $search = QuickSearch::find()->
                    select(['url', 'time_update'])->
                    where(['sitemap' => 0])->
                    asArray()->
                    all();
            foreach ($search as $row) {
                $sitemap .= $this->createUrlSitemap(['/site/search', 'url' => $row['url']], $row['time_update'], 'daily', 0.9);
            }


            // Предложения
            $service = Service::find()->
                    select(['url', 'time_update'])->
                    where(['sitemap' => 0])->
                    asArray()->
                    all();
            foreach ($service as $row) {
                $sitemap .= $this->createUrlSitemap(['/site/details', 'url' => $row['url']], $row['time_update'], 'weekly', 0.8);
            }

            // Новости
            $service = News::find()->
            select(['url', 'time_update'])->
            where(['sitemap' => 0, 'is_blocked' => 0])->
            asArray()->
            all();
            foreach ($service as $row) {
                $sitemap .= $this->createUrlSitemap(['/site/details', 'url' => $row['url']], $row['time_update'], 'daily', 0.9);
            }

            // Информация
            $service = Page::find()->
            select(['url', 'time_update'])->
            where(['sitemap' => 0, 'is_blocked' => 0])->
            asArray()->
            all();
            foreach ($service as $row) {
                $sitemap .= $this->createUrlSitemap(['/site/details', 'url' => $row['url']], $row['time_update'], 'daily', 0.9);
            }

            $sitemap .= "</urlset>";
            
            Yii::$app->cache->set('sitemap', $sitemap, 1800);
        }


        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        header('Content-Type: application/xml');
        
        return $sitemap;
    }

    /**
     * 
     * @param string $url
     * @param int $time
     * @param string $freq
     * @param double $priority
     * @return string
     */
    public function createUrlSitemap($url, $time, $freq, $priority) {
        return "<url>\n" .
                "<loc>" . Html::encode(Url::toRoute($url, true)) . "</loc>\n" .
                "<lastmod>" . date('Y-m-d', $time) . "</lastmod>\n" .
                "<changefreq>" . $freq . "</changefreq>\n" .
                "<priority>" . $priority . "</priority>\n" .
                "</url>\n";
    }

}

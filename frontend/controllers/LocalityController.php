<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use frontend\components\CityHelper;
use common\components\Controller;
use common\models\Locality;
use common\models\Region;

/**
 * Выбор города
 */
class LocalityController extends Controller {

    /**
     * Список регионов и городов
     * @return type
     */
    public function actionList() {
        $search = trim(Yii::$app->getRequest()->get('search'));

        $cond = ['AND'];

        if (!empty($search)) {
            foreach (explode(' ', $search) as $str) {
                $cond[] = ['OR', ['LIKE', 'l.name', $str], ['LIKE', 'r.name', $str]];
            }
        }

        $rows = Region::find()->
                select(['id', 'name'])->
                orderBy('code ASC')->
                indexBy('id')->
                asArray()->
                all();
        foreach ($rows as $regionId => $row) {
            $rows[$regionId]['items'] = [];
        }

        $locality = Locality::find()->
                select(['l.id', 'l.region_id', 'l.name'])->
                from(['l' => Locality::tableName()])->
                leftJoin(['r' => Region::tableName()], 'r.id=l.region_id')->
                where($cond)->
                orderBy('name ASC')->
                asArray()->
                all();
        foreach ($locality as $row) {
            $regionId = $row['region_id'];
            if (isset($rows[$regionId])) {
                $temp = explode('|', $row['name']);

                $rows[$regionId]['items'][] = [
                    'id' => $row['id'],
                    'name' => trim($temp[0]),
                ];
            }
        }

        return Json::encode(['status' => 'success', 'data' => [
                        'list' => array_values($rows),
        ]]);
    }

    /**
     * Изменяем город пользователя
     * @param type $id
     * @return type
     */
    public function actionSave($id) {
        $locality = Locality::findOne(['id' => $id]);

        if ($locality instanceof Locality) {
            $name = $locality->getName();

            CityHelper::setCity($name);

            return Json::encode(['status' => 'success', 'data' => [
                            'name' => $name,
            ]]);
        } else {
            return Json::encode(['status' => 'error', 'message' => 'Город не найден.']);
        }
    }

}

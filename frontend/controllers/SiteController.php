<?php
namespace frontend\controllers;

use common\components\Controller;
use common\models\Locality;
use common\models\Service;
use common\models\ServiceType;
use common\models\Settings;
use common\models\User;
use common\models\Company;
use common\models\QuickSearch;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class SiteController extends Controller {
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        // Ссылки быстрого поиска
        $quickSearch = QuickSearch::find()->
                with(['serviceType'])->
                where(['is_show' => 1])->
                limit(8)->
                all();

        return $this->render('index', [
            'about' => Settings::getSetting('about'),
            'banner' => Settings::getSetting('banner'),
            'quickSearch' => $quickSearch,
        ]);
    }

    public function actionDetails($url) {
        $service = Service::find()->
                from(['s' => Service::tableName()])->
                leftJoin(['c' => Company::tableName()], 'c.id = s.company_id')->
                leftJoin(['u' => User::tableName()], 'u.id = c.user_id')->
                where(['AND', 's.url=:url', 's.is_blocked!=1', 'c.is_blocked!=1', 'u.is_blocked!=1'], [':url' => (string) $url])->
                one();

        if (!$service) {
            throw new NotFoundHttpException('service not found');
        }

        return $this->render('detail', [
            'model' => $service,
            'discount_condition_text' => Settings::getSetting('discount_condition_text'),
        ]);
    }

    public function actionSearch($service_type = null, $chars = null, $localities = null, $page = 0) {
        $request = Yii::$app->getRequest();

        $priceFrom = $request->get('price_from');
        $priceTo = $request->get('price_to');
        $search = trim($request->get('search'));
        $sort = trim($request->get('sort'));

        $sort = ($sort == 'price_desc' ? 's.price DESC' : 's.price ASC');
        $page = $page < 0 ? 0 : $page;

        $selectedChars = [];

        if (empty($service_type)) {
            // Выведим фильтр "Коллекция", если не выбран раздел
            // Найдем первый товар с указанным названием и выставим категорию для поиска
            if (!empty($search)) {
                $service_type = Service::findNotBlocked()->
                        select(['s.service_type_id'])->
                        andWhere(['s.name' => $search])->
                        limit(1)->
                        scalar();
            }
        }

        // Быстрый поиск
        $url = $request->get('url');
        if (!empty($url)) {
            $quickSearch = QuickSearch::findOne(['url' => (string) $url]);
            if (!$quickSearch instanceof QuickSearch) {
                throw new NotFoundHttpException('page not found');
            }

            if (is_null($chars)) {
                $chars = Json::encode($quickSearch->doSearch());
            }

            if (is_null($service_type)) {
                $service_type = $quickSearch->service_type_id;
            }
        } else {
            $quickSearch = null;
        }

        $serviceType = ServiceType::findOne(['id' => (int) $service_type]);
        if (!$serviceType instanceof ServiceType) {
            //return $this->redirect(['/']);
        }

        $chars = Json::decode($chars);
        $localities = Json::decode($localities);

        $query = Service::doSearch($chars, $service_type, Json::encode($localities), $priceFrom, $priceTo);



        // Поиск по фразе
        if (!empty($search)) {
            $query->andWhere(['OR', ['LIKE', 's.name', $search], ['LIKE', 's.code', $search]]);
        }

        $limit = 45;

        $pages = ceil($query->count() / $limit);
        $searchResult = $query->
                orderBy($sort)->
                offset($page * $limit)->
                limit($limit)->
                all();

        $serviceCharValues = [];
        foreach ($searchResult as $service) {
            if ($service->chars === null) {
                continue;
            }

            foreach ($service->chars as $char) {
                if (isset($serviceCharValues[$char->char_id])) {
                    if (!in_array($char->value_id, $serviceCharValues[$char->char_id])) {
                        $serviceCharValues[$char->char_id][] = $char->value_id;
                    }
                } else {
                    $serviceCharValues[$char->char_id][] = $char->value_id;
                }
            }
        }

        if (\Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'serviceCharValues' => $serviceCharValues,
                'html' => $this->renderPartial('group-items', [
                    'services' => $searchResult,
                ]),
                'nextPage' => $pages > $page + 1 ? $page + 1 : -1,
            ];
        }

        return $this->render('search', [
            'pages' => $pages,
            'serviceType' => $serviceType,
            'searchResult' => $searchResult,
            'chars' => $chars,
            'serviceCharValues' => $serviceCharValues,
            'quickSearch' => $quickSearch,
            'search' => $search,
            'selectedChars' => $chars,
        ]);
    }

    /**
     * Запрос на восставноление пароля
     * @return type
     */
    public function actionResetAccess() {
        $email = (string) Yii::$app->getRequest()->post('email');

        $user = User::findOne([
                    'email' => $email,
                    'is_blocked' => 0,
        ]);
        if ($user instanceof User) {
            $user->requestResetPassword();

            return Json::encode(['status' => 'success', 'message' => 'На ваш e-mail отправлено письмо с инструкциями для восстановления пароля.']);
        } else {
            return Json::encode(['status' => 'error', 'message' => 'E-mail не найден.']);
        }
    }

    /**
     * Сброс пароля
     * @return type
     */
    public function actionResetPassword($token) {
        $user = User::findByPasswordResetToken($token);

        if ($user instanceof User) {
            Yii::$app->session->setFlash('notify', 'На ваш e-mail отправлен новый пароль.');

            $user->resetPassword();
        }

        return $this->redirect('/');
    }

    public function actionLoadMore($page = 2) {
        $services = Service::find()->where(['is_popular' => 1])->limit(3)->offset(($page - 1) * 3);

        return $this->renderPartial('popular', [
                    'services' => $services,
        ]);
    }

    public function actionParseRegions() {
        throw new ServerErrorHttpException('test error');
        if (($handle = fopen("regions.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $locality = new Locality(['region_id' => 3, 'name' => $data[2]]);
                $locality->save();
            }
            fclose($handle);
            die('ok');
        } else {
            die('error');
        }
    }

}

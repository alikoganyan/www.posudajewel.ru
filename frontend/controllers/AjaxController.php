<?php

namespace frontend\controllers;

use common\components\Controller;
use common\models\Char;
use common\models\Service;
use common\models\ServiceType;
use yii\helpers\Json;
use yii\web\Response;

class AjaxController extends Controller {

    public function actionAnotherPrice($serviceId, $page = 0) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Service::findOne(['id' => $serviceId]);
        $query = $model->getAnotherPrice();

        $pages = ceil($query->count() / 6);
        $services = $query->offset($page * 6)->limit(6)->all();

        return [
            'html' => $this->renderPartial('another-price', [
                'services' => $services,
            ]),
            'nextPage' => $pages > $page + 1 ? $page + 1 : -1,
        ];
    }

    public function actionFilters($service_type, $service_char_values) {
        $serviceType = ServiceType::findOne(['id' => (int) $service_type]);

        if ($serviceType instanceof ServiceType) {
            return $this->renderPartial('filters', [
                'chars' => $serviceType->chars,
                'serviceCharValues' => json_decode($service_char_values, true)
            ]);
        } else {
            return '';
        }
    }

}

<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;
use common\components\Controller;
use common\models\News;

/**
 * Новости
 */
class NewsController extends Controller {

    /**
     * Просмотр новости
     * @param type $url
     * @return type
     */
    public function actionView($url) {
        $news = News::findOne(['url' => $url, 'is_blocked' => 0]);

        if (!$news instanceof News) {
            throw new NotFoundHttpException('news not found');
        }

        return $this->render('view', [
                    'news' => $news
        ]);
    }

}

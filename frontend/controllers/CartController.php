<?php

namespace frontend\controllers;
use common\models\Service;
use Yii;

class CartController extends \common\components\Controller {
    public $layout = false;
    public function actionCreate($id) {
        $product = Service::findOne($id);
        if ($product) {
            Yii::$app->cart->create($product);
            return $this->render('_content');
        }
    }
    public function actionIndex() {
        return $this->render('_content');
    }
    public function actionDelete($id) {
        $product = Service::findOne($id);
        if ($product) {
            Yii::$app->cart->delete($product);
            return $this->render('_content');
        }
    }
    public function actionUpdate($id, $quantity) {
        $product = Service::findOne($id);
        if ($product) {
            Yii::$app->cart->update($product, $quantity);
            return $this->render('_content');
        }
    }

    public function actionCheckout() {
        $this->redirect(['/admin/order/user-create']);
    }

}

<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use common\components\Controller;
use common\models\Service;

/**
 * Поиск
 */
class SearchController extends Controller {

    /**
     * Подсказки при поиске
     * @return type
     */
    public function actionList() {
        $type = (int) Yii::$app->getRequest()->get('service_type');
        $search = trim(Yii::$app->getRequest()->get('term'));

        $cond = ['AND'];
        $params = [];

        // Поиск в определенной категории
        if ($type > 0) {
            $cond[] = 's.service_type_id=:type';
            $params[':type'] = $type;
        }

        // Поиск по фразе
        if (!empty($search)) {
            $cond[] = ['LIKE', 's.name', $search];
        }


        $list = Service::findNotBlocked()->
                select(['s.name'])->
                andWhere($cond, $params)->
                limit(10)->
                column();

        return Json::encode($list);
    }

}

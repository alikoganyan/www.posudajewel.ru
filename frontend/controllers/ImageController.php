<?php
namespace frontend\controllers;

use common\components\Controller;
use yii\web\Response;

class ImageController extends Controller {
    public function actionIndex($imgName) {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        \Yii::$app->response->headers->add('content-type','image/png');
        \Yii::$app->response->data = file_get_contents(\Yii::getAlias('@backend') . '/web/uploads' . $imgName);
        return \Yii::$app->response;
    }
}

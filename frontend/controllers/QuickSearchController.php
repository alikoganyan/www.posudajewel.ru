<?php
namespace frontend\controllers;

use common\components\Controller;
use common\models\Company;
use common\models\QuickSearch;
use common\models\Service;
use yii\helpers\Json;
use yii\web\Response;

class QuickSearchController extends Controller {
    public function actionIndex($localities = null, $page = 0) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $quickSearch = QuickSearch::find()->all();
        $res         = array_chunk($quickSearch, 6);
        $result      = [];

        if ($res) {
            foreach ($res[$page - 1] as $item) {
                /** @var $item QuickSearch */
                $search = Service::doSearch($item->doSearch(), $item->service_type_id, $localities);
                if ($search->count() > 0) {
                    $result[$item->id] = $search;
                }
            }
        }

        $pages = ceil(count($quickSearch) / 6);

        return [
            'html' => $this->renderAjax('index', [
                'quickSearch' => $quickSearch,
                'result'      => $result,
                'localities'  => $localities,
            ]),
            'nextPage' => $pages > $page ? $page + 1 : -1,
        ];
    }
}

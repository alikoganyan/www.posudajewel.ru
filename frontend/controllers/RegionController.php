<?php
namespace frontend\controllers;

use common\components\Controller;
use common\models\Company;
use common\models\Locality;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class RegionController extends Controller {
    public function actionLocalities($region_id) {
        $localities = Locality::findAll(['region_id' => $region_id]);
        $companies = Company::findAll(['locality_id' => ArrayHelper::getColumn($localities, 'id')]);

        $result = [];
        foreach ($companies as $company) {
            if ($company->services != null) {
                $result[] = $company->locality_id;
            }
        }

        return Json::encode(Locality::findAll(['id' => $result]));
    }
}

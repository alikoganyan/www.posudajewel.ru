<?php
namespace frontend\controllers;

use Yii;
use common\components\Controller;
use common\models\User;
use yii\authclient\OAuth2;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AuthController extends Controller {
    public function actions() {
        return [
            'auth' => [
                'class'           => 'common\components\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }

    /**
     * @param $client OAuth2
     */
    public function authSuccess($client) {
        $authType = \Yii::$app->request->getQueryParam('authclient');
        $attributes = $client->getUserAttributes();

        switch ($authType) {
            case 'facebook':
                $user = User::findOne(['email' => $attributes['email']]);
                if ($user) {
                    $user->setScenario(User::SCENARIO_SOCIAL);
                    $user->fb = $attributes['link'];
                    $user->save();

                    $user->login();
                }
                else {
                    $user = new User([
                        'first_name' => $attributes['first_name'],
                        'last_name'  => $attributes['last_name'],
                        'gender'     => $attributes['gender'],
                        'fb'         => $attributes['link'],
                        'email'      => $attributes['email'],
                    ]);
                    $user->setScenario(User::SCENARIO_SOCIAL);

                    if ($user->validate()) {
                        $user->save();
                        $user->login();
                    }
                }

                break;

            case 'vkontakte':
                $user = null;
                if (isset($attributes['email'])) {
                    $user = User::findOne(['email' => $attributes['email']]);
                }

                if (!$user) {
                    $user = User::findOne(['vk_id' => $attributes['uid']]);
                }

                if ($user) {
                    $user->setScenario(User::SCENARIO_SOCIAL);
                    $user->vk_id = $attributes['uid'];
                    $user->save();

                    $user->login();
                }
                else {
                    $user = new User([
                        'first_name' => $attributes['first_name'],
                        'last_name'  => $attributes['last_name'],
                        'gender'     => User::$genders[$attributes['sex'] - 1],
                        'email'      => $attributes['email'],
                        'vk_id'         => $attributes['uid'],
                    ]);
                    $user->setScenario(User::SCENARIO_SOCIAL);

                    if ($user->validate()) {
                        $user->save();
                        $user->login();
                    }
                }

                break;

            case 'google':
                $email = $attributes['emails'][0]['value'];

                $user = User::findOne(['email' => $email]);
                if ($user) {
                    $user->setScenario(User::SCENARIO_SOCIAL);
                    $user->google_id = $attributes['id'];
                    $user->save();

                    $user->login();
                }
                else {
                    $gender = User::$genders[$attributes['gender'] == 'male'];
                    $user = new User([
                        'first_name' => $attributes['name']['givenName'],
                        'last_name'  => $attributes['name']['familyName'],
                        'gender'     => $gender,
                        'email'      => $email,
                        'google_id'  => $attributes['id'],
                    ]);
                    $user->setScenario(User::SCENARIO_SOCIAL);

                    if ($user->validate()) {
                        $user->save();
                        $user->login();
                    }
                }

                break;

            case 'yandex':
                $user = User::findOne(['email' => $attributes['default_email']]);
                if ($user) {
                    $user->setScenario(User::SCENARIO_SOCIAL);
                    $user->yandex = $attributes['id'];
                    $user->save();

                    $user->login();
                }
                else {
                    $gender = User::$genders[$attributes['sex'] == 'male'];

                    $user = new User([
                        'first_name' => $attributes['first_name'],
                        'last_name'  => $attributes['last_name'],
                        'gender'     => $gender,
                        'email'      => $attributes['default_email'],
                        'yandex'     => $attributes['id'],
                    ]);
                    $user->setScenario(User::SCENARIO_SOCIAL);

                    if ($user->validate()) {
                        $user->save();
                        $user->login();
                    }
                }

                break;
        }

        return $this->redirect($this->goToPreviousPage());
    }

    public function actionRegistration() {
        $model = new User();
        $model->setScenario(User::SCENARIO_CREATE);

        if (\Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sendConfirmEmail();
            $model->save();
        }

        return $this->redirect($this->goToPreviousPage());
    }

    public function actionResetRegistration() {
        $model = User::findOne(['email' => \Yii::$app->user->identity->email]);

        if (!$model) {
            throw new NotFoundHttpException('user does not exist');
        }

        $model->sendConfirmEmail();
        $model->save();

        return $this->redirect($this->goToPreviousPage());
    }

    public function actionSignIn() {
        $model = new User();
        $model->setScenario(User::SCENARIO_LOGIN);
        
        if (\Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        $model = User::findOne(['email' => Yii::$app->request->post('User')['email']]);
        if ($model instanceof User) {
            $model->setScenario(User::SCENARIO_LOGIN);
            $model->load(Yii::$app->request->post());
            $model->login();
        }

        return $this->redirect($this->goToPreviousPage());
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionConfirmRegistration($token) {
        $user = User::findOne(['confirm_token' => $token]);

        if (!$user) {
            throw new NotFoundHttpException('user does not exist');
        }

        $user->setScenario(User::SCENARIO_CONFIRM_REG);
        $user->save();

        return $this->redirect($this->goToPreviousPage());
    }

    public function actionFinishRegistration() {
        $userId = Yii::$app->request->post('User')['id'];
        $user = User::findOne(['id' => $userId]);

        if (!$user) {
            throw new NotFoundHttpException('user does not exist');
        }

        if (\Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $user->load(Yii::$app->request->post());
            return ActiveForm::validate($user);
        }

        $user->setScenario(User::SCENARIO_FINISH_REG);
        $user->email = Yii::$app->request->post('User')['email'];
        $user->save();

        return $this->redirect($this->goToPreviousPage());
    }
}

<?php
namespace frontend\controllers;

use yii\web\NotFoundHttpException;
use common\components\Controller;
use common\models\Page;

class PageController extends Controller {
    public function actionView($url) {
        $page = Page::findOne(['url' => $url, 'is_blocked' => 0]);

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        $listPage = Page::find()
            ->select(['url', 'name'])
            ->where(['is_blocked' => 0])
            ->asArray()
            ->all();

        return $this->render('view', [
            'page' => $page,
            'listPage' => $listPage,
        ]);
    }
}

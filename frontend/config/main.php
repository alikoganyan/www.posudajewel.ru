<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Интерхолдинг СПМ',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'common\components\CartBootstrap'
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => '/',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'sitemap', 'route' => '/sitemap/index', 'suffix' => '.xml'],
                '/<controller:\w+>/<action:\w+>/<id:\d+>' => '/<controller>/<action>',
                '/offer/<url:[a-z0-9\_\-]+>' => 'site/details',
                '/news/<url:[a-z0-9\_\-]+>' => 'news/view',
                '/page/<url:[a-z0-9\_\-]+>' => 'page/view',
                '/search/<action:(list)>' => 'search/list',
                '/search/<url:[a-z0-9\_\-]+>' => 'site/search',
            ],
        ],
    ],
    'params' => $params,
];

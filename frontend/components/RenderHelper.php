<?php

namespace frontend\components;

/**
 * Вспомогательный класс для отображение данных
 */
class RenderHelper {

    /**
     * Обрезка длины названия
     * @param string $name
     * @return string
     */
    public static function serviceName($name) {
        if (mb_strlen($name, 'UTF-8') > 75) {
            $name = mb_substr($name, 0, 75, 'UTF-8') . '...';
        }

        return $name;
    }

}

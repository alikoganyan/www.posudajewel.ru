<?php

namespace frontend\components;

use Yii;
use yii\web\Cookie;

/**
 * 
 */
class CityHelper {

    /**
     * Ключ к данным
     */
    const CITY_KEY = 'city';

    /**
     * Город по умолчанию
     */
    const DEFAULT_CITY = 'Барнаул';

    /**
     * Данные
     * @var type 
     */
    protected static $city;

    /**
     * Город пользователя
     * @return string
     */
    public static function getCity() {
        self::loadData();

        return empty(self::$city) ? self::DEFAULT_CITY : self::$city;
    }

    /**
     * Сохраняем город
     * @param string $name
     */
    public static function setCity($name) {
        self::$city = trim((string) $name);

        Yii::$app->response->cookies->add(new Cookie([
            'name' => self::CITY_KEY,
            'value' => self::$city,
            'expire' => time() + 86400 * 180,
        ]));
    }

    /**
     * Загрузка данных
     */
    public static function loadData() {
        if (is_null(self::$city)) {
            self::$city = (string) Yii::$app->getRequest()->getCookies()->getValue(self::CITY_KEY);
        }
    }

}

<?php
namespace frontend\components;

class Marker extends \dosamigos\google\maps\overlays\Marker {
    public $markerName;

    public function getName($autoGenerate = true) {
        return $this->markerName . '_gMarker';
    }

    public function getJs() {
        $js = $this->getInfoWindowJs();

        $js[] = "{$this->getName()} = new google.maps.Marker({$this->getEncodedOptions()});";
        $js[] = "maps.addMarker('{$this->getName()}', {$this->getName()});";

        foreach ($this->events as $event) {
            /** @var \dosamigos\google\maps\Event $event */
            $js[] = $event->getJs($this->getName());
        }

        return implode("\n", $js);
    }
}

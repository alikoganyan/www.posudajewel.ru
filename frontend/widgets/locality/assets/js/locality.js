$(function () {

    $('#delivery form').submit(function () {
        return false;
    });

    $('#delivery form input[name=locality-search]').bind('keyup', function () {
        searchCity($(this).val());
    });

    $('.delivery-link').click(function (e) {
        if (!$('#delivery').hasClass('vis')) {
            searchCity('');

            $('.dropdown').removeClass('vis');
            $('#delivery').addClass('vis');
            setTimeout(function () {
                $('#regions').getNiceScroll().resize();
                $('#regions').getNiceScroll().show();
            }, 700);
        }
        e.preventDefault();
    });
    $('#delivery .crossy').click(function () {
        dropup();
    });



    $('#regions').on('click', 'li.region', function () {
        $('#regions li.region').removeClass('active');
        $(this).addClass('active');

        var region = $(this).children('span').text();
        $('#region-title').text(region);

        $('#regions li.region ul.cities').hide();
        $('#regions li.active ul.cities').show();
    });


    $('#delivery').on('click', 'a.locality-choice', function () {
        City.setCity($(this).data('id'));

        $('#delivery .crossy').click();

        return false;
    });

});

var searchCity = function (search) {
    var list = City.loadCity(search),
            regions = $('#regions');

    regions.find('li').remove();
    $.each(list, function (index) {
        if (this.items.length > 0) {
            var content = '<span>' + $('<div/>').html(this.name).text() + '</span>' +
                    '<ul class="cities" id="reg' + this.id + '">';

            $.each(this.items, function () {
                content += '<li><a href="#" class="locality-choice" data-id=' + this.id + '>' + $('<div/>').html(this.name).text() + '</a></li>';
            });

            content += '</ul>';

            regions.append('<li class="region">' + content + '</li>');
        }
    });

    $('#regions li.region:eq(0)').click();

    $('#regions').niceScroll({
        cursorcolor: '#518bbd',
        cursorborder: '1px solid #518bbd',
        cursoropacitymin: 1,
        cursorwidth: 8,
        railoffset: {left: 2},
        mousescrollstep: 10
    });
};

var City = {
    loadCity: function (search) {
        var list = [];

        $.ajax({
            data: {
                search: search
            },
            url: $('#delivery').data('list'),
            async: false,
            method: 'GET',
            dataType: 'json',
            success: function (data, textStatus) {
                if (data.status == 'success') {
                    list = data.data.list;
                }
            },
            error: function () {
            }
        });

        return list;
    },
    setCity: function (id) {
        $.ajax({
            data: {
                id: id
            },
            url: $('#delivery').data('save'),
            method: 'GET',
            dataType: 'json',
            success: function (data, textStatus) {
                if (data.status == 'success') {
                    $('.city-choose span').text(data.data.name);
                }
            },
            error: function () {
            }
        });
    }
};
<?php

namespace frontend\widgets\locality;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{

    public $sourcePath = '@frontend/widgets/locality/assets';
    public $css = [
    ];
    public $js = [
        'js/locality.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}

<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<!-- delivery -->
<div id="delivery" class="dropdown" data-save="<?= Url::to('/locality/save') ?>" data-list="<?= Url::to('/locality/list') ?>">
    <div class="crossy">
        <div class="line1"></div>
        <div class="line2"></div>
    </div>
    <div class="top-part">
        <h3>Доставка осуществляется на территории всей России</h3>
        <form action="#" method="post" class="dropdown-search flex-wrapper flex-space-b">
            <input class="flex-grow-1" type="search" name="locality-search" id="search2" placeholder="Город или область">
            <input type="submit" value="Найти">
        </form>
        <p>Например: 
            <?php
            $index = 0;
            foreach ($quickChoice as $localityId => $localityName) {
                if ($index > 0) {
                    echo ", ";
                }
                echo Html::a(Html::encode($localityName), '#', ['class' => 'locality-choice', 'data-id' => $localityId]);

                $index++;
            }
            ?>
        </p>
    </div>
    <div class="bottom-part">
        <div class="h3-wrap">
            <h3>Все города и регионы</h3>
            <p class="region-title" id="region-title"></p>
        </div>
        <div class="regions-wrap">
            <div class="wrapper">
                <div class="lead"></div>
                <div class="top-padd"></div>
                <div class="bot-padd"></div>
                <ul class="regions" id="regions">
                </ul>
            </div>
        </div>
        <p>Выбор города поможет предоставить актуальную информацию о наличии товара, его цены и способов доставки в вашем городе! Это поможет сохранить больше свободного времени для вас!</p>
    </div>
</div>
<!-- </delivery -->
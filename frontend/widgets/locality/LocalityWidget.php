<?php

namespace frontend\widgets\locality;

use Yii;
use yii\base\Widget;
use common\models\Locality;

/**
 * Выбор города
 */
class LocalityWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {

        $view = Yii::$app->getView();
        Asset::register($view);

        // Список городов для быстрого выбора
        $quickChoice = Locality::find()->
                select(['name', 'id'])->
                where(['is_quick_choice' => 1])->
                indexBy('id')->
                asArray()->
                column();

        foreach ($quickChoice as &$name) {
            $temp = explode('|', $name);
            $name = trim($temp[0]);
        }

        return $this->render('locality', [
                    'quickChoice' => $quickChoice,
        ]);
    }

}

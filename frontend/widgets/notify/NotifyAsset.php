<?php
namespace frontend\widgets\notify;

use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle {
    public $sourcePath = '@frontend/widgets/notify';
    public $js = [
        'notify.min.js',
    ];
    public $css = [
        'style.css',
    ];

    public $depends  = [
        'yii\web\YiiAsset',
    ];
}

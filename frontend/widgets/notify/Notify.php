<?php

namespace frontend\widgets\notify;

use yii\base\Widget;

class Notify extends Widget {

    public $message;
    public $type = 'success';

    public function run() {
        NotifyAsset::register($this->view);

        if (!empty($this->message)) {
            $this->view->registerJs("$.notify('{$this->message[0]}', '{$this->type}')");
        }
    }

}

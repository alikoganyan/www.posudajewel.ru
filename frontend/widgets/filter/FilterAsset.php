<?php
namespace frontend\widgets\filter;

use yii\web\AssetBundle;

class FilterAsset extends AssetBundle {
    public $sourcePath = '@frontend/widgets/filter';

    public $js = [
        'custom.js'
    ];

    public $depends  = [
        'yii\web\YiiAsset',
    ];
}

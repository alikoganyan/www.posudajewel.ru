<?php
namespace frontend\widgets\filter;

use common\models\Locality;
use common\models\Region;
use common\models\ServiceType;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Filter extends Widget {
    public $queryParams;
    public $chars;
    public $serviceType;
    public $search;
    public $serviceCharValues;
    public $selectedChars = [];

    public function run() {
        FilterAsset::register($this->getView());

        $regions       = Region::find()->all();
        $serviceTypes  = ServiceType::find()->all();
        $localities    = [];
        $region        = $regions[0];

        if (isset($this->queryParams['chars']))
            $this->selectedChars = Json::decode($this->queryParams['chars']);

        if (isset($this->queryParams['localities'])) {
            $localities = Json::decode($this->queryParams['localities']);
            if (!$localities) {
                $localities = ArrayHelper::getColumn(Region::find()->one()->localities, 'id');
            }
            else {
                $locality = Locality::findOne(['id' => $localities[0]]);
                $region   = $locality->region;
            }
        }

        return $this->render('index', [
            'region'        => $region,
            'regions'       => ArrayHelper::map($regions, 'id', 'name'),
            'serviceTypes'  => ArrayHelper::map($serviceTypes, 'id', 'name'),
            'chars'         => $this->chars,
            'selectedChars' => $this->selectedChars,
            'localities'    => $localities,
            'serviceCharValues' => $this->serviceCharValues,
            'serviceType'   => isset($this->queryParams['service_type']) && !empty($this->queryParams['service_type']) ? Json::decode($this->queryParams['service_type']): $this->serviceType,
            'search' => $this->search,
        ]);
    }
}

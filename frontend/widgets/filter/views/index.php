<?php

/**
 * @var $this          \yii\web\View
 * @var $regions       \common\models\Region[]
 * @var $region        \common\models\Region
 * @var $chars         \common\models\Char[]
 * @var $serviceCharValues []
 * @var $localities    []
 * @var $serviceType   integer
 * @var $selectedChars []
 * @var $serviceTypes  []
 */
use \yii\bootstrap\Html;
use \yii\helpers\ArrayHelper;
use \backend\widgets\activeForm\ActiveForm;
?>

<form id="filter-form" action="#" method="post" class="aside">
    <input type="hidden" name="search" value="<?= Html::encode($search) ?>"/>
    <div class="aside-block">
        <h3>Вид товара</h3>
        <div class="aside-block-cont">
            <?=
            Html::dropDownList('service_type', $serviceType, ['' => 'Все товары'] + $serviceTypes, [
                'class' => 'sel',
                'onchange' => 'filter.onSubmit(this)'
            ])
            ?>
        </div>
    </div>

    <div class="aside-block sm">
        <h3>Стоимость</h3>
        <div class="aside-block-cont">
            <div class="digit-field-group flex-wrapper flex-space-b">
                <label for="from">
                    От
                    <input type="text" class="" name="price_from" id="from">
                </label>
                <label for="to">
                    До
                    <input type="text" class="" name="price_to" id="to">
                </label>
            </div>
        </div>
    </div>

    <div id="filters">
        <?php foreach ($chars as $char): ?>
            <?php if ($char->is_filter): ?>
                <div class="aside-block filters">
                    <h3 class="clpsd"><?= Html::encode($char->name) ?></h3>
                    <div class="aside-block-cont" style="display:none">
                        <?php if ($char->is_multiple): ?>
                            <?php foreach ($char->values as $value): ?>
                            <?php if (isset($serviceCharValues[$char->id])): ?>
                                <?php if (in_array($value->id, $serviceCharValues[$char->id])): ?>
                                    <label>
                                        <input value="<?= $value->id ?>" type="checkbox" char="<?= $char->id ?>"
                                               <?php if (isset($selectedChars[$char->id]) && in_array($value->id, $selectedChars[$char->id])): ?>checked<?php endif ?>
                                        ><?= Html::encode($value->name) ?>
                                    </label>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <?php

                                $filters = (isset($serviceCharValues[$char->id]))
                                    ? ArrayHelper::filter(ArrayHelper::map($char->values, 'id', 'name'), $serviceCharValues[$char->id])
                                    : ArrayHelper::map($char->values, 'id', 'name');

                                echo Html::dropDownList("char[{$char->id}][]", isset($selectedChars[$char->id]) ? $selectedChars[$char->id] : null,
                                    ['' => 'Любое значение'] + $filters, [
                                    'class' => 'sel filter-item',
                                    'char' => $char->id,
                                    'data-smart-positioning' => 'false',
                                ]);
                            ?>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif ?>
        <?php endforeach ?>
    </div>
    <div class="aside-block">
        <input type="submit" onclick="filter.onSubmit(this)" value="Применить">
    </div>
</form>
<!--
<div class="extended-search" id="extended-search">
    <form id="filter-form" class="smart-form">
        <div class="form-section opened">
            <h3>Место отдыха</h3>
            <div class="section-content rest-place">
<?=
Html::dropDownList('region', $region->id, $regions, [
    'id' => 'select-region-2',
    'onchange' => 'filter.getLocalities(this)',
])
?>

                <div id="punkt">
                    <p><span>ВСЕ</span></p>
                </div>
                <div id="select-punkt">
                    <div class="control-buttons">
                        <div id="choose-all-localities" onclick="localitySelect.chooseAll(this);">Выбрать все</div>
                        <div id="cancel-all-localities" onclick="localitySelect.cancelAll(this);">Убрать выбор</div>
                    </div>
                    <div id="localities">
<?php foreach ($region->localities as $key => $locality): ?>
                                                                                    <label for="item<?= $key ?>">
                                                                                        <input type="checkbox" onclick="localitySelect.selectLocality(this);" name="item<?= $key ?>" id="item<?= $key ?>" value="<?= $locality->id ?>"
    <?php if (in_array($locality->id, $localities)): ?>checked<?php endif ?>>
    <?= $locality->name ?>
                                                                                    </label>
<?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-section opened">
            <h3>Категория</h3>
            <div class="section-content rest-place">
<?=
Html::dropDownList('service_type', $serviceType, $serviceTypes, [
    'onchange' => 'filter.loadCategories(this)'
])
?>
            </div>
        </div>

        <div id="filters">
<?php foreach ($chars as $char): ?>
                                                                        <div class="form-section filters<?php if (isset($selectedChars[$char->id])): ?> opened<?php endif ?>">
                                                                            <h3><?= $char->name ?></h3>
                                                                            <div class="section-content service-type"<?php if (isset($selectedChars[$char->id])): ?> style="display: block;<?php endif ?>">
    <?php if ($char->is_multiple): ?>
        <?php foreach ($char->values as $value): ?>
                                                                                                                                                                                                        <label for="just-married">
                                                                                                                                                                                                            <input value="<?= $value->id ?>" type="checkbox" class="filter-item" char="<?= $char->id ?>"
            <?php if (isset($selectedChars[$char->id]) && in_array($value->id, $selectedChars[$char->id])): ?>checked<?php endif ?>
                                                                                                                                                                                                            ><?= $value->name ?>
                                                                                                                                                                                                        </label>
        <?php endforeach ?>
    <?php else: ?>
        <?=
        Html::dropDownList("char[{$char->id}][]", isset($selectedChars[$char->id]) ? $selectedChars[$char->id] : null, ['' => 'Выберите значение'] + ArrayHelper::map($char->values, 'id', 'name'), [
            'class' => 'filter-item',
            'char' => $char->id,
        ])
        ?>
    <?php endif ?>
                                                                            </div>
                                                                        </div>
<?php endforeach ?>
        </div>

        <div class="submit-wrap">
            <input class="apply-filter" type="button" onclick="filter.onSubmit(this)" value="Применить фильтр">
        </div>
    </form>
</div>

-->

class Filter {
    loadCategories(e, service_char_values) {
          if (e) {
            $.ajax({
              url: '/ajax/filters',
              method: 'GET',
              data: {
                service_type: $(e).val(),
                service_char_values: JSON.stringify(service_char_values)
              }
            }).done(response => {
              $('#filters').html(response);
              $('#filters').find('select, input[type="checkbox"], input[type="radio"]').styler();
            });
          }
    }

    openFilter(e) {
        if (jQuery(e).siblings('.section-content').is(':visible')) {
            jQuery(e).siblings('.section-content').slideUp();
            jQuery(e).parent('.form-section').removeClass('opened');
        }
        else {
            jQuery(e).siblings('.section-content').slideDown();
            jQuery(e).parent('.form-section').addClass('opened');
        }
    }

    onSubmit(e, page) {
        let result     = {},
            localities = [];
    
        if(!page){
            $('.search-results .view-more').data('page', 0);
            page = 0;
        }

        $('#filter-form .filters :selected').each((key, value) => {
            if ($(value).val().length < 1) {
                return;
            }

            let charId     = $(value).parents('.filter-item').attr('char');
            result[charId] = [$(value).val()];
        });

        $('#filter-form .filters [type="checkbox"]:checked').each((key, value) => {
            let charId = $(value).attr('char');

            if (result.hasOwnProperty(charId)) {
                result[charId].push($(value).val());
            }
            else {
                result[charId] = [$(value).val()];
            }
        });

        $('#localities :checked').each((key, value) => {
            localities.push($(value).val());
        });

        let params = location.search.split('&');
        params[0]  = 'service_type=' + $('#filter-form [name="service_type"]').val();
        params[1]  = 'chars=' + encodeURI(JSON.stringify(result));
        params[2]  = 'localities=' + encodeURI(JSON.stringify(localities));
        params[3]  = 'price_from=' + $('[name="price_from"]').val();
        params[4]  = 'price_to=' + $('[name="price_to"]').val();
        params[5]  = 'search=' + $('#filter-form [name="search"]').val();
        params[6]  = 'sort=' + ($('.search-results span.sort-price').hasClass('up')?'price_desc':'price_asc');

        localStorage.setItem('filter-search', '1');

        //window.location = '/site/search?' + params.join('&');
        
            $.ajax({
                type: 'GET',
                url: '/site/search?page=' + page + '&' + params.join('&'),
                dataType: 'json',
                success: function (data, textStatus) {
                    $('.search-results .view-more').data('page', data.nextPage);
                    
                    if(page > 0){
                      $('#quick-search-more').append(data.html);
                    }else{
                      $('#quick-search-more').html(data.html);
                    }
            
                    if(data.nextPage > 0){
                        $('.search-results .view-more').show();
                    }else{
                        $('.search-results .view-more').hide(); 
                    }
                    filter.loadCategories(e, data.serviceCharValues);
                }
            });
    }

    getLocalities(el) {
        $.ajax({
            url: '/region/localities',
            method: 'GET',
            data: {
                region_id: $(el).val()
            }
        }).done(response => {
            let html = '';

            $.parseJSON(response).forEach(locality => {
                html += '<label for="item1">' +
                    '<input type="checkbox" onclick="localitySelect.selectLocality(this);" value="' + locality.id + '" checked name="item'+locality.id+'" id="item'+locality.id+'">' +
                    locality.name +
                    '</label>';
            });

            this.localityEvents();

            $('#localities').html(html);
            
            filter.onSubmit();
        });
    }

    localityEvents() {
        jQuery('#choose-all-localities').click(function() {
            var allCheckboxes = jQuery('#localities input:checkbox');
            var notChecked = allCheckboxes.not(':checked');
            notChecked.prop('checked', true);
            notChecked.prop('checked', true);
            filter.onSubmit();
        });
        jQuery('#cancel-all-localities').click(function() {
            var allCheckboxes = jQuery('#localities input:checkbox');
            var notChecked = allCheckboxes.not(':checked');
            notChecked.prop('checked', true);
            allCheckboxes.prop('checked', false);
            filter.onSubmit();
        });
    }
}

let filter = new Filter();

$(function () {
    filter.localityEvents();
});

<?php
namespace frontend\widgets\pager;

use yii\web\AssetBundle;

class PagerAsset extends AssetBundle {
    public $sourcePath = '@frontend/widgets/pager';

    public $css = [
        'styles.css',
    ];

    public $js = [
        'loader.js'
    ];
}
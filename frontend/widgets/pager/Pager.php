<?php
namespace frontend\widgets\pager;

use yii\base\Widget;
use yii\bootstrap\Html;

class Pager extends Widget {
    public $pagination;

    public function run() {
        PagerAsset::register($this->getView());

        return '<div id="more">' . Html::button('Показать больше', [
                'class'   => 'btn btn-default view-more load-more',
                'onclick' => "loader.load()",
            ]) . '</div>';
    }
}

class Loader {
    constructor() {
        this.page = 1;
    }

    load(page) {
        $.ajax({
            url: '/site/index',
            method: 'GET',
            data: {page: this.page}
        }).done(response => {
            $('.more').append(response);
            this.page++;
        });
    }
}

let loader = new Loader();

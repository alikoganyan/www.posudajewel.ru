<?php

namespace frontend\widgets\news;

use yii\base\Widget;
use common\models\News;

/**
 * Вывод новостей на главной
 */
class NewsWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {
        $news = News::find()->
                select(['id', 'type', 'url', 'name', 'image', 'description', "DATE_FORMAT(date,'%d.%m.%Y') as date_create"])->
                where(['is_blocked' => 0])->
                orderBy('date DESC, id DESC')->
                limit(8)->
                all();

        return $this->render('news', [
                    'news' => $news,
        ]);
    }

}

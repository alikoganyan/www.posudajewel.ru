<?php

use yii\helpers\Url;
use yii\helpers\Html;

$count = count($news);
?>
<?php if ($count > 0): ?>
    <section class="section">
        <div class="container">
            <h2 class="padding-lr-15">Новости, статьи, обзоры</h2>

            <div class="tabs-group flex-wrapper flex-space-b flex-flow-wrap padding-lr-15">
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php if (isset($news[$i])): ?>
                        <?php $item = $news[$i]; ?>
                        <a href="<?= Url::to(['news/view', 'url' => $item->url]) ?>" class="tab2">
                            <span class="tab-ins">
                                <span class="img-wrap">
                                    <img src="<?= !empty($item->image) ? $item->getUrlImage() : '/img/img-2.png' ?>" alt="">
                                </span>
                                <span class="tab-descript">
                                    <span class="descr-title"><?= Html::encode($item->name) ?></span>
                                    <?php if (!empty($item->description)): ?>
                                        <span class="descr-text"><?= $item->description ?></span>
                                    <?php endif; ?>
                                </span>
                                <span class="bot-grad"></span>
                            </span>
                            <span class="bottom-part">
                                <span class="news-type"><?= Html::encode($item->getTypeName()) ?></span>
                                <span class="news-date"><?= $item->date_create ?></span>
                            </span>
                        </a>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

            <?php if ($count > 4): ?>
                <div class="more">
                    <div class="tabs-group flex-wrapper flex-space-b flex-flow-wrap padding-lr-15">
                        <?php for ($i = 4; $i < $count; $i++): ?>
                            <?php if (isset($news[$i])): ?>
                                <?php $item = $news[$i]; ?>
                                <a href="<?= Url::to(['news/view', 'url' => $item->url]) ?>" class="tab2">
                                    <span class="tab-ins">
                                        <span class="img-wrap">
                                            <img src="<?= !empty($item->image) ? $item->getUrlImage() : '/img/img-2.png' ?>" alt="">
                                        </span>
                                        <span class="tab-descript">
                                            <span class="descr-title"><?= Html::encode($item->name) ?></span>
                                            <?php if (!empty($item->description)): ?>
                                                <span class="descr-text"><?= $item->description ?></span>
                                            <?php endif; ?>
                                        </span>
                                        <span class="bot-grad"></span>
                                    </span>
                                    <span class="bottom-part">
                                        <span class="news-type"><?= Html::encode($item->getTypeName()) ?></span>
                                        <span class="news-date"><?= $item->date_create ?></span>
                                    </span>
                                </a>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                </div>

                <div class="view-more-wrap padding-lr-15">
                    <p class="view-more">Показать больше &rarr;</p>
                    <p class="hide-this none">Скрыть</p>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>
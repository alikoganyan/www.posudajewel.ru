<?php

namespace frontend\widgets\search;

use Yii;
use yii\base\Widget;
use common\models\ServiceType;

/**
 * Вывод формы поиска
 */
class SearchWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {
        $view = Yii::$app->getView();
        Asset::register($view);

        // Категории
        $categories = ServiceType::find()->
                select(['name', 'id'])->
                indexBy('id')->
                column();

        return $this->render('search', [
                    'categories' => $categories,
        ]);
    }

}

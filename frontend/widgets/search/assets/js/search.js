$(function () {
    $('#search-top input[name=search]').autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: $('#search-top input[name=search]').data('url'),
                data: {
                    service_type: $('#search-top select[name=service_type]').val(),
                    term: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        change: function () {
            $('#search-top').submit();
        },
        minLength: 1
    });
});
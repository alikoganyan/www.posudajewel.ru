<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="search-button" id="search-button"></div>
<form id="search-top" action="<?= Url::to(['site/search']) ?>" method="GET" class="search-group flex-wrapper flex-space-b flex-grow-1">
    <select id="categories" name="service_type" data-smart-positioning="false">
        <?php foreach ($categories as $catId => $catName): ?>
            <option value="<?= $catId ?>"><?= Html::encode($catName) ?></option>
        <?php endforeach; ?>
        <option value="0" selected>Все категории</option>
    </select>
    <input class="flex-grow-1" type="search" name="search" id="search" data-url="<?= Url::to(['/search/list']) ?>">
    <input type="submit" value="Найти">
</form>
<?php

namespace frontend\widgets\search;

use yii\web\AssetBundle;

class Asset extends AssetBundle {

    public $sourcePath = '@frontend/widgets/search/assets';
    public $css = [
        'css/search.css',
    ];
    public $js = [
        'js/search.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
    ];

}

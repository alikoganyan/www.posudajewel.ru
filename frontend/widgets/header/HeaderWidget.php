<?php

namespace frontend\widgets\header;

use yii\base\Widget;

/**
 * 
 */
class HeaderWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {

        return $this->render('header', [
        ]);
    }

}

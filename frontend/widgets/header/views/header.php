<?php

use yii\helpers\Html;
?>
<div class="top-panel flex-wrapper flex-space-b">
    <div class="item flex-wrapper flex-align-center">
        <img src="/img/pict-2.png" alt="">
        <p>Весь товар сертифицирован</p>
    </div>
    <div class="item flex-wrapper flex-align-center">
        <img src="/img/pict-3.png" alt="">
        <p>14 дней на обмен и возврат</p>
    </div>
    <div class="item flex-wrapper flex-align-center">
        <img src="/img/pict-4.png" alt="">
        <p>Оплата любыми способами без %</p>
    </div>
    <div class="item-a">
        <p class="tel">8 (800) 700-07-68</p>
        <p class="work">Пн-Пт: 8:00-17:00 </p>
    </div>
</div>
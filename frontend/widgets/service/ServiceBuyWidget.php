<?php

namespace frontend\widgets\service;

use yii\base\Widget;
use common\models\Service;
use common\models\ServiceBuy;

/**
 * С этим товаром покупают
 */
class ServiceBuyWidget extends Widget {

    /**
     * Id просматриваемого товара
     * @var int 
     */
    public $id;

    /**
     * 
     * @return type
     */
    public function run() {
        // Похожие товары
        $serviceId = ServiceBuy::find()->
                select(['buy_id'])->
                where(['service_id' => $this->id])->
                column();

        if (!empty($serviceId)) {
            $items = Service::findService(['s.id' => $serviceId]);
        } else {
            $items = [];
        }


        return $this->render('buy', [
                    'items' => $items,
        ]);
    }

}

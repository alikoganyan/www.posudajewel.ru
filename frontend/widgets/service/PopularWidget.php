<?php

namespace frontend\widgets\service;

use yii\base\Widget;
use common\models\Service;

/**
 * Вывод популярных товаров
 */
class PopularWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {
        // Популярные товары
        $items = Service::findService(['s.is_popular' => 1]);


        return $this->render('popular', [
                    'items' => $items,
        ]);
    }

}

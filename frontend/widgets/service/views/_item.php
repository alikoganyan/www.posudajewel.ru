<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\components\RenderHelper;
?>
<style type="text/css">
.tabs-group .tab {height: 495px;}
.new-bottom-part {
    display: flex !important;
    justify-content: space-between;
    padding: 5px;
    overflow: hidden;
    height: 45px;
}
.product-name-group .product-price-wrap{
    display: flex !important;
    justify-content: space-between;
    margin: 15px 0 0px;
}
.product-name-group .product-price-wrap .old-price {
    color: #909090;
    font-size: 17px;
    text-decoration: line-through;
    text-indent: 5px;
    margin-bottom: 10px;
    font-family: 'PT Sans', serif;
    font-weight: bold;
}
.product-name-group .product-price-wrap .item-price {
    font-size: 20px;
    color: #528cbc;
}

.new-bottom-part .add-to-wrap .compare-link,
.new-bottom-part .add-to-wrap .favorite-link{
    display: inline-block;
    border: 2px solid #518bbd;
    color: #518bbd;
    border-radius: 4px;
    padding: 3px;
    font-size: 20px;
    margin: 0 3px;
}

.new-bottom-part .buy-button {
    font-size: 14px;
    width: auto;
    padding: 7px 14px;
    height: auto;
}
.new-bottom-part .buy-button span {
    display: inline-block;
    margin-right: 3px;
}
</style>
<div class="tab">
    <span class="tab-ins flex-space-b flex-dir-col flex-align-center">
        <span class="recomm">Рекомендуем</span>
        <?=Html::a(Html::img($item['image']), ['/site/details', 'url' => $item['url']]) ?>
        <span class="product-name-group">
            <span class="cat-name"><?= Html::encode($item['type_name']) ?></span>
            <span class="product-name"><?= Html::a(Html::encode(RenderHelper::serviceName($item['name'])), ['/site/details', 'url' => $item['url']]) ?></span>
            <span class="product-articul"><?= $item['code'] ?></span>
            <span class="product-price-wrap">
                <?php if (Yii::$app->user->id && Yii::$app->user->identity->discountToUser): ?>
                    <span class="old-price"><?= number_format($item['price'], 2, '.', ' ') ?> &#8381;</span>
                    <span class="item-price">
                        <?= number_format(
                            $item['price'] * (1-Yii::$app->user->identity->discountToUser->discount->discount),
                            2, '.', ' '
                        ) ?> &#8381;
                    </span>
                <?php else: ?>
                    <span class="item-price"><?= number_format($item['price'], 2, '.', ' ') ?> &#8381;</span>
                <?php endif ?>
            </span>
        </span>
    </span>
    <span class="new-bottom-part flex-align-center">
        <div class="add-to-wrap">
        <?php echo Html::tag( 'span', '<span class="glyphicon glyphicon-heart-empty"></span>', ['item-id'=>$item['id'],'class'=>'favorite-link']) ?>
        <?php echo Html::tag( 'span', '<span class="glyphicon glyphicon-stats"></span>', ['item-id'=>$item['id'],'class'=>'compare-link']) ?>
        </div>
        <?php echo Html::button('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;В корзину', ['item-id'=>$item['id'],'class'=>'blue-button buy-button']) ?>
    </span>
</div>

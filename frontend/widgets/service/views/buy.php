<?php

use yii\helpers\Url;
use yii\helpers\Html;

$count = count($items);
?>
<?php if ($count > 0): ?>
    <section class="section">
        <div class="container">
            <h2 class="padding-lr-15">Похожие товары</h2>

            <div class="tabs-group flex-wrapper flex-space-b flex-flow-wrap padding-lr-15">
                <?php
                for ($i = 0; $i < 4; $i++) {
                    if (isset($items[$i])) {
                        echo $this->render('_item', ['item' => $items[$i]]);
                    }
                }
                ?>
            </div>

            <?php if ($count > 4): ?>
                <div class="more">
                    <div class="tabs-group flex-wrapper flex-space-b flex-flow-wrap padding-lr-15">
                        <?php
                        for ($i = 4; $i < $count; $i++) {
                            echo $this->render('_item', ['item' => $items[$i]]);
                        }
                        ?>
                    </div>
                </div>

                <div class="view-more-wrap padding-lr-15">
                    <p class="view-more">Показать больше &rarr;</p>
                    <p class="hide-this none">Скрыть</p>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>
<?php
namespace frontend\widgets\service;

use yii\base\Widget;
use common\models\Service;

class NewWidget extends Widget {
    public function run() {
        $items = Service::findService(['s.is_new' => 1]);

        return $this->render('new', [
            'items' => $items,
        ]);
    }
}

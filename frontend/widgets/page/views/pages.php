<?php

use yii\helpers\Html;
?>

<h3 class="var2">Информация:</h3>
<ul>
    <?php foreach ($pages as $page): ?>
        <li><?= Html::a(Html::encode($page['name']), ['/page/view', 'url' => $page['url']]) ?></li>
    <?php endforeach; ?>
</ul>
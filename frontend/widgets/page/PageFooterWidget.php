<?php

namespace frontend\widgets\page;

use yii\base\Widget;
use common\models\Page;

/**
 * Блок информация
 */
class PageFooterWidget extends Widget {

    /**
     * 
     * @return type
     */
    public function run() {
        $pages = Page::find()->
                select(['name', 'url'])->
                where(['is_footer' => 1, 'is_blocked' => 0])->
                limit(5)->
                all();

        return $this->render('pages', [
                    'pages' => $pages,
        ]);
    }

}

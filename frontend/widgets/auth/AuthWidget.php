<?php
namespace frontend\widgets\auth;

use Yii;
use yii\base\Widget;
use common\models\User;

class AuthWidget extends Widget {
    public function run() {
        $view = Yii::$app->getView();
        Asset::register($view);
        
        $model = new User();

        return $this->render('auth', [
            'model' => $model,
        ]);
    }
}

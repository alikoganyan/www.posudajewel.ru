$(function () {

    $('#resetpasswordform').on('beforeSubmit', function () {
        $.ajax({
            data: $(this).serializeArray(),
            url: $(this).attr('action'),
            method: 'POST',
            dataType: 'json',
            success: function (data, textStatus) {
                if (data.status == 'success') {
                    $.notify(data.message, 'success');
                    
                    $('#restore .crossy').click();
                } else {
                    $.notify(data.message, 'error');
                }
            },
            error: function () {
                $.notify('Возникла ошибка при отправке данных', 'error');
            }
        });

        return false;
    });
});
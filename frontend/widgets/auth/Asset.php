<?php

namespace frontend\widgets\auth;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{

    public $sourcePath = '@frontend/widgets/auth/assets';
    public $css = [
        'css/auth.css',
    ];
    public $js = [
        'js/auth.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}

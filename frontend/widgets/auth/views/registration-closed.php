<?php
/**
 * @var $this \yii\web\View
 */
?>

<div id="registration" class="dropdown">
    <div class="crossy">
        <div class="line1"></div>
        <div class="line2"></div>
    </div>
    <div class="main-part">
        <h2>Регистрация временно недоступна. Зарегистрироваться на сайте можно будет с 01.09.2017</h2>
    </div>
</div>

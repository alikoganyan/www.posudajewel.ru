<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\User
 */

use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \frontend\widgets\social\Social;
use common\models\Settings;

$condition_text = Settings::getSetting('condition_text');
?>

<div id="registration" class="dropdown">
    <div class="crossy">
        <div class="line1"></div>
        <div class="line2"></div>
    </div>
    <div class="main-part">
        <h2>Регистрация</h2>

        <? $form = ActiveForm::begin([
            'id' => 'reg-form',
            'action' => Url::to(['auth/registration']),
            'method' => 'POST',
            'enableAjaxValidation' => true,
        ]) ?>
        <?= $form->field($model, 'email', ['enableAjaxValidation' => true])->textInput(['placeholder' => 'admin@email.com'])->label(false) ?>
        <div class="wrap">
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>
            <img id="eye" src="/img/pict-6.png" alt="">
        </div>
        <?= $form->field($model, 'repeat_password')->passwordInput(['placeholder' => 'Подтверждение пароля'])->label(false) ?>

        <label for="conditions" class="for-agree flex-wrapper flex-space-c">
            <input type="checkbox" name="conditions" id="conditions" required>
            Я принимаю &nbsp;<a href="#" data-toggle="modal" data-target="#conditionText">Пользовательское соглашение</a>
        </label>

        <p class="error"></p>
        <?= Html::input('submit', null, 'Регистрация', ['class' => 'blue-button']) ?>
        <?php ActiveForm::end() ?>
    </div>
    <div class="link-part flex-wrapper flex-space-b flex-align-center">
        <span class="entry-link">Войти</span>
        <div class="">
            <?= Social::widget() ?>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="conditionText">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="crossy close" data-dismiss="modal" aria-label="Close">
                    <div class="line1"></div>
                    <div class="line2"></div>
                </div>
                <h4 class="modal-title">Пользовательское соглашение</h4>
            </div>
            <div class="modal-body">
                <p>
                <?=$condition_text?>
                </p>
            </div>
            <div class="modal-footer hidden">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

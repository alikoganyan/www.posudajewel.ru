<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\User
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\widgets\social\Social;
?>

<?php if (Yii::$app->user->isGuest): ?>

    <!-- entry -->
    <div id="entry" class="dropdown">
        <div class="crossy">
            <div class="line1"></div>
            <div class="line2"></div>
        </div>
        <div class="main-part">
            <h2>Вход</h2>

            <?php
            $form = ActiveForm::begin([
                        'id' => 'sign-in-form',
                        'action' => Url::to(['auth/sign-in']),
                        'method' => 'POST',
                        'options' => [
                            'onchange' => "resetForm()",
                        ],
                    ])
            ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'admin@email.com'])->label(false)->hint(false) ?>
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false)->hint(false) ?>

            <p class="error"></p>

            <div class="wrap flex-wrapper flex-space-b">
                <label class="flex-wrapper flex-align-center" for="remember">
                    <input type="checkbox" name="User[rememberMe]" id="remember">
                    Запомнить меня
                </label>
                <?= Html::input('submit', null, 'Войти', ['class' => 'blue-button', 'onclick' => 'return signIn(this)']) ?>
            </div>

            <?php ActiveForm::end() ?>
        </div>
        <div class="link-part flex-wrapper flex-space-b flex-align-center">
            <span class="restore-link">Вспомнить пароль</span>
            <span class="register-link">Регистрация</span>
        </div>
        <div class="social-part flex-wrapper flex-space-c flex-align-center">
            <?= Social::widget() ?>
        </div>
    </div>
    <!-- </entry -->

    <!-- registration -->
    <?= $this->render('registration', [
        'model' => $model,
    ]) ?>
    <!-- </registration -->

    <!-- restore -->
    <div id="restore" class="dropdown">
        <div class="crossy">
            <div class="line1"></div>
            <div class="line2"></div>
        </div>
        <div class="main-part">
            <h2>Восстановить пароль</h2>
            <p>Укажите данные для восстановления пароля</p>

            <?php
            $form = ActiveForm::begin([
                        'id' => 'resetpasswordform',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'action' => Url::to(['site/reset-access']),
                    ])
            ?>
            <?= Html::textInput('email', '', ['placeholder' => 'Ваш email']); ?>
            <?= Html::input('submit', null, 'Восстановить', ['class' => 'blue-button']) ?>
            <?php ActiveForm::end() ?>
        </div>
        <div class="link-part flex-wrapper flex-space-b flex-align-center">
            <span class="entry-link">Войти</span>
            <span class="register-link">Регистрация</span>
        </div>
    </div>
    <!-- </restore -->

<?php endif; ?>

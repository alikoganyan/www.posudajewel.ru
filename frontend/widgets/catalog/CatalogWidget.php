<?php

namespace frontend\widgets\catalog;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use common\models\Catalog;
use common\models\CatalogLink;
use common\models\QuickSearch;
use common\models\Service;

/**
 * Вывод разделов каталога
 */
class CatalogWidget extends Widget {

    /**
     * false-каталог свернут, true-каталог раскрыт
     * @var boolean 
     */
    public $show = false;

    /**
     * 
     * @return type
     */
    public function run() {
        $catalog = Catalog::find()->
                orderBy('sort ASC')->
                all();
        $link = CatalogLink::find()->
                select(['l.parent_id', 'l.id', 'qs.name', 'qs.url'])->
                from(['l' => CatalogLink::tableName()])->
                leftJoin(['qs' => QuickSearch::tableName()], 'l.search_id=qs.id')->
                where(['AND', 'qs.id IS NOT NULL'])->
                asArray()->
                all();
        $link = ArrayHelper::index($link, null, 'parent_id');

        $services = $items = $serviceId = [];

        // Основные разделы
        foreach ($catalog as $cat) {
            if (empty($cat->parent_id)) {
                $items[$cat->id] = [
                    'name' => $cat->name,
                    'image' => $cat->getUrlPrev(),
                    'service' => $cat->service_id,
                    'items' => [],
                ];

                if (!empty($cat->service_id)) {
                    $serviceId[$cat->service_id] = $cat->service_id;
                }
            }
        }

        // Подразделы
        foreach ($catalog as $cat) {
            $parentId = $cat->parent_id;

            if (!empty($parentId) && isset($items[$parentId])) {
                $items[$parentId]['items'][] = [
                    'name' => $cat->name,
                    'items' => isset($link[$cat->id]) ? $link[$cat->id] : [],
                    'is_hidden_subsections' => $cat->is_hidden_subsections,
                ];
            }
        }


        if (!empty($serviceId)) {
            $services = Service::find()->
                    where(['id' => $serviceId])->
                    indexBy('id')->
                    all();
        }

        return $this->render('catalog', [
                    'items' => $items,
                    'show' => $this->show,
                    'services' => $services,
        ]);
    }

}

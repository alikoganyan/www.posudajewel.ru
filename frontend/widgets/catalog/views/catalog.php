<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<aside class="flex-shrink-0 padding-lr-15">
    <div class="flag"></div>
    <button class="blue-button <?= $show ? '' : 'clpsd' ?>">
        Каталог товаров
        <span class="arrow"></span>
    </button>
    <ul class="sb <?= $show ? 'special' : 'ordinary' ?>">
        <?php foreach ($items as $item): ?>
            <li class="cat <?php if(!empty($item['image'])) echo 'catalog-img' ?>">
                <a href="#" class="a-cat"><?= Html::encode($item['name']) ?></a>
                <div class="ul-wrap">
                    <?php if (!empty($item['image'])): ?>
                        <div class="advert">
                            <a href="<?= isset($services[$item['service']]) ? $services[$item['service']]->getUrl() : '#' ?>">
                                <div class="img-wrap">
                                    <div class="cover"></div>
                                    <img src="<?= $item['image'] ?>" alt="">
                                </div>
                                <?php if (isset($services[$item['service']])): ?>
                                    <p class="title"><?= Html::encode($services[$item['service']]->name) ?></p>
                                    <p class="price"><?= number_format($services[$item['service']]->price, 0, '.', ' ') ?> <span class="item-price">&#8381;</span></p>
                                <?php endif; ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <ul class="">
                        <?php for ( $i = 0; $i < count($item['items']); $i++ ): ?>
                            <?php
                              $sub = $item['items'][$i];
                              //get next subsection hidden information to clear extra space between them if two consecutive subsections' titles are hidden
                              $next_sub = (!empty($item['items'][$i + 1]))? $item['items'][$i + 1] : [];
                            ?>
                            <li <?=($sub['is_hidden_subsections'] == true && !empty($next_sub) && $next_sub['is_hidden_subsections'] == true) ? "class=\"main-section-no-margin-bottom\"" : ""?>>
                                <div class="cat-items">
                                    <?php if ($sub['is_hidden_subsections'] == false): ?>
                                        <a href="#"><?= Html::encode($sub['name']) ?></a>
                                    <?php endif ?>
                                    <ul>
                                        <?php foreach ($sub['items'] as $link): ?>
                                            <li><a href="/search/<?= $link['url'] ?>"><?= Html::encode($link['name']) ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</aside>

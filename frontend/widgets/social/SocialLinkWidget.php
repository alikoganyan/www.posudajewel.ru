<?php
namespace frontend\widgets\social;

use yii\base\Widget;
use common\models\Settings;
use yii\helpers\Json;

class SocialLinkWidget extends Widget {
    private $images = [
        'vk' => '/uploads/social/vk.png',
        'ok' => '/uploads/social/ok.png',
        'fb' => '/uploads/social/fb.png',
        'google' => '/uploads/social/gl.png',
    ];

    public function run() {
        $socialSettings = Settings::getSetting(['vk', 'ok', 'fb', 'google']);;
        $links = [];

        foreach ($socialSettings as $key => $value) {
            $data = Json::decode($value);

            $links[] = [
                'url' => $data['description'],
                'img' => $this->images[$key],
            ];
        }

        return $this->render('social_link', [
            'links' => $links,
        ]);
    }
}

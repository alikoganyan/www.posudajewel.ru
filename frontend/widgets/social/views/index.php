<?php

/**
 * @var $this \yii\web\View
 */
use \yii\helpers\Url;
?>
<a href="<?= Url::to(['auth/auth', 'authclient' => 'facebook']) ?>"><img src="img/pict-soc-fb2.png" alt=""></a>
<a href="<?= Url::to(['auth/auth', 'authclient' => 'vkontakte']) ?>"><img src="img/pict-soc-vk2.png" alt=""></a>
<a href="<?= Url::to(['auth/auth', 'authclient' => 'google']) ?>"><img src="img/pict-soc-gl2.png" alt=""></a>
<a href="<?= Url::to(['auth/auth', 'authclient' => 'yandex']) ?>"><img src="img/pict-soc-ok2.png" alt=""></a>

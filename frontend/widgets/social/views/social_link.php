<?php
/**
 * @var $this \yii\web\View
 * @var $links []
 */

use yii\helpers\Html;

?>
<?php foreach ($links as $link): ?>
    <?= Html::a(Html::img($link['img']), $link['url'], ['target' => '_blank']) . ' '; ?>
<?php endforeach; ?>

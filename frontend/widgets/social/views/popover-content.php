<?php
/**
 * @var $this \yii\web\View
 */

use \yii\helpers\Url;
?>

<a href="<?= Url::to(['auth/auth', 'authclient' => 'google']) ?>" class="button google"></a>
<a href="<?= Url::to(['auth/auth', 'authclient' => 'yandex']) ?>" class="button yandex"></a>

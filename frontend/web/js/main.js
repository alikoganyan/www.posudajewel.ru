jQuery(document).ready(function() {

  //стилизация селектов
  jQuery('select, input[type="checkbox"], input[type="radio"]').styler();


  //слайдеры
  jQuery('#slider').slick({
    dots: true,
    arrows: false
  });

  jQuery('#prod-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 300,
    easing: 'swing',
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          speed: 300,
          easing: 'swing'
        }
      }
    ]
  });


  // destroy slider over 768px in Property
  var flag, b;

  function slick_destroy() {
    jQuery('#tab-links, #brands').slick('unslick');
  }
  function slick_init() {
    jQuery('#tab-links, #brands').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  function b_determ() {
    if (jQuery('.tab-links-flag').is(':visible')) {
      b = 1;
    }
    else {
      b = 0;
    }
  }

  b_determ();

  if (b == 1) {
    slick_init();
    flag = b;
  }

  flag = b;

  jQuery(window).resize(function() {
    b_determ();
    if (flag > b) {
      flag = b;
      slick_destroy();
    }
    if (flag < b) {
      flag = b;
      slick_init();
    }
  });


  //скрытие верхней полосы
  jQuery('.top-strip button').click(function() {
    jQuery('.top-strip').slideUp(250);
  });



  //окно Вход
  jQuery('.entry-link').click(function(e) {
    dropup();
    jQuery('#entry').addClass('vis');
    e.preventDefault();
  });
  jQuery('#entry .crossy').click(function() {
    jQuery('#entry').removeClass('vis');
  });


  //окно Регистрация
  jQuery('.register-link').click(function(e) {
    dropup();
    jQuery('#registration').addClass('vis');
    e.preventDefault();
  });
  jQuery('#registration .crossy').click(function() {
    jQuery('#registration').removeClass('vis');
  });
  jQuery('#eye').click(function() {
    if (jQuery('#pass2').attr('type') == 'password') {
      jQuery('#pass2').attr('type','text');
    }
    else {
      jQuery('#pass2').attr('type','password');
    }
  });


  //окно Восстановление пароля
  jQuery('.restore-link').click(function(e) {
    dropup();
    jQuery('#restore').addClass('vis');
    e.preventDefault();
  });
  jQuery('#restore .crossy').click(function() {
    jQuery('#restore').removeClass('vis');
  });


  //поиск на узком экране
  jQuery('#search-button').click(function() {
    jQuery('header .search-group').toggleClass('drp-down');
  });
  jQuery(document).mouseup(function (e){
    var div = jQuery('#search-button, header .search-group');
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
      jQuery('header .search-group').removeClass('drp-down');
    }
  });


  //боковое меню
  jQuery(window).resize(function() {
    if (!jQuery('.main-section aside .flag').is(':visible')) {
      jQuery('.sb li.cat .ul-wrap').removeAttr('style');
    }
  });

  jQuery('body').on('click', '.main-section aside button, header button.blue-button', function() {
    if (jQuery('.main-section aside .flag').is(':visible')) {
      jQuery('.sb.special').slideToggle();
      jQuery('.main-section aside button .arrow').toggleClass('up');
      jQuery('header button.blue-button .arrow').toggleClass('up');
    }
    else {
      jQuery('.sb.special').removeAttr('style');
    }
  });

  jQuery('body').on('click', '.main-section aside button.clpsd, header button.blue-button', function() {
    jQuery('.sb.ordinary').slideToggle();
    jQuery('.main-section aside button .arrow').toggleClass('up');
  });

  jQuery(document).mouseup(function (e){
    var div = jQuery('.main-section .sb.ordinary, .main-section aside button, header button.blue-button');
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
      jQuery('.main-section .sb.ordinary').slideUp();
      jQuery('.main-section aside button .arrow').removeClass('up');
    }
  });

  jQuery('.sb').mouseover(function() {
    jQuery('.cover').fadeIn();
  });
  jQuery('.sb').mouseleave(function() {
    jQuery('.cover').fadeOut();
  });

  jQuery('.sb li.cat a.a-cat').click(function(e) {
    if (!jQuery(this).siblings('.sb li.cat .ul-wrap').is(':visible')) {
      jQuery('.sb li.cat .ul-wrap').removeAttr('style');
      jQuery(this).siblings('.sb li.cat .ul-wrap').fadeIn();
    }
    else {
      jQuery(this).siblings('.sb li.cat .ul-wrap').fadeOut(function() {
        jQuery('.sb li.cat .ul-wrap').removeAttr('style');
      });
    }
    e.preventDefault();
  });


  //Показать больше
  jQuery('.view-more-wrap').click(function() {
    jQuery(this).siblings('.more').slideToggle();
    jQuery(this).children('p').toggleClass('none');
  });


  //Галерея в карточке товара


  $('#prod-img .img-slide-panel .img-slide-point').click(function(){
    $('#prod-img a').attr('href', $(this).attr('data-href'));


    $('#prod-img img').hide().attr('src', $(this).attr('data-src')).load(function(){
      $(this).fadeIn();
    });

    $('#prod-img .img-slide-point.active').removeClass('active');
    $(this).addClass('active');
  });


  //изменение позиции поп-апа

  $('.question-tooltip').tooltip({ position: { my: "left-35 top+17", at: "right bottom" } })


  //сворачивание блоков в панели фильтров
  jQuery('.search-results').on('click', '.aside-block h3', function() {
    jQuery(this).siblings('.aside-block-cont').slideToggle();
    jQuery(this).toggleClass('clpsd');
  });


  //панель фильтров на узком экране
  jQuery('#filter').click(function() {
    jQuery('.search-results .right-part').slideToggle();
    if (jQuery('.search-results .right-part').is(':visible')) {
      jQuery(this).parent('.filter-button').toggleClass('in-cross');
    }
    jQuery('body').toggleClass('oh');
  });

  function search_results_style() {
    if (jQuery('.search-results-flag').is(':hidden')) {
      jQuery('.search-results .right-part').removeAttr('style');
      jQuery('body').removeAttr('class');
    }
  }
  search_results_style();
  jQuery(window).resize(function() {
    search_results_style();
  });


  //изменение стрелки в селектах
  jQuery('.jq-selectbox').click(function() {
    if (jQuery(this).hasClass('opened')) {
      jQuery('.jq-selectbox__trigger-arrow').addClass('up');
    }
    else {
      jQuery('.jq-selectbox__trigger-arrow').removeClass('up');
    }
  });
  jQuery(document).mouseup(function (e){
    var div = jQuery('.jq-selectbox');
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
      jQuery('.jq-selectbox__trigger-arrow').removeClass('up');
    }
  });


  //изменение стрелки сортировки
  jQuery('.search-results .left-part .section .filter p span.sort-price').click(function() {
    jQuery(this).toggleClass('up');

    filter.onSubmit();
  });


  // ползунок
  var crawler = document.getElementById('crawler');
  if (crawler !== null) {
    noUiSlider.create(crawler, {
      start: [30, 122],
      connect: true,
      step: 1,
      tooltips: [wNumb({ decimals: 0 }), wNumb({ decimals: 0 })],
      range: {
        'min': 0,
        'max': 150
      }
    });
  }


  $('#filter-form').on('change', 'input, select', function(){
    filter.onSubmit();
  });
  $('.search-results .view-more').click(function(e){
    filter.onSubmit(e, $(this).data('page'));
  });
  $('#filter-form').submit(function(){
    return false;
  });

  $('[data-toggle="tooltip"]').tooltip();

  $('#subscribeform').submit(function(){
    var form = $(this);

    $.ajax({
      data: $(this).serializeArray(),
      url: $(this).attr('action'),
      method: 'POST',
      dataType: 'json',
      success: function (data, textStatus) {
        if (data.status == 'success') {
          $.notify(data.message, 'success');

          form.find('input[name=email]').val('');
        } else {
          $.notify(data.message, 'error');
        }
      },
      error: function () {
        $.notify('Возникла ошибка при отправке данных', 'error');
      }
    });


    return false;
  });
  // Сворачивание-разворачивание по дабл-клику на таб.
  $('.bottom-cart .nav-tabs > li').click(function () {
    if($(this).hasClass('active')){
      // $(this).removeClass('active');
      $('#collapsed-cart-content.collapse').toggleClass('in');
      $('.cover').fadeToggle();
      $(this).removeClass('active');
      return false;
    }else {
      $('.cover').fadeIn();
    }
  })
  // Показать развернуть содержимое таба по клику на него
  $('.bottom-cart a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $('#collapsed-cart-content.collapse').addClass('in');

  })
  $('.cover').click(function () {
    $(this).fadeOut();
    $('#collapsed-cart-content.collapse').removeClass('in');
    $('.bottom-cart .nav-tabs > li.active').removeClass('active');
  });

});




function signIn(e) {
  let form = $('#sign-in-form');

  $.ajax({
    url: form.attr('action'),
    method: 'POST',
    data: form.serialize(),
  }).done(response => {
    if (response.length < 1) {
      form.submit();
      return false;
    }

    let error = response[Object.keys(response)[0]][0];

    form.find('.has-error').removeClass('has-error');
    form.addClass('has-error');

    form.find('.error').removeClass('hide').html('Ошибка: <span>' + $('<div/>').html(error).text() + '</span>');
  });

  return false;
};

function resetForm() {
  $('#sign-in-form').removeClass('has-error');
  $('.error').addClass('hide');
};

function dropup() {
  $('.dropdown').removeClass('vis');
  $('#regions').getNiceScroll().hide();
}







<?php

use yii\helpers\Url;
use yii\helpers\Html;
 ?>
<?php if (Yii::$app->cart->getIsEmpty()): ?>
<div class="message-no-found">
    Ваша корзина пуста.
</div>
<?php else: ?>
    <?php $products = Yii::$app->cart->getItems(); ?>

    <?php foreach ($products as $product): ?>
    <div class="cart-item">
        <?=Html::a('<span class="glyphicon glyphicon-remove-circle"></span>', ['/cart/delete', 'id' => $product->id], ['class'=>'delete']);?>
        <?=Html::a(Html::img($product->logo), [$product->getUrl()]);?>
        <div class="info">
            <span class="category"><?=$product->serviceTypeName;?></span>
            <span class="name"><?=$product->name;?></span>
            <span class="vendor">Артикул: <?=$product->code;?></span>

            <div class="quantity input-group">
                <span class="input-group-btn">
                    <?php $options = [
                        "class"=>"btn btn-default btn-number",
                        "data" => [
                            "type"=>"minus",
                            "field"=>"quant[".$product->id."]",
                        ]
                    ];?>
                    <?php if ($product->quantity < 2):
                        $options += ['disabled'=>'disabled'];
                    endif;?>
                    <?=Html::button('<span class="glyphicon glyphicon-triangle-left"></span>', $options) ?>
                </span>
                <input type="text" name="quant[<?=$product->id?>]" item-id="<?=$product->id?>" class="form-control input-number" value="<?=$product->quantity;?>" min="1" max="99">
                <span class="input-group-btn">
                    <?php $options = [
                        "class"=>"btn btn-default btn-number",
                        "data" => [
                            "type"=>"plus",
                            "field"=>"quant[".$product->id."]",
                        ]
                    ];?>
                    <?php if ($product->quantity > 98):
                        $options += ['disabled'=>'disabled'];
                    endif;?>
                    <?=Html::button('<span class="glyphicon glyphicon-triangle-right"></span>', $options) ?>

                </span>
            </div>
            <div class="price-wrap">
                <span class="subtotal-price"><?=number_format($product->cost, 2, '.', ' ');?> &#8381;</span>
                <span class="price">(<?=number_format($product->getPrice(), 2, '.', ' ');?> &#8381; за ед.)</span>
            </div>
        <!-- <?=Html::a("Больше", ['/cart/update', 'id' => $product->id, 'quantity' => $product->quantity + 1]);?> -->
        </div>
    </div>
    <!-- <hr> -->
    <?php endforeach;?>
<?php endif;?>
<?php
    $url = Url::to(['/cart/index']);
    $js = <<<JS
$('.cart-item .quantity .btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});

$('.cart-item .quantity .input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});

$('.cart-item .quantity .input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        $(this).val($(this).data('oldValue'));
        return false;
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        $(this).val($(this).data('oldValue'));
        return false;
    }

    var data = {
        id: $(this).attr('item-id'),
        quantity: valueCurrent
    }
    $.get("/cart/update", data, function(){
        $.pjax({url: '{$url}', container: '#shopping-cart', push:false, scrollTo:false})
    });
});

$(".cart-item .quantity .input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
JS;
$this->registerJs($js);
?>
<script type="text/javascript">
<?php echo $js ?>
</script>

<?php //Update cart panel tabs ?>
<script type="text/javascript">
    var signs_for_footer = {
        cart_count: <?=Yii::$app->cart->getCount()?>,
        cart_total: '<?=number_format(Yii::$app->cart->getCost(), 2, '.', ' ')?> &#8381;'
    }
    $('.bottom-cart .nav-tabs li a[aria-controls="shop-cart-list"] span.count').text(signs_for_footer.cart_count);
    $('.bottom-cart .nav-tabs li a span.cart_total').html(signs_for_footer.cart_total);
</script>

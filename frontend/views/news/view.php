<?php

use yii\helpers\Html;
use frontend\widgets\catalog\CatalogWidget; 
use frontend\widgets\header\HeaderWidget;
$this->title = !empty($news->meta_title) ? $news->meta_title : $news->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $news->meta_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $news->meta_keywords,
]);
?>


<div class="main-section variant">
    <div class="container">
        <div class="wrap flex-wrapper">

            <?= CatalogWidget::widget() ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
            </div>
        </div>

        <ol class="breadcrumb padding-lr-15">
            <li><a href="/">Главная</a></li>
            <li class="active"><a href="#"><?= Html::encode($news->name) ?></a></li>
        </ol>


        <div class="about flex-wrapper flex-space-b">

            <aside class="left-part flex-shrink-0 padding-lr-15">
                <ul class="sidemenu">
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Гарантия</a></li>
                    <li><a href="#">Условия возврата</a></li>
                    <li><a href="#">Наши контакты</a></li>
                    <li><a href="#">О компании</a></li>
                </ul>
            </aside>

            <div class="right-part flex-grow-1 padding-lr-15">
                <div class="about-content">
                    <h2><?= Html::encode($news->name) ?></h2>
                    <div class="paragraph-group not-reset">
                        <?= $news->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * @var $this \yii\web\View
 * @var $page \common\models\Page
 * @var $listPage []
 */

use yii\helpers\Html;
use frontend\widgets\catalog\CatalogWidget;
use frontend\widgets\header\HeaderWidget;

/**
 * @var $this \yii\web\View
 * */

$this->title = !empty($page->meta_title) ? $page->meta_title : $page->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->meta_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->meta_keywords,
]);
?>

<div class="main-section variant">
    <div class="container">
        <div class="wrap flex-wrapper">
            <?= CatalogWidget::widget() ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
            </div>
        </div>

        <ol class="breadcrumb padding-lr-15">
            <li><a href="/">Главная</a></li>
            <li class="active"><a href="#"><?= Html::encode($page->name) ?></a></li>
        </ol>

        <div class="about flex-wrapper flex-space-b">
            <aside class="left-part flex-shrink-0 padding-lr-15">
                <ul class="sidemenu">
                    <? foreach ($listPage as $row): ?>
                        <li><?= Html::a(Html::encode($row['name']), ['/page/view', 'url' => $row['url']]) ?></li>
                    <? endforeach ?>
                </ul>
            </aside>

            <div class="right-part flex-grow-1 padding-lr-15">
                <div class="about-content html-content">
                    <h2><?= Html::encode($page->name) ?></h2>
                    <div id="page-content" class="paragraph-group">
                        <?= $page->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
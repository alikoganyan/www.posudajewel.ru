<?php
/**
 * @var $this        \yii\web\View
 * @var $quickSearch \common\models\QuickSearch[]
 * @var $result      \yii\db\ActiveQuery[]
 * @var $localities  string
 */

use \yii\helpers\Url;
use \common\models\Service;
use \yii\helpers\Json;
?>

<div class="row">
    <?php $count = 0;
    foreach ($quickSearch as $item): ?>
        <?php if (!isset($result[$item->id])) continue; ?>
        <div class="col-xxs-12 col-xs-6 col-sm-4 col-md-2">
            <a href="<?= Url::to(['site/search', 'url' => $item->url, 'localities' => $localities]) ?>" class="tab">
                <div class="img-wrap">
                    <img src="<?= $item->getLogo() ?>" alt="" width="100" height="100">
                </div>
                <h3><?= $item->name ?></h3>
                <p>(предложений: <?= $result[$item->id]->count() ?>)</p>
            </a>
        </div>
    <?php $count++; endforeach ?>

    <?php if ($count == 0): ?>
        <?php $this->registerJs("$('#quick-search-more-button').hide()") ?>
        <div class="col-md-12 not-found-result">Не выбран(ы) населенные пункты в поле Место отдыха, выберите место отдыха</div>
    <?php endif ?>
</div>

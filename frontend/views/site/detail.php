<?php

/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Service
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\widgets\catalog\CatalogWidget;
use frontend\widgets\service\PopularWidget;
use frontend\widgets\service\ServiceBuyWidget;
use frontend\widgets\header\HeaderWidget;
use branchonline\lightbox\Lightbox;
use yii\bootstrap\Modal;
$this->title = !empty($model->meta_title) ? $model->meta_title : $model->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_keywords,
]);

$price = $model->getPriceLabel(true);
$images = $model->images;
?>
<div class="main-section">
    <div class="container">
        <div class="wrap flex-wrapper">

            <?= CatalogWidget::widget() ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
            </div>
        </div>

        <ol class="breadcrumb padding-lr-15">
            <li><a href="/">Главная</a></li>
            <li><a href="<?= Url::to(['/site/search']) ?>">Каталог</a></li>
            <?php if (!is_null($model->serviceType)): ?>
                <li><a href="<?= Url::to(['/site/search', 'service_type' => $model->serviceType->id]) ?>"><?= Html::encode($model->serviceType->name) ?></a></li>
            <?php endif; ?>
            <li class="active"><a href="#"><?= Html::encode($model->name) ?></a></li>
        </ol>

        <div class="product-wrap padding-lr-15">
            <div class="product">
                <div class="parts flex-wrapper">
                    <div class="left-part">
                        <div class="img-group  <?php if (count($images) == 1) echo "one-img-group" ?> flex-wrapper flex-dir-col flex-space-b">
                            <!--change load svg or gif-->
                          <style>#prod-img { background-image: url(../img/load.svg)}</style>
                            <div class="img-wrap flex-wrapper flex-align-center" id="prod-img">
                                <?= Lightbox::widget([
                                    'files' => [
                                        [
                                            'thumb' => $images ? $images[0]->getUrlPrev() : '/img/img-prod1.jpg',
                                            'original' => $images ? $images[0]->getUrlImage() : '/img/img-prod1.jpg',
                                        ],
                                    ]
                                ])
                                ?>

                                <?php if (count($images) > 1): ?>

                                  <div class="img-slide-panel">
                                    <?php $slide_counter = 0 ?>

                                    <?php foreach($images as $img): ?>
                                      <span class="img-slide-point <?php if($slide_counter == 0) echo 'active'?>" data-src="<?= $img->getUrlPrev() ?>" data-href="<?= $img->getUrlImage()?>" data-id="<?php echo $slide_counter ?>"></span>

                                      <?php $slide_counter++ ?>
                                    <?php endforeach ?>
                                  </div>
                                <?php endif ?>

                            </div>

                        </div>
                    </div>
                    <div class="right-part flex-wrapper flex-dir-col">
                        <div>
                            <div class="h2-group">
                                <h2><?= Html::encode($model->name) ?></h2>
                                <div class="articul-group flex-wrapper flex-space-b flex-align-center">
                                    <?php if (!empty($model->in_stock)): ?>
                                        <p class="presence">Товар в наличии</p>
                                    <?php else: ?>
                                        <p class="presence no-stock">Товара нет в наличии</p>
                                    <?php endif; ?>
                                    <?php if (!empty($model->code)): ?>
                                        <p class="articul">Артикул: <?= $model->code ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="compare-wrap">
                                <?php echo Html::a(
                                    '',
                                    //'<span class="glyphicon glyphicon-stats"></span> Добавить к сравнению',
                                    '#',
                                    ['item-id'=>$model->id, 'class'=>'compare-link']
                                );?>
                            </div>
                            <div class="price-and-discount-wrap">
                                <div class="price-wrap">
                                <?php if (Yii::$app->user->id && Yii::$app->user->identity->discountToUser): ?>
                                    <p class="old-price"><?= $price?></p>
                                    <p class="item-price">
                                        <?= number_format(
                                            $model->price * (1-Yii::$app->user->identity->discountToUser->discount->discount),
                                            2, '.', ' '
                                        ) ?> &#8381; <span>Ваша цена</span>
                                    </p>
                                <?php else: ?>
                                    <p class="item-price"><?= $price?></p>
                                <?php endif ?>
                                </div>

                                <div class="want-discount-wrap">
                                <?php //if (Yii::$app->user->isGuest || !Yii::$app->user->identity->discountToUser): ?>
                                <?php Modal::begin([
                                    'header' => '<h4 class="modal-title">Условия предоставления цены</h4>',
                                    'toggleButton' => ['tag'=>'a', 'label' => 'Хочу получить скидку на товар', 'class'=>'cond'],
                                ]);?>

                                <?php echo $discount_condition_text;?>

                                <?php Modal::end();?>
                                <?php //endif; ?>
                                </div>
                            </div>
                            <div class="add-to-wrap">
                                <div class="buy-button-wrap">
                                    <?php echo Html::button('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Добавить в корзину', ['item-id'=>$model->id,'class'=>'blue-button buy-button']) ?>
                                </div>
                                <div class="add-to-favorites">
                                    <?php echo Html::a('<span class="glyphicon glyphicon-heart-empty"></span>&nbsp;Сохранить в избранное',
                                        '#',
                                        ['item-id'=>$model->id,'class'=>'favorite-link']
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <div class="text-group" style="display: none;">
                            <p class="top">Курьерская доставка СДЭК - <span>410 руб</span>. Дата доставки после 1 июля</p>
                            <h3>Другие способы доставки</h3>
                            <p>Пункт выдачи boxberry - <span>410 руб</span>. Дата доставки после 1 июля</p>
                            <p>Пункт выдачи boxberry - <span>410 руб</span>. Дата доставки после 1 июля</p>
                            <a href="#" class="show-ways">Показать все способы доставки</a>
                        </div>
                    </div>
                </div>
                <?php if (!empty($model->description)): ?>
                    <div class="paragraph not-reset">
                        <h2>Описание товара</h2>
                        <p><?= $model->description ?></p>
                    </div>
                <?php endif; ?>
                <div class="paragraph">
                    <h2>Характеристики</h2>
                    <div class="columns">
                        <?php foreach ($model->getCharsValues() as $key => $char): ?>
                            <?php if ($char->is_show): ?>
                                <div class="string flex-wrapper flex-space-b">
                                    <p class="charact-title flex-shrink-0"><?= Html::encode($char->name) ?></p>
                                    <div class="points flex-grow-1"></div>
                                    <p class="charact"><?= Html::encode(implode(', ', ArrayHelper::getColumn($model->getCharValues($char->id), 'name'))) ?></p>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<style type="text/css">
    .product .compare-wrap,
    .product .price-and-discount-wrap,
    .product .add-to-wrap {
        padding: 0 0 20px;
        margin: 20px 0;
        border-bottom: 1px solid #e5e5e5;
        display: flex;
        justify-content: space-between;
    }
    .product .compare-wrap {
        padding: 0; /*TEMPORARY!!!!*/
    }
    .product .add-to-wrap {
        border: none;
        align-items: baseline;
    }
    .product .price-and-discount-wrap{
        align-items: flex-end;
    }

    .product .price-wrap .old-price {
        color: #909090;
        font-size: 21px;
        text-decoration: line-through;
        text-indent: 5px;
        margin-bottom: 10px;
        font-family: 'PT Sans', serif;
        font-weight: bold;
    }
    .product .price-wrap .item-price {
        text-indent: 5px;
        font-size: 28px;
        color: #528cbc;
    }
    .product .price-wrap .item-price span{
        font-size: 15px;
        vertical-align: top;
        font-weight: 100;
    }

    .product .add-to-wrap .buy-button {
        padding: 0px 20px;
        font-size: 18px;
        box-shadow: none;
    }


    .product-wrap .product .parts .right-part a.cond {
        margin: 0;
        cursor: pointer;
    }

    .product .compare-link,
    .product .favorite-link{
        font-family: 'Roboto-Condensed', sans-serif;
        font-size: 17px;
    }
    .product .favorite-link {
        vertical-align: middle;
    }
    .product .compare-link span{padding-right: 7px;}

    @media(max-width: 550px) {
        .product .add-to-wrap .buy-button-wrap {padding: 15px 0}

        .product .compare-wrap,
        .product .price-and-discount-wrap,
        .product .add-to-wrap {
            display: block;
            text-align: center;
        }
    }
</style>
<?= PopularWidget::widget() ?>

<?= ServiceBuyWidget::widget(['id' => $model->id]) ?>

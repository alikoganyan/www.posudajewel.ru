<?php
/**
 * @var $this \yii\web\View
 */

use \yii\bootstrap\ActiveForm;
use \yii\bootstrap\Html;
use \yii\helpers\Url;
?>

<div id="confirm-registration" class="restore-modal">
    <div class="cross" id="restore-cross"></div>
    <div class="modal-window">
        <h2>Завершите регистрацию</h2>

        <p>На Ваш E-Mail было отправлено письмо с подтверждением регистрации</p>
        <a href="<?= Url::to(['auth/reset-registration']) ?>">Повторить отправку</a>
    </div>
</div>


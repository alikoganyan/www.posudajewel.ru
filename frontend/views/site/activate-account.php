<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\User
 */

use \yii\bootstrap\ActiveForm;
use \yii\bootstrap\Html;
?>

<div id="finish-registration" class="restore-modal">
    <div class="cross" id="restore-cross"></div>
    <div class="modal-window">
        <h2>Завершите регистрацию</h2>

        <?php $form = ActiveForm::begin([
            'action' => 'auth/finish-registration',
            'enableAjaxValidation' => true,
        ]) ?>
            <?= $form->field($model, 'id')->label(false)->hiddenInput() ?>
            <?= $form->field($model, 'email')->label($model->getAttributeLabel('email')); ?>
            <?= Html::input('submit', 'Отправить') ?>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use frontend\widgets\catalog\CatalogWidget;
use frontend\widgets\header\HeaderWidget;

$this->title = 'Страница не найдена';
?>
<div class="main-section variant404">
    <div class="container">
        <div class="wrap flex-wrapper">

            <?= CatalogWidget::widget() ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
            </div>
        </div>
        
        <div class="wrap padding-lr-15">
            <div class="content404 flex-wrapper flex-space-b">
                <div class="left-part">
                    <div class="group404">
                        <h2>4<span>0</span>4</h2>
                        <h3>Страница не найдена</h3>
                        <p>Неправильно набран адрес или такой страницы на&nbsp;сайте не&nbsp;существует.</p>
                        <a href="/" class="blue-button">Вернуться на главную</a>
                    </div>
                </div>
                <div class="right-part">
                    <img src="/img/img-3.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<?php

/**
 * @var $this      \yii\web\View
 * @var $populars  \common\models\Service[]
 * @var $pages     integer
 * @var $settings  []
 * @var $advantage string
 */
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\catalog\CatalogWidget;
use frontend\widgets\service\PopularWidget;
use frontend\widgets\service\NewWidget;
use frontend\widgets\news\NewsWidget;
use frontend\widgets\header\HeaderWidget;
use common\models\Settings;

$this->title = Settings::getSetting('index_title');
$this->registerMetaTag([
    'name' => 'description',
    'content' => Settings::getSetting('index_description'),
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Settings::getSetting('index_keywords'),
]);
?>

<div class="main-section">
    <div class="container">
        <div class="wrap flex-wrapper">

            <?= CatalogWidget::widget(['show' => true]) ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
                <div class="wrapper flex-wrapper flex-space-b flex-grow-1 flex-dir-col">
                    <div class="slider-wrap">
                        <div class="slider" id="slider">
                            <?php if (!empty($banner)): ?>
                                <div class="slider-item">
                                    <img src="/uploads/<?= $banner ?>" alt="">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="tab-links flex-wrapper flex-space-b flex-flow-wrap" id="tab-links">
                        <?php foreach ($quickSearch as $search): ?>
                            <a href="<?= Url::to(['site/search', 'url' => $search->url]) ?>" class="tab-link">
                                <img src="<?= $search->getLogo() ?>" alt="">
                                <span class="tab-title"><?= Html::encode($search->name) ?></span>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="tab-links-flag"></div>
                </div>
            </div>
        </div>	
    </div>
</div>


<?= PopularWidget::widget() ?>

<?= NewWidget::widget() ?>

<?= NewsWidget::widget(); ?>


<section class="section">
    <div class="container">
        <h3 class="padding-lr-15">О компании</h3>
        <div class="text padding-lr-15">
            <?= $about ?>
        </div>
    </div>
</section>
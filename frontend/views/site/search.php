<?php

/**
 * @var $this         \yii\web\View
 * @var $searchResult \common\models\Service[]
 * @var $chars        []
 * @var $pages        integer
 * @var $serviceCharValues []
 * @var $serviceType  \common\models\ServiceType
 */
use frontend\widgets\catalog\CatalogWidget;
use frontend\widgets\filter\Filter;
use frontend\widgets\header\HeaderWidget;

$this->title = 'Результаты поиска';

if (!is_null($quickSearch)) {
    if (!empty($quickSearch->title)) {
        $this->title = $quickSearch->title;
    }
    $this->registerMetaTag(['name' => 'keywords', 'content' => $quickSearch->keywords]);
    $this->registerMetaTag(['name' => 'description', 'content' => $quickSearch->description]);
}
?>


<div class="main-section variant">
    <div class="container">
        <div class="wrap flex-wrapper">

            <?= CatalogWidget::widget() ?>

            <div class="right-part flex-wrapper flex-space-b padding-lr-15 flex-dir-col">
                <?= HeaderWidget::widget() ?>
            </div>
        </div>

        <ol class="breadcrumb padding-lr-15">
            <li><a href="/">Главная</a></li>
            <li class="active"><a href="#">Результаты поиска</a></li>
        </ol>


        <div class="search-results flex-wrapper flex-space-b">

            <div class="search-results-flag"></div>

            <div class="filter-button">
                <button class="blue-button" id="filter">
                    <span class="fi">Фильтр</span>
                    <span class="cr">&times;</span>
                </button>
            </div>


            <aside class="right-part flex-shrink-0 padding-lr-15">
                <?=
                Filter::widget([
                    'queryParams' => Yii::$app->request->getQueryParams(),
                    'chars' => !is_null($serviceType) ? $serviceType->chars : [],
                    'serviceType' => !is_null($serviceType) ? $serviceType->id : 0,
                    'search' => $search,
                    'serviceCharValues' => $serviceCharValues,
                    'selectedChars' => $selectedChars
                ])
                ?>
            </aside>

            <div class="left-part flex-grow-1 padding-lr-15">

                <section class="section search-results">
                    <h2 class="">Результаты поиска</h2>
                    <div class="filter">
                        <p><span class="sorting">Сортировка: &nbsp;&nbsp;</span><span class="sort-price">по цене</span></p>
                    </div>
                    <div id="quick-search-more" class="tabs-group variant2 flex-wrapper flex-flow-wrap flex-space-b">

                        <?php
                        if (!empty($searchResult)) {
                            echo $this->render('group-items', [
                                'services' => $searchResult,
                            ]);
                        } else {
                            echo "<div>По Вашему запросу ничего не найдено, попробуйте изменить параметры запроса.</div>";
                        }
                        ?>
                    </div>

                    <div class="view-more-wrap">
                        <p class="view-more" data-page="1" style="<?= $pages < 2 ? 'display:none' : '' ?>">Показать больше &rarr;</p>
                    </div>

                </section>

            </div>

        </div>

    </div>
</div>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
 ?>
<div class="navbar-fixed-bottom">
    <div class="bottom-cart container">
        <div class="padding-lr-15">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation">
                            <?php echo Html::a(
                                '<span class="glyphicon glyphicon-heart-empty"></span>' .
                                '<span class="cart-item-caption">Избранное</span>' .
                                '<span class="count">' . 2 . '</span>' .
                                '<span class="glyphicon glyphicon-triangle-bottom" style="font-size: 10px;"></span>',
                                "#favorites-tab",
                                ["aria-controls"=>"favorites-tab", "role"=>"tab", "data-toggle"=>"tab"]
                            )?>
                        </li>
                        <li role="presentation">
                            <?php echo Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>' .
                                '<span class="cart-item-caption">Просмотренные</span>' .
                                '<span class="count">' . 2 . '</span>' .
                                '<span class="glyphicon glyphicon-triangle-bottom" style="font-size: 10px;"></span>',
                                "#viewed-tab",
                                ["aria-controls"=>"viewed-tab", "role"=>"tab", "data-toggle"=>"tab"]
                            )?>
                        </li>
                        <li role="presentation">
                            <?php echo Html::a(
                                '<span class="glyphicon glyphicon-shopping-cart"></span>' .
                                '<span class="cart-item-caption">Корзина</span>' .
                                '<span class="count">' . Yii::$app->cart->getCount() . '</span>' .
                                '<span class="glyphicon glyphicon-triangle-bottom" style="font-size: 10px;"></span>',
                                "#shop-cart-list",
                                ["aria-controls"=>"shop-cart-list", "role"=>"tab", "data-toggle"=>"tab"]
                            )?>
                        </li>
                        <li role="presentation">
                            <?php echo Html::a(
                                'Оформить заказ ' .
                                '<span class="cart_total">'.number_format(Yii::$app->cart->getCost(), 2, '.', ' ') . ' &#8381;</span>',
                                ['/cart/checkout'],
                                [
                                    'style' => 'background:#518bbd', 'onclick' => Yii::$app->user->isGuest ?
                                    '$("html, body").animate({ scrollTop: 0 }, "slow"); $(".entry-link").click();return false;' :
                                    ''
                                ]
                            ) ?>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content collapse" id="collapsed-cart-content">
                        <div role="tabpanel" class="tab-pane" id="shop-cart-list">
                            <?php \yii\widgets\Pjax::begin(['id'=>'shopping-cart','enablePushState'=>false,]); ?>
                            <?php echo $this->render('/cart/_content') ?>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="favorites-tab">
                            <div class="message-no-found">
                                Пусто.
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="viewed-tab">
                            <div class="message-no-found">
                                Пусто.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .nav.nav-tabs {text-align: right;}
    .nav.nav-tabs li{display: inline-block; float: none;vertical-align: middle;}
    #collapsed-cart-content {
        height: 70vh;
        /*height: 320px;*/
        background: #fff;
        overflow: auto;
        position: relative;
        padding: 0px;
    }
    .bottom-cart {max-height: 95vh;}
    #shopping-cart {
        display: flex;
        flex-flow: row wrap;
    }

    .tab-content::-webkit-scrollbar {
        width: 7px;
    }
    .tab-content::-webkit-scrollbar-track {
        /*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);*/
        background-color: #c4c4c4;
        background-clip: content-box;
        border-left: 2px solid transparent;
    }
    .tab-content::-webkit-scrollbar-thumb {
        outline: 1px solid slategrey;
        background-color: #518bbd;
        border-radius: 10px;
    }

    .message-no-found {
        color: #448fd0!important;
        font-size: 18px;
        text-transform: uppercase;
        margin: 10% auto 0;
        text-align: center;
    }
    .bottom-cart .panel{
        margin: 0;
        border: none;
        border-radius: 20px 20px 0 0;
    }
    .bottom-cart .panel-heading{
        background-color: #485b6a;
        padding: 0;
        border-radius: 15px 15px 0 0;
    }
    .bottom-cart .panel-body {padding: 0}
    .bottom-cart .nav-tabs li:last-child a {
        border-radius: 0 15px 0 0;
    }
    .bottom-cart .nav-tabs li:first-child a {
        border-radius: 15px 0 0 0;
    }


    .bottom-cart .nav-tabs li a img{
        display: inline-block;
        vertical-align: middle;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    .bottom-cart .nav-tabs li.active a,
    .bottom-cart .nav-tabs li:hover a{background: #424244;}
    .bottom-cart .nav-tabs a,.bottom-cart .nav-tabs li.active a{
        cursor: pointer;
        color: #fff;
        border: none;
        border-left: 1px solid #707072;
        border-radius: 0;
        line-height: 50px;
        padding: 0 45px;
        margin: 0;
        font-size: 18px;
    }
    .bottom-cart .nav-tabs li a span.count {
        background: #eceff6;
        color: #485b6a;
        border-radius: 4px;
        padding: 3px 9px;
        margin: 0 10px;
    }

    .cart-item {
        position: relative;
        display: inline-block;
        vertical-align: top;
        max-width: calc(50% - 2px);
        width: 100%;
        border: 1px solid #e5e5e5;
        border-right: none;
        border-top: none;
        padding: 10px;
    }
    .cart-item:nth-child(odd) {
        border-left: none;
    }
    /*FIX LAST odd elem border*/
    .cart-item:nth-child(even) {
        border-right: none !important;
    }
    .cart-item:last-child {
        border-right: 1px solid #e5e5e5;
    }

    .cart-item img {
        max-width: 125px;
        float: left;
        margin: 0 20px 15px 0;
        position: relative;
        z-index: 10;
    }
    .cart-item .info {
        padding: 12px 25px 10px 10px;
        display: block;
        position: relative;
    }
    .cart-item .info > span {
        display: block;
        padding: 0 0 10px;
        font-size: 17px;
        line-height: 20px;
    }
    .cart-item .info > span.category,
    .cart-item .info > span.vendor {
        color: #939393;
        font-size: 13px;
        font-weight: normal;
        font-family: 'PT Sans', serif;
    }
    .cart-item .delete {
        width: 23px;
        height: 23px;
        border: 0;
        position: absolute;
        right: 25px;
        top: 20px;
        color: #e6e6e6;
        font-size: 25px;
        z-index: 10;
    }
    .cart-item .quantity, .cart-item .price-wrap {
        display: inline-table;
        vertical-align: middle;
    }
    .cart-item .quantity .input-group-btn {width: auto;}
    .cart-item .quantity .input-number{
        width: 50px;
        text-align: center;
        border-radius: 10px !important;
        height: 25px;
    }
    .cart-item .quantity .btn-number{
        border: none;
        color: #b0b0b0;
        padding: 2px 5px;
        margin: 0;
    }
    .cart-item .price-wrap span.subtotal-price {
        color: #538bbc;
        font-size: 16px;
        margin: 5px 8px;
    }
    .cart-item .price-wrap span.price {
        font-size: 16px;
        color: #949494;
    }

    /*Правки*/
    .blue-button.buy-button{
        box-shadow: none;
        margin-right: 5px;
    }
    .blue-button.buy-button:hover{box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.75);}

    .bottom-cart .nav-tabs a:first-child {border-left: none}
    .bottom-cart .nav-tabs li a[aria-controls] span:last-child {display: none;}
    .bottom-cart .nav-tabs li.active a[aria-controls] span:last-child {display: inline-block;}
    .nav-tabs>li>a:hover {
        border-left: 1px solid transparent;
    }
    .nav>li>a:focus, .nav>li>a:hover {
        text-decoration: none;
        background-color: #485b6a;
    }

    .nav.nav-tabs {text-align: center; display: flex;}
    .nav.nav-tabs li{flex: 1;}
    .bottom-cart .nav-tabs a,.bottom-cart .nav-tabs li.active a{
        padding: 0;
        font-family: 'PT Sans', serif;
    }
    .new-bottom-part .compare-link {visibility: hidden;}
    span.cart-item-caption {
        margin-left: 10px;
    }

    footer {padding: 32px 0 65px;min-height: 268px;}
    .tabs-group .tab .tab-ins .product-name-group .product-name a {color: #505050 !important}

    @media (max-width: 1200px) {
        .bottom-cart .nav-tabs a, .bottom-cart .nav-tabs li.active a { font-size: 15px }
    }
    @media (max-width: 997px) {
        /*.bottom-cart .nav-tabs li:first-child a,*/
        /*.bottom-cart .nav-tabs li:last-child a{border-radius: initial;}*/
        /*.bottom-cart .nav-tabs a,.bottom-cart .nav-tabs li.active a{padding: 0 40px}*/
        /*.nav.nav-tabs li{display: block;}*/
        /*.nav.nav-tabs {text-align: left;display: block;}*/
        .bottom-cart .nav-tabs a, .bottom-cart .nav-tabs li.active a { font-size: 13px }
    }
    @media (max-width: 700px) {
        .cart-item {
            display: block;
            max-width: 100%;
            border: none;
            border-bottom: 1px solid #e5e5e5;
        }
        .cart-item img{max-width: 160px;}
        span.cart-item-caption {display: none;}
        .nav.nav-tabs li:last-child{flex: 2;}
    }
    @media (max-width: 450px) {
        .cart-item img{
            float: none;
            margin: 0 auto;
            display: block;
            max-width: 200px;
        }
        .bottom-cart .nav-tabs li:last-child a { font-size: 11px }
        .nav.nav-tabs li:last-child{flex: 3;}
        .bottom-cart .nav-tabs li a span.count{margin: 0 0 0 3px;}
        .bottom-cart .nav-tabs li.active a span:last-child{display: none !important}
    }

</style>
<?php
    $url = Url::to(['/cart/index']);
?>
<?php $js=<<<JS
    $('.blue-button.buy-button').click(function(){
        $.get("/cart/create",{
            id: $(this).attr('item-id'),
        }, function(){
            $.pjax({url: '{$url}', container: '#shopping-cart', push:false, scrollTo:false})
        });
        return false;
    });
JS;
$this->registerJs($js);

?>

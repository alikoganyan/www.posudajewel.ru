<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\components\CityHelper;
use frontend\widgets\auth\AuthWidget;
use frontend\widgets\locality\LocalityWidget;
use frontend\widgets\notify\Notify;
use frontend\widgets\search\SearchWidget;
use frontend\widgets\page\PageFooterWidget;
use frontend\widgets\social\SocialLinkWidget;
use common\models\Settings;
use \yii\helpers\Json;

AppAsset::register($this);
$infoSetting = Settings::getSetting('info');
$analyticScripts = Settings::getSetting('analytics_scripts');
$info = Json::decode($infoSetting);
$logo = Settings::getUrlLogo();
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <?php $this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/png', 'href' => '/uploads/' . Settings::getSetting('favicon')]); ?>
        <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/uploads/' . Settings::getSetting('favicon')]); ?>

        <?php $this->head() ?>
        <!--[if lt IE 9]>
          <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <script type="text/javascript" src="https://yastatic.net/browser-updater/v1/script.js" charset="utf-8"></script>
        <script>var yaBrowserUpdater = new ya.browserUpdater.init({"lang": "ru", "browsers": {"yabrowser": "15.12", "chrome": "21.0", "ie": "10", "opera": "12.1", "safari": "7.1", "fx": "18.0"}, "theme": "yellow"});</script>

        <?php $this->beginBody() ?>

        <?php if (!empty($info)): ?>
            <div class="top-strip padding-lr-15">
                <?= !empty($info['description']) ? Html::a($info['title'], $info['description']) : Html::tag('p', $info['title']) ?>
            </div>
        <?php endif; ?>

        <header>
            <div class="container dropdown-wrap">
                <div class="wrapper flex-wrapper flex-space-b flex-align-center height100">
                    <div class="button-wrap flex-shrink-0">
                        <button class="blue-button">
                            Каталог товаров
                            <span class="arrow"></span>
                        </button>
                    </div>
                    <div class="img-wrap padding-lr-15">
                        <a href="/"><img src="<?= !empty($logo) ? $logo : '/img/logo.png' ?>" alt=""></a>
                    </div>
                    <div class="right-part padding-lr-15 flex-wrapper flex-space-b flex-align-center flex-grow-1">

                        <?= SearchWidget::widget() ?>

                        <div class="links flex-wrapper flex-space-c">
                            <a href="#" class="city-choose delivery-link flex-shrink-0">
                                <span><?= Html::encode(CityHelper::getCity()) ?></span>
                            </a>
                            <?php if (Yii::$app->user->isGuest): ?>
                                <a href="#" class="entry-link flex-shrink-0">
                                    <span class="pic"><img src="/img/signin2.png" alt=""></span>
                                    <span class="word">Войти</span>
                                </a>
                                <a href="#" class="register-link flex-shrink-0">
                                    <span class="pic"><img src="/img/add_user.png" alt=""></span>
                                    <span class="word">Зарегистрироваться</span>
                                </a>
                            <?php else: ?>
                                <a href="/admin" class="flex-shrink-0" onclick="account(this)" >Личный кабинет</a>
                                <a href="<?= Url::to(['auth/logout']) ?>" class="flex-shrink-0">Выход</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?= LocalityWidget::widget() ?>
                <?= AuthWidget::widget(); ?>
            </div>
        </header>


        <div class="wrapper">

            <div class="cover"></div>

            <?= $content ?>

        </div>


        <footer>
            <?php echo $this->render('_shopping_cart') ?>
            <div class="container">
                <div class="wrapper flex-wrapper flex-space-b">
                    <div class="ft-block ft1 padding-lr-15">
                        <p class="big-p big-p--small-margin">Интернет-магазин</p>
                        <h3 class="big-p">посуды Jewel</h3>
                        <p class="small-p">8 (800) 700-07-68</p>
                        <p class="big-p">Оптовые продажи</p>
                        <p class="small-p">8 (495) 960-97-96</p>
                    </div>
                    <div class="ft-block ft2 padding-lr-15 padding-l-50">
                        <?= PageFooterWidget::widget() ?>
                    </div>
                    <div class="ft-block ft3 padding-lr-15">
                        <h3 class="var2">Методы оплаты:</h3>
                        <div class="pay-group">
                            <div class="pay-line flex-wrapper">
                                <div class="pay-tab flex-wrapper flex-align-center">
                                    <img src="/img/pict-pay-1.png" alt="">
                                </div>
                                <div class="pay-tab flex-wrapper flex-align-center">
                                    <img src="/img/pict-pay-2.png" alt="">
                                </div>
                            </div>
                            <div class="pay-line flex-wrapper">
                                <div class="pay-tab flex-wrapper flex-align-center">
                                    <img src="/img/pict-pay-3.png" alt="">
                                </div>
                                <div class="pay-tab flex-wrapper flex-align-center">
                                    <img src="/img/pict-pay-4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-block ft4 padding-lr-15">
                        <h3 class="var3">Компания Интерхолдинг-СПМ в социальных сетях</h3>
                        <div class="socials">
                            <?= SocialLinkWidget::widget() ?>
                        </div>
                        <h3 class="var4">Подпишитесь на наши рассылки</h3>
                        <?=
                        Html::beginForm(['/subscribe/create'], 'post', [
                            'id' => 'subscribeform',
                            'class' => 'flex-wrapper flex-space-b',
                        ])
                        ?>
                        <input class="flex-grow-1" type="text" name="email" placeholder="Укажите Email">
                        <input type="submit" value="">
                        <?= Html::endForm(); ?>
                    </div>
                </div>
            </div>
        </footer>

        <?=
        Notify::widget([
            'message' => Yii::$app->session->getFlash('notify'),
        ])
        ?>
        <?= $analyticScripts ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

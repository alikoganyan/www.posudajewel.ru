<?php

/**
 * @var $this  \yii\web\View
 * @var $chars \common\models\Char[]
 * @var $serviceCharValues []
 */
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

?>
<?php foreach ($chars as $char): ?>
    <?php if ($char->is_filter): ?>
        <div class="aside-block filters">
            <h3 class="clpsd"><?= Html::encode($char->name) ?></h3>
            <div class="aside-block-cont" style="display:none">
                <?php if ($char->is_multiple): ?>
                    <?php foreach ($char->values as $value): ?>
                        <label>
                            <?php if (isset($serviceCharValues[$char->id])): ?>
                                <?php if (in_array($value->id, $serviceCharValues[$char->id])): ?>
                                    <input value="<?= $value->id ?>" type="checkbox" char="<?= $char->id ?>"
                                    <?php if (isset($selectedChars[$char->id]) && in_array($value->id, $selectedChars[$char->id])): ?>checked<?php endif ?>
                                    ><?= Html::encode($value->name) ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </label>
                    <?php endforeach ?>
                <?php else: ?>
                    <?php
                        $filters = (isset($serviceCharValues[$char->id]))
                            ? ArrayHelper::filter(ArrayHelper::map($char->values, 'id', 'name'), $serviceCharValues[$char->id])
                            : ArrayHelper::map($char->values, 'id', 'name');

                        echo Html::dropDownList("char[{$char->id}][]", isset($selectedChars[$char->id]) ? $selectedChars[$char->id] : null,
                            ['' => 'Любое значение'] + $filters, [
                            'class' => 'sel filter-item',
                            'char' => $char->id,
                            'data-smart-positioning' => 'false',
                        ]);
                    ?>
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>
<?php endforeach ?>

<?php
/**
 * @var $this     \yii\web\View
 * @var $services \common\models\Service[]
 */
?>

<?php foreach ($services as $service): ?>
    <div class="price-item">
        <div class="price-item-cell cell-left">
            <div class="img-wrap">
                <img src="<?= $service->getLogo() ?>" alt="">
            </div>
        </div>
        <div class="price-item-cell cell-center">
            <a href="<?= $service->url ?>"><h3><?= $service->name ?></h3></a>
            <div class="description">
                <p><?= $service->company->name ?></p>
                <p><?= $service->address ?></p>
            </div>
            <button class="show-phone" onclick="showPhone(this)">Показать телефон</button>
            <p class="phone"><?= $service->company->main_phone ?></p>
        </div>
        <div class="price-item-cell cell-right">
            <p><?= $service->getPriceLabel() ?></p>
        </div>
    </div>
<?php endforeach ?>

<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'formatter' => [
            'nullDisplay' => '&nbsp;',
        ],
        'cart' => [
            'class' => 'hscstudio\cart\Cart',
            'storage' => [
               'class' => 'common\components\DBCookieStorage',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@frontend') . '/runtime/cache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js'=>[]
                ],
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyAf81UthEAsMWr2rbTUmCovUqy-oJpQScQ',
                        'language' => 'ru',
                        'version' => '3.1.18'
                    ]
                ]
            ]
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'common\components\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'info@e-ccs.ru',
                'password' => 'INQzPm00AL',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'ImageEditor' => [
            'class' => 'common\components\ImageEditor',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'common\components\Facebook',
                    'clientId' => '619093551622105',
                    'clientSecret' => '54492b3c7604f6629ce83d36bf8bdaa2',
                ],
                'vkontakte' => [
                    'class' => 'common\components\Vkontakte',
                    'clientId' => '5957997',
                    'clientSecret' => 'ntnAnx0UQoKSNJcWIXtW',
                    'scope' => ['email'],
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '983459808002-d73qs8ssnpkhfsl8jdlj35o2kvort7it.apps.googleusercontent.com',
                    'clientSecret' => 'bHBkoq02WO2jTLo-FhGCss0v',
                ],
                'yandex' => [
                    'class' => 'common\components\Yandex',
                    'clientId' => '4878a1c6c58a416cbf13a960ee294558',
                    'clientSecret' => 'a1b7bb752a5c4d0eb7f9bed7f5707cce',
                ],
            ],
        ]
    ],
    'language' => 'ru-RU',
];

<?php
namespace common\models;

use backend\components\ActiveRecord;

/**
 * This is the model class for table "char_value".
 *
 * @property integer $char_id
 * @property integer $value_id
 */
class CharValue extends ActiveRecord {
    public static function tableName() {
        return 'char_value';
    }

    public function rules() {
        return [
            [['char_id', 'value_id'], 'required'],
            [['char_id', 'value_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'char_id'    => 'Char ID',
            'value_id'   => 'Value ID',
        ];
    }
}

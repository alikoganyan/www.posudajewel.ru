<?php
namespace common\models;

use backend\components\ActiveRecord;
use dosamigos\google\maps\services\GeocodingClient;
use GuzzleHttp\Client;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "region".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $longitude
 * @property string $latitude
 * @property Locality[] $localities
 */
class Region extends ActiveRecord {

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }
    
    public static function tableName() {
        return 'region';
    }

    public function rules() {
        return [
            [['name', 'code'], 'required'],
            [['name', 'code', 'longitude', 'latitude'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'         => 'Ид',
            'name'       => 'Регион',
            'code'       => 'Код',
            'longitude'  => 'Долгота',
            'latitude'   => 'Широта',
        ];
    }

    public function afterFind() {
        if (!$this->latitude || !$this->longitude) {
            $geoCoder = new GeocodingClient();
            $result = $geoCoder->lookup(['address' => $this->name]);     

            if (isset($result->results[0])) {
                $this->latitude = $result->results[0]->geometry->location->lat;
                $this->longitude = $result->results[0]->geometry->location->lng;
            }
        }

        parent::afterFind();
    }

    public function getLocalities() {
        return $this->hasMany(Locality::className(), ['region_id' => 'id']);
    }

    public function getLocalityList() {
        return ArrayHelper::map($this->localities, 'id', 'name');
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existLocality = Locality::find()->
                where(['region_id' => $this->id])->
                exists();
        return !$existLocality;
    }

}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property string $id
 * @property int $user_id
 * @property string $name
 * @property string $value
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class Cart extends \backend\components\ActiveRecord {
    const CART_CREATED  = 0;
    const CART_CHECKOUT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'name', 'value'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['value'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'user_id'    => 'Пользователь',
            'name'       => 'Ключ',
            'value'      => 'Содержимое',
            'status'     => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function getOrder() {
        $cart = self::find()
            ->where(['and', 'status' => self::CART_CHECKOUT, 'user_id' => Yii::$app->user->getId()])
            ->orderBy(['updated_at' => SORT_DESC, 'id' => SORT_DESC])
            ->one();
        if (!$cart) {
            return false;
        }

        $order = Order::find()->select('id')->where(['cached_cart_id' => $cart->id])->one();
        if ($order) {
            return false;
        }

        return $cart;
    }
}

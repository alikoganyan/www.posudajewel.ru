<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id
 * @property string $cached_cart_id
 * @property string $fio
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $comment
 * @property string $delivery_cost
 * @property string $total
 * @property int $delivery_id
 * @property int $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property DeliveryService $delivery
 * @property Status $status
 * @property User $user
 */

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;

class Order extends \backend\components\ActiveRecord {
    public $model_type = null;
    function __construct(array $config = [])
    {
        if(isset($config['model_type'])){
            $this->model_type = $config['model_type'];
        }
        parent::__construct($config);
    }

    const STATUS_PLACED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules() {

        if ($this->model_type == 2) {
            return [
                [['user_id', 'cached_cart_id', 'fio', 'phone', 'email', 'address', 'delivery_id' ], 'required'],
                [['user_id', 'delivery_id', 'status_id','postcode' ,'region_id', 'locality_id', 'account_number','corr_number','bik','type'], 'integer'],
                [['notification'], 'boolean'],
                [['address', 'comment'], 'string'],
                [['delivery_cost', 'total'], 'number'],
                [['created_at', 'updated_at'], 'safe'],
                [['cached_cart_id', 'fio', 'phone', 'email', 'organization', 'inn' , 'kpp', 'law_address', 'actual_address','account_number', 'bank', 'bank_city'], 'string', 'max' => 255],
                [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryService::className(), 'targetAttribute' => ['delivery_id' => 'id']],
                [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            ];
        } else {
            return [
                [['user_id', 'cached_cart_id', 'fio', 'phone', 'email', 'address', 'delivery_id' , 'organization'], 'required'],
                [['user_id', 'delivery_id', 'status_id','postcode' ,'region_id', 'locality_id', 'account_number','corr_number','bik','type'], 'integer'],
                [['notification'], 'boolean'],
                [['address', 'comment'], 'string'],
                [['delivery_cost', 'total'], 'number'],
                [['created_at', 'updated_at'], 'safe'],
                [['cached_cart_id', 'fio', 'phone', 'email', 'organization', 'inn' , 'kpp', 'law_address', 'actual_address','account_number', 'bank', 'bank_city'], 'string', 'max' => 255],
                [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryService::className(), 'targetAttribute' => ['delivery_id' => 'id']],
                [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            ];
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'             => 'ID',
            'cached_cart_id' => 'Корзина',
            'user_id'        => 'Пользователь',
            'fio'            => 'Фамилия Имя Отчество',
            'phone'          => 'Мобильный телефон',
            'email'          => 'Электронная почта',
            'address'        => 'Адрес',
            'comment'        => 'Примечание',
            'delivery_cost'  => 'Стоимость доставки',
            'delivery_id'    => 'Способ доставки',
            'status_id'      => 'Статус',
            'total'          => 'Стоимость',
            'created_at'     => 'Дата создания',
            'updated_at'     => 'Дата обновления',
            'notification'   => 'Хочу получать персональные предложения и скидки по эл. почте',
            'postcode'       => 'Почтовый индекс',
            'region_id'      => 'Регион',
            'locality_id'    => 'Нас. пункт',
            'organization'   => 'Наименование организации',
            'law_address'    => 'Юридический адрес',
            'actual_address' => 'Фактический адрес',
            'account_number' => 'Номер счета (Р/С)',
            'bank'           => 'Банк',
            'corr_number'    => 'Корр. счет (К/С)',
            'bik'            => 'БИК',
            'bank_city'      => 'Город банка',
        ];
    }
    public function sendUpdatedStatus() {
        $mail = Yii::$app->mailer->compose(
                ['html' => 'orderStatusUpdated-html'], ['order' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => \Yii::$app->request->hostName])
            ->setTo($this->email)
            ->setSubject('Обновление статуса вашего заказа');
        return $mail->send();
    }
    public function sendPlcedOrder() {
        $mail = Yii::$app->mailer->compose(
                ['html' => 'orderPlaced-html'], ['order' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => \Yii::$app->request->hostName])
            ->setTo($this->email)
            ->setSubject('Ваш заказ #'.$this->id);
        return $mail->send();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery() {
        return $this->hasOne(DeliveryService::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getCart() {
        return $this->hasOne(Cart::className(), ['id' => 'cached_cart_id']);
    }
}

<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "service_type_char".
 *
 * @property integer $service_type_id
 * @property integer $char_id
 */
class ServiceTypeChar extends ActiveRecord {
    public static function tableName() {
        return 'service_type_char';
    }

    public function rules() {
        return [
            [['service_type_id', 'char_id'], 'required'],
            [['service_type_id', 'char_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'service_type_id' => 'Service Type ID',
            'char_id'         => 'Наименование характеристики',
        ];
    }
}

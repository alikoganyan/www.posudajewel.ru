<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\UploadedFile;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;
use backend\components\ActiveRecord;
use common\components\DataHelper;
use hscstudio\cart\ItemInterface;
use hscstudio\cart\ItemTrait;

/**
 * This is the model class for table "service".
 *
 * @property integer            $id
 * @property integer            $company_id
 * @property integer            $unit_id
 * @property integer            $service_type_id
 * @property string             $name
 * @property string             $description
 * @property integer            $price
 * @property integer            $wholesale
 * @property integer            $is_popular
 * @property integer            $is_blocked
 * @property string             $meta_description
 * @property string             $meta_keywords
 * @property string             $meta_title
 * @property integer            $is_new
 * @property integer            $in_stock
 * @property string             $code
 * @property string             $url
 * @property integer            $time_create
 * @property integer            $time_update
 * @property integer            $user_update
 * @property integer            $user_create
 * @property integer            $sort
 * @property integer            $sitemap
 * @property Company            $company
 * @property ServiceType        $serviceType
 * @property ServiceCharValue[] $chars
 * @property ServiceOptionGroup[] $optionGroups
 * @property ServiceImage[]     $images
 * @property Unit               $unit
 * @property []                 $galleryImages
 * @property string             $address
 * @property string             $serviceTypeName
 * @property string             $companyName
 * @property string             $userName
 * @property User               $userLastUpdated
 * @property User               $userCreated
 * @property ServiceOption[]    $options
 */
class Service extends ActiveRecord implements ItemInterface {
    use ItemTrait;

    /** @var UploadedFile[] $image */
    public $image;
    public $imgPath;
    public $char = [];
    public $groups = [];
    private $_serviceTypeName;
    private $_companyName;
    private $_userName;
    public $saveOption = true;
    public $buy;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'service';
    }

    public function rules() {
        return [
            [['service_type_id', 'company_id', 'name'], 'required'],
            [['service_type_id', 'company_id', 'is_popular', 'is_blocked', 'unit_id'], 'integer'],
            [['description', 'meta_title', 'meta_description', 'meta_keywords', 'id_1c'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
            [['is_new', 'in_stock', 'sitemap'], 'in', 'range' => [0, 1]],
            [['char', 'groups', 'image', 'buy'], 'safe'],
            [['url'], 'match', 'pattern' => '/^[a-z0-9\_\-]+$/i', 'message' => 'Можно использовать английские буквы, цифры, знак подчеркивания и дефис'],
            [['url'], 'unique'],
            [['price', 'wholesale'], 'number'],
            [['sort'], 'integer', 'min' => 0],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Ид',
            'unit_id' => 'Ед. изм.',
            'company_id' => 'Компания',
            'price' => 'Розничная цена',
            'wholesale' => 'Оптовая цена',
            'service_type_id' => 'Категория',
            'serviceTypeName' => 'Категория',
            'companyName' => 'Компания',
            'userName' => 'Пользователь',
            'name' => 'Предложение',
            'description' => 'Описание предложения',
            'image' => 'Фотографии',
            'is_popular' => 'Популярное',
            'is_blocked' => 'Блокировка',
            'meta_title' => 'Заголовок',
            'meta_description' => 'Описание',
            'meta_keywords' => 'Ключи',
            'is_new' => 'Новое предложение',
            'in_stock' => 'В наличии на складе',
            'code' => 'Артикул',
            'id_1c' => 'ID',
            'url' => 'Адрес страницы',
            'time_create' => 'Дата и время создания',
            'time_update' => 'Дата и время изменения',
            'user_update' => 'Кем изменено',
            'sort' => 'Сортировка',
            'user_create' => 'Кем создано',
            'buy' => 'С этим товаром покупают',
            'sitemap' => 'Блокировать в sitemap.xml',
            'options' => 'Опции',
            'chars' => 'Свойства',
            'quantity' => 'Количество',
            'cost' => 'Стоимость',
        ];
    }

    public function getPrice() {
        $price = $this->price;
        if (Yii::$app->user->id) {
            $price = !Yii::$app->user->identity->discountToUser ? $this->price : $this->price * (1-Yii::$app->user->identity->discountToUser->discount->discount);
        }
        return $price;
    }

    public function getId() {
        return $this->id;
    }


    public function __get($name) {
        switch ($name) {
            case 'serviceTypeName':
                if ($this->serviceType) {
                    return $this->serviceType->name;
                }

                return $this->_serviceTypeName;

            case 'companyName':
                if ($this->company) {
                    return $this->company->name;
                }

                return $this->_companyName;

            case 'userName':
                if ($this->company && $this->company->user) {
                    return $this->company->user->fullName;
                }

                return $this->_userName;

            default:
                return parent::__get($name);
        }
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'serviceTypeName':
                $this->_serviceTypeName = $value;
                break;

            case 'companyName':
                $this->_companyName = $value;
                break;

            case 'userName':
                $this->_userName = $value;
                break;

            default:
                parent::__set($name, $value);
                break;
        }
    }

    public function attributes() {
        return ArrayHelper::merge(parent::attributes(), ['char', 'options', 'chars']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (empty($this->url)) {
                DataHelper::generateUniqueUrl($this);
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->saveOption) {
            $this->saveChars();
            $this->saveOptions();
            $this->saveBuy();

            if (!empty($this->image) && is_array($this->image)) {
                $image = array_map('intval', $this->image);
                ServiceImage::updateAll(['service_id' => $this->id], ['service_id' => null, 'id' => $image]);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    private function saveChars() {
        ServiceCharValue::deleteAll(['service_id' => $this->id]);

        foreach ($this->char as $charId => $chars) {
            foreach ($chars as $valueId) {
                $model = new ServiceCharValue([
                    'service_id' => $this->id,
                    'char_id' => $charId,
                ]);
                if ($valueId != 0) {
                    $model->value_id = $valueId;
                }

                try {
                    $model->save();
                } catch (Exception $e) {

                }
            }
        }
    }

    private function saveOptions() {
        // TODO: need to apply cascade delete (by FK's)
        $groups = ServiceOptionGroup::findAll(['service_id' => $this->id]);

        foreach ($groups as $group) {
            ServiceOption::deleteAll(['group_id' => $group->id]);
        }
        ServiceOptionGroup::deleteAll(['service_id' => $this->id]);

        foreach ($this->groups as $group) {
            if (isset($group['name']) && isset($group['one']) && !empty($group['name'])) {
                $model = new ServiceOptionGroup([
                    'service_id' => $this->id,
                    'name' => $group['name'],
                    'is_multiple' => $group['one'] ? 0 : 1,
                ]);

                try {
                    if ($model->save() && isset($group['options'])) {
                        foreach ($group['options'] as $option) {
                            if (isset($option['name']) && isset($option['price']) && !empty($option['name'])) {
                                $optionModel = new ServiceOption([
                                    'group_id' => $model->id,
                                    'name' => $option['name'],
                                    'price' => round($option['price'], 2),
                                ]);

                                $optionModel->save();
                            }
                        }
                    }
                } catch (Exception $e) {

                }
            }
        }
    }

    public function upload() {
        $path = \Yii::getAlias('@frontend') . '/web/uploads';
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        foreach ($this->image as $image) {
            $newPath = \Yii::$app->security->generateRandomString() . '.' . $this->image[0]->extension;
            $this->imgPath = $newPath;
            $sizes = getimagesize($image->tempName);
            Image::thumbnail($image->tempName, 500, 300)->save("$path/$newPath", ['quality' => 50]);
        }

        return true;
    }

    public function getCompaniesList() {
        $companyQuery = Company::find();
        /** @var User $user */
        $user = \Yii::$app->user->identity;

        if (!$user->isAdmin()) {
            $companyQuery->andWhere(['user_id' => $user->id]);
        }
        $companies = $companyQuery->all();

        $result = [];
        foreach ($companies as $company) {
            $result[$company->id] = $company->name;
            $result[$company->id] .= ($company->regionName) ? ', ' . $company->regionName : '';
            $result[$company->id] .= ($company->localityName) ? ', ' . $company->localityName : '';
            $result[$company->id] .= ($company->userName) ? ', ' . $company->userName : '';
        }
        return $result;
    }

    public function getServiceTypesList() {
        return ArrayHelper::map(ServiceType::find()->asArray()->all(), 'id', 'name');
    }

    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getServiceType() {
        return $this->hasOne(ServiceType::className(), ['id' => 'service_type_id']);
    }

    public function getChars() {
        return $this->hasMany(ServiceCharValue::className(), ['service_id' => 'id']);
    }

    /**
     * @return Char[]
     */
    public function getCharsValues() {
        return Char::find()
                        ->innerJoin(ServiceCharValue::tableName(), 'id = char_id')
                        ->where(['service_id' => $this->id])
                        ->groupBy(['id'])
                        ->orderBy('sort ASC')
                        ->all();
    }

    public function getOptionGroups() {
        return $this->hasMany(ServiceOptionGroup::className(), ['service_id' => 'id']);
    }

    public function getImages() {
        return $this->hasMany(ServiceImage::className(), ['service_id' => 'id']);
    }

    public function getGalleryImages() {
        return ArrayHelper::getColumn($this->images, function ($img) {
                    return $img->getUrlPrev();
                });
    }

    public function getCharValues($charId) {
        return Value::find()
                        ->from(['v' => 'value'])
                        ->innerJoin(
                                ['scv' => ServiceCharValue::tableName()], "scv.service_id = {$this->id} AND scv.char_id = {$charId} AND scv.value_id = v.id")
                        ->all();
    }

    public function getGalleryImagesConfig() {
        $config = [];

        foreach ($this->images as $img) {
            $config[] = [
                'url' => Url::to(['service/image-remove', 'id' => $img->id]),
            ];
        }

        return $config;
    }

    /**
     * Стоимость с ед.измерения
     * @param boolean $detail
     * @return string
     */
    public function getPriceLabel($detail = false) {
        return ($detail ? '' : 'от ') . $this->price . ' &#8381;';
    }

    public function getUnitList() {
        return ArrayHelper::map(Unit::find()->all(), 'id', 'designation');
    }

    public function getAddress() {
        return $this->company->locality->region->name . ', ' . $this->company->locality->name;
    }

    /**
     * Url страницы товара
     * @return string
     */
    public function getUrl() {
        return '/offer/' . $this->url;
    }

    public function getUnit() {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    public function getLogo() {
        if (!$this->images) {
            return '/img/img-1.png';
        }

        return $this->images[0]->getUrlPrev();
    }

    public function getShortDescription() {
        return substr($this->description, 0, 150);
    }

    public function getMoreDescription() {
        return substr($this->description, 100);
    }

    public function getInterests() {
        $additionalServices = Service::find()->
                from(['s' => Service::tableName()])->
                leftJoin(['c' => Company::tableName()], 'c.id = s.company_id')->
                leftJoin(['u' => User::tableName()], 'u.id = c.user_id')->
                where(['AND', 's.id!=:id', 's.company_id=:company', 's.is_blocked!=1', 'c.is_blocked!=1', 'u.is_blocked!=1'], [':id' => $this->id, ':company' => $this->company_id])->
                all();

        $services = Service::find()->
                from(['s' => Service::tableName()])->
                leftJoin(['c' => Company::tableName()], 'c.id = s.company_id')->
                leftJoin(['u' => User::tableName()], 'u.id = c.user_id')->
                where(['AND', 's.id!=:id', 's.company_id!=:company', ['NOT IN', 's.id', ArrayHelper::getColumn($additionalServices, 'id')], 's.is_blocked!=1', 'c.is_blocked!=1', 'u.is_blocked!=1'], [':id' => $this->id, ':company' => $this->company_id])->
                all();
        $result = [];

        foreach ($services as $service) {
            if ($service == null) {
                continue;
            }
            /** @var $service self */
            $result[$service->serviceType->name][] = $service;
        }

        $result['Предложения от компании'] = $additionalServices;

        return array_reverse($result);
    }

    public function getSimilar() {
        return Service::find()->
                        from(['s' => Service::tableName()])->
                        leftJoin(['c' => Company::tableName()], 'c.id = s.company_id')->
                        leftJoin(['u' => User::tableName()], 'u.id = c.user_id')->
                        where(['AND', 's.service_type_id=:type', 's.company_id!=:company', 's.is_blocked!=1', 'c.is_blocked!=1', 'u.is_blocked!=1'], [':type' => $this->service_type_id, ':company' => $this->company->id])->
                        all();
    }

    /**
     * @return ActiveQuery
     */
    public function getAnotherPrice() {
        $chars = ServiceCharValue::find()
                ->select(['scv.value_id'])
                ->from(['scv' => ServiceCharValue::tableName()])
                ->innerJoin(['s' => self::tableName()], "s.id = scv.service_id")
                ->where(['s.id' => $this->id])
                ->groupBy(['scv.value_id'])
                ->orderBy('scv.value_id')
                ->asArray()
                ->all();

        $services = self::find()->where("id != {$this->id}")->all();
        $result = [];

        foreach ($services as $service) {
            /** @var $service self */
            $serviceChars = ArrayHelper::getColumn($service->getChars()->orderBy('value_id')->all(), 'value_id');
            $flag = true;
            foreach ($serviceChars as $key => $item) {
                if ($item != $chars[$key]['value_id']) {
                    $flag = false;
                    break;
                }
            }

            if ($flag) {
                $result[] = $service;
            }
        }

        return self::find()->where(['id' => ArrayHelper::getColumn($result, 'id')]);
    }

    /**
     * @param $chars []
     * @return ActiveQuery
     */
    public static function getSearchQuery($chars) {
        $result = self::find()
                ->from(['s' => self::tableName()])
                ->innerJoin(['scv' => ServiceCharValue::tableName()], 's.id = scv.service_id')
                ->groupBy(['s.id']);

        foreach ($chars as $charId => $valueId) {
            $result->orFilterWhere(['scv.char_id' => $charId, 'scv.value_id' => $valueId]);
        }

        return $result;
    }

    /**
     * @param null $chars
     * @param null $service_type
     * @param null $localities
     * @param mixed $priceFrom
     * @param mixed $priceTo
     * @return ActiveQuery
     */
    public static function doSearch($chars = null, $service_type = null, $localities = null, $priceFrom = null, $priceTo = null) {

        $serviceId = null;

        if (is_array($chars) && !empty($chars)) {

            foreach ($chars as $charId => $charValue) {
                $charValue = array_map('intval', $charValue);

                if (!empty($charValue)) {
                    $temp = ServiceCharValue::find()->
                            select(['service_id', 'count(service_id) AS value_count'])->
                            where(['AND', 'char_id=:char_id', ['IN', 'value_id', $charValue]], [':char_id' => (int) $charId])->
                            groupBy('service_id')->
                            having(['AND', 'value_count=:count'], [':count' => count($charValue)])->
                            column();

                    if (is_null($serviceId)) {
                        $serviceId = $temp;
                    } else {
                        $serviceId = array_intersect($serviceId, $temp);
                    }
                }
            }
        }

        $query = Service::findNotBlocked()->andWhere(['s.in_stock' => 1]);

        // Поиск по категории
        if ($service_type > 0) {
            $query->andWhere(['s.service_type_id' => $service_type]);
        }



        if (is_array($serviceId)) {
            if (empty($serviceId)) {
                // После фильтров ничего не найдено. Вернем пустой результат.
                $query->andWhere('0=1');
            } else {
                $query->andWhere(['s.id' => $serviceId]);
            }
        }

        // Минимальная цена
        if (!is_null($priceFrom) && $priceFrom !== '') {
            $query->andWhere(['AND', 's.price>=:price_from'], [':price_from' => (int) $priceFrom]);
        }
        // Максимальная цена
        if (!is_null($priceTo) && $priceTo !== '') {
            $query->andWhere(['AND', 's.price<=:price_to'], [':price_to' => (int) $priceTo]);
        }

        if ($localities) {
            //$query->andWhere(['c.locality_id' => Json::decode($localities)]);
        }

        return $query;
    }

    /**
     * Поиск не заблокированных предложений
     * @return ActiveQuery
     */
    public static function findNotBlocked() {
        return Service::find()->
                        from(['s' => Service::tableName()])->
                        leftJoin(['c' => Company::tableName()], 'c.id = s.company_id')->
                        leftJoin(['u' => User::tableName()], 'u.id = c.user_id')->
                        where(['AND', 's.is_blocked!=1', 'c.is_blocked!=1', 'u.is_blocked!=1', 's.in_stock=1']);
    }

    /**
     * Список предложений
     * @param array $cond
     * @param int $limit
     * @return array
     */
    public static function findService($cond, $limit = 8) {
        $items = Service::findNotBlocked()->
                select(['s.id', 's.url', 's.name', 's.price', 't.name AS type_name', 's.code'])->
                leftJoin(['t' => ServiceType::tableName()], 't.id=s.service_type_id')->
                andWhere($cond)->
                limit($limit)->
                orderBy('RAND()')->
                asArray()->
                all();

        if (!empty($items)) {
            $itemsId = ArrayHelper::getColumn($items, 'id');

            $images = ServiceImage::find()->
                    where(['service_id' => $itemsId])->
                    indexBy('service_id')->
                    orderBy('id DESC')->
                    all();

            foreach ($items as &$item) {
                $id = $item['id'];

                $item['image'] = isset($images[$id]) ? $images[$id]->getUrlPrev() : '/img/img-1.png';
            }
        }

        return $items;
    }

    /**
     * Копирование предложения со всеми свойствами
     */
    public function copy() {
        $chars = ServiceCharValue::find()->
                where(['service_id' => $this->id])->
                all();

        $optionGroup = ServiceOptionGroup::find()->
                where(['service_id' => $this->id])->
                indexBy('id')->
                all();

        if (!empty($optionGroup)) {
            $option = ServiceOption::find()->
                    where(['group_id' => array_keys($optionGroup)])->
                    all();
        } else {
            $option = [];
        }

        $service = clone $this;

        $service->id = null;
        $service->setIsNewRecord(true);

        // Создадим уникальный url
        $url = $service->url;
        $index = 1;
        do {
            $service->url = $url . '-' . ($index++);

            $exists = Service::find()->where(['url' => $service->url])->exists();
        } while ($exists);

        // Выставим свойства
        $service->char = [];
        foreach ($chars as $char) {
            if (!isset($service->char[$char->char_id])) {
                $service->char[$char->char_id] = [];
            }
            $service->char[$char->char_id][] = $char->value_id;
        }

        // Допоолнительные опции
        $service->groups = [];
        foreach ($option as $serviceOption) {
            $groupId = $serviceOption->group_id;

            if (!isset($service->groups[$groupId])) {
                $service->groups[$groupId] = [
                    'name' => $optionGroup[$groupId]->name,
                    'one' => $optionGroup[$groupId]->is_multiple ? 0 : 1,
                    'options' => [],
                ];
            }

            $service->groups[$groupId]['options'][] = [
                'name' => $serviceOption->name,
                'price' => $serviceOption->price,
            ];
        }

        // С этим товаром покупают
        $service->buy = $this->getServiceBuy();


        if ($service->insert(false)) {
            // Копируем изобрвжения
            $images = ServiceImage::findAll(['service_id' => $this->id]);

            $path = Yii::getAlias('@frontend') . '/web/uploads/';

            foreach ($images as $img) {
                $ext = pathinfo($img->name, PATHINFO_EXTENSION);

                do {
                    $file = Yii::$app->security->generateRandomString() . '.' . $ext;
                } while (file_exists($path . $file));

                copy($path . $img->name, $path . $file);
                copy($path . $img->name_crop, $path . 'pre_' . $file);

                $serviceImage = new ServiceImage();
                $serviceImage->setScenario('insert');
                $serviceImage->service_id = $service->id;
                $serviceImage->name = $file;
                $serviceImage->name_crop = 'pre_' . $file;
                $serviceImage->save(false);
            }
        }
    }

    /**
     * Сохранение товаров "С этим товаром покупают"
     */
    public function saveBuy() {
        $this->buy = is_array($this->buy) ? array_map('intval', $this->buy) : [];

        $buy = $this->getServiceBuy();

        if (!empty($this->buy)) {
            // Оставим только существующие значения
            $this->buy = Service::find()->
                    select(['id'])->
                    where(['id' => $this->buy])->
                    indexBy('id')->
                    column();

            // Добавим новые значения
            foreach ($this->buy as $buyId) {
                if (!isset($buy[$buyId])) {
                    $serviceBuy = new ServiceBuy();
                    $serviceBuy->service_id = $this->id;
                    $serviceBuy->buy_id = $buyId;
                    $serviceBuy->save();
                }
            }
        }

        // Удалим старые значения
        $delete = [];
        foreach ($buy as $buyId) {
            if (!isset($this->buy[$buyId])) {
                $delete[] = $buyId;
            }
        }

        if (!empty($delete)) {
            ServiceBuy::deleteAll(['service_id' => $this->id, 'buy_id' => $delete]);
        }
    }

    /**
     * С этим товаром покупают
     * @return array
     */
    public function getServiceBuy() {
        return ServiceBuy::find()->
                        select(['buy_id'])->
                        where(['service_id' => $this->id])->
                        indexBy('buy_id')->
                        column();
    }

    public function getUserLastUpdated() {
        return Yii::$app->cache->getOrSet(CacheConstants::SERVICE_USER_LAST_UPDATED . "{$this->id}_{$this->user_update}", function () {
            return User::findOne(['id' => $this->user_update]);
        });
    }

    public function getUserCreated() {
        return Yii::$app->cache->getOrSet(CacheConstants::SERVICE_USER_LAST_UPDATED . "{$this->id}_{$this->user_create}", function () {
            return User::findOne(['id' => $this->user_create]);
        });
    }

    public function getOptions() {
        $options = [];

        foreach ($this->optionGroups as $group) {
            $options = ArrayHelper::merge($options, $group->options);
        }

        return $options;
    }

    public static function export() {
        $products = self::find();
        $filePath = \Yii::getAlias('@runtime') . '/export.csv';
        $file = fopen($filePath, 'w');
        $model = new Service();

        $labels = $model->attributeLabels();
        $attributes = $model->getAttributes();
        $values = [];

        unset($attributes['char']);

        foreach ($attributes as $key => $attr) {
            if (isset($labels[$key])) {
                $values[] = iconv("utf-8", "windows-1251", $labels[$key]);
            }
        }

        fputcsv($file, $values, ';');

        foreach ($products->each() as $product) {
            /** @var $product Service */
            $row = [];
            $attributes = $product->getAttributes();
            unset($attributes['char'], $attributes['chars'], $attributes['options']);

            $attributes['unit_id'] = $product->unit ? $product->unit->name : '';
            $attributes['company_id'] = $product->companyName;
            $attributes['service_type_id'] = $product->serviceTypeName;
            $attributes['user_create'] = $product->userCreated != null ? $product->userCreated->fullName : '';
            $attributes['user_update'] = $product->userLastUpdated != null ? $product->userLastUpdated->fullName : '';

            foreach ($attributes as $key => $item) {
                $row[] = iconv("utf-8", "windows-1251", $item);
            }

            $options = $product->options;
            $row['options'] = $options != null ? iconv("utf-8", "windows-1251", implode(', ', ArrayHelper::getColumn($options, 'name'))) : '';

            $chars = [];
            foreach ($product->getCharsValues() as $char) {
                $charValues = $product->getCharValues($char->id);
                foreach ($charValues as &$item) {
                    $item = iconv("utf-8", "windows-1251", $item->name);
                }

                $chars[] = iconv("utf-8", "windows-1251", $char->name) . ': ' . implode(', ', $charValues);
            }

            $row[] = implode('; ', $chars);

            fputcsv($file, $row, ';');
        }

        return $filePath;
    }
}

<?php
namespace common\models;

use backend\components\ActiveRecord;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property string $name
 * @property integer $code
 * @property string $designation
 */
class Unit extends ActiveRecord {
    
    /**
     * Список ед.измерения
     * @var null|array
     */
    protected static $data;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }
    
    public static function tableName() {
        return 'unit';
    }

    public function rules() {
        return [
            [['designation'], 'string'],
            [['code'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'          => 'ID',
            'name'        => 'Ед. измерения',
            'code'        => 'Код',
            'designation' => 'Обозначение',
        ];
    }
    
    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        return !Service::find()->
                        where(['unit_id' => $this->id])->
                        exists();
    }

    /**
     * Название ед.измерения
     * @param int $unit
     * @return string
     */
    public static function getUnitName($unit) {
        self::listUnit();

        return isset(self::$data[$unit]) ? self::$data[$unit] : '';
    }

    /**
     * Список ед.измерения
     * @return array
     */
    public static function listUnit() {
        if (is_null(self::$data)) {
            self::$data = self::find()->
                    select(['designation', 'id'])->
                    indexBy('id')->
                    column();
        }

        return self::$data;
    }

}

<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use backend\components\ActiveRecord;
use common\components\CropImageUploadBehavior;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $role_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $birthday
 * @property string $phone
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $fb
 * @property string $yandex
 * @property string $confirm_code
 * @property string $panel_settings
 * @property integer $google_id
 * @property integer $vk_id
 * @property string $gender
 * @property integer $is_blocked
 * @property integer $is_activated
 * @property string $confirm_token
 * @property string $img
 * @property string $fullName
 * @property string $roleName
 * @property string $image
 * @property string $image_crop
 * @property Role $role
 * @property Company[] $companies
 */
class User extends ActiveRecord implements IdentityInterface {

    const SCENARIO_CREATE = 0;
    const SCENARIO_LOGIN = 1;
    const SCENARIO_SOCIAL = 2;
    const SCENARIO_FINISH_REG = 3;
    const SCENARIO_CHANGE_PASSWORD = 4;
    const SCENARIO_RESET_ACCESS = 5;
    const SCENARIO_CONFIRM_REG = 6;
    const SCENARIO_CONFIRM_CODE = 7;

    public static $genders = [
        'Женский',
        'Мужской',
    ];
    public $password;
    public $repeat_password;
    public $old_password;
    public $rememberMe;
    public $code;
    private $_fullName;
    private $_roleName;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => CropImageUploadBehavior::className(),
                'attribute' => 'image',
                'path' => '@backend/web' . self::getPath(),
                'url' => self::getPath(true),
                'ratio' => 120 / 120,
                'cropped_field' => 'image_crop',
                'scenarios' => [self::SCENARIO_CREATE, self::SCENARIO_DEFAULT],
            ],
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'user';
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'fullName':
                $this->_fullName = $value;
                break;

            case 'roleName':
                $this->_roleName = $value;
                break;

            default:
                parent::__set($name, $value);
        }
    }

    public function __get($name) {
        switch ($name) {
            case 'fullName':
                if ($this->first_name || $this->last_name) {
                    return $this->last_name . ' ' . $this->first_name;
                }

                return $this->email;

            case 'roleName':
                if ($this->role) {
                    return $this->role->name;
                }

                return $this->_roleName;

            default:
                return parent::__get($name);
        }
    }

    public function scenarios() {
        return ArrayHelper::merge(parent::scenarios(), [
                    self::SCENARIO_CREATE => ['email', 'password', 'repeat_password'],
                    self::SCENARIO_LOGIN => ['email', 'password'],
                    self::SCENARIO_FINISH_REG => ['email'],
                    self::SCENARIO_SOCIAL => [],
                    self::SCENARIO_CHANGE_PASSWORD => ['old_password', 'password', 'repeat_password'],
        ]);
    }

    public function rules() {
        return [
            [['birthday', 'is_blocked', 'password', 'repeat_password', 'rememberMe', 'code'], 'safe'],
            [['email'], 'email'],
            [['is_activated', 'vk_id', 'google_id', 'role_id'], 'integer'],
            [['first_name', 'confirm_code', 'confirm_token', 'phone', 'gender', 'img', 'last_name', 'password_hash', 'password_reset_token', 'fb', 'yandex'],
                'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'required', 'on' => self::SCENARIO_FINISH_REG],
            [['password_reset_token'], 'unique'],
            // create
            [['email', 'password', 'repeat_password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['email', 'password'], 'required', 'on' => self::SCENARIO_LOGIN],
            [['password'], 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD],
            [['old_password'], 'validateOldPassword', 'on' => self::SCENARIO_CHANGE_PASSWORD, 'message' => 'Неверный пароль'],
            [['email'], 'validatePassword', 'on' => [self::SCENARIO_LOGIN]],
            ['repeat_password', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'on' => self::SCENARIO_CREATE,
                'message' => 'Пароли не совпадают'],
            [['email'], 'isEmailExistValidator', 'on' => self::SCENARIO_RESET_ACCESS],
            [['email'], 'validateEmail', 'on' => self::SCENARIO_CREATE],
            [['code'], 'validateConfirmCode', 'on' => [self::SCENARIO_CONFIRM_CODE]],
            [['code'], 'required', 'on' => [self::SCENARIO_CONFIRM_CODE]],
            [['image'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_DEFAULT]],
        ];
    }

    public function validateConfirmCode($attribute, $params) {
        $user = self::findOne(['id' => $this->id]);

        if ($user->confirm_code != $this->code) {
            $this->addError($attribute, 'Не правильный код');
        }
    }

    public function isEmailExistValidator($attribute, $params) {
        $user = self::findOne(['email' => $this->email]);

        if (!$user) {
            $this->addError($attribute, 'Пользователь не существует');
        }
    }

    public function attributes() {
        return ArrayHelper::merge(parent::attributes(), [
                    'password', 'fullName', 'roleName',
        ]);
    }

    public function validateOldPassword($attribute, $params) {
        $user = User::findOne(['email' => $this->email]);

        if (!$user || !$user->isPasswordValid($this->old_password)) {
            $this->addError($attribute, 'Неверный логин/пароль');
        }
    }

    public function validatePassword($attribute, $params) {
        $user = User::findOne(['email' => $this->email]);

        if (!$user || !$user->isPasswordValid($this->password) || $this->password == null) {
            $this->addError($attribute, 'Неверный логин/пароль');
        }
    }

    public function isEmailExist($attribute, $params) {
        $user = self::findOne(['email' => $this->email]);

        if ($user && $this->getScenario() == self::SCENARIO_CREATE) {
            $this->addError($attribute, 'Такой E-Mail уже зарегестрирован');
        } else if (!$user && $this->getScenario() == self::SCENARIO_LOGIN) {
            $this->addError($attribute, 'Такой E-Mail не зарегестрирован');
        }
    }

    public function validateEmail($attribute, $params) {
        $user = self::findOne(['email' => $this->email]);

        if ($user) {
            $this->addError($attribute, 'Пользователь с таким E-Mail уже зарегистрирован! Если забыли пароль, воспользуйтесь функцией восстановления пароля');
        }
    }

    public function getCompanies() {
        return $this->hasMany(Company::className(), ['user_id' => 'id']);
    }
    public function getDiscountToUser() {
        return $this->hasOne(DiscountToUser::className(), ['user_id' => 'id']);
    }

    public function getServices($serviceTypeId) {
        $result = [];

        foreach ($this->companies as $company) {
            $query = $company->getServices()->where(['service_type_id' => $serviceTypeId]);

            foreach ($query->all() as $service) {
                $result[] = $service;
            }
        }

        return $result;
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->role_id == null) {
                    $this->role_id = Role::getDefaultRole()->id;
                }

                if ($this->getScenario() == self::SCENARIO_CREATE) {
                    $this->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
                }
            }

            if (!empty($this->image) && empty($this->image_crop)) {
                $this->addError('image', 'Не удалось сохранить изображение.');
                return false;
            }

            if ($this->birthday) {
                $date = new \DateTime($this->birthday);
                $this->birthday = $date->format('Y-m-d');
            }

            if ($this->getScenario() == self::SCENARIO_CHANGE_PASSWORD) {
                $this->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
            } else if ($this->getScenario() == self::SCENARIO_SOCIAL) {
                $this->is_activated = 1;
            }

            if ($this->getScenario() == self::SCENARIO_CONFIRM_REG) {
                $this->is_activated = 1;
                $this->confirm_token = null;
            }

            return true;
        } else {
            return false;
        }
    }

    public function generateAuthKey() {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public function getRole() {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function attributeLabels() {
        return [
            'id' => 'Ид',
            'code' => 'Код подтверждения',
            'first_name' => 'Имя',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
            'old_password' => 'Старый пароль',
            'last_name' => 'Фамилия',
            'fullName' => 'Пользователь',
            'roleName' => 'Роль',
            'repeat_password' => 'Повторите пароль',
            'role_id' => 'Роль',
            'google_id' => 'Google Plus',
            'vk_id' => 'Вконтакте',
            'email' => 'Электронная почта',
            'birthday' => 'Дата рождения',
            'phone' => 'Телефон',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'fb' => 'Facebook',
            'gender' => 'Пол',
            'yandex' => 'Яндекс',
            'is_blocked' => 'Блокировка',
        ];
    }

    public function getRoleList() {
        return ArrayHelper::map(Role::find()->all(), 'id', 'name');
    }

    public function login() {
        return Yii::$app->user->login($this, $this->rememberMe ? 3600 * 24 * 30 : 0);
    }

    public function isPasswordValid($password) {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'is_blocked' => 0,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function isActivated() {
        return $this->email != null;
    }

    public function requestResetPassword() {
        if (!User::isPasswordResetTokenValid($this->password_reset_token)) {
            $this->generatePasswordResetToken();
            $this->save(false);
        }

        return Yii::$app->mailer->
                        compose(
                                ['html' => 'requestPasswordReset-html'], ['user' => $this]
                        )->
                        setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])->
                        setTo($this->email)->
                        setSubject('Восстановление пароля')->
                        send();
    }

    public function resetPassword() {
        $password = Yii::$app->security->generateRandomString(10);
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);

        $this->removePasswordResetToken();
        $this->save();

        return Yii::$app->mailer->
                        compose(
                                ['html' => 'passwordResetToken-html'], ['user' => $this, 'newPassword' => $password]
                        )->
                        setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])->
                        setTo($this->email)->
                        setSubject('Ваш новый пароль')->
                        send();
    }

    public function sendConfirmEmail() {
        $this->confirm_token = Yii::$app->security->generateRandomString(32);
        \Yii::$app->session->addFlash('notify', 'На вашу почту было выслано письмо с подтверждением');

        return Yii::$app
                        ->mailer
                        ->compose(
                                ['html' => 'confirmRegistration-html'], ['user' => $this]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                        ->setTo($this->email)
                        ->setSubject('Подтверждение электронной почты')
                        ->send();
    }

    public function isAdmin() {
        return $this->role->name == 'Администратор';
    }

    public function sendConfirmCode() {
        $this->confirm_code = Yii::$app->security->generateRandomString(6);

        return Yii::$app
                        ->mailer
                        ->compose(
                                ['html' => 'confirmRegistrationCode-html'], ['user' => $this]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => \Yii::$app->request->hostName])
                        ->setTo($this->email)
                        ->setSubject('Подтверждение регистрации')
                        ->send();
    }

    /**
     * Url превью изображения
     * @return string
     */
    public function getUrlPrev() {
        if (!empty($this->image_crop)) {
            return self::getPath(true) . '/' . $this->image_crop;
        } else {
            return '';
        }
    }

    /**
     * Путь до директории с картинками
     * @param boolean $web
     * @return string
     */
    public static function getPath($web = false) {
        return ($web ? '/admin' : '') . "/uploads/user";
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existCompany = Company::find()->
                where(['user_id' => $this->id])->
                exists();

        return !$existCompany;
    }

}

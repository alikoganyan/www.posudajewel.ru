<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use backend\components\ActiveDataProvider;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "catalog_link".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $search_id
 * @property int $sort
 */
class CatalogLink extends ActiveRecord {

    public $name;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'catalog_link';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['search_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuickSearch::className(), 'targetAttribute' => ['search_id' => 'id'], 'on' => ['insert']],
            [['name'], 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent_id' => 'Элемент каталога',
            'search_id' => 'Ссылка быстрого поиска',
            'sort' => 'Сортировка',
            'name' => 'Наименование',
        ];
    }

    /**
     * Поиск
     * @param type $params
     * @return \common\models\ActiveDataProvider
     */
    public function search($params) {
        $query = QuickSearch::find()->
                from(['qs' => QuickSearch::tableName()])->
                leftJoin(['l' => CatalogLink::tableName()], "l.search_id=qs.id")->
                where(['AND', 'l.parent_id=:parent_id'], [':parent_id' => $this->parent_id]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'qs.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'qs.name', $this->name]);

        return $dataProvider;
    }

}

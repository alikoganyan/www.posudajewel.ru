<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property int $id
 * @property double $discount
 * @property string $name
 *
 * @property DiscountToUser[] $discountToUsers
 */
class Discount extends \backend\components\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'discount'], 'required'],
            [['discount'], 'number', 'max' => 100, 'min' => 0],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'       => 'ID',
            'discount' => 'Discount',
            'name'     => 'Name',
        ];
    }

    public function setPercent($property, $value) {
        Yii::$app->formatter->decimalSeparator = '.';
        $this->{$property} = Yii::$app->formatter->asDecimal($value/100, 2);
    }

    public function getPercent($property) {
        return Yii::$app->formatter->asDecimal($this->{$property}*100, 0);
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existDiscount = DiscountToUser::find()->
                where(['discount_id' => $this->id])->
                exists();
        return !$existDiscount;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountToUsers() {
        return $this->hasMany(DiscountToUser::className(), ['discount_id' => 'id']);
    }
}

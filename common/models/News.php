<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use backend\components\ActiveDataProvider;
use common\components\ImageHelper;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property string $url
 * @property string $image
 * @property string $description
 * @property string $content
 * @property string $date
 * @property int $is_blocked
 * @property integer            $sitemap
 * @property string             $meta_description
 * @property string             $meta_keywords
 * @property string             $meta_title
 */
class News extends ActiveRecord {

    /**
     * Файл с изображением
     * @var type 
     */
    public $file;
    
    /**
     *
     * @var type 
     */
    public $date_create;

    /**
     * Типы контента
     * @var array 
     */
    protected static $_type = [
        10 => 'Новость',
        20 => 'Статья',
        30 => 'Обзор',
    ];

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'url', 'type', 'is_blocked'], 'required', 'on' => ['insert', 'update']],
            [['name', 'url'], 'string', 'max' => 180, 'on' => ['insert', 'update']],
            [['description'], 'string', 'max' => 180, 'on' => ['insert', 'update']],
            [['meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['content'], 'string', 'on' => ['insert', 'update']],
            [['date'], 'safe', 'on' => ['insert', 'update']],
            [['type'], 'in', 'range' => array_keys(self::$_type), 'on' => ['insert', 'update']],
            [['is_blocked', 'sitemap'], 'in', 'range' => [0, 1], 'on' => ['insert', 'update']],
            [['url'], 'match', 'pattern' => '/^[a-z0-9\_\-]+$/i', 'message' => 'Можно использовать английские буквы, цифры, знак подчеркивания и дефис'],
            [['url'], 'unique'],
            ['file', 'file', 'extensions' => ['jpg', 'jpeg', 'png'], 'skipOnEmpty' => true, 'maxSize' => 7340032, 'on' => ['update', 'insert']],
            [['name', 'type', 'date', 'is_blocked'], 'safe', 'on' => ['search']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Контент',
            'name' => 'Наименование',
            'image' => 'Изображение',
            'description' => 'Краткое описание',
            'content' => 'Описание',
            'date' => 'Дата',
            'is_blocked' => 'Блокировка',
            'sitemap' => 'Блокировать в sitemap.xml',
            'meta_title' => 'Заголовок',
            'meta_description' => 'Описание',
            'meta_keywords' => 'Ключи',
            'url' => 'Адрес страницы',
        ];
    }

    /**
     * Поиск
     * @param type $params
     * @return \common\models\ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'type' => $this->type,
            'is_blocked' => $this->is_blocked,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Список типов контента
     * @return array
     */
    public static function listType() {
        return self::$_type;
    }

    /**
     * Название типа контента
     * @return string
     */
    public function getTypeName() {
        return isset(self::$_type[$this->type]) ? self::$_type[$this->type] : '-';
    }

    /**
     * Сохранение данных
     * @return boolean
     * @throws \Exception
     */
    public function saveNews() {
        if (!$this->validate() || !$this->save(false)) {
            return false;
        }

        if ($this->file instanceof \yii\web\UploadedFile) {
            $path = $this->getPath();
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }

            $this->image = md5(mt_rand(1000, 999999) . '_' . time()) . '.' . $this->file->getExtension();

            if ($this->file->saveAs($path . $this->image)) {
                ImageHelper::thumbnail($path . $this->image, $path . 'thumb_' . $this->image, 253, 157, ImageHelper::RESIZE_PROPORTION);

                $this->save(false, ['image']);
            }
        }

        return true;
    }

    /**
     * Путь до картинки
     * @return string
     */
    public function getPath($web = false) {
        $path = '/uploads/news/' . $this->id . '/';
        return $web ? $path : Yii::getAlias('@frontend/web') . $path;
    }

    /**
     * Url изображения
     * @param string $type
     * @return string
     */
    public function getUrlImage($type = 'thumb_') {
        return $this->getPath(true) . $type . $this->image;
    }

}

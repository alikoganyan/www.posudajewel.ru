<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use common\components\CropImageUploadBehavior;
use common\components\ImageHelper;

/**
 * This is the model class for table "service_image".
 *
 * @property integer $id
 * @property integer $service_id
 * @property string $name
 * @property string $name_crop
 */
class ServiceImage extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => CropImageUploadBehavior::className(),
                'attribute' => 'name',
                'path' => '@frontend/web/uploads',
                'url' => '/uploads',
                'ratio' => 590 / 590,
                'crop_field' => 'name_crop',
            ],
        ];
    }

    public static function tableName() {
        return 'service_image';
    }

    public function rules() {
        return [
            ['name', 'file', 'extensions' => 'jpg, jpeg, gif, png'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'name' => 'Фотография',
            'name_crop' => 'Name',
        ];
    }

    /**
     *
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (empty($this->name) || empty($this->name_crop)) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param type $insert
     * @param type $changedAttributes
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $path = self::getPath();

            ImageHelper::thumbnail($path . '/' . $this->name, $path . '/thumb_' . $this->name, 590, 590, ImageHelper::RESIZE_PROPORTION);
        }
    }

    /**
     * Url превью фотографии
     * @return string
     */
    public function getUrlPrev() {
        return self::getPath(true) . '/thumb_' . $this->name;
    }

    /**
     * Url превью фотографии
     * @return string
     */
    public function getUrlImage() {
        return self::getPath(true) . '/' . $this->name;
    }

    /**
     *
     * @param type $web
     * @return type
     */
    public static function getPath($web = false) {
        return $web ? '/uploads' : Yii::getAlias('@frontend/web/uploads/');
    }

}

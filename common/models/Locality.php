<?php
namespace common\models;

use backend\components\ActiveRecord;
use dosamigos\google\maps\services\GeocodingClient;
use GuzzleHttp\Client;
use yii\helpers\Json;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "locality".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property string $longitude
 * @property string $latitude
 * @property integer $gismeteo_id
 * @property integer $is_quick_choice
 * @property Region $region
 */
class Locality extends ActiveRecord {

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }
    
    public static function tableName() {
        return 'locality';
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['region_id', 'gismeteo_id'], 'integer'],
            [['name', 'longitude', 'latitude'], 'string', 'max' => 255],
            [['is_quick_choice'], 'in', 'range' => [0, 1]]
        ];
    }

    public function attributeLabels() {
        return [
            'id'          => 'Ид',
            'region_id'   => 'Ид региона',
            'name'        => 'Название',
            'longitude'   => 'Долгота',
            'latitude'    => 'Широта',
            'gismeteo_id' => 'Гисметео',
            'is_quick_choice' => 'Быстрый выбор',
        ];
    }

    public function setCoords() {
        if (!$this->latitude || !$this->longitude) {
            $geoCoder = new GeocodingClient();
            $result = $geoCoder->lookup(['address' => $this->name]);

            if (isset($result->results[0])) {
                $this->latitude = $result->results[0]->geometry->location->lat;
                $this->longitude = $result->results[0]->geometry->location->lng;
            }
            else {
                $this->latitude = $this->region->latitude;
                $this->longitude = $this->region->longitude;
            }
        }
    }


    public function getRegion() {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * Название населенного пункта
     * @return string
     */
    public function getName() {
        $temp = explode('|', $this->name);

        return trim($temp[0]);
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existCompany = Company::find()->
                where(['locality_id' => $this->id])->
                exists();
        return !$existCompany;
    }

}

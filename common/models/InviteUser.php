<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "invite_user".
 *
 * @property integer $invite_id
 * @property integer $user_id
 * @property string $email
 * @property string $phone
 * @property string $description
 * @property integer $is_add_number
 * @property integer $send_sms
 * @property integer $send_email
 * @property integer $is_accepted
 */
class InviteUser extends ActiveRecord {
    public static function tableName() {
        return 'invite_user';
    }

    public function rules() {
        return [
            [['invite_id', 'user_id', 'is_add_number', 'send_sms', 'send_email', 'is_accepted'], 'integer'],
            [['description'], 'string'],
            [['email', 'phone'], 'string', 'max' => 255],
            [['invite_id', 'user_id'], 'unique', 'targetAttribute' => ['invite_id', 'user_id'], 'message' => 'The combination of Invite ID and User ID has already been taken.'],
        ];
    }

    public function attributeLabels() {
        return [
            'invite_id'     => 'Invite ID',
            'user_id'       => 'User ID',
            'email'         => 'Email',
            'phone'         => 'Phone',
            'description'   => 'Description',
            'is_add_number' => 'Is Add Number',
            'send_sms'      => 'Send Sms',
            'send_email'    => 'Send Email',
            'is_accepted'   => 'Is Accepted',
        ];
    }
}

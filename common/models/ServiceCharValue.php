<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "service_char_value".
 *
 * @property integer $service_id
 * @property integer $char_id
 * @property integer $value_id
 * @property Char $char
 * @property Value $value
 */
class ServiceCharValue extends ActiveRecord {
    public static function tableName() {
        return 'service_char_value';
    }

    public function rules() {
        return [
            [['service_id', 'char_id', 'value_id'], 'required'],
            [['service_id', 'char_id', 'value_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'service_id' => 'Service ID',
            'char_id'    => 'Char ID',
            'value_id'   => 'Value ID',
        ];
    }

    public function getChar() {
        return $this->hasOne(Char::className(), ['id' => 'char_id']);
    }

    public function getValue() {
        return $this->hasOne(Value::className(), ['id' => 'value_id']);
    }
}

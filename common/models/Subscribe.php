<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use backend\components\ActiveDataProvider;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "subscribe".
 *
 * @property int $id Id
 * @property string $email E-mail
 * @property int $status Статус: 1-подписан, 0-отписан
 * @property int $time_create Время подписки
 * @property int $time_update Время изменения
 */
class Subscribe extends ActiveRecord {


    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email'], 'required', 'on' => ['insert', 'update']],
            [['email'], 'email', 'on' => ['insert', 'update']],
            [['email', 'status'], 'safe', 'on' => ['search']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'status' => 'Статус подписки',
            'time_create' => 'Дата подписки',
            'time_update' => 'Время обновления',
        ];
    }

    /**
     * 
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            // Создадим хэш
            if (empty($this->hash)) {
                $this->generateHash();
            }
            
            return true;
        } else {
            return false;
        }
    }

    /**
     * Поиск
     * @param type $params
     * @return \common\models\ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

    /**
     * Статус подписки
     * @return string
     */
    public function getStatus() {
        return $this->status ? 'Да' : 'Нет';
    }

    /**
     * Создание уникального хэша
     */
    public function generateHash() {
        do {
            $this->hash = md5($this->email . '_' . time() . '_' . mt_rand(100, 99999));

            $exists = self::find()->where(['hash' => $this->hash])->exists();
        } while ($exists);
    }

    /**
     * Изменение сценария
     */
    public function updateScenario() {
        $this->setScenario($this->isNewRecord ? 'insert' : 'update');
    }

}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_buy".
 *
 * @property int $id
 * @property int $service_id
 * @property int $buy_id
 */
class ServiceBuy extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'service_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['service_id', 'buy_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'buy_id' => 'Buy ID',
        ];
    }

}

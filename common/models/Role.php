<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $name
 * @property string $permissions
 * @property integer $is_undeleted
 * @property User[] $users
 */
class Role extends ActiveRecord
{
    public $entities;

    public static function tableName()
    {
        return 'role';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['permissions'], 'string'],
            [['is_undeleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'permissions' => 'Permissions',
        ];
    }

    public static function getDefaultRole() {
        return self::findOne(['name' => 'Пользователь']);
    }

    public function getUsers() {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }

    public function getParsedPermissions() {
        return Json::decode($this->permissions);
    }

    public function setPermissions($data) {
        $entityKeys = array_keys(Entity::$collection);
        $result = [];

        foreach ($data as $key => $value) {
            if (!in_array($key, $entityKeys)) {
                continue;
            }

            $result[$key] = $value;
        }

        $this->permissions = Json::encode($result);
    }
}

<?php

namespace common\models;

use backend\components\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "char".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $is_multiple
 * @property integer $is_filter
 * @property integer $is_show
 * @property integer $sort
 * @property Value[] $values
 * @property string $isMultiple
 */
class Char extends ActiveRecord {

    private $_isMultiple;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'char';
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'isMultiple':
                $this->_isMultiple = $value;
                break;

            default:
                parent::__set($name, $value);
                break;
        }
    }

    public function __get($name) {
        switch ($name) {
            case 'isMultiple':
                return $this->is_multiple == 0 ? 'Одно свойство' : 'Мультивыбор';

            default:
                return parent::__get($name);
        }
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['is_multiple', 'is_filter', 'is_show'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['sort'], 'integer', 'min' => 0],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Ид',
            'name' => 'Свойство',
            'is_multiple' => 'Вариант выбора',
            'is_filter' => 'Выводить в фильтр',
            'is_show' => 'Выводить в карточку товара',
            'sort' => 'Сортировка',
            'isMultiple' => 'Вариант выбора',
        ];
    }

    public function getValues() {
        return $this->hasMany(Value::className(), ['id' => 'value_id'])->viaTable(CharValue::tableName(), ['char_id' => 'id']);
    }

    public function getValueList() {
        return ArrayHelper::map($this->values, 'id', 'name');
    }

    /**
     * 
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (empty($this->sort)) {
                $this->sort = 1;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existType = ServiceTypeChar::find()->
                where(['char_id' => $this->id])->
                exists();


        return !$existType;
    }

}

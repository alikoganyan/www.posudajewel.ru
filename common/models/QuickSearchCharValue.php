<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "quick_search_char_value".
 *
 * @property integer $quick_search_id
 * @property integer $char_id
 * @property integer $value_id
 * @property Char $char
 * @property Value $value
 */
class QuickSearchCharValue extends ActiveRecord {
    public static function tableName() {
        return 'quick_search_char_value';
    }

    public function rules() {
        return [
            [['quick_search_id', 'char_id', 'value_id'], 'required'],
            [['quick_search_id', 'char_id', 'value_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'quick_search_id' => 'Quick Search ID',
            'char_id'         => 'Характеристика',
            'value_id'        => 'Значение',
        ];
    }

    public function getChar() {
        return $this->hasOne(Char::className(), ['id' => 'char_id']);
    }

    public function getValue() {
        return $this->hasOne(Value::className(), ['id' => 'value_id']);
    }
}

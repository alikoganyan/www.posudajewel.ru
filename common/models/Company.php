<?php

namespace common\models;

use backend\components\ActiveRecord;
use dosamigos\google\maps\services\GeocodingClient;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $locality_id
 * @property string $name
 * @property string $web
 * @property string $first_phone
 * @property string $main_phone
 * @property string $second_phone
 * @property string $email
 * @property string $address
 * @property integer $longitude
 * @property integer $latitude
 * @property integer $is_blocked
 * @property User $user
 * @property Locality $locality
 * @property Service[] $services
 * @property [] $userList
 * @property string $localityName
 * @property string $regionName
 * @property string $userName
 */
class Company extends ActiveRecord {
    const SCENARIO_GEO = 0;

    private $_localityName;
    private $_regionName;
    private $_userName;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'company';
    }

    public function rules() {
        return [
            [['user_id', 'locality_id', 'is_blocked'], 'integer'],
            [['name', 'locality_id'], 'required'],
            [['longitude'], 'required', 'message' => 'Выберите место на карте'],
            [['latitude','longitude'], 'number', 'message' => 'Выберите место на карте'],
            [['email'], 'email'],
            [['name', 'web', 'first_phone', 'email', 'address', 'main_phone', 'second_phone'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert) {
        if (!$this->user_id) {
            $this->user = \Yii::$app->user->identity->id;
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels() {
        return [
            'id'           => 'Ид',
            'user_id'      => 'Пользователь',
            'locality_id'  => 'Нас. пункт',
            'userName'     => 'Пользователь',
            'name'         => 'Компания',
            'web'          => 'Web-сайт',
            'first_phone'  => 'Доп. телефон',
            'second_phone' => 'Доп. телефон',
            'main_phone'   => 'Основной телефон',
            'regionName'   => 'Регион',
            'localityName' => 'Нас. пункт',
            'email'        => 'E-Mail',
            'address'      => 'Адрес',
            'longitude'    => 'Долгота',
            'latitude'     => 'Широта',
            'is_blocked'   => 'Блокировка',
        ];
    }

//    public function afterFind() {
//        if ($this->locality && (!$this->latitude || !$this->longitude)) {
//            $geoCoder = new GeocodingClient();
//            $result = $geoCoder->lookup(['address' => $this->locality->name]);
//
//            $this->latitude = $result->results[0]->geometry->location->lat;
//            $this->longitude = $result->results[0]->geometry->location->lng;
//        }
//
//        parent::afterFind();
//    }

    public function __get($name) {
        switch ($name) {
            case 'localityName':
                if ($this->locality) {
                    return $this->locality->name;
                }

                return $this->_localityName;

            case 'userName':
                if ($this->user) {
                    return $this->user->fullName;
                }

                return $this->_userName;

            case 'regionName':
                if ($this->locality) {
                    return $this->locality->region->name;
                }

                return $this->_regionName;

            default:
                return parent::__get($name);
        }
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'localityName':
                $this->_localityName = $value;
                break;

            case 'regionName':
                $this->_regionName = $value;
                break;

            case 'userName':
                $this->_userName = $value;
                break;

            default:
                parent::__set($name, $value);
        }
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserList() {
        return ArrayHelper::map(User::find()->all(), 'id', function ($user) {
            /** @var User $user */
            return $user->fullName;
        });
    }

    public function getLocality() {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    public function getRegionList() {
        return ArrayHelper::map(Region::find()->orderBy('name')->all(), 'id', 'name');
    }

    public function getServices() {
        return $this->hasMany(Service::className(), ['company_id' => 'id']);
    }

    public function getPhonesLabel() {
        $phones = [];

        foreach ([$this->main_phone, $this->first_phone, $this->second_phone] as $phone) {
            if (strlen($phone) > 0)
                $phones[] = Html::tag('span', $phone);
        }

        return implode(', ', $phones);
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existService = Service::find()->
                where(['company_id' => $this->id])->
                exists();


        return !$existService;
    }
}

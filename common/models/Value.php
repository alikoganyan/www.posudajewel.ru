<?php
namespace common\models;

use backend\components\ActiveRecord;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "value".
 *
 * @property integer $id
 * @property string $name
 * @property Char $char
 */
class Value extends ActiveRecord {
    public $charId;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'value';
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'         => 'Ид',
            'name'       => 'Значение',
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            $model = new CharValue([
                'char_id'  => $this->charId,
                'value_id' => $this->id,
            ]);
            $model->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getChar() {
        return $this->hasOne(Char::className(), ['id' => 'char_id'])->viaTable(CharValue::tableName(), ['value_id' => 'id']);
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existSearch = QuickSearchCharValue::find()->
                where(['value_id' => $this->id])->
                exists();
        $existService = ServiceCharValue::find()->
                where(['value_id' => $this->id])->
                exists();

        return !$existSearch && !$existService;
    }

}

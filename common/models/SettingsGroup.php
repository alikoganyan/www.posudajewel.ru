<?php

namespace common\models;

use backend\components\ActiveRecord;

/**
 * This is the model class for table "settings_group".
 *
 * @property int $id
 * @property string $name
 * @property int $order
 *
 * @property Settings[] $settings
 */
class SettingsGroup extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_group';
    }

    public function rules()
    {
        return [
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['order'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order' => 'Order',
        ];
    }

    public function getSettings()
    {
        return Settings::find()->where(['group_id' => $this->id])->orderBy('order')->all();
    }
}

<?php

namespace common\models;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "settings".
 *
 * @property string $key
 * @property string $value
 * @property string $type
 * @property string $order
 * @property string $group_name
 * @property string $title
 */
class Settings extends \yii\db\ActiveRecord {
    const TYPE_JSON = 'json';
    const TYPE_TEXT = 'text';
    const TYPE_IMAGE = 'image';

    /**
     * Настройки
     * @var null|array
     */
    protected static $settings = null;

    public static function tableName() {
        return 'settings';
    }

    public function rules() {
        return [
            [['key', 'value', 'group_name'], 'string', 'max' => 255],
            [['order'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'key'        => 'Key',
            'value'      => 'Value',
        ];
    }
    public function attributes() {
        return ArrayHelper::merge(parent::attributes(), ['banner', 'logo', 'favicon']);
    }

    /**
     * Возвращает значение настройки
     * @param string | array $name
     * @param boolean $reload
     * @return string | array | null
     */
    public static function getSetting($name, $reload = false) {
        if (is_null(self::$settings) || $reload) {
            self::$settings = Settings::find()->
                    select(['value', 'key'])->
                    indexBy('key')->
                    column();
        }

        if (is_array($name)) {
            $result = [];

            foreach (self::$settings as $key => $value) {
                if (in_array($key, $name)) {
                    $result[$key] = $value;
                }
            }

            return $result;
        }

        return isset(self::$settings[$name]) ? self::$settings[$name] : null;
    }

    /**
     * Сохранение значения настройки
     * @param string $name
     * @param mixed $value
     * @return boolean
     */
    public static function setSetting($name, $value) {
        $setting = Settings::findOne(['key' => $name]);

        if (!$setting instanceof Settings) {
            $setting = new Settings();
        }

        $setting->key = $name;
        $setting->value = $value;

        return $setting->save();
    }

    /**
     * 
     * @return string
     */
    public static function primaryKey() {
        return ['key'];
    }

    /**
     * Адрес баннера
     * @return string
     */
    public static function getUrlBanner() {
        return '/uploads/' . self::getSetting('banner');
    }

    /**
     * Адрес логотипа
     * @return string
     */
    public static function getUrlLogo() {
        return '/uploads/' . self::getSetting('logo');
    }

}

<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use backend\components\ActiveDataProvider;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;
use common\components\DataHelper;

/**
 * This is the model class for table "page".
 *
 * @property int $id Id
 * @property string $name Наименование
 * @property string $url Url
 * @property string $content Описание
 * @property int $is_footer Вывод в футере
 * @property int $is_blocked Блокировка
 * @property int $time_create Время добавления
 * @property int $time_update Время редактирования
 * @property int $user_create Кто добавил
 * @property int $user_update Кто изменил
 * 
 * @property integer            $sitemap
 * @property string             $meta_description
 * @property string             $meta_keywords
 * @property string             $meta_title
 *
 *
 * @property User $userCreate
 * @property User $userUpdate
 */
class Page extends ActiveRecord {

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'is_blocked'], 'required', 'on' => ['insert', 'update']],
            [['name', 'url'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            [['content'], 'string', 'on' => ['insert', 'update']],
            [['meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['is_footer', 'is_blocked'], 'in', 'range' => [0, 1], 'on' => ['insert', 'update']],
            [['url'], 'match', 'pattern' => '/^[a-z0-9\_\-]+$/i', 'message' => 'Можно использовать английские буквы, цифры, знак подчеркивания и дефис', 'on' => ['insert', 'update']],
            [['url'], 'unique', 'on' => ['insert', 'update']],
            [['sitemap'], 'in', 'range' => [0, 1]],
            [['name', 'url', 'is_footer', 'is_blocked'], 'safe', 'on' => ['search']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'content' => 'Описание',
            'is_footer' => 'Вывод в футере',
            'is_blocked' => 'Блокировка',
            'time_create' => 'Время добавления',
            'time_update' => 'Время редактирования',
            'user_create' => 'Кто создал',
            'user_update' => 'Кто изменил',
            'sitemap' => 'Блокировать в sitemap.xml',
            'meta_title' => 'Заголовок',
            'meta_description' => 'Описание',
            'meta_keywords' => 'Ключи',
            'url' => 'Адрес страницы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate() {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate() {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }

    /**
     * 
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {

            if (empty($this->url)) {
                DataHelper::generateUniqueUrl($this);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Поиск
     * @param type $params
     * @return \common\models\ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_footer' => $this->is_footer,
            'is_blocked' => $this->is_blocked,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }

    /**
     * Изменение сценария
     */
    public function updateScenario() {
        $this->setScenario($this->isNewRecord ? 'insert' : 'update');
    }

}

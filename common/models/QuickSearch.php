<?php
namespace common\models;

use backend\components\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;
use common\components\DataHelper;

/**
 * This is the model class for table "quick_search".
 *
 * @property integer $id
 * @property integer $service_type_id
 * @property string $name
 * @property string $image
 * @property string $url
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property integer $is_show
 * @property ServiceType $serviceType
 * @property string $serviceTypeName
 * @property integer            $sitemap
 */
class QuickSearch extends ActiveRecord {
    /** @var UploadedFile $imageFile */
    public $imageFile;
    public $char = [];

    private $_serviceTypeName;

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    public static function tableName() {
        return 'quick_search';
    }

    public function rules() {
        return [
            [['name', 'service_type_id'], 'required'],
            [['service_type_id'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255],
            [['title', 'url', 'keywords'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['content'], 'string', 'max' => 30000],
            [['url'], 'match', 'pattern' => '/^[a-z0-9\_\-]+$/i', 'message' => 'Можно использовать английские буквы, цифры, знак подчеркивания и дефис'],
            [['url'], 'unique'],
            [['is_show', 'sitemap'], 'in', 'range' => [0, 1]],
            [['char'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id'              => 'ID',
            'service_type_id' => 'Категория',
            'serviceTypeName' => 'Категория',
            'name'            => 'Ссылка',
            'imageFile'       => 'Картинка',
            'url'             => 'Адрес страницы',
            'title'           => 'Заголовок',
            'keywords'        => 'Ключи',
            'description'     => 'Описание',
            'content'         => 'SEO описание',
            'is_show'         => 'Вывести на главной странице',
            'sitemap' => 'Блокировать в sitemap.xml',
        ];
    }

    public function __get($name) {
        switch ($name) {
            case 'serviceTypeName':
                if ($this->serviceType) {
                    return $this->serviceType->name;
                }

                return $this->_serviceTypeName;

            default:
                return parent::__get($name);
        }
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'serviceTypeName':
                $this->_serviceTypeName = $value;
                break;

            default:
                parent::__set($name, $value);
        }
    }

    public function upload() {
        if ($this->imageFile) {
            $path = \Yii::getAlias('@frontend') . '/web/uploads';

            if (!file_exists($path)) {
                FileHelper::createDirectory($path);
            }

            $name = \Yii::$app->security->generateRandomString();
            $this->image = "$name.{$this->imageFile->extension}";

            return $this->imageFile->saveAs("$path/{$this->image}");
        }

        return false;
    }

    public function getImg() {
        return $this->image ? ["/uploads/{$this->image}"] : [];
    }

    public function attributes() {
        return ArrayHelper::merge(parent::attributes(), ['char']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->imageFile) {
                $this->upload();
            }
            
            if(empty($this->url)){
                DataHelper::generateUniqueUrl($this);
            }

            return true;
        } else {
            return false;
        }
    }

    public function getChars() {
        return Char::find()
            ->innerJoin(QuickSearchCharValue::tableName(), 'char_id = id')
            ->where(['quick_search_id' => $this->id])
            ->groupBy(['id']);
    }

    public function getCharValues($charId) {
        return Value::find()
                    ->from(['v' => 'value'])
                    ->innerJoin(
                        ['q' => QuickSearchCharValue::tableName()],
                        "q.quick_search_id = {$this->id} AND q.char_id = {$charId} AND q.value_id = v.id")
                    ->all();
    }

    public function getCharList() {
        $charIds = ArrayHelper::getColumn($this->getChars()->all(), 'id');

        return ArrayHelper::map(Char::find()->where(['not in','id', $charIds])->all(), 'id', 'name');
    }

    public function getServiceType() {
        return $this->hasOne(ServiceType::className(), ['id' => 'service_type_id']);
    }

    public function getServiceTypeList() {
        return ArrayHelper::map(ServiceType::find()->all(), 'id', 'name');
    }

    public function afterSave($insert, $changedAttributes) {
        QuickSearchCharValue::deleteAll(['quick_search_id' => $this->id]);

        foreach ($this->char as $charId => $chars) {
            foreach ($chars as $valueId) {
                $model = new QuickSearchCharValue([
                    'quick_search_id' => $this->id,
                    'char_id'         => $charId,
                    'value_id'        => $valueId,
                ]);

                $model->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getLogo() {
        if (!$this->image) {
            return '/img/img-tab1.jpg';
        }

        return "/uploads/{$this->image}";
    }

    public function doSearch() {
        $resultQuery = QuickSearchCharValue::find()
            ->from(['qs' => QuickSearchCharValue::tableName()])
            ->innerJoin(['q' => self::tableName()], 'q.id = qs.quick_search_id')
            ->where(['q.id' => $this->id])
            ->all();

        $result = [];
        foreach ($resultQuery as $item) {
            /** @var $item QuickSearchCharValue */
            $result[$item->char_id][] = $item->value_id;
        }

        return $result;
    }

    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existCatalog = CatalogLink::find()->
                where(['search_id' => $this->id])->
                exists();
        
        return !$existCatalog;
    }
}

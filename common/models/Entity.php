<?php
namespace common\models;

use yii\base\Model;

class Entity extends Model {
    public static $collection = [
        'company'      => [
            'label'  => 'Компания',
            'fields' => [
                'user_id'      => 'Пользователь',
                'region'       => 'Регион',
                'locality_id'  => 'Город',
                'map'          => 'Карта',
                'name'         => 'Название',
                'web'          => 'Сайт',
                'first_phone'  => 'Телефон',
                'second_phone' => 'Телефон',
                'main_phone'   => 'Основной Телефон',
                'email'        => 'E-Mail',
                'address'      => 'Адрес',
                'is_blocked'   => 'Статус',
                'grid'         => 'Таблица',
                'add'          => 'Добавление записей',
                'delete'       => 'Удаление записей',
            ],
        ],
        'order'       => [
            'label'  => 'Заказ',
            'fields' => [
                'user_id'       => 'Пользователь',
                'cached_cart_id'=> 'Корзина',
                'fio'           => 'ФИО',
                'phone'         => 'Телефон',
                'email'         => 'e-mail',
                'address'       => 'Адрес',
                'comment'       => 'Примечание',
                'delivery_cost' => 'Стоимость доставки',
                'delivery_id'   => 'Служба доставки',
                'status_id'     => 'Статус',
                'grid'          => 'Таблица',
                'add'           => 'Добавление записей',
                'delete'        => 'Удаление записей',
                'notification'  => 'Хочу получать персональные предложения и скидки по эл. почте',
                'postcode'      => 'Почтовый индекс',
                'region_id'     => 'Регион',
                'locality_id'   => 'Нас. пункт',
                'organization'  => 'Наименование организации',
                'law_address'   => 'Юридический адрес',
                'actual_address'=> 'Фактический адрес',
                'account_number'=> 'Номер счета (Р/С)',
                'bank'          => 'Банк',
                'corr_number'   => 'Корр. счет (К/С)',
                'bik'           => 'БИК',
                'bank_city'     => 'Город банка'
            ],
        ],
        'user'         => [
            'label'  => 'Пользователь',
            'fields' => [
                'first_name'      => 'Имя',
                'last_name'       => 'Фамилия',
                'password'        => 'Пароль',
                'repeat_password' => 'Повторите пароль',
                'role_id'         => 'Роль',
                'email'           => 'Электронная почта',
                'birthday'        => 'Дата рождения',
                'phone'           => 'Телефон',
                'vk_id'           => 'Вконтакте',
                'fb'              => 'Facebook',
                'google_id'       => 'Google Plus',
                'yandex'          => 'Яндекс',
                'is_blocked'      => 'Статус',
                'grid'            => 'Таблица',
                'add'             => 'Добавление записей',
                'delete'          => 'Удаление записей',
            ],
        ],
        'discount'    => [
            'label'  => 'Типы скидок',
            'fields' => [
                'name'     => 'Наименование скидки',
                'discount' => 'Скидка (%)',
                'grid'     => 'Таблица',
                'add'      => 'Добавление записей',
                'delete'   => 'Удаление записей',
            ],
        ],
        'role'         => [
            'label'  => 'Права доступа',
            'fields' => [
                'access' => 'Разрешить',
                'add'    => 'Добавление записей',
                'delete' => 'Удаление записей',
            ],
        ],
        'char'         => [
            'label'  => 'Характеристики',
            'fields' => [
                'name'        => 'Наименование',
                'is_multiple' => 'Вариант выбора',
                'is_filter'   => 'Выводить в фильтр',
                'is_show'     => 'Выводить в карточку товара',
                'sort'        => 'Сортировка',
                'grid'        => 'Таблица',
                'add'         => 'Добавление записей',
                'delete'      => 'Удаление записей',
            ],
        ],
        'value'         => [
            'label'  => 'Значения характеристик',
            'fields' => [
                'name'        => 'Наименование',
                'grid'        => 'Таблица',
                'add'         => 'Добавление записей',
                'delete'      => 'Удаление записей',
            ],
        ],
        'invite'       => [
            'label'  => 'Приглашения',
            'fields' => [
                'name'        => 'Наименование приглашения',
                'description' => 'Текст приглашения',
                'add'         => 'Добавление записей',
                'delete'      => 'Удаление записей',
            ],
        ],
        'locality'     => [
            'label'  => 'Нас. пункт',
            'fields' => [
                'region_id' => 'Регион',
                'name'      => 'Название',
                'longitude' => 'Долгота',
                'latitude'  => 'Широта',
                'is_quick_choice' => 'Быстрый выбор',
                'add'       => 'Добавление записей',
                'delete'    => 'Удаление записей',
            ],
        ],
        'catalog' => [
            'label'  => 'Каталог предложений',
            'fields' => [
                'name'        => 'Наименование',
                'sort'        => 'Сортировка',
                'is_hidden_subsections'        => 'Вывод подразделы в меню каталога',
                'add'         => 'Добавление записей',
                'delete'      => 'Удаление записей',
            ],
        ],
        'quicksearch' => [
            'label'  => 'Быстрый поиск',
            'fields' => [
                'service_type_id' => 'Категория',
                'name'            => 'Наименование',
                'image'           => 'Картинка',
                'chars'           => 'Характеристики',
                'sitemap' => 'Блокировать в sitemap.xml',
                'add'          => 'Добавление записей',
                'delete'       => 'Удаление записей',
            ],
        ],
        'region'       => [
            'label'  => 'Регион',
            'fields' => [
                'name'      => 'Наименование региона',
                'code'      => 'Код',
                'longitude' => 'Долгота',
                'latitude'  => 'Широта',
                'grid'      => 'Таблица',
                'add'       => 'Добавление записей',
                'delete'    => 'Удаление записей',
            ],
        ],
        'news' => [
            'label' => 'Новости',
            'fields' => [
                'type' => 'Контент',
                'name' => 'Наименование',
                'url' => 'Url',
                'image' => 'Изображение',
                'description' => 'Краткое описание',
                'content' => 'Описание',
                'date' => 'Дата',
                'is_blocked' => 'Блокировка',
                'grid' => 'Таблица',
                'sitemap' => 'Блокировать в sitemap.xml',
                'add' => 'Добавление записей',
                'delete' => 'Удаление записей',
            ],
        ],
        'page' => [
            'label' => 'Страницы',
            'fields' => [
                'id' => 'ID',
                'name' => 'Наименование',
                'url' => 'Адрес',
                'content' => 'Описание',
                'is_footer' => 'Вывод в футере',
                'is_blocked' => 'Блокировка',
                'grid' => 'Таблица',
                'sitemap' => 'Блокировать в sitemap.xml',
                'add' => 'Добавление записей',
                'delete' => 'Удаление записей',
            ],
        ],
        'service'      => [
            'label'  => 'Продукция',
            'fields' => [
                'unit_id'          => 'Ед. измерения',
                'company_id'       => 'Компания',
                'price'            => 'Цена',
                'service_type_id'  => 'Вид услуги',
                'name'             => 'Наименование',
                'description'      => 'Описание',
                'image'            => 'Изображение',
                'is_popular'       => 'Популярная',
                'is_blocked'       => 'Заблокирована',
                'chars'            => 'Характеристики',
                'grid'             => 'Таблица',
                'meta_description' => 'Meta Description',
                'meta_keywords'    => 'Meta Keywords',
                'is_new'           => 'Новое предложение',
                'in_stock'         => 'В наличии на складе',
                'code'             => 'Артикул',
                'id_1c'            => 'ID',
                'url'              => 'Url',
                'buy'              => 'С этим товаром покупают',
                'sort'             => 'Сортировка',
                'sitemap'          => 'Блокировать в sitemap.xml',
                'add'              => 'Добавление записей',
                'delete'           => 'Удаление записей',
            ],
        ],
        'servicetype' => [
            'label'  => 'Категория',
            'fields' => [
                'name'    => 'Наименование вида услуги',
                'is_mass' => 'Массовая услуга',
                'chars'   => 'Характеристики',
                'is_blocked' => 'Заблокирована',
                'grid'    => 'Таблица',
                'add'     => 'Добавление записей',
                'delete'  => 'Удаление записей',
            ],
        ],
        'unit' => [
            'label'  => 'Ед. измерения',
            'fields' => [
                'name'        => 'Наименование',
                'code'        => 'Код обозначения',
                'designation' => 'Краткое обозначение',
                'add'         => 'Добавление записей',
                'delete'      => 'Удаление записей',
            ],
        ],
    ];

    public static function getEntityFields($entityName) {
        if (!isset(self::$collection[$entityName])) {
            throw new \Exception('entity does not exist');
        }

        $entity = ucfirst(self::$collection[$entityName]);
        /** @var Model $model */
        $model = new $entity();

        return $model->getAttributes();
    }
}

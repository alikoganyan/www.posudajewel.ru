<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "service_option_group".
 *
 * @property integer $id
 * @property integer $service_id
 * @property string $name
 * @property integer $is_multiple
 * @property ServiceOption[] $options
 */
class ServiceOptionGroup extends \yii\db\ActiveRecord {
    public static function tableName() {
        return 'service_option_group';
    }

    public function rules() {
        return [
            [['is_multiple', 'service_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'          => 'ID',
            'service_id'  => 'Услуга',
            'name'        => 'Название группы',
            'is_multiple' => 'Одно значение',
        ];
    }

    public function getOptions() {
        return $this->hasMany(ServiceOption::className(), ['group_id' => 'id']);
    }
}

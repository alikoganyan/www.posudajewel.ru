<?php
namespace common\models;

use backend\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "service_option".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property integer $price
 */
class ServiceOption extends ActiveRecord {
    public static function tableName() {
        return 'service_option';
    }

    public function rules() {
        return [
            [['name', 'group_id'], 'required'],
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'group_id'   => 'Group ID',
            'name'       => 'Название',
            'price'      => 'Цена',
        ];
    }
}

<?php

namespace common\models;

use backend\components\ActiveRecord;

/**
 * This is the model class for table "invite".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 */
class Invite extends ActiveRecord {
    public static function tableName() {
        return 'invite';
    }

    public function rules() {
        return [
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'          => 'Ид',
            'name'        => 'Наименование приглашения',
            'description' => 'Текст приглашения',
        ];
    }
}

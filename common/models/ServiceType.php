<?php
namespace common\models;

use backend\components\ActiveRecord;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "service_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_mass
 * @property Char[] $chars
 * @property Service[] $services
 */
class ServiceType extends ActiveRecord {

    /**
     * 
     * @return type
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }
    
    public static function tableName() {
        return 'service_type';
    }

    public function rules() {
        return [
            [['is_mass'], 'boolean'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id'         => 'Ид',
            'name'       => 'Категория',
            'is_mass'    => 'Массовая услуга',
        ];
    }

    public function getChars() {
        return $this->hasMany(Char::className(), ['id' => 'char_id'])
            ->viaTable(ServiceTypeChar::tableName(), ['service_type_id' => 'id']);
    }

    public function getServices() {
        return $this->hasMany(Service::className(), ['service_type_id' => 'id']);
    }
    
    /**
     * Разрешено ли удаление записи
     * @return boolean
     */
    public function allowDelete() {
        $existService = Service::find()->
                where(['service_type_id' => $this->id])->
                exists();

        $existSearch = QuickSearch::find()->
                where(['service_type_id' => $this->id])->
                exists();

        return !$existService && !$existSearch;
    }

}

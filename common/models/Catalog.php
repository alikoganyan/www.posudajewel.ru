<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;
use backend\components\ActiveDataProvider;
use common\components\CropImageUploadBehavior;
use common\components\BlameableBehavior;
use common\components\TimestampBehavior;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $sort
 * @property string $image
 * @property string $image_crop
 * @property int $service_id
 * @property int $is_hidden_subsections
 */
class Catalog extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => CropImageUploadBehavior::className(),
                'attribute' => 'image',
                'path' => '@frontend/web' . self::getPath(),
                'url' => self::getPath(),
                'ratio' => 288 / 212,
                'cropped_field' => 'image_crop',
                'scenarios' => ['insert', 'update'],
            ],
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required', 'on' => ['insert', 'update']],
            [['name'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            [['sort'], 'integer', 'min' => 0, 'on' => ['insert', 'update']],
            [['is_hidden_subsections'], 'in', 'range' => [0, 1], 'on' => ['insert', 'update']],
            [['image'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id'], 'on' => ['insert', 'update']],
            [['name'], 'safe', 'on' => 'search']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'name' => 'Название раздела',
            'sort' => 'Сортировка',
            'image' => 'Изображение',
            'image_crop' => 'Уменьшенное изображение',
            'service_id' => 'Предложение',
            'is_hidden_subsections' => 'Не выводить подразделы в меню каталога',
        ];
    }

    /**
     * 
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!empty($this->image) && empty($this->image_crop)) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     */
    public function afterDelete() {
        parent::afterDelete();

        Catalog::deleteAll(['parent_id' => $this->id]);
        CatalogLink::deleteAll(['parent_id' => $this->id]);
    }

    /**
     * Поиск
     * @param type $params
     * @return \common\models\ActiveDataProvider
     */
    public function search($params) {
        $query = self::find()->where(['parent_id' => $this->parent_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Url превью изображения
     * @return string
     */
    public function getUrlPrev() {
        if (!empty($this->image_crop)) {
            return self::getPath() . '/' . $this->image_crop;
        } else {
            return '';
        }
    }

    /**
     * Путь до директории с картинками
     */
    public static function getPath() {
        return "/uploads/catalog";
    }

}

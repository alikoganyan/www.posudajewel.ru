<?php
/**
 * @var $this \yii\web\View
 * @var $user \common\models\User
 */
?>

<div class="password-reset">
    <?= $user->confirm_code ?>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $order \common\models\Order
 */
;?>

<div class="updated-order">
    Статус вашего заказа #<?= $order->id ?> обновился:
    <span>
        <?= $order->status->name ?>
    </span>
</div>

<?php
/**
 * @var $this yii\web\View
 * @var $user common\models\User
 * @var $newPassword string
 */
use yii\helpers\Url;

?>

<div class="password-reset">
    <p>Здравствуйте <?= $user->fullName ?>.</p>
    <p>Для сброса пароля от вашей учетной записи перейдя по <a href="<?= Url::to(['site/reset-password', 'token' => $user->password_reset_token], true) ?>">этой ссылке</a></p>
</div>

<?php
/**
 * @var $this \yii\web\View
 * @var $user \common\models\User
 */

use \yii\helpers\Url;
?>

<div class="password-reset">
    <p>Здравствуйте.</p>
    <p>Подтвердите регистрацию, перейдя по <a href="<?= Url::to(['auth/confirm-registration', 'token' => $user->confirm_token], true) ?>">этой ссылке</a></p>
</div>
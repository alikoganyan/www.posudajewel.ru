<?php

/**
 * @var $this yii\web\View
 * @var $user common\models\User
 * @var $newPassword string
 */
use yii\helpers\Url;

$url = Url::to('/', true);
?>

<div class="password-reset">
    <p>Здравствуйте <?= $user->fullName ?>.</p>
    <p>Ваш новый пароль для входа в <a href="<?= $url ?>"><?= $url ?></a>: <?= $newPassword ?></p>
</div>

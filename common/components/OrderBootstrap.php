<?php
/**
 * @link https://www.github.com/hscstudio/yii2-cart
 * @copyright Copyright (c) 2016 HafidMukhlasin.com
 * @license http://www.yiiframework.com/license/
 */

namespace common\components;

use backend\components\ActiveRecord;
use common\models\Order;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Event;

/**
 * Bootstrap class for checking sync between two storages.
 *
 * @author Hafid Mukhlasin <hafidmukhlasin@gmail.com>
 * @since 1.0
 *
 */
class OrderBootstrap implements BootstrapInterface {
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app) {
        Event::on(Order::className(), ActiveRecord::EVENT_AFTER_UPDATE, function ($event) {
            $model = $event->sender;
            if (isset($event->changedAttributes['status_id']) && ($event->changedAttributes['status_id'] != $model->status_id)) {
                $model->sendUpdatedStatus();
            }
        });
        Event::on(Order::className(), ActiveRecord::EVENT_AFTER_INSERT, function ($event) {
            $model = $event->sender;
            $model->sendPlcedOrder();
        });
    }
}

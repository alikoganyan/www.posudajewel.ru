<?php

namespace common\components;

/**
 * Вспомогательный класс для работы с данными
 */
class DataHelper {

    /**
     * Создание url 
     * @param string $str
     * @return string
     */
    public static function createUrl($str) {
        $translit = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'ё' => 'ye', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'kh', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh',
            'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'й' => 'y', 'ь' => '-', 'ъ' => '-', ' ' => '-',
        ];

        return preg_replace('/[^a-z0-9_\-]+/', '', strtr(mb_strtolower($str, 'UTF-8'), $translit));
    }

    /**
     * Генерация уникального Url
     */
    public static function generateUniqueUrl($obj, $attrName = 'name', $attrUrl = 'url') {
        $class = get_class($obj);
        $url = DataHelper::createUrl($obj->$attrName);

        for ($index = 1;; $index++) {

            $obj->$attrUrl = $url;

            if ($index > 1) {
                $obj->$attrUrl .= '-' . $index;
            }

            $exists = $class::find()->
                    where(['AND', 'url=:url', 'id!=:id'], [':url' => $obj->$attrUrl, ':id' => $obj->id])->
                    exists();
            if (!$exists) {
                break;
            }
        }
    }

}

<?php
namespace common\components;


use yii\base\Object;

class ImageEditor extends Object
{

    /**
     * @var \Imagick
     */
    private $originImage;
    private $originGeo;

    /**
     * Load file image
     * @param $path
     * @param bool $isRemote
     * @throws \Exception
     */
    public function load($path, $isRemote = false)
    {
        if (!$isRemote) {
            if (file_exists( $path )) {
                $this->originImage = new \Imagick($path);
                $this->originGeo   = $this->originImage->getImageGeometry();
            } else {
                throw new \Exception( "File {$path} doesn't exists", 404 );
            }
        } else {
            $fileHeaders = @get_headers($path);
            if ($fileHeaders && $fileHeaders[0] != 'HTTP/1.0 404 Not Found'){
                $this->originImage = new \Imagick($path);
                $this->originGeo   = $this->originImage->getImageGeometry();
            } else {
                throw new \Exception( "File {$path} doesn't exists", 404 );
            }
        }
    }

    /**
     * Create image
     * @param $width
     * @param $height
     * @param $path
     * @param string $format
     * @param string $bg
     * @throws \Exception
     */
    public function softThumb( $width, $height, $path, $format = 'jpeg', $bg = 'transparent' )
    {
        if ($this->originImage) {
           // echo "\nWrite image to `{$path}`\n";
            $resizeParams = $this->getSizes( $width, $height );
            $overlay      = clone $this->originImage;
            $overlay->scaleImage( $resizeParams['width'], $resizeParams['height'] );
            $overlayGeo = $overlay->getImageGeometry();
            if ($overlayGeo['width'] > $overlayGeo['height']) {
                $resizeParams['top']  = ( $height - $overlayGeo['height'] ) / 2;
                $resizeParams['left'] = 0;
            } else {
                $resizeParams['top']  = 0;
                $resizeParams['left'] = ( $width - $overlayGeo['width'] ) / 2;
            }
            $thumb = new \Imagick();
            $thumb->newImage( $width, $height, $bg );
            $thumb->setImageFormat($format);
            $thumb->setCompression( \Imagick::COMPRESSION_RLE );
            $thumb->setImageCompressionQuality( 80 );
            $thumb->compositeImage( $overlay, \Imagick::COMPOSITE_DEFAULT, $resizeParams['left'],
                $resizeParams['top'] );
            $thumb->writeImageFile( fopen( $path, "wb" ) );
            $thumb->destroy();
        } else {
            throw new \Exception( "As first You must load image", 404 );
        }
    }

    /**
     * Get size
     * @param $width
     * @param $height
     * @return array
     */
    private function getSizes( $width, $height )
    {
        $result = [ ];
        if ($this->originGeo['width'] >= $this->originGeo['height']) {
            $result['width']  = $width;
            $result['height'] = 0;
        } else {
            $proportion       = $this->originGeo['width'] / $this->originGeo['height'];
            $result['width']  = 0;
            $result['height'] = $height;
            $result['left']   = (int) ( ( $width - ( $width * $proportion ) ) / 2 );
            $result['top']    = 0;
        }
        return $result;
    }

    /**
     * Get image geometry
     * @return mixed
     */
    public function getImageGeometry()
    {
        return $this->originGeo;
    }
}
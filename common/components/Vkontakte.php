<?php
namespace common\components;

class Vkontakte extends \yii\authclient\clients\VKontakte {
    public $attributeNames = [
        'uid',
        'first_name',
        'last_name',
        'nickname',
        'screen_name',
        'sex',
        'bdate',
        'city',
        'country',
        'email',
        'timezone',
        'photo'
    ];
}

<?php
namespace common\components;

class Marker extends \dosamigos\google\maps\overlays\Marker {
    public function getName($autoGenerate = true) {
        return 'gMarker';
    }

    public function getJs()
    {
        $js = $this->getInfoWindowJs();

        $js[] = "{$this->getName()} = new google.maps.Marker({$this->getEncodedOptions()});";

        foreach ($this->events as $event) {
            /** @var \dosamigos\google\maps\Event $event */
            $js[] = $event->getJs($this->getName());
        }

        return implode("\n", $js);
    }
}

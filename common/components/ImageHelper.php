<?php

namespace common\components;

use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * Класс для обработки изображений
 */
class ImageHelper
{
    /**
     * Пропорциональное уменьшение
     */

    const RESIZE_PROPORTION = 10;

    /**
     * Автоматическое определение нужного размера
     */
    const RESIZE_AUTO = 20;

    /**
     * Автоматическое определение ширины при заданной высоте
     */
    const RESIZE_AUTO_WIDTH = 21;

    /**
     * Автоматическое определение высоты при заданной ширине
     */
    const RESIZE_AUTO_HEIGHT = 22;

    /**
     * Создание превью картинок
     * @param string $sourcePath путь до оригинала изображения
     * @param string $savePath путь до нового изображения
     * @param int $width
     * @param int $height
     * @param int $type
     */
    public static function thumbnail ($sourcePath, $savePath, $width, $height, $type = self::RESIZE_PROPORTION)
    {
        // Открываем изображение
        $imagine = Image::getImagine()->open($sourcePath);

        switch ($type)
        {
            case self::RESIZE_PROPORTION:
                // Пропорциональное уменьшение картинки с обрезанием крайних фрагметов, которые вышли за кадр
                $currentSize = $imagine->getSize();

                $ratioWidth = $currentSize->getWidth() / $width;
                $ratioHeight = $currentSize->getHeight() / $height;

                if ($ratioWidth > $ratioHeight)
                {
                    $newWidth = round($currentSize->getWidth() / $ratioHeight);
                    $newHeight = $height;
                } else
                {
                    $newWidth = $width;
                    $newHeight = round($currentSize->getHeight() / $ratioWidth);
                }

                $cropWidth = 0;
                $cropHeight = 0;

                if ($newWidth > $width)
                {
                    $cropWidth = round(($newWidth - $width) / 2);
                }

                if ($newHeight > $height)
                {
                    $cropHeight = round(($newHeight - $height) / 2);
                }

                $imagine->resize(new Box($newWidth, $newHeight))->
                        crop(new Point($cropWidth, $cropHeight), new Box($width, $height));
                break;

            case self::RESIZE_AUTO;
            case self::RESIZE_AUTO_WIDTH;
            case self::RESIZE_AUTO_HEIGHT;
                // Уменьшение изображение пропорционально
                $currentSize = $imagine->getSize();

                $ratioWidth = $currentSize->getWidth() / $width;
                $ratioHeight = $currentSize->getHeight() / $height;

                $newWidth = $width;
                $newHeight = round($currentSize->getHeight() / $ratioWidth);


                if ($type == self::RESIZE_AUTO_WIDTH || ($type == self::RESIZE_AUTO && $ratioWidth < $ratioHeight))
                {
                    $newWidth = round($currentSize->getWidth() / $ratioHeight);
                    $newHeight = $height;
                } else
                {
                    $newWidth = $width;
                    $newHeight = round($currentSize->getHeight() / $ratioWidth);
                }

                $imagine->resize(new Box($newWidth, $newHeight));

                break;

            default:
                break;
        }


        // Сохраняем новое изображение
        $imagine->save($savePath, ['quality' => 90]);
    }

}
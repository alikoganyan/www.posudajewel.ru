<?php

namespace common\components;

class BlameableBehavior extends \yii\behaviors\BlameableBehavior {

    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the creator ID.
     */
    public $createdByAttribute = 'user_create';

    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the updater ID.
     */
    public $updatedByAttribute = 'user_update';

}

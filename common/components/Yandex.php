<?php
namespace common\components;

use Yii;

class Yandex extends \yii\authclient\clients\Yandex {
    public function buildAuthUrl(array $params = []) {
        $defaultParams = [
            'client_id'          => $this->clientId,
            'response_type'      => 'code',
            'redirect_uri'       => $this->getReturnUrl(),
            'xoauth_displayname' => Yii::$app->name,
        ];
        if (!empty($this->scope)) {
            $defaultParams['scope'] = $this->scope;
        }

        if ($this->validateAuthState) {
            $authState = $this->generateAuthState();
            $this->setState('authState', $authState);
            $defaultParams['state'] = $authState;
        }

        return $this->composeUrl($this->authUrl, array_merge($defaultParams, $params));
    }

    protected function composeUrl($url, array $params = []) {
        if (!empty($params)) {
            if (strpos($url, '?') === false) {
                $url .= '?';
            }
            else {
                $url .= '&';
            }

            foreach ($params as $key => $value) {
                $url .= "$key=$value&";
            }
        }
        return $url;
    }
}

<?php

namespace common\components;

/**
 * 
 */
class CropImageUploadBehavior extends \karpoff\icrop\CropImageUploadBehavior {

    /**
     * @param $filename
     * @return string
     */
    protected function getCropFileName($filename) {
        return 'prev_' . $filename;
    }

}

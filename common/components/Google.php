<?php
namespace common\components;

class Google extends \yii\authclient\clients\Google {
    protected function composeUrl($url, array $params = []) {
        if (!empty($params)) {
            if (strpos($url, '?') === false) {
                $url .= '?';
            } else {
                $url .= '&';
            }

            foreach ($params as $key => $value) {
                $url .= "$key=$value&";
            }
        }
        return $url;
    }
}

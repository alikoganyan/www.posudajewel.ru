<?php
namespace common\components;

use yii\helpers\VarDumper;

class DbTarget extends \yii\log\DbTarget {
    public function export() {
        $tableName = $this->db->quoteTableName($this->logTable);
        $sql = "INSERT INTO $tableName ([[level]], [[category]], [[date]], [[url]], [[prefix]], [[message]])
                VALUES (:level, :category, :date, :url, :prefix, :message)";
        $command = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            if (!is_string($text)) {
                if ($text instanceof \Throwable || $text instanceof \Exception) {
                    $text = (string) $text;
                } else {
                    $text = VarDumper::export($text);
                }
            }
            $command->bindValues([
                ':level' => $level,
                ':category' => $category,
                ':date' => date('Y-m-d H:i:s', $timestamp),
                ':url' => \Yii::$app->request->absoluteUrl,
                ':prefix' => $this->getMessagePrefix($message),
                ':message' => $text,
            ])->execute();
        }
    }
}

<?php
/**
 * @author Ilya Fesyuk <ilya.fesyuk@gmail.com>
 * @copyright Copyright (c) 2017
 */

namespace common\components;

use Yii;
use yii\db\Query;
use yii\di\Instance;
use hscstudio\cart\Storage;
use hscstudio\cart\Cart;
use yii\db\Expression;
/**
 *
 */
class DBCookieStorage extends Storage {
    public $db = 'db';

    public $table = 'cart';

    public $cart_db_id = '';
    private $_user_id  = 0;

    /**
     *
     */
    public function init() {
        parent::init();
        $this->_user_id   = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
        $this->db         = Instance::ensure($this->db, 'yii\db\Connection');
    }

    public function read(Cart $cart) {
        if ($data = $this->select($cart)) {
            $this->unserialize($data['value'], $cart);
        }
    }

    public function write(Cart $cart) {
        if ($this->select($cart)) {
            $this->update($cart);
        } else {
            $this->insert($cart);
        }
    }

    public function lock($drop, Cart $cart) {
        if ($data = $this->select($cart)) {
            if ($drop) {
                $this->db->createCommand()->update($this->table, [
                    'and',
                    ['or',
                        ['and',
                            ['>', 'user_id', 0],
                            ['user_id' => $this->_user_id],
                        ],
                        ['id' => $this->getCartId($cart)],
                    ],
                    ['name' => $cart->id],
                    ['status' => 0],
                ]
                )->execute();
            } else {
                $this->db->createCommand()->update($this->table, [
                    'status' => 1,
                ],
                    [
                        'and',
                        ['or',
                            ['and',
                                ['>', 'user_id', 0],
                                ['user_id' => $this->_user_id],
                            ],
                            ['id' => $this->getCartId($cart)],
                        ],
                        ['name' => $cart->id],
                        ['status' => 0],
                    ]
                )->execute();
                $this->killCart($cart);
            }
        }
    }

    /**
     * @param Cart $cart
     * @return array|bool
     */
    public function select(Cart $cart) {
        return (new Query())
            ->select('*')
            ->from($this->table)
            ->where(['or', 'user_id = ' . $this->_user_id, 'id = \'' . $this->getCartId($cart) . '\''])
            ->andWhere([
                'name'   => $cart->id,
                'status' => 0,
            ])
            ->orderBy(['updated_at' => SORT_DESC,'id' => SORT_DESC])
            ->limit(1)
            ->one($this->db);
    }

    /**
     * @param Cart $cart
     */
    public function insert(Cart $cart) {
        $this->db->createCommand()->insert($this->table, [
            'id'      => $this->getCartId($cart),
            'user_id' => $this->_user_id,
            'name'    => $cart->id,
            'value'   => $this->serialize($cart),
            'status'  => 0,

            'updated_at' => new Expression('NOW()'),
            'created_at' => new Expression('NOW()'),
        ])->execute();
    }

    /**
     * @param Cart $cart
     */
    public function update(Cart $cart) {
        $update = [
            'value' => $this->serialize($cart),
            'updated_at' => new Expression('NOW()'),
        ];

        $this->db->createCommand()->update($this->table, $update,
            [
                'and',
                ['or',
                    ['and',
                        ['>', 'user_id', 0],
                        ['user_id' => $this->_user_id],
                    ],
                    ['id' => $this->getCartId($cart)],
                ],
                ['name' => $cart->id],
                ['status' => 0],
            ]
        )->execute();
    }

    public function getCartId(Cart $cart) {
        $id = $this->cart_db_id ? $this->cart_db_id : $this->setCartId($cart);

        return $id;
    }
    public function setCartId(Cart $cart) {
        $cookies = \Yii::$app->request->cookies;
        if (isset($cookies[$cart->id])) {
            return $cookies[$cart->id];
        }
        $cookies = \Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name'  => $cart->id,
            'value' => $id = $this->generateCartId(),
        ]));
        return $id;
    }
    public function generateCartId() {
        $session = Yii::$app->session;
        return $session->getId();
    }
    public function killCart(Cart $cart) {
        Yii::$app->session->regenerateID(true);
        $cookies = Yii::$app->response->cookies;
        $cookies->remove($cart->id);
        unset($cookies[$cart->id]);

    }

    public function catchUser($cart)
    {
        $update = ['user_id'=> $this->_user_id];
        $this->db->createCommand()->update($this->table, $update,
            [
                'and',
                ['or',
                    ['and',
                        ['>', 'user_id', 0],
                        ['user_id' => $this->_user_id],
                    ],
                    ['id' => $this->getCartId($cart)],
                ],
                ['name' => $cart->id],
                ['status' => 0],
            ]
        )->execute();
    }
}

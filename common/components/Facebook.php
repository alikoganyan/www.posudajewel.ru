<?php
namespace common\components;

class Facebook extends \yii\authclient\clients\Facebook {
    public $attributeNames = [
        'name',
        'email',
        'first_name',
        'last_name',
        'gender',
        'link',
    ];
}

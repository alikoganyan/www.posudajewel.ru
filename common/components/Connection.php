<?php
namespace common\components;

class Connection extends \yii\db\Connection {
    public $commandClass = 'common\components\Command';
}
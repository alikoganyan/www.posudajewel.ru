<?php
namespace common\components;

use dosamigos\google\maps\Event;
use dosamigos\google\maps\ObjectAbstract;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

class Map extends \dosamigos\google\maps\Map {
    private $_js = [];

    public function getJs()
    {
        $name = $this->getName();
        $width = strpos($this->width, "%") ? $this->width : $this->width . 'px';
        $height = strpos($this->height, "%") ? $this->height : $this->height . 'px';
        $containerId = ArrayHelper::getValue($this->containerOptions, 'id', $name . '-map-canvas');
        $overlaysJs = [];
        $js = [];
        foreach ($this->getOverlays() as $overlay) {
            /** @var ObjectAbstract $overlay */
            if (!ArrayHelper::keyExists("{$name}infoWindow", $this->getClosureScopedVariables()) &&
                method_exists($overlay, 'getIsInfoWindowShared')
                && $overlay->getIsInfoWindowShared()
            ) {
                $this->setClosureScopedVariable("{$name}infoWindow");
                $this->appendScript("{$name}infoWindow = new google.maps.InfoWindow();");
            }
            $overlay->options['map'] = new JsExpression($this->getName());
            $overlaysJs[] = $overlay->getJs();
        }
        $js[] = "(function(){";
        $js[] = $this->getClosureScopedVariablesScript();
        $js[] = "function initialize(){";
        $js[] = "var mapOptions = {$this->getEncodedOptions()};";
        $js[] = "var container = document.getElementById('{$containerId}');";
        $js[] = "container.style.width = '{$width}';";
        $js[] = "container.style.height = '{$height}';";
        $js[] = "{$this->getName()} = new google.maps.Map(container, mapOptions);";
        $js = ArrayHelper::merge($js, $overlaysJs);
        foreach ($this->events as $event) {
            /** @var Event $event */
            $js[] = $event->getJs($name);
        }

        foreach ($this->getPlugins()->getInstalledPlugins() as $plugin) {
            /** @var \dosamigos\google\maps\PluginAbstract $plugin */
            $plugin->map = $this->getName();
            $js[] = $plugin->getJs($name);
        }

        $js = ArrayHelper::merge($js, $this->_js);
        $js[] = "};";
        $js[] = "initialize();";
        $js[] = "})();";

        return implode("\n", $js);
    }

    public function getName($autoGenerate = true) {
        return 'gMap';
    }
}

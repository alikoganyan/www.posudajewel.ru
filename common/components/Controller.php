<?php
namespace common\components;

class Controller extends \yii\web\Controller {
    public function redirect($url, $statusCode = 301) {
        return parent::redirect($url, $statusCode);
    }

    protected function goToPreviousPage() {
        $redirectUrl = \Yii::$app->request->referrer;
        if (!$redirectUrl)
            $redirectUrl = '/';

        return $redirectUrl;
    }
}
